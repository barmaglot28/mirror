#!/bin/sh

sed -i "s/__CI_IMAGE_ID__/$CI_COMMIT_SHA/g" ./deployment/service-deployment.yaml
sed -i "s/__CI_IMAGE_ID__/$CI_COMMIT_SHA/g" ./deployment/dashboard-deployment.yaml
sed -i "s/__CI_IMAGE_ID__/$CI_COMMIT_SHA/g" ./deployment/portal-deployment.yaml