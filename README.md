# Cabdo Backend

## Development with hot-reload

1. `docker-compose build && docker-compose up`
2. Visit Admin Dashboard [`http://127.0.0.1:3000`](http://127.0.0.1:3000)
2. Visit Corporate Portal [`http://127.0.0.1:3003`](http://127.0.0.1:3003)

Development users are created automatically.  
Their credentials are printed in the service console: 

`service_1    | Creating user abc@xyz.com of type admin with password 123  
service_1    | Creating user abc@xyz2.com of type rider with password 123  
service_1    | Creating user abc@xyz3.com of type rider with password 123  
service_1    | Creating user abc@xyz3.com of type driver with password 123  
service_1    | Creating user abc@xyz4.com of type driver with password 123
service_1    | Creating user hotel@corp.com of type corporate with password 123
service_1    | Creating user hospital@corp.com of type corporate with password 123`

## Run service tests

1. `docker-compose build`
2. `docker-compose run --rm service npm run test`