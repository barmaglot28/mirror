import Moment from 'moment';

export default {
  getName(user) {
    if (user && user.fname && user.lname)
      return `${user.fname} ${user.lname}`;
    else if (user && user.fname)
      return user.fname;
    else if (user && user.lname)
      return user.lname;
    else
      return 'Unknown';
  },
  getTime(date) {
    return date ? Moment(date).format('H:mm:ss dd') : 'NA';
  },
  getDateTime(date) {
    return date ? Moment(date).format('D MMM YY [at] H:mm:ss') : 'NA';
  },
  getFullDateTime(date) {
    return date ? Moment(date).format('DD.MM.YYYY, H:mm ddd') : 'NA';
  },
  getDuration(date) {
    return date ? `${Moment.duration(date).hours()}:${Moment.duration(date).minutes()}:${Moment.duration(date).seconds()}` : 'NA';
  },
  getArrive(data) {
    let minDur = null;

    data.forEach((res) => {
      if (res.duration && res.distance) {
        if (!minDur) {
          minDur = res.duration;
        } else {
          minDur = res.duration.value < minDur.value ? res.duration : minDur;
        }
      }
    });
    return minDur;
  }
};

