import { maps as mapConfig } from '../config';

export default {
  init(element) {
    return (new google.maps.Map(element, {
      zoom: 5,
      center: { lat: 54.5260, lng: 15.2551 } // EU
    }));
  },
  autocomplete(map, element) {
    const autocomplete = new google.maps.places.Autocomplete(element);
    autocomplete.bindTo('bounds', map);
    return autocomplete;
  },
  info() {
    return (new google.maps.InfoWindow());
  },
  marker(map) {
    return (new google.maps.Marker({
      map,
      anchorPoint: new google.maps.Point(0, -29)
    }));
  },
  onAddressChange({ map, autocomplete, infowindow, marker }, callback) {
    autocomplete.addListener('place_changed', () => {
      infowindow.close();
      marker.setVisible(false);

      const place = autocomplete.getPlace();
      if (!place.geometry) {
        return window.alert(`No details available for input: ${place.name}`);
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }

      marker.setIcon(/** @type {google.maps.Icon} */({
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(35, 35)
      }));
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      let address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      infowindow.setContent(`<div><strong>${place.name}</strong><br>${address}`);
      infowindow.open(map, marker);

      callback({
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
        addr: place.formatted_address
      });
    });
  },
  getDistance(params, callback) {
    const service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
      origins: [new google.maps.LatLng(params.srcLoc[1], params.srcLoc[0])],
      destinations: [new google.maps.LatLng(params.destLoc[1], params.destLoc[0])],
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        callback(null, {
          distance: response.rows[0].elements[0].distance,
          duration: response.rows[0].elements[0].duration
        });
      } else {
        callback(status);
      }
    });
  },
  getDistances(sources, destination, callback) {
    const service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
      origins: sources.map(loc => ( new google.maps.LatLng(loc[1], loc[0]) )),
      destinations: [ new google.maps.LatLng(destination[1], destination[0]) ],
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        callback(null, response.rows.map(row => (row.elements[0])));
      } else {
        callback(status);
      }
    });
  },
  setLocation({ map, marker, infowindow }, { gpsLoc, fullAddress }) {
    map.setCenter({ lat: gpsLoc[1], lng: gpsLoc[0] });
    map.setZoom(17);

    marker.setIcon(({
      url: mapConfig.google.marker,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition({ lat: gpsLoc[1], lng: gpsLoc[0] });
    marker.setVisible(true);

    infowindow.setContent(`<b>Pickup Address</b><br/>${fullAddress}`);
    infowindow.open(map, marker);
  }
};
