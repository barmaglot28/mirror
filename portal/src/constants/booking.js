export default {
  now: 'now',
  later: 'later',
  startNY: '2017-12-31 22:00',
  endNY: '2018-01-01 10:00'
};