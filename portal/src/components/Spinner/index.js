import React, { Component } from 'react';

class Spinner extends Component {
  static propTypes = {
    componentsId: React.PropTypes.string,
  }

  render() {
    return (
      <div id={this.props.componentsId} className="loading-wrap" style={{ minHeight: 500 }}>
        <div className="loading">
          <div id="spinner">
            <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
              <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
            </svg>
          </div>
        </div>
      </div>
    );
  }
}

export default Spinner;
