import React, { Component } from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';

import { setLocale } from '../../store/view/intl/action';
import s from './Sidebar.css';
import Link from '../Link/Link';
import icon from '../../common/images/cabdo-icon.png';

class Sidebar extends Component {

  render() {
    return (
      <aside className={s.sidebar}>
        <div className={`sidenav-outer ${s.sidenavOuter}`}>
          <ul>
            {/*
            <li>
              <Link to="/portal/users">
                <span className="glyphicon glyphicon-user" />
                <span className="title">Users</span>
              </Link>
            </li>*/}

            <li>
              <Link to="/portal/startTrip" className={s.linkWrapper}>
                  <div>
                    <img src={icon} alt="cabdo-icon"/>
                  </div>
                  <FormattedMessage className="title"
                                    id='app.form.startTrip'
                                    defaultMessage='Start Trip'
                  />
              </Link>
            </li>
            <li>
              <Link to="/portal/trips" className={s.linkWrapper}>
                <div>
                  <img src={icon} alt="cabdo-icon"/>
                </div>
                <FormattedMessage className="title"
                                  id='app.form.trips'
                                  defaultMessage='Trips'
                />
              </Link>
            </li>
            {/*
            <li>
              <Link to="/portal/createUser">
                <span className="glyphicon glyphicon-user" />
                <span className="title">Create New User</span>
              </Link>
            </li>*/}
          </ul>
          <div>
            <div className={s.navbarLangSwitch}>
              <a href="https://www.cabdo.de/impressum" className={s.linkWrapper}>Imprint</a>
              <div>
                <a onClick={() => this.props.setLocale({locale: 'en-US'})}>EN</a>
                <a onClick={() => this.props.setLocale({locale: 'de-DE'})}>DE</a>
              </div>
            </div>
          </div>
        </div>
      </aside>
    );
  }
}
function mapStateToProps(state) {
  return state;
}

function bindActions(dispatch) {
  return {
    setLocale: (data) => dispatch(setLocale(data))
  };
}

export default injectIntl(connect(mapStateToProps, bindActions)(withStyles(s)(Sidebar)));
