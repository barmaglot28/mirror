import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Layout.css';
import Header from '../Header';
import Sidebar from '../Sidebar';

class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  render() {
    return (
      <div className={`dashboard-page ${s.dashboardPage}`}>
        <Header />
        <Sidebar />
        <section id={s.bodyContainer} className={s.uiView}>
          {this.props.children}
        </section>
      </div>
    );
  }
}

export default withStyles(s)(Layout);
