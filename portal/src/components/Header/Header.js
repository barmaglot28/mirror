import React, { Component } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { FormattedMessage, defineMessages } from 'react-intl';
import { injectIntl, intlShape, FormattedRelative } from 'react-intl';
import $ from 'jquery';
import { connect } from 'react-redux';
import {
  NavDropdown,
  MenuItem,
} from 'react-bootstrap';
import Link from '../Link';
import s from './Header.css';
import history from '../../core/history';
import { clearUserData } from '../../store/domain/auth/action';
import logo from '../../common/images/cabdo-logo.png';

const profileImage = require('../../common/images/flat-avatar.png');

function showMenu() {
  $('.dashboard-page').toggleClass('push-right');
}

const messages = defineMessages({
  language: {
    id: 'app.home.language',
    description: 'Message to set the new language.',
    defaultMessage: 'Language',
  },
});

class TopNav extends Component {
  static propTypes = {
    userDetails: React.PropTypes.object,
    clearUserData: React.PropTypes.func,
    intl: intlShape.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  logOutUser(event) {
    event.preventDefault();
    this.props.clearUserData();
    history.push('/');
  }
  render() {
    return (
      <nav className={`navbar navbar-fixed-top ${s.topNavbar} role="navigation`}>
        <div className={`navbar-header text-center ${s.navbarHeader}`}>
          <button type="button" className="navbar-toggle" onClick={(e) => { e.preventDefault(); showMenu(); }} target="myNavbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar" />
            <span className="icon-bar" />
            <span className="icon-bar" />
          </button>
        </div>

        <div id="myNavbar" className={`collapse navbar-collapse ${s.navbarCollapse}`}>
          <div className={s.cabdoLogo}>
            <img src={logo} alt="logo" />
            <span>For Business</span>
          </div>
        </div>
        <div className={s.navbarRightSection}>
          <a href="#" className={s.ulNavbar}>
            <span onClick={(e) => { e.preventDefault(); history.push('/'); }}>Logout</span>
          </a>
        </div>
      </nav>
    );
  }
}
function mapStateToProps(state) {
  return {
    userDetails: state.domainReducer.auth.user.user,
  };
}

function bindActions(dispatch) {
  return {
    clearUserData: () => dispatch(clearUserData()),
  };
}

export default injectIntl(connect(mapStateToProps, bindActions)(withStyles(s)(TopNav)));
