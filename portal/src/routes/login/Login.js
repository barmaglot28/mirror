import React, { Component } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { FormGroup, FormControl, Modal, Button } from 'react-bootstrap';
import { FormattedMessage, defineMessages, injectIntl, intlShape } from 'react-intl';
import s from './Login.css';
import logo from '../../common/images/cabdo-icon.png';
import { loginUser, recovery } from '../../store/domain/auth/action';
import History from '../../core/history';

const messages = defineMessages({
  flatAvatar: {
    id: 'app.login.flatAvatar',
    defaultMessage: 'Flat Avatar'
  },
  email: {
    id: 'app.login.email',
    defaultMessage: 'Email'
  },
  invalidEmail: {
    id: 'app.login.emailInvalid',
    defaultMessage: 'Email invalid'
  },
  password: {
    id: 'app.login.password',
    defaultMessage: 'Password'
  },
  logIn: {
    id: 'app.login.logIn',
    defaultMessage: 'Log in'
  },
  emailSubject: {
    id: 'app.login.recoverySubject',
    defaultMessage: 'Cabdo recovery subject'
  },
  emailText: {
    id: 'app.login.recoveryText',
    defaultMessage: 'Cabdo recovery text'
  },
  recoverySuccess: {
    id: 'app.login.recoverySuccess',
    defaultMessage: 'You will receive a confirmation email shortly'
  },
  recoveryFail: {
    id: 'app.login.recoveryFail',
    defaultMessage: 'This email is not registered'
  },
  enterEmail: {
    id: 'app.login.enterEmail',
    defaultMessage: 'Enter your email here'
  },
  controlBtnOk: {
    id: 'app.login.controlBtnOk',
    defaultMessage: 'Ok'
  },
  controlBtnCancel: {
    id: 'app.login.controlBtnCancel',
    defaultMessage: 'Cancel'
  },
  sendMailBtn: {
    id: 'app.login.sendMailBtn',
    defaultMessage: 'Send mail'
  },
  passRecoveryTitle: {
    id: 'app.login.passRecoveryTitle',
    defaultMessage: 'Password recovery'
  }
});

class Login extends Component {

  static propTypes = {
    loginUser: React.PropTypes.func,
    recovery: React.PropTypes.func,
    user: React.PropTypes.any,
    invalidCredentials: React.PropTypes.bool,
    invalidRecovery: React.PropTypes.bool,
    loggedInStatus: React.PropTypes.bool,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      userType: 'corporate',
      email: '',
      password: '',
      errorEmail: false,
      recoveryEmail: '',
      modalVisible: false,
      invalidRecoveryVisible: false,
      recoveryText: '',
    };
    this.handleLogin = this.handleLogin.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onRecoverPassword = this.onRecoverPassword.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
  }


  componentDidMount() {
  }
  componentWillReceiveProps(nextProps) {
    localStorage.setItem('id_token', nextProps.user.jwtAccessToken);
    if (nextProps.user && nextProps.user.jwtAccessToken) {
      History.push('/portal/startTrip');
    }

    if (this.props.invalidRecovery !== nextProps.invalidRecovery && nextProps.invalidRecovery) {
      this.setState({
        invalidRecovery: true
      })
    }
  }

  handleEmail(e) {
    this.setState({
      email: e.target.value,
      errorEmail: !this.validateEmail(e.target.value)
    });
  }

  handlePassword(e) {
    this.setState({
      password: e.target.value,
    });
  }

  handleLogin(e) {
    e.preventDefault();
    this.props.loginUser(this.state);
    return false;
  }

  onEmailChange(e) {
    this.setState({
      recoveryEmail: e.target.value,
      errorEmail: !this.validateEmail(e.target.value)
    });

  }

  async onRecoverPassword() {
    let {formatMessage} = this.props.intl;
    if (this.state.recoveryEmail.length !== 0) {
      const recovery = await this.props.recovery({ email: this.state.recoveryEmail,
                            emailSubject: formatMessage(messages.emailSubject),
                            emailText: formatMessage(messages.emailText)
                          });
      if (recovery) {
        this.setState({
          invalidRecoveryVisible: true,
          recoveryText: formatMessage(messages.recoverySuccess),
        })
      } else {
        this.setState({
          invalidRecoveryVisible: true,
          recoveryText: formatMessage(messages.recoveryFail),
        })
      }
    }
    this.setState({
      modalVisible: false,
      recoveryEmail: ''
    });
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  render() {
    let {formatMessage} = this.props.intl;
    return (
      <div className={`login-page animate ${s.loginPage}`}>
        <div className={`${s.loginForm}`}>
          <div className={s.userAvatar}>
            <img src={logo} alt={formatMessage(messages.flatAvatar)} className="user-avatar" />
          </div>
          <h1>
            <FormattedMessage
              id='app.form.cabdoPortal'
              defaultMessage="Cabdo Portal"
            />
          </h1>
          {
            this.props.invalidCredentials
            &&
              <div className="alert alert-danger">
                <FormattedMessage
                  id='app.warning.usernameOrPasswordWrong'
                  defaultMessage="Username or password wrong."
                />
              </div>
          }
          <form role="form" onSubmit={this.handleLogin}>
            <div className={s.formContent}>
              <FormGroup>
                <FormControl id="email" type="text" placeholder={formatMessage(messages.email)} className={s.inputStyle} onChange={this.handleEmail} value={this.state.email} />
                { this.state.errorEmail && !this.state.modalVisible && <div className={s.invalidEmail}>{formatMessage(messages.invalidEmail)}</div> }
              </FormGroup>
              <FormGroup>
                <FormControl id="password" type="password" placeholder={formatMessage(messages.password)} className={s.inputStyle} onChange={this.handlePassword} value={this.state.password} />
              </FormGroup>
            </div>
            <button type="submit" className={`btn progress-login progress-button btn-block ${s.btn}`} data-style="fill" data-horizontal >{formatMessage(messages.logIn)}</button>
          </form>
          <div className={s.imprintLinkWrap}>
            <div className={s.forgotPassLink} onClick={ () => { this.setState({ modalVisible: true, errorEmail: false }) }}>
              <FormattedMessage
                id='app.form.forgotPass'
                defaultMessage="Forgot password?"
              />
            </div>
            <a className={s.imprintLink} href="https://www.cabdo.de/impressum">
              <FormattedMessage
                id='app.form.imprint'
                defaultMessage="Imprint"
              />
            </a>
          </div>
        </div>

        <Modal
          show={this.state.modalVisible}
          onHide={() => { this.setState({ modalVisible: false })} }
          container={this}
          aria-labelledby="contained-modal-title"
          className={s.modalWindow}
        >
          <Modal.Header closeButton className={s.modalHeader}>
            <Modal.Title id="contained-modal-title">{formatMessage(messages.passRecoveryTitle)}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormGroup>
              <FormControl type="text" value={this.state.recoveryEmail} placeholder={formatMessage(messages.enterEmail)} onChange={this.onEmailChange} />
              { this.state.errorEmail && <div className={s.invalidEmail}>{formatMessage(messages.invalidEmail)}</div> }
            </FormGroup>
            <div className={s.modalBtnWrapper}>
              <Button onClick={ () => { this.setState({ modalVisible: false }) }}>{formatMessage(messages.controlBtnCancel)}</Button>
              <Button onClick={this.onRecoverPassword}>{formatMessage(messages.sendMailBtn)}</Button>
            </div>
          </Modal.Body>
        </Modal>

        <Modal
          show={this.state.invalidRecoveryVisible}
          onHide={() => { this.setState({ invalidRecoveryVisible: false })} }
          container={this}
          aria-labelledby="contained-modal-title"
          className={s.modalWindow}
        >
          <Modal.Body>
            <div className={s.modalRecoveryText}>{this.state.recoveryText}</div>
            <div className={s.modalBtnWrapper}>
              <Button onClick={ () => { this.setState({ invalidRecoveryVisible: false, errorEmail: false }) }}>{formatMessage(messages.controlBtnOk)}</Button>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.domainReducer.auth.user,
    invalidCredentials: state.domainReducer.auth.invalidCredentials,
    invalidRecovery: state.domainReducer.auth.invalidRecovery,
    loggedInStatus: state.domainReducer.auth.loggedInStatus,
  };
}

function bindActions(dispatch) {
  return {
    loginUser: userCredentials => dispatch(loginUser(userCredentials)),
    recovery: (obj) => dispatch(recovery(obj)),
  };
}

export default injectIntl(connect(mapStateToProps, bindActions)(withStyles(s)(Login)));
