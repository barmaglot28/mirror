import React, { Component } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { Form, FormGroup, FormControl, ControlLabel, Panel, Button, Col, Row, Collapse, ListGroup, ListGroupItem, ButtonToolbar, ToggleButtonGroup, ToggleButton, Label } from 'react-bootstrap';
import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import Datetime from 'react-datetime';
import Moment from 'moment';
import * as Toastr from 'toastr';
import { FormattedMessage, defineMessages, injectIntl, intlShape } from 'react-intl';
import bookingConst from '../../../constants/booking';
import s from './StartTrip.css';
import TripAction from '../../../store/domain/trip/action';
import Map from '../../../helpers/map';
import Utils from '../../../helpers/utils';
import car from '../../../common/images/logo-transparent.png';
import carMarker from '../../../common/images/car-marker.png';
import carDefault from '../../../common/images/car2.png';
import carOptional from '../../../common/images/car3.png';

const messages = defineMessages({
  noFreeDrivers: {
    id: 'app.warning.noFreeDrivers',
    defaultMessage: "There are no free drivers now, select another taxi type",
  },
  bookingDateTimeErr: {
    id: 'app.error.bookingDateTime',
    defaultMessage: "Booking date time should be minimum 30 minutes in advance & maximum 30 days in advance",
  },
  bookingNYErr: {
    id: 'app.error.bookingNY',
    defaultMessage: "Pre-bookings are unfortunately not possible for the New Year's Eve in the time from 22:00 - 10:00",
  },
  pickUpAddressNotSet: {
    id: 'app.error.pickUpAddressNotSet',
    defaultMessage: "Pick up address or destination address are not set",
  },
  carCategoriesNotFetched: {
    id: 'app.error.carCategoriesNotFetched',
    defaultMessage: "Car categories are not fetched yet",
  },
  startTrip: {
    id: 'app.form.startTrip',
    defaultMessage: 'Start Trip'
  },
  firstName: {
    id: "app.form.riderFirstName",
    defaultMessage: "Rider First Name"
  },
  lastName: {
    id: 'app.form.riderLastName',
    defaultMessage: "Rider Last Name"
  },
  sourceAddress: {
    id: 'app.form.sourceAddress',
    defaultMessage: "Source Address"
  },
  destinationAddress: {
    id: 'app.form.destinationAddress',
    defaultMessage: "Destination Address"
  },
  rideNow: {
    id: 'app.form.rideNow',
    defaultMessage: 'Ride Now'
  },
  rideLater: {
    id: 'app.form.rideLater',
    defaultMessage: 'Ride Later'
  },
  selectDateTime: {
    id: 'app.form.selectDateTime',
    defaultMessage: 'Select Date/time'
  },
  selectDateTimePlaceholder: {
    id: 'app.form.selectDateTimePlaceholder',
    defaultMessage: 'Select min 30 minutes & max 30 days in advance'
  },
  carCategory: {
    id: 'app.form.carCategory',
    defaultMessage: 'Car category'
  },
  calculateFare: {
    id: 'app.form.calculateFare',
    defaultMessage: 'Calculate fare'
  },
  approximatePrice: {
    id: 'app.form.approximatePrice',
    defaultMessage: 'Approximate price'
  },
  from: {
    id: 'app.form.from',
    defaultMessage: 'from'
  },
  guaranteed: {
    id: 'app.form.guaranteed',
    defaultMessage: 'guaranteed'
  },
  passengers: {
    id: 'app.form.passengers',
    defaultMessage: 'Passengers'
  },
  upto: {
    id: 'app.form.upto',
    defaultMessage: 'Up to'
  },
});

class StartTrip extends Component {
  static propTypes = {
    startTripLoading: React.PropTypes.bool,
    newTrip: React.PropTypes.object, // response from server
    errorStatusPostTrip: React.PropTypes.bool,
    errorPostTrip: React.PropTypes.object,
    createTrip: React.PropTypes.func,
    fetchCarCategories: React.PropTypes.func,
    fetchCorporateUser: React.PropTypes.func,
    carCategories: React.PropTypes.array,
    defaultCategory: React.PropTypes.object,
    clearState: React.PropTypes.func,
    intl: intlShape.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      trip: {
        riderFirstName: '',
        riderLastName: '',
        pickUpAddress: '',
        destAddress: '',
        taxiType: '',
        srcLoc: null,
        destLoc: null,
        latitudeDelta: null,
        longitudeDelta: null,
        preBooking: bookingConst.now,
        dateTime: null,
        fareDetails: {}
      },
      arriveData: null,
      drivers: [],
      map: null,
      carCategory: this.props.defaultCategory,
      submitDisabled: true,
    };
    this.handleRiderFirstName = this.handleRiderFirstName.bind(this);
    this.handleRiderLastName = this.handleRiderLastName.bind(this);
    this.handlePickUpAddress = this.handlePickUpAddress.bind(this);
    this.handleDestAddress = this.handleDestAddress.bind(this);
    this.handleTaxiType = this.handleTaxiType.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handlePreBooking = this.handlePreBooking.bind(this);
    this.changeBooking = this.changeBooking.bind(this);
    this.calcFare = this.calcFare.bind(this);
  }
  componentDidMount() {
    /* init map, autocopleters, marker, info popup */
    const map = Map.init(document.getElementById('map'));
    const srcAutocomplete = Map.autocomplete(map, document.getElementById('srcAddress'));
    const destAutocomplete = Map.autocomplete(map, document.getElementById('destAddress'));
    const infowindow = Map.info();
    const marker = Map.marker(map);

    this.setState({ map });

    /* address search callbacks */
    Map.onAddressChange({ map, autocomplete: srcAutocomplete, infowindow, marker }, (data) => {
      this.setSrcGeoData([data.lng, data.lat], data.addr);
      this.setState({ arriveData: null });
      this.props.clearFare();
      this.calcArrive();
    });
    Map.onAddressChange({ map, autocomplete: destAutocomplete, infowindow, marker }, (data) => {
      this.setDestGeoData([data.lng, data.lat], data.addr);
      this.props.clearFare();
    });

    this.props.fetchCorporateUser().then((response) => {
      const trip = this.state.trip;

      /* preselect corporate location as pickup address and f/l name */
      if (response.data.gpsLoc && response.data.fullAddress) {
        Map.setLocation({ map, marker, infowindow }, { gpsLoc: response.data.gpsLoc, fullAddress: response.data.fullAddress });

        trip.srcLoc = response.data.gpsLoc;
        trip.pickUpAddress = response.data.fullAddress;
        this.setState({trip});
        this.checkAvailability();
        this.calcArrive();
      }

      trip.riderFirstName = response.data.fname;
      trip.riderLastName = response.data.lname;
    });

    this.props.clearState(); // remove response msg
    this.props.clearFare();
    this.props.fetchCarCategories().then((response) => {
      // pre select default category
      const trip = this.state.trip;
      trip.taxiType = response.defaultCategory ? response.defaultCategory._id : '';

      // prepare markers for map
      const drivers = this.state.drivers;
      response.data.forEach((carCat) => {
        carCat.drivers.forEach((driver) => {
          drivers.push({
            marker: new google.maps.Marker({
              position: { lat: driver.gpsLoc[1], lng: driver.gpsLoc[0]},
              title: driver.email,
              icon: carMarker }),
            carCategory: driver.carDetails.carCategory
          });
        });
      });
      this.setState({
        trip,
        drivers
      });
      this.calcArrive();

      // render drivers on map
      this.renderDrivers({ map, carCategory: trip.taxiType, drivers });

    });
  }

  componentDidUpdate(){
    // need to stop prop of the touchend event
    if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) {
      let toggleButtons = document.getElementsByClassName('toggle-button');
      for(let i=0; i< toggleButtons.length; i++) {
        toggleButtons[i].addEventListener('touchend', function(e) {
          e.stopPropagation();
        });
      }
      let pacContainers = document.getElementsByClassName('pac-container');
      for(let i=0; i< pacContainers.length; i++) {
        pacContainers[i].addEventListener('touchend', function(e) {
          e.stopPropagation();
        });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.fareData !== nextProps.fareData && nextProps.fareData) {
      if (nextProps.fareData.hasOwnProperty('cost')) {
        const trip = this.state.trip;
        trip.fareDetails = nextProps.fareData.cost[this.state.carCategory.name];
        this.setState({
          ...this.state,
          trip
        });
        this.checkAvailability();
      }
    }
  }
  renderDrivers({ map, carCategory, drivers }) {
    let cnt = 0;
    let { formatMessage } = this.props.intl;
    drivers.forEach((driver) => {
      if (driver.carCategory === carCategory) {
        driver.marker.setMap(map);
        cnt += 1;
      } else {
        driver.marker.setMap(null);
      }
    });
    // show warning if ride now, but there are no drivers
    const trip = this.state.trip;
    if (cnt === 0 && trip.preBooking === bookingConst.now) {
      Toastr.warning(formatMessage(messages.noFreeDrivers));
      this.setState({submitDisabled: true})
    } else {
      const trip = this.state.trip;
      const checkRes = !trip.srcLoc || !trip.pickUpAddress || !trip.taxiType || !this.state.carCategory;
      this.setState({submitDisabled: checkRes})
    }
  }
  setSrcGeoData(srcLoc, pickUpAddress) {
    const trip = this.state.trip;
    trip.srcLoc = srcLoc;
    trip.pickUpAddress = pickUpAddress;
    this.setState({trip});
    this.checkAvailability();
    if (trip.destAddress) this.calcFare();
  }
  setDestGeoData(destLoc, destAddress) {
    const trip = this.state.trip;
    trip.destLoc = destLoc;
    trip.destAddress = destAddress;
    this.setState({trip});
    this.checkAvailability();
    if (trip.pickUpAddress) this.calcFare();
  }
  clearResponse() {
    this.props.errorPostTrip = null;
    this.props.errorStatusPostTrip = false;
    this.props.newTrip = null;
  }
  handleRiderFirstName(e) {
    e.preventDefault();
    const trip = this.state.trip;
    trip.riderFirstName = e.target.value;
    this.setState({trip});
    this.checkAvailability();
  }
  handleRiderLastName(e) {
    e.preventDefault();
    const trip = this.state.trip;
    trip.riderLastName = e.target.value;
    this.setState({trip});
  }
  handlePickUpAddress(e) {
    e.preventDefault();
    const trip = this.state.trip;
    trip.pickUpAddress = e.target.value;
    this.setState({trip});
    this.checkAvailability();
  }
  handleDestAddress(e) {
    e.preventDefault();
    const trip = this.state.trip;
    trip.destAddress = e.target.value;
    this.setState({trip});
    this.checkAvailability();
  }
  handleTaxiType(value) {

    let category = this.props.carCategories.find((arr, i) => {
      return arr._id === value;
    });
    const trip = this.state.trip;
    trip.taxiType = category._id;
    trip.fareDetails = this.props.fareData ? this.props.fareData.cost[category.name] : null;
    this.setState({
      trip,
      carCategory: category
    });
    this.calcArrive();

    const drivers = this.state.drivers;
    const map = this.state.map;
    this.renderDrivers({
      map,
      carCategory: value,
      drivers
    });
  }
  handleKeyDown(event) {
    if (event.keyCode === 13 ) {
      event.preventDefault();
    }
  }
  handleSubmit(e) {
    e.preventDefault();
    let { formatMessage } = this.props.intl;
    const trip = this.state.trip;
    trip.tripAmt = (trip.destLoc) ? trip.fareDetails.totalFare : null;
    if (trip.dateTime && trip.preBooking === bookingConst.later) {
      if (!(Moment().add(30, 'minute').valueOf() < trip.dateTime.valueOf() && Moment().add(30, 'day').valueOf() > trip.dateTime.valueOf())) {
        return Toastr.error(formatMessage(messages.bookingDateTimeErr));
      }
      if (Moment(trip.dateTime).isBetween(bookingConst.startNY, bookingConst.endNY)) {
        return Toastr.error(formatMessage(messages.bookingNYErr));
      }
    }
    this.props.clearState(); // remove response msg

    this.props.createTrip(this.state.trip, this.state.carCategory);
  }
  handleReset(e) {
    e.preventDefault();
    this.props.clearState(); // remove response msg
    const trip = this.state.trip;
    const defaultCategory = this.props.defaultCategory;
    trip.riderFirstName = '';
    trip.riderLastName = '';
    trip.pickUpAddress = '';
    trip.destAddress = '';
    trip.srcLoc = null;
    trip.destLoc = null;
    trip.taxiType = defaultCategory ? defaultCategory.name : '';
    trip.preBooking = bookingConst.now;
    trip.dateTime = null;
    this.setState({
      trip,
      arriveData: null
    });
    this.checkAvailability();
    this.props.clearFare();
  }
  handlePreBooking(e) {
    e.preventDefault();
    const trip = this.state.trip;
    trip.preBooking = e.target.value;
    this.setState({trip});
    this.checkAvailability();
  }
  changeBooking(selected) {
    const trip = this.state.trip;
    trip.dateTime = selected;
    this.setState({trip});
    this.checkAvailability();
  }
  calcFare() {
    let { formatMessage } = this.props.intl;
    const trip = this.state.trip;
    const { srcLoc, destLoc } = trip;

    if (!trip.pickUpAddress || !trip.destAddress || !trip.srcLoc || !trip.destLoc) {
      return Toastr.error(formatMessage(messages.pickUpAddressNotSet));
    } else if (!this.props.carCategories) {
      return Toastr.error(formatMessage(messages.carCategoriesNotFetched));
    }

    const originLoc = `${srcLoc[1]},${srcLoc[0]}`;
    const destinationLoc = `${destLoc[1]},${destLoc[0]}`;
    this.props.calculateFare(originLoc, destinationLoc);
  }
  calcArrive() {
    const trip = this.state.trip;

    if (this.props.carCategories && trip.pickUpAddress && trip.srcLoc && trip.taxiType) {
      const selectedCat = this.props.carCategories.filter((carCat) => ( carCat._id === trip.taxiType ))[0] || null;

      if (selectedCat && selectedCat.drivers.length) {
        Map.getDistances(
          selectedCat.drivers.map(driver => (driver.gpsLoc)), // source points
          trip.srcLoc, // destination point
          (err, data) => {
          if (err) return Toastr.error(err);
          this.setState({ arriveData: Utils.getArrive(data) });
        });
      } else {
        this.setState({ arriveData: null });
      }
    }
  }
  checkAvailability() {
    const trip = this.state.trip;
    const checkRes = !trip.srcLoc || !trip.pickUpAddress || !trip.taxiType || !this.state.carCategory;
    const drivers = this.state.drivers;
    const map = this.state.map;
    let cnt = 0;
    drivers.forEach((driver) => {
      if (driver.carCategory === trip.taxiType) {
        driver.marker.setMap(map);
        cnt += 1;
      } else {
        driver.marker.setMap(null);
      }
    });
    this.setState({submitDisabled: (cnt === 0 && trip.preBooking === bookingConst.now) ? true : checkRes})
  }

  isValid( current ) {
    var yesterday = Datetime.moment().subtract( 1, 'day' );
    return current.isAfter( yesterday );
  };

  roundUpDateTo5Minutes() {
    const now = new Date();
    let minutes = now.getMinutes();
    var temp = minutes % 5;
    temp && (minutes = minutes - temp + 5);
    now.setMinutes(minutes);
    return now;
  };



  render() {
    const selectedCat = this.props.carCategories.filter((carCat) => ( carCat._id === this.state.trip.taxiType ))[0] || null;
    let taxiName = '';
    if (selectedCat) {
      if (selectedCat.name === 'CabdoBlack') {
        taxiName = 'Cabdo';
      } else {
        taxiName = selectedCat.name;
      }
    }
    let {formatMessage} = this.props.intl;
    const value = this.state.trip.dateTime === null ? this.roundUpDateTo5Minutes() : '';
    return (
      <Row className={`${s['fullHeight-md']} animate ${s['start-trip']}`}>
        <Col md={4} className={s['fullHeight-md']}>
          <Panel className={s.customHeader} header={formatMessage(messages.startTrip)} bsStyle="primary" >
            {
              this.props.errorStatusPostTrip
              ?
                <div className="alert alert-danger">
                  <FormattedMessage
                    id='app.form.errorPostTrip.message'
                    defaultMessage='Please enter pickup location'
                  />
                </div>
              : null
            }
            {
              this.props.newTrip
              ?
              <div className="alert alert-success">
                <FormattedMessage
                  id='app.form.newTrip.message'
                  defaultMessage="The request for a trip is in processing"
                />
              </div>
              : null
            }
            <div
              style={{display: this.props.startTripLoading ? 'block' : 'none', minHeight: 500 }}
              id="tripObjectLoading" className="loading-wrap">
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>

            <Form
              style={{display: this.props.startTripLoading ? 'none' : 'block' }}
              horizontal
              onKeyDown={this.handleKeyDown}
              onSubmit={(e) => { this.handleSubmit(e); }}
              onReset={(e) => { this.handleReset(e); }}
            >
              <FormGroup>
                <ControlLabel>
                  <FormattedMessage
                    id="app.form.riderFirstName"
                    defaultMessage="Rider First Name"
                  />
                </ControlLabel>
                <InputGroup>
                  <InputGroupAddon>
                    <i className="glyphicon glyphicon-user" />
                  </InputGroupAddon>
                  <FormControl type="text" value={this.state.trip.riderFirstName} placeholder={formatMessage(messages.firstName)} onChange={(e) => { this.handleRiderFirstName(e); }} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <ControlLabel>
                  <FormattedMessage
                    id='app.form.riderLastName'
                    defaultMessage="Rider Last Name"
                  />
                </ControlLabel>
                <InputGroup>
                  <InputGroupAddon>
                    <i className="glyphicon glyphicon-user" />
                  </InputGroupAddon>
                  <FormControl type="text" value={this.state.trip.riderLastName} placeholder={formatMessage(messages.lastName)} onChange={(e) => { this.handleRiderLastName(e); }} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <ControlLabel>
                  <FormattedMessage
                    id='app.form.pickupAddress'
                    defaultMessage="Pickup Address"
                  /> *
                </ControlLabel>
                <InputGroup>
                  <InputGroupAddon>
                    <i className="glyphicon glyphicon-map-marker" />
                  </InputGroupAddon>
                  <FormControl type="text" value={this.state.trip.pickUpAddress} placeholder={formatMessage(messages.sourceAddress)} id="srcAddress" onChange={(e) => { this.handlePickUpAddress(e); }} />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <ControlLabel>
                  <FormattedMessage
                    id='app.form.destinationAddress'
                    defaultMessage="Destination Address"
                  />
                </ControlLabel>
                <InputGroup>
                  <InputGroupAddon>
                    <i className="glyphicon glyphicon-map-marker" />
                  </InputGroupAddon>
                  <FormControl type="text" value={this.state.trip.destAddress} placeholder={formatMessage(messages.destinationAddress)} id="destAddress" onChange={(e) => { this.handleDestAddress(e); }} />
                </InputGroup>
              </FormGroup>

              <FormGroup>
                <ControlLabel>
                  <FormattedMessage
                    id='app.form.preBooking'
                    defaultMessage="Pre booking"
                  />
                </ControlLabel>
                <InputGroup>
                  <InputGroupAddon>
                    <i className="glyphicon glyphicon-time" />
                  </InputGroupAddon>
                  <FormControl componentClass="select" value={this.state.trip.preBooking} onChange={(e) => { this.handlePreBooking(e); }}>
                    <option value="now">{formatMessage(messages.rideNow)}</option>
                    <option value="later">{formatMessage(messages.rideLater)}</option>
                  </FormControl>
                </InputGroup>
              </FormGroup>
              <Collapse in={this.state.trip.preBooking === bookingConst.later}>
                <FormGroup>
                  <ControlLabel>{formatMessage(messages.selectDateTime)}</ControlLabel>
                  <InputGroup>
                    <InputGroupAddon>
                        <i className="glyphicon glyphicon-calendar"/>
                    </InputGroupAddon>
                    <Datetime
                      defaultValue={ value }
                      value={this.state.trip.dateTime}
                      onChange={ this.changeBooking }
                      strictParsing={true}
                      dateFormat="DD/MM/YYYY"
                      timeFormat="HH:mm"
                      input={true}
                      timeConstraints={ { minutes: { min: 0, max: 59, step: 5 }} }
                      isValidDate={ this.isValid }
                      inputProps={{
                        placeholder: formatMessage(messages.selectDateTimePlaceholder),
                      }}/>
                  </InputGroup>
                </FormGroup>
              </Collapse>
              <FormGroup>
                <ControlLabel>
                  <FormattedMessage
                    id='app.form.taxiType'
                    defaultMessage="Select service"
                  /> *
                </ControlLabel>
                <InputGroup>
                  <ButtonToolbar>
                    <ToggleButtonGroup type="radio" name="options"
                                       value={ this.state.carCategory ? this.state.carCategory._id : null }
                                       onChange={(value) => { this.handleTaxiType(value); }}
                    >
                      { this.props.carCategories.map((item, index) => (
                        <ToggleButton key={index} value={item._id} className={s.taxiWrapper+' toggle-button'} active={item !== null && item.hasOwnProperty("_id") && this.state.carCategory !== null && this.state.carCategory.hasOwnProperty("_id") && item._id === this.state.carCategory._id}>
                          <p className={s.taxiName}>{item.name}</p>
                          <img src={ item.default ? carDefault : carOptional } className={s.carIcons} alt={formatMessage(messages.carCategory)}/>
                          {
                            this.props.fareData && this.props.fareData.categories &&
                            this.props.fareData.categories.map((fare, fareIndex) => (
                              fare.name === item.name &&
                              (<p key={fareIndex} className={item.default ? s.defaultFare : s.fare}>
                                {fare.isDefaultCat ? `${fare.cost.toFixed(2)}€ - ${formatMessage(messages.guaranteed)}` : `${formatMessage(messages.from)} ${fare.cost.toFixed(2)}€`}
                              </p>)
                            ))
                          }
                          <p className={s.taxiCapacity}>{item.name === 'TaxiXL' && formatMessage(messages.upto)} {item.capacity} {formatMessage(messages.passengers)}</p>
                        </ToggleButton>
                      )) }
                    </ToggleButtonGroup>
                  </ButtonToolbar>
                </InputGroup>
              </FormGroup>
              <Collapse in={this.state.arriveData && this.state.trip.preBooking === bookingConst.now && selectedCat}>
                <p className="text-success">
                  <FormattedMessage
                    id='app.form.taxiWillArriveIn'
                    defaultMessage="{taxiName} will arrive in {arriveData}"
                    values={{ taxiName: taxiName, arriveData: this.state.arriveData && this.state.arriveData.text }}
                  />
                </p>
              </Collapse>
              <Button type="reset"> Reset </Button> &nbsp;&nbsp;
              <Button type="submit" bsStyle="primary" disabled={this.state.submitDisabled}>
                <FormattedMessage
                  id='app.form.submit'
                  defaultMessage="Submit"
                />
              </Button>
            </Form>
          </Panel>
        </Col>
        <Col md={8} className={`${s['fixedHeight-xs']} ${s['fullHeight-md']}`}>
          <div id="map"
            style={{ height: '100%' }}
          ></div>
        </Col>
      </Row>
    );
  }
}

function mapStateToProp(state) {
  return {
    startTripLoading: state.domainReducer.trip.startTripLoading,
    errorStatusPostTrip: state.domainReducer.trip.errorStatusPostTrip,
    errorPostTrip: state.domainReducer.trip.errorPostTrip,
    newTrip: state.domainReducer.trip.newTrip,
    carCategories: state.domainReducer.trip.carCategories,
    defaultCategory: state.domainReducer.trip.defaultCategory,
    fareData: state.domainReducer.trip.fareData,
  };
}

function bindActions(dispatch) {
  return {
    createTrip: (trip, carCategory) => dispatch(TripAction.createTrip(trip, carCategory)),
    fetchCarCategories: () => (dispatch(TripAction.fetchCarCategories())),
    fetchCorporateUser: () => (dispatch(TripAction.fetchCorporateUser())),
    clearState: () => dispatch(TripAction.clearState()),
    calculateFare: (origin, destination) => dispatch(TripAction.estimateFare(origin, destination)),
    clearFare: () => dispatch(TripAction.clearFare())
  };
}

// export default withStyles(s)(StartTrip);
export default injectIntl(connect(mapStateToProp, bindActions)(withStyles(s)(StartTrip)));
