import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/portal/startTrip',

  async action() {
    const StartTrip = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./StartTrip').default);
      }, 'startTrip');
    });

    return {
      title: 'Start Trip',
      component: <Layout><StartTrip key="startTrip" /></Layout>,
    };
  },
};
