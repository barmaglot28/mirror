import React, { Component } from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-bootstrap-date-picker';
import Moment from 'moment';
import { Pagination, Button, Panel, FormGroup, ControlLabel, Collapse, Row, Col } from 'react-bootstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { FormattedMessage } from 'react-intl';
import s from './Trips.css';
import tripAction from '../../../store/domain/trip/action';
import Utils from '../../../helpers/utils';

import car2 from '../../../common/images/car2.png';
import car3 from '../../../common/images/car3.png';
import pickupIcon from '../../../common/images/pickup_icon.png';
import destIcon from '../../../common/images/dest_icon.png';
import timeIcon from '../../../common/images/time_icon.png';
import cashIcon from '../../../common/images/cash-icon.png';
import rider from '../../../common/images/rider.png';
import driver from '../../../common/images/person-icon.png';


class Trips extends Component {

  static propTypes = {
    preBookedList: React.PropTypes.object,
    preBookedLoading: React.PropTypes.bool,
    preBookedPageNo: React.PropTypes.any,
    preBookedMeta: React.PropTypes.object,
    fetchPreBooked: React.PropTypes.func,
    removePreBooked: React.PropTypes.func,
    removeLoading: React.PropTypes.bool,
    refreshPreBooked: React.PropTypes.func,

    activeTripList: React.PropTypes.object,
    activeTripLoading: React.PropTypes.bool,
    activeTripPageNo: React.PropTypes.any,
    activeTripMeta: React.PropTypes.object,
    fetchActiveTrips: React.PropTypes.func,
    cancelActiveTrip: React.PropTypes.func,
    cancelLoading: React.PropTypes.bool,
    refreshActiveTrips: React.PropTypes.func,

    completedTripList: React.PropTypes.object,
    completedTripLoading: React.PropTypes.bool,
    completedTripPageNo: React.PropTypes.any,
    completedTripMeta: React.PropTypes.object,
    fetchCompletedTrips: React.PropTypes.func,
    refreshCompletedTrips: React.PropTypes.func,

    exportCompleted: React.PropTypes.func,
    tripFilter: React.PropTypes.string // not used
  };

  constructor(props) {
    super(props);
    this.state = {
      preBookedActivePage: 1,
      activeTripActivePage: 1,
      completedTripActivePage: 1,
      fetchIntervalId: 0,
      exportOpen: false,
      startRange: null,
      endRange: null
    };
    this.handlePreBookedPagination = this.handlePreBookedPagination.bind(this);
    this.handleActiveTripPagination = this.handleActiveTripPagination.bind(this);
    this.handleCompletedTripPagination = this.handleCompletedTripPagination.bind(this);
    this.handleExportCompleted = this.handleExportCompleted.bind(this);
    // this.handleTripFilter = this.handleTripFilter.bind(this);
  }
  componentWillMount() {
    const interval = setInterval(() => {
      this.props.refreshPreBooked(this.state.preBookedActivePage, this.props.tripFilter);
      this.props.refreshActiveTrips(this.state.activeTripActivePage, this.props.tripFilter);
      this.props.refreshCompletedTrips(this.state.completedTripActivePage, this.props.tripFilter);
    }, 5000)

    this.setState({ fetchIntervalId: interval })

    this.props.fetchPreBooked(this.state.preBookedActivePage, this.props.tripFilter);
    this.props.fetchActiveTrips(this.state.activeTripActivePage, this.props.tripFilter);
    this.props.fetchCompletedTrips(this.state.completedTripActivePage, this.props.tripFilter);
  }
  componentWillUnmount() {
    clearInterval(this.state.fetchIntervalId);
  }
/*  handleTripFilter(filter) {
    if (filter !== this.state.activeFilter) {
      this.setState({ activeFilter: filter, activePage: 1 });
      this.props.fetchTrips(1, filter);
    }
  }*/
  handlePreBookedPagination(eventkey) {
    this.setState({ preBookedActivePage: eventkey });
    this.props.fetchPreBooked(eventkey, this.props.tripFilter);
  }
  handleActiveTripPagination(eventkey) {
    this.setState({ activeTripActivePage: eventkey });
    this.props.fetchActiveTrips(eventkey, this.props.tripFilter);
  }
  handleCompletedTripPagination(eventkey) {
    this.setState({ completedTripActivePage: eventkey });
    this.props.fetchCompletedTrips(eventkey, this.props.tripFilter);
  }
  handleExportCompleted() {
    this.props.exportCompleted({
      startRange: this.state.startRange ? Moment(this.state.startRange).startOf('day').toISOString() : null,
      endRange: this.state.endRange ? Moment(this.state.endRange).endOf('day').toISOString() : null
    });
  }
  render() {
    return (
      <div className={`animate ${s.trips}`}>
        {/* PRE BOOKED TRIP REQUESTS */}
        {
          this.props.preBookedLoading
            ?
            <div id="table-pre-booked" className="loading-wrap" style={{ minHeight: 500 }} key="spinnerdiv0">
              <div className="loading">
                <div className="loader">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
            :
            <div key="ddiv0">
              <div className="row">
                <div className="col-sm-12">
                  <div className={`panel panel-default ${s.borderNone}`}>
                    <div className={`panel-heading ${s.customTitle}`}>
                      <h3 className="panel-title">
                        <FormattedMessage
                          id='app.form.preBooked'
                          defaultMessage="Pre Booked"
                        />
                      </h3>
                    </div>
                    <div className="panel-body">
                      {
                        this.props.preBookedList.data === null
                          ?
                          <div>
                            <FormattedMessage
                              id='app.trip.noTripAvailable'
                              defaultMessage="No trip Available"
                            />
                          </div>
                          :
                          <div className="table-responsive">
                            <table className="table">
                              <tbody>
                                <tr>
                                  <th>
                                    <FormattedMessage
                                      id='app.trip.sNo'
                                      defaultMessage="S.No"
                                    />
                                  </th>
                                  <th>
                                    <FormattedMessage
                                      id='app.trip.ID'
                                      defaultMessage="ID"
                                    />
                                  </th>
                                  <th>
                                    <FormattedMessage
                                      id='app.trip.rider'
                                      defaultMessage="Rider"
                                    />
                                  </th>
                                  <th>
                                    <FormattedMessage
                                      id='app.trip.pickUpAddress'
                                      defaultMessage="Pick-up Address"
                                    />
                                  </th>
                                  <th>
                                    <FormattedMessage
                                      id='app.trip.destinationAddress'
                                      defaultMessage="Destination Address"
                                    />
                                  </th>
                                  <th>
                                    <FormattedMessage
                                      id='app.trip.bookingTime'
                                      defaultMessage="Booking Time"
                                    />
                                  </th>
                                  <th>
                                    <FormattedMessage
                                      id='app.trip.action'
                                      defaultMessage="Action"
                                    />
                                  </th>
                                </tr>
                                {
                                  this.props.preBookedList.data.map((item, index) => (
                                    <tr key={index}>
                                      <td>
                                        {
                                          (
                                            (this.state.preBookedActivePage - 1) * this.props.preBookedMeta.limit
                                          ) + (index + 1)
                                        }
                                      </td>
                                      <td>
                                        <FormattedMessage
                                          id='app.trip.tripTitle'
                                          defaultMessage="Trip"
                                        />
                                        <span>{` #${item.tripNumber}`}</span>
                                      </td>
                                      <td>{Utils.getName(item.rider)}</td>
                                      <td className="text-capitalize">{item.pickUpAddress}</td>
                                      <td className="text-capitalize">{item.destAddress || 'NA' }</td>
                                      <td className="text-capitalize">{Utils.getTime(item.preBookedTime)}</td>
                                      <td style={{ minWidth: 100 }}>
                                        <button className="btn btn-danger" title="Remove" disabled={this.props.removeLoading} onClick={() => this.props.removePreBooked(item)}>
                                          <i className="glyphicon glyphicon glyphicon-remove" />
                                        </button>
                                      </td>
                                    </tr>),
                                  )
                                }
                              </tbody>
                            </table>
                            {this.props.preBookedMeta.totalNoOfPages > 1 &&
                              <Pagination
                                bsSize="medium"
                                prev
                                next
                                first
                                last
                                ellipsis
                                boundaryLinks
                                maxButtons={5}
                                items={this.props.preBookedMeta ? this.props.preBookedMeta.totalNoOfPages : 1}
                                activePage={this.state.preBookedActivePage}
                                onSelect={this.handlePreBookedPagination}
                              />
                            }
                          </div>
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
        }
        {/* TRIP REQUESTS */}
        {
          this.props.activeTripLoading
            ?
            <div id="table-trip-request" className="loading-wrap" style={{ minHeight: 500 }} key="spinnerdiv1">
              <div className="loading">
                <div className="loader">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
            :
            <div key="ddiv1">
              <div className="row">
                <div className="col-sm-12">
                  <div className={`panel panel-default ${s.borderNone}`}>
                    <div className={`panel-heading ${s.customTitle}`}>
                      <h3 className="panel-title">
                        <FormattedMessage
                          id='app.trip.activeTrips'
                          defaultMessage="Active Trips"
                        />
                      </h3>
                    </div>
                    <div>
                      {
                        this.props.activeTripList.data === null
                          ?
                          <div>
                            <FormattedMessage
                              id='app.trip.noTripAvailable'
                              defaultMessage="No trip Available"
                            />
                          </div>
                          :
                          <div className={`table-responsive ${s.activeTripWrapper}`}>
                              {
                                this.props.activeTripList.data.map((item, index) => (
                                  <div key={index} className={s.activeTripItem}>

                                    <div className={s.tripFigureWrapper}>
                                      <p>
                                        <FormattedMessage
                                          id='app.trip.tripTitle'
                                          defaultMessage="Trip"
                                        />
                                        <span>{` #${item.tripNumber}`}</span>
                                      </p>
                                      <div className={s.tripFigure}>
                                        <img src={pickupIcon} className={s.destIcons}/>
                                        <div className={s.sideDashedLines} />
                                        {item.taxiType === 'CabdoBlack' ?
                                          <img src={car2} className={s.carIcon}/>
                                          :
                                          <img src={car3} className={s.carIcon}/>
                                        }
                                        <div className={s.sideDashedLines}/>
                                        <img src={destIcon} className={s.destIcons}/>
                                      </div>
                                      <p style={{ textAlign: 'center' }}>{item.taxiType}</p>
                                    </div>

                                    <ul className={s.mainTripInfo}>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={pickupIcon} alt="loc icon"/>
                                        </div>
                                        <span>{item.pickUpAddress}</span>
                                      </li>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={pickupIcon} alt="loc icon"/>
                                        </div>
                                        <span>{item.destAddress || 'NA'}</span>
                                      </li>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={cashIcon} alt="cash icon"/>
                                        </div>
                                        <span>{item.tripAmt || item.fareDetails ? item.fareDetails.totalFare : 'NA'}</span>
                                      </li>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={timeIcon} alt="time icon"/>
                                        </div>
                                        <span>{Utils.getFullDateTime(item.bookingTime)}</span>
                                      </li>
                                    </ul>
                                  </div>),
                                )
                              }
                            {
                              this.props.activeTripMeta.totalNoOfPages > 1 &&
                                <Pagination
                                  bsSize="medium"
                                  prev
                                  next
                                  first
                                  last
                                  ellipsis
                                  boundaryLinks
                                  maxButtons={5}
                                  items={this.props.activeTripMeta ? this.props.activeTripMeta.totalNoOfPages : 1}
                                  activePage={this.state.activeTripActivePage}
                                  onSelect={this.handleActiveTripPagination}
                                />
                            }
                          </div>
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
        }
        {/* TRIPS*/}
        {
          this.props.completedTripLoading
          ?
            <div id="table-trip" className="loading-wrap" style={{ minHeight: 500 }} key="spinnerdiv2">
              <div className="loading">
                <div className="loader">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
          :
            <div key="ddiv2">
              <div className="row">
                <div className="col-sm-12">
                  <div className={`panel panel-default ${s.borderNone}`}>
                    <div className={`panel-heading ${s.customTitle}`}>
                      <h3 className="panel-title">
                        <FormattedMessage
                          id='app.trip.completed'
                          defaultMessage="Completed"
                        />
                      </h3>
                    </div>
                    <div className="panel-body">

                      <Button onClick={ ()=> this.setState({ exportOpen: !this.state.exportOpen })}>
                        <FormattedMessage
                          id='app.trip.exporting'
                          defaultMessage="Exporting"
                        />
                      </Button>
                      <Collapse in={this.state.exportOpen}>
                        <Panel>
                          <Row>
                            <Col md={4}>
                              <FormGroup>
                                <ControlLabel>
                                  <FormattedMessage
                                    id='app.trip.fromDate'
                                    defaultMessage="From date"
                                  />
                                </ControlLabel>
                                <DatePicker
                                  dateFormat="DD/MM/YYYY"
                                  value={this.state.startRange}
                                  onChange={(startRange) => { this.setState({ startRange }); }}
                                  calendarPlacement="top"
                                />
                              </FormGroup>
                            </Col>
                            <Col md={4}>
                              <FormGroup>
                                <ControlLabel>
                                  <FormattedMessage
                                    id='app.trip.toDate'
                                    defaultMessage="To date"
                                  />
                                </ControlLabel>
                                <DatePicker
                                  dateFormat="DD/MM/YYYY"
                                  value={this.state.endRange}
                                  onChange={(endRange) => { this.setState({ endRange }); }}
                                  calendarPlacement="top"
                                />
                              </FormGroup>
                            </Col>
                            <Col md={4}>
                              <FormGroup>
                                <ControlLabel className="visible-md visible-lg" >&nbsp;</ControlLabel>
                                <Button type="button" bsStyle="primary" style={{ display: 'block' }}
                                  onClick={this.handleExportCompleted}
                                >
                                  <FormattedMessage
                                    id='app.trip.exportCompletedTrips'
                                    defaultMessage="Export completed trips"
                                  />
                                </Button>
                              </FormGroup>
                            </Col>
                          </Row>
                        </Panel>
                      </Collapse>

                      <hr />

                      {/*
                      <div className="row" style={{ marginBottom: 15 }}>
                        <div className="col-sm-12 text-center">
                          <div className="btn-group">
                            <button type="button" onClick={() => this.handleTripFilter('All')} className="btn btn-primary">All</button>
                            <button type="button" onClick={() => this.handleTripFilter('Ongoing')} className="btn btn-primary">Ongoing</button>
                            <button type="button" onClick={() => this.handleTripFilter('Completed')} className="btn btn-primary">Completed</button>
                          </div>
                        </div>
                      </div>
                      */}
                      {
                        this.props.completedTripList.data === null
                        ?
                          <div>
                            <FormattedMessage
                              id='app.trip.noTripAvailable'
                              defaultMessage="No trip Available"
                            />
                          </div>
                        :
                          <div className={`table-responsive ${s.activeTripWrapper}`}>
                            {
                              this.props.completedTripList.data.map((item, index) => (
                                <div key={index} className={s.activeTripItem}>
                                    <div className={s.tripFigureWrapper}>
                                      <p>
                                        <FormattedMessage
                                          id='app.trip.tripTitle'
                                          defaultMessage="Trip"
                                        />
                                        <span>{` #${item.tripNumber}`}</span>
                                      </p>
                                      <div className={s.tripFigure}>
                                      <img src={pickupIcon} className={s.destIcons}/>
                                      <div className={s.sideDashedLines} />
                                      {item.taxiType === 'CabdoBlack' ?
                                        <img src={car2} className={s.carIcon}/>
                                        :
                                        <img src={car3} className={s.carIcon}/>
                                      }
                                      <div className={s.sideDashedLines}/>
                                       <img src={destIcon} className={s.destIcons}/>
                                      </div>
                                      <p style={{ textAlign: 'center' }}>{item.taxiType}</p>
                                      <div className={s.restTripInfo}>
                                        <div>
                                          <p className={s.restTripIfoItem}>
                                            <span>Driver: </span>{Utils.getName(item.driver)}
                                          </p>
                                          <p className={s.restTripIfoItem}>
                                            <span>Rider: </span>{Utils.getName(item.rider)}
                                          </p>
                                          <p className={s.restTripIfoItem}>
                                            <span>Trip status: </span>{item.tripStatus}
                                          </p>
                                        </div>
                                        <div>
                                          <p className={s.restTripIfoItem}>
                                            <span>Driver's rating: </span>{item.driverRatingByRider || 'NA'}
                                          </p>
                                          <p className={s.restTripIfoItem}>
                                            <span>Rider's rating: </span>{item.riderRatingByDriver || 'NA'}
                                          </p>
                                          <p className={s.restTripIfoItem}>
                                            <span>Travel time: </span>{Utils.getDuration(item.travelTime)}
                                          </p>
                                        </div>
                                      </div>
                                    </div>

                                    <ul className={s.mainTripInfo}>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={pickupIcon} alt="loc icon"/>
                                        </div>
                                        <span>{item.pickUpAddress}</span>
                                      </li>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={pickupIcon} alt="loc icon"/>
                                        </div>
                                        <span>{item.destAddress || 'NA'}</span>
                                      </li>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={cashIcon} alt="cash icon"/>
                                        </div>
                                        <span>{item.tripAmt || 'NA'}</span>
                                      </li>
                                      <li className="text-capitalize">
                                        <div className={s.iconWrapper}>
                                          <img src={timeIcon} alt="time icon"/>
                                        </div>
                                        <span>{Utils.getDateTime(item.endTime)}</span>
                                      </li>
                                    </ul>
                                </div>)
                              )
                            }
                            { this.props.completedTripMeta.totalNoOfPages > 1 &&
                              <Pagination
                                bsSize="medium"
                                prev
                                next
                                first
                                last
                                ellipsis
                                boundaryLinks
                                maxButtons={5}
                                items={this.props.completedTripMeta ? this.props.completedTripMeta.totalNoOfPages : 1}
                                activePage={this.state.completedTripActivePage}
                                onSelect={this.handleCompletedTripPagination}
                              />
                            }
                          </div>
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
        }
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    preBookedList: state.domainReducer.trip.preBookedList,
    preBookedLoading: state.domainReducer.trip.preBookedLoading,
    preBookedMeta: state.domainReducer.trip.preBookedMeta,
    removeLoading: state.domainReducer.trip.removeLoading,

    activeTripList: state.domainReducer.trip.activeTripList,
    activeTripLoading: state.domainReducer.trip.activeTripLoading,
    activeTripMeta: state.domainReducer.trip.activeTripMeta,
    cancelLoading: state.domainReducer.trip.cancelLoading,

    completedTripList: state.domainReducer.trip.completedTripList,
    completedTripLoading: state.domainReducer.trip.completedTripLoading,
    completedTripMeta: state.domainReducer.trip.completedTripMeta,

    tripFilter: state.domainReducer.trip.tripFilter // not used
  };
}

function bindActions(dispatch) {
  return {
    fetchPreBooked: (pageNo, filter) => dispatch(tripAction.fetchPreBooked(pageNo, filter)),
    removePreBooked: (item) => dispatch(tripAction.removePreBooked(item)),
    refreshPreBooked: (pageNo, filter) => dispatch(tripAction.refreshPreBooked(pageNo, filter)),

    fetchActiveTrips: (pageNo, filter) => dispatch(tripAction.fetchActiveTrips(pageNo, filter)),
    cancelActiveTrip: (item) => dispatch(tripAction.cancelActiveTrip(item)),
    refreshActiveTrips: (pageNo, filter) => dispatch(tripAction.refreshActiveTrips(pageNo, filter)),

    fetchCompletedTrips: (pageNo, filter) => dispatch(tripAction.fetchCompletedTrips(pageNo, filter)),
    refreshCompletedTrips: (pageNo, filter) => dispatch(tripAction.refreshCompletedTrips(pageNo, filter)),

    exportCompleted: (ranges) => dispatch(tripAction.exportCompleted(ranges))
  };
}
export default connect(mapStateToProps, bindActions)(withStyles(s)(Trips));
