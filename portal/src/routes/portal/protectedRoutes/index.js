import StartTrip from '../StartTrip';
import Trips from '../Trips';

export default {
  path: '/portal',
  children: [
    StartTrip,
    Trips
  ],
  async action({ next, store }) {
    const route = await next();
    const authObj = store.getState().domainReducer.auth;
    if (authObj && !authObj.loggedInStatus) {
      console.log('inside protectedRoutes');
      return { redirect: '/' };
    }
    return route;
  },
};
