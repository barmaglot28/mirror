/* eslint-disable global-require */

// The top-level (parent) route
import StartTrip from './portal/StartTrip';
import Trips from './portal/Trips';

export default {
  path: '/',

  // Keep in mind, routes are evaluated in order
  children: [
    require('./login').default,
    require('./portal/protectedRoutes').default,
    StartTrip ,
    Trips,
    // Wildcard routes, e.g. { path: '*', ... } (must go last)
    require('./notFound').default,
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next();
    route.title = `${route.title || 'Untitled Page'}`;
    route.description = route.description || '';
    return route;
  },
};
