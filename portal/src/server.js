import 'babel-polyfill';
import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import requestLanguage from 'express-request-language';
import bodyParser from 'body-parser';
import React from 'react';
import ReactDOM from 'react-dom/server';
import UniversalRouter from 'universal-router';
import PrettyError from 'pretty-error';
import { IntlProvider } from 'react-intl';
import fs from 'fs';
import Promise from 'bluebird';

import './serverIntlPolyfill';
import App from './components/App';
import Html from './components/Html';
import { ErrorPageWithoutStyle } from './routes/error/ErrorPage';
import errorPageStyle from './routes/error/ErrorPage.css';
import routes from './routes';
import assets from './assets'; // eslint-disable-line import/no-unresolved
import configureStore from './store/configureStore';
import { setRuntimeVariable } from './store/view/runtime/action';
import { setLoggedInStatus } from './store/domain/auth/action';
import { setLocale } from './store/view/intl/action';
import { port, locales } from './config';

const app = express();

//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(requestLanguage({
  languages: locales,
  queryName: 'lang',
  cookie: {
    name: 'lang',
    options: {
      path: '/',
      maxAge: 3650 * 24 * 3600 * 1000, // 10 years in miliseconds
    },
    url: '/lang/{language}',
  },
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/setToken', async (req, res) => {
  const token = req.body.jwtAccessToken;
  res.cookie('id_token', token, { httpOnly: true });
  res.send({
    succes: true,
    message: 'cookie set',
  });
});

app.get('/unsetToken', async (req, res) => {
  // const token = req.body.jwtAccessToken;
  res.clearCookie('id_token');
  res.send({
    succes: true,
    message: 'cookie set',
  });
});

// extracting Messages for the requested language
const CONTENT_DIR = path.join(__dirname, './messages');
const readFile = Promise.promisify(fs.readFile);

app.post('/set-language', async (req, res, next) => {
  const locale = req.body.locale;
  res.cookie('lang', locale, { httpOnly: true });
  if (!locales.includes(locale)) {
    const err = new Error(`Locale '${locale}' not supported`);
    return next(err);
  }
  let localeData;
  try {
    localeData = await readFile(path.join(CONTENT_DIR, `${locale}.json`));
  } catch (err) {
    if (err.code === 'ENOENT') {
      const error = new Error(`Locale '${locale}' not found`);
      next(error);
    }
  }
  const newLocaleData = [];
  localeData = JSON.parse(localeData);
  localeData.forEach((item) => {
    newLocaleData.push({ id: item.id, message: item.message });
  });
  return res.send({ intl: newLocaleData });
});
//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
app.get('*', async (req, res, next) => {
  try {
    const store = configureStore({}, {
      cookie: req.headers.cookie,
    });
    store.dispatch(setRuntimeVariable({
      name: 'initialNow',
      value: Date.now(),
    }));
    if (req.cookies.id_token) {
      await store.dispatch(setLoggedInStatus(
        {
          name: 'setLoggedInStatus',
          loggedInStatus: true,
          jwtAccessToken: req.cookies.id_token,
        },
      ));
    } else {
      await store.dispatch(setLoggedInStatus(
        {
          name: 'setLoggedInStatus',
          jwtAccessToken: false,
        },
      ));
    }
    await store.dispatch(setRuntimeVariable({
      name: 'availableLocales',
      value: locales,
    }));

    const locale = req.language;
    await store.dispatch(setLocale({
      locale,
    }));
    const css = new Set();

    // Global (context) variables that can be easily accessed from any React component
    // https://facebook.github.io/react/docs/context.html
    const context = {
      // Enables critical path CSS rendering
      // https://github.com/kriasoft/isomorphic-style-loader
      insertCss: (...styles) => {
        // eslint-disable-next-line no-underscore-dangle
        styles.forEach(style => css.add(style._getCss()));
      },
      // Initialize a new Redux store
      // http://redux.js.org/docs/basics/UsageWithReact.html
      store,
    };

    const route = await UniversalRouter.resolve(routes, {
      ...context,
      path: req.path,
      query: req.query,
      locale,
    });

    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect);
      return;
    }

    const data = { ...route };
    data.children = ReactDOM.renderToString(<App context={context}>{route.component}</App>);
    data.style = [...css].join('');
    data.script = assets.main.js;
    data.state = context.store.getState();
    data.lang = locale;
    data.chunk = assets[route.chunk] && assets[route.chunk].js;
    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);

    res.status(route.status || 200);
    res.send(`<!doctype html>${html}`);
  } catch (err) {
    next(err);
  }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  console.log(pe.render(err)); // eslint-disable-line no-console
  const locale = req.language;
  const html = ReactDOM.renderToStaticMarkup(
    <Html
      title="Internal Server Error"
      description={err.message}
      style={errorPageStyle._getCss()} // eslint-disable-line no-underscore-dangle
      lang={locale}
    >
      {ReactDOM.renderToString(
        <IntlProvider locale={locale}>
          <ErrorPageWithoutStyle error={err} />
        </IntlProvider>,
      )}
    </Html>,
  );
  res.status(err.status || 500);
  res.send(`<!doctype html>${html}`);
});


app.listen(port, () => {
  console.log(`The server is running at http://localhost:${port}/`);
});
