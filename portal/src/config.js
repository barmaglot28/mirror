/* eslint-disable max-len */

export const port = process.env.PORT || 3003;
export const host = process.env.WEBSITE_HOSTNAME || `http://localhost:${port}`;

// default locale is the first one
export const locales = ['en-US', 'de-DE'];

export const auth = {
  jwt: { secret: process.env.JWT_SECRET || 'React Starter Kit' },
};

export const maps = {
  google: {
    apiKey: process.env.GOOGLE_MAPS_API_KEY || 'AIzaSyA-rEfh3RHxj4utD1nd8sfu8bXpv8cCOho',
    libraries: 'places',
    marker: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png'
  }
};
