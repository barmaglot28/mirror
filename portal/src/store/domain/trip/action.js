import * as Toastr from 'toastr';
import File from '../../../helpers/file';

import {
  FETCHING_PRE_BOOKED,
  FETCH_PRE_BOOKED_SUCCESS,
  FETCH_PRE_BOOKED_FAILED,

  FETCHING_ACTIVE_TRIPS,
  FETCH_ACTIVE_TRIPS_SUCCESS,
  FETCH_ACTIVE_TRIPS_FAILED,

  FETCHING_COMPLETED_TRIPS,
  FETCH_COMPLETED_TRIPS_SUCCESS,
  FETCH_COMPLETED_TRIPS_FAILED,

  CREATING_NEW_TRIP,
  CREATING_NEW_TRIP_SUCCESS,
  CREATING_NEW_TRIP_FAILED,

  FETCHING_CAR_CATEGORIES_SUCCESS,
  FETCHING_CAR_CATEGORIES_FAILED,
  CLEAR_STATE,

  CANCEL_ACTIVE_TRIP_START,
  CANCEL_ACTIVE_TRIP_SUCCESS,
  CANCEL_ACTIVE_TRIP_FAILED,

  REMOVE_PRE_BOOKED_START,
  REMOVE_PRE_BOOKED_SUCCESS,
  REMOVE_PRE_BOOKED_FAILED,

  REFRESH_PRE_BOOKED_SUCCESS,
  REFRESH_ACTIVE_TRIPS_SUCCESS,
  REFRESH_COMPLETED_TRIPS_SUCCESS,

  ESTIMATE_FARE_SUCCESS,
  ESTIMATE_FARE_CLEAR
} from './actionType';

import TripService from '../../../services/trip';
import RiderService from '../../../services/rider';
import CarCategoryService from '../../../services/carCategory';
import UserService from '../../../services/user';
/* PRE BOOKED */
function fetchingPreBooked(filter) {
  return {
    type: FETCHING_PRE_BOOKED,
    filter,
  };
}
function fetchPreBookedSuccess(data) {
  return {
    type: FETCH_PRE_BOOKED_SUCCESS,
    data,
  };
}
function fetchPreBookedFailed(data) {
  return {
    type: FETCH_PRE_BOOKED_FAILED,
    data,
  };
}
/* ACTIVE TRIPS */
function fetchingActiveTrips(filter) {
  return {
    type: FETCHING_ACTIVE_TRIPS,
    filter,
  };
}
function fetchActiveTripsSuccess(data) {
  return {
    type: FETCH_ACTIVE_TRIPS_SUCCESS,
    data,
  };
}
function fetchActiveTripsFailed(data) {
  return {
    type: FETCH_ACTIVE_TRIPS_FAILED,
    data,
  };
}
/* COMPLETED TRIPS */
function fetchingCompletedTrips(filter) {
  return {
    type: FETCHING_COMPLETED_TRIPS,
    filter,
  };
}
function fetchCompletedTripsSuccess(data) {
  return {
    type: FETCH_COMPLETED_TRIPS_SUCCESS,
    data,
  };
}
function fetchCompletedTripsFailed(data) {
  return {
    type: FETCH_COMPLETED_TRIPS_FAILED,
    data,
  };
}
/* CREATE NEW TRIP */
function createNewTrip() {
  return {
    type: CREATING_NEW_TRIP,
  };
}
function createNewTripSuccess(data) {
  return {
    type: CREATING_NEW_TRIP_SUCCESS,
    newTrip: data,
  };
}
function createNewTripFalied(data) {
  return {
    type: CREATING_NEW_TRIP_FAILED,
    errorPostTrip: data,
  };
}
/* CAR CATEGORIES */
function fetchCarCategoriesSuccess(data, defaultCategory) {
  return {
    type: FETCHING_CAR_CATEGORIES_SUCCESS,
    data,
    defaultCategory
  };
}
function fetchCarCategoriesFailed(data) {
  return {
    type: FETCHING_CAR_CATEGORIES_FAILED
  };
}
/* STATE */
function clearState() {
  return (dispatch) => {
    dispatch({
      type: CLEAR_STATE
    });
  };
}
/* CANCEL TRIP */
function cancelActiveTripStart() {
  return {
    type: CANCEL_ACTIVE_TRIP_START
  };
}
function cancelActiveTripSuccess(data) {
  return {
    type: CANCEL_ACTIVE_TRIP_SUCCESS,
    data
  };
}
function cancelActiveTripFailed() {
  return {
    type: CANCEL_ACTIVE_TRIP_FAILED
  };
}
/* REMOVE TRIP */
function removePreBookedStart() {
  return {
    type: REMOVE_PRE_BOOKED_START
  };
}
function removePreBookedSuccess(data) {
  return {
    type: REMOVE_PRE_BOOKED_SUCCESS,
    data
  };
}
function removePreBookedFailed() {
  return {
    type: REMOVE_PRE_BOOKED_FAILED
  };
}
/* REFRESHING */
function refreshPreBookedSuccess(data) {
  return (dispatch) => {
    dispatch({
      type: REFRESH_PRE_BOOKED_SUCCESS,
      data
    });
  };
}
function refreshActiveTripsSuccess(data) {
  return (dispatch) => {
    dispatch({
      type: REFRESH_ACTIVE_TRIPS_SUCCESS,
      data
    });
  };
}
function refreshCompletedTripsSuccess(data) {
  return (dispatch) => {
    dispatch({
      type: REFRESH_COMPLETED_TRIPS_SUCCESS,
      data
    });
  };
}
function fareEstimated(data) {
  return {
    type: ESTIMATE_FARE_SUCCESS,
    payload: data
  }
}
function clearFare() {
  return {
    type: ESTIMATE_FARE_CLEAR
  }
}

/* ACTIONS */
function fetchPreBooked(pageNo, filter) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingPreBooked(filter));
    TripService.list(token, { pageNo, filter, status: 'booked' })
      .then((response) => {
        if (response.success) {
          dispatch(fetchPreBookedSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchPreBookedFailed(e));
      });
  };
}

function fetchActiveTrips(pageNo, filter) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingActiveTrips(filter));
    TripService.list(token, { pageNo, filter, status: 'active' })
      .then((response) => {
        if (response.success) {
          dispatch(fetchActiveTripsSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchActiveTripsFailed(e));
      });
  };
}

function fetchCompletedTrips(pageNo, filter) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCompletedTrips(filter));
    TripService.list(token, { pageNo, filter, status: 'completed' })
    .then((response) => {
      if (response.success) {
        dispatch(fetchCompletedTripsSuccess(response));
      }
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchCompletedTripsFailed(e));
    });
  };
}

function createTrip(formData, carCategory) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;

    dispatch(createNewTrip());

    RiderService.postCorporateRider(token, formData)
      .then((newRider) => {

        TripService.post(token, { riderId: newRider.data._id }, formData, carCategory)
          .then((newTrip) => {
            dispatch(createNewTripSuccess(newTrip));
          })
          .catch((e) => {
            if (e.message === 'Unauthorized') {
              dispatch({ type: 'FLUSH_DATA' });
            }
            dispatch(createNewTripFalied(e));
          });
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(createNewTripFalied(e));
      });
  };
}

function fetchCarCategories() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;

    const promise = CarCategoryService.getCarCategories(token);
    promise
      .then((response) => {
        const categories = response.data;
        categories.sort((a, b) => a.name > b.name);
        dispatch(fetchCarCategoriesSuccess(categories, response.defaultCategory));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchCarCategoriesFailed());
      });
    return promise;
  };
}

function fetchCorporateUser() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;

    const promise = UserService.getCurrent(token);
    promise
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
      });
    return promise;
  };
}

function cancelActiveTrip(item) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;

    dispatch(cancelActiveTripStart());

    TripService.cancel(token, { id: item._id })
      .then((response) => {
        dispatch(cancelActiveTripSuccess(response.data));

        Toastr.success('trip cancel successful');
      })
      .catch((e) => {
        dispatch(cancelActiveTripFailed());

        Toastr.error('trip cancel failed');
      });
  };
}

function removePreBooked(item) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(removePreBookedStart());

    TripService.remove(token, { id: item._id })
      .then((response) => {
        dispatch(removePreBookedSuccess({ _id: item._id }));

        Toastr.success('trip remove successful');
      })
      .catch((e) => {
        dispatch(removePreBookedFailed());

        Toastr.error('trip remove failed');
      });
  };
}

function refreshPreBooked(pageNo, filter) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    TripService.list(token, { pageNo, filter, status: 'booked' })
      .then((response) => {
        if (response.success) {
          dispatch(refreshPreBookedSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        Toastr.error('fetching pre-booked trips failed');
      });
  };
}

function refreshActiveTrips(pageNo, filter) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    TripService.list(token, { pageNo, filter, status: 'active' })
      .then((response) => {
        if (response.success) {
          dispatch(refreshActiveTripsSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        Toastr.error('fetching active trips failed');
      });
  };
}


function refreshCompletedTrips(pageNo, filter) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    TripService.list(token, { pageNo, filter, status: 'completed' })
      .then((response) => {
        if (response.success) {
          dispatch(refreshCompletedTripsSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        Toastr.error('fetching completed trips failed');
      });
  };
}

function exportCompleted(ranges) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    // dispatch(fetchingUserTrips());
    TripService.exportCompleted(token, ranges)
      .then((response) => {
        if (response) {
          File.downloadBinary(response, 'completed-trips.xlsx');
          //   dispatch(fetchUserTripsSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        // dispatch(fetchUserTripsFailed(e));
      });
  };
}

function estimateFare(origin, destination) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(clearFare());

    TripService.estimateFare(token, origin, destination)
      .then((response) => {
        const { data, success } = response;
        if (success && data) {
          dispatch(fareEstimated({ categories: data.categories, cost: data.cost }));
        }
      })
      .catch((e) => {
        Toastr.error('Fare calculation failed');
      });
  };
}

export default {
  fetchPreBooked,
  fetchActiveTrips,
  fetchCompletedTrips,
  createTrip,
  fetchCarCategories,
  clearState,
  cancelActiveTrip,
  removePreBooked,
  refreshActiveTrips,
  refreshCompletedTrips,
  refreshPreBooked,
  fetchCorporateUser,
  exportCompleted,
  estimateFare,
  clearFare
};
