import {
  FETCHING_PRE_BOOKED,
  FETCH_PRE_BOOKED_SUCCESS,
  FETCH_PRE_BOOKED_FAILED,

  FETCHING_ACTIVE_TRIPS,
  FETCH_ACTIVE_TRIPS_SUCCESS,
  FETCH_ACTIVE_TRIPS_FAILED,

  FETCHING_COMPLETED_TRIPS,
  FETCH_COMPLETED_TRIPS_SUCCESS,
  FETCH_COMPLETED_TRIPS_FAILED,

  CREATING_NEW_TRIP,
  CREATING_NEW_TRIP_SUCCESS,
  CREATING_NEW_TRIP_FAILED,

  FETCHING_CAR_CATEGORIES_SUCCESS,
  FETCHING_CAR_CATEGORIES_FAILED,
  CLEAR_STATE,

  CANCEL_ACTIVE_TRIP_START,
  CANCEL_ACTIVE_TRIP_SUCCESS,
  CANCEL_ACTIVE_TRIP_FAILED,

  REMOVE_PRE_BOOKED_START,
  REMOVE_PRE_BOOKED_SUCCESS,
  REMOVE_PRE_BOOKED_FAILED,

  REFRESH_PRE_BOOKED_SUCCESS,
  REFRESH_ACTIVE_TRIPS_SUCCESS,
  REFRESH_COMPLETED_TRIPS_SUCCESS,

  ESTIMATE_FARE_SUCCESS,
  ESTIMATE_FARE_CLEAR
} from './actionType';

const initialState = {
  preBookedList: { data: null },
  preBookedLoading: true,
  preBookedPageNo: 1,
  preBookedMeta: null,
  removeLoading: false,

  activeTripList: { data: null },
  activeTripLoading: true,
  activeTripPageNo: 1,
  activeTripMeta: null,
  cancelLoading: false,

  completedTripList: { data: null },
  completedTripLoading: true,
  completedTripPageNo: 1,
  completedTripMeta: null,

  tripFilter: 'All',

  startTripLoading: false,
  errorStatusPostTrip: false,
  newTrip: null,
  errorPostTrip: null,
  carCategories: [],
  defaultCategory: null,
  fareData: null,
};

const trips = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_PRE_BOOKED:
      return { ...state, preBookedLoading: true, tripFilter: action.filter };
    case FETCH_PRE_BOOKED_SUCCESS:
      return {
        ...state,
        preBookedList: action.data,
        preBookedLoading: false,
        preBookedMeta: action.data.meta,
      };
    case FETCH_PRE_BOOKED_FAILED:
      return { ...state, preBookedLoading: false };
    case FETCHING_ACTIVE_TRIPS:
      return { ...state, activeTripLoading: true, tripFilter: action.filter };
    case FETCH_ACTIVE_TRIPS_SUCCESS:
      return {
        ...state,
        activeTripList: action.data,
        activeTripLoading: false,
        activeTripMeta: action.data.meta,
      };
    case FETCH_ACTIVE_TRIPS_FAILED:
      return { ...state, activeTripLoading: false };
    case FETCHING_COMPLETED_TRIPS:
      return { ...state, completedTripLoading: true, tripFilter: action.filter };
    case FETCH_COMPLETED_TRIPS_SUCCESS:
      return {
        ...state,
        completedTripList: action.data,
        completedTripLoading: false,
        completedTripMeta: action.data.meta,
      };
    case FETCH_COMPLETED_TRIPS_FAILED:
      return { ...state, completedTripLoading: false };
    case CREATING_NEW_TRIP:
      return { ...state, startTripLoading: true };
    case CREATING_NEW_TRIP_SUCCESS:
      return { ...state, startTripLoading: false, newTrip: action.newTrip };
    case CREATING_NEW_TRIP_FAILED:
      return {
        ...state,
        startTripLoading: false,
        errorPostTrip: action.errorPostTrip,
        errorStatusPostTrip: true,
      };
    case FETCHING_CAR_CATEGORIES_SUCCESS:
      return {
        ...state,
        carCategories: action.data,
        defaultCategory: action.defaultCategory
      };
    case FETCHING_CAR_CATEGORIES_FAILED:
      return {
        ...state,
        carCategories: [],
        defaultCategory: null
      };
    case CLEAR_STATE:
      return {
        ...state,
        newTrip: null,
        errorPostTrip: null,
        errorStatusPostTrip: false,
        startTripLoading: false
      };
    case REMOVE_PRE_BOOKED_START:
      return {
        ...state,
        removeLoading: true
      };
    case REMOVE_PRE_BOOKED_SUCCESS:
      const preBookedList = state.preBookedList;
      const items = preBookedList.data.filter((item)=>(item._id !== action.data._id));
      preBookedList.data = items;
      return {
        ...state,
        removeLoading: false,
        preBookedList
      };
    case REMOVE_PRE_BOOKED_FAILED:
      return {
        ...state,
        removeLoading: false
      };
    case CANCEL_ACTIVE_TRIP_START:
      return {
        ...state,
        cancelLoading: true
      };
    case CANCEL_ACTIVE_TRIP_SUCCESS:
      const activeTripList = state.activeTripList;
      const list = activeTripList.data.filter((item)=>(item._id !== action.data._id));
      activeTripList.data = list;
      return {
        ...state,
        cancelLoading: false,
        activeTripList
      };
    case CANCEL_ACTIVE_TRIP_FAILED:
      return {
        ...state,
        cancelLoading: false
      };
    case REFRESH_PRE_BOOKED_SUCCESS:
      return {
        ...state,
        preBookedList: action.data,
        preBookedtMeta: action.data.meta,
      };
    case REFRESH_ACTIVE_TRIPS_SUCCESS:
      return {
        ...state,
        activeTripList: action.data,
        activeTripMeta: action.data.meta,
      };
    case REFRESH_COMPLETED_TRIPS_SUCCESS:
      return {
        ...state,
        completedTripList: action.data,
        completedTripMeta: action.data.meta,
      };
    case ESTIMATE_FARE_SUCCESS:
      return { ...state, fareData: action.payload };
    case ESTIMATE_FARE_CLEAR:
      return { ...state, fareData: null };
    default:
      return state;
  }
};

export default trips;
