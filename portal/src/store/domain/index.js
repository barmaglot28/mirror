import { combineReducers } from 'redux';
import auth from './auth/reducer';
import trip from './trip/reducer';

export default combineReducers({
  auth,
  trip
});
