import {
  CLEAR_USER_DATA,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  SET_LOGGED_IN_STATUS,
} from './actionType';
import AuthService from '../../../services/auth';

export function clearUser() {
  return {
    type: CLEAR_USER_DATA,
  };
}

export function clearUserData() {
  return dispatch => {
    AuthService.unsetCookie()
    .then((response) => {
      if (response.success) {
        return dispatch(clearUser());
      }
      return dispatch(clearUser());
    })
    .catch(e => e);
  };
}

export function loginUserSuccess(data) {
  return {
    type: LOGIN_USER_SUCCESS,
    data,
  };
}

export function loginUserFailed(data) {
  return {
    type: LOGIN_USER_FAILED,
    data,
  };
}

export function setLoggedInStatus({ jwtAccessToken, loggedInStatus }) {
  return {
    type: SET_LOGGED_IN_STATUS,
    jwtAccessToken,
    loggedInStatus,
  };
}

export function loginUser(userCredentials) {
  return dispatch => AuthService.loginUser(userCredentials)
    .then((response) => {
      if (response.success) {
        AuthService.setCookie(response);
        dispatch(loginUserSuccess(response.data));
      }
      else {
        dispatch(loginUserFailed(response));
      }
    })
    .catch((e) => {
      dispatch(loginUserFailed(e));
    });
}

export function recovery(emailObj) {
  return dispatch => AuthService.recoveryAsync(emailObj)
  .then((response) => {
      return response;
  })
  .catch((e) => {
    console.log('error', e);
  });
}
