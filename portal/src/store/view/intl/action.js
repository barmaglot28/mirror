/* eslint-disable import/prefer-default-export */
import {
  SET_LOCALE_START,
  SET_LOCALE_SUCCESS,
  SET_LOCALE_ERROR,
} from './actionType';
import fetch from '../../../core/fetch';

export function setLocale({ locale }) {
  return async (dispatch) => {
    dispatch({
      type: SET_LOCALE_START,
      payload: {
        locale,
      },
    });

    try {
      const response = await fetch('/set-language', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          locale,
        }),
        credentials: 'same-origin',
      });
      await response.json()
      .then((data) => {
        const messages = data.intl.reduce((msgs, msg) => {
          msgs[msg.id] = msg.message; // eslint-disable-line no-param-reassign
          return msgs;
        }, {});
        dispatch({
          type: SET_LOCALE_SUCCESS,
          payload: {
            locale,
            messages,
          },
        });
      });

      // remember locale for every new request
      if (process.env.BROWSER) {
        const maxAge = 3650 * 24 * 3600; // 10 years in seconds
        document.cookie = `lang=${locale};path=/;max-age=${maxAge}`;
      }
    } catch (error) {
      dispatch({
        type: SET_LOCALE_ERROR,
        payload: {
          locale,
          error,
        },
      });
      return false;
    }

    return true;
  };
}

export function setLocaleSuccess(data) {
  return {
    type: SET_LOCALE_SUCCESS,
    data,
  };
}

export function setLocaleFailure(data) {
  return {
    type: SET_LOCALE_ERROR,
    data,
  };
}
