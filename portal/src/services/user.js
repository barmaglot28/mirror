import ApiService from './apiService';

async function getCurrent(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  // apiObject.body = userObj;
  apiObject.endpoint = 'api/users';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside get current user data', response);
  return response;
}
export default { getCurrent };
