import ApiService from './apiService';

async function postCorporateRider(token, riderObj) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.endpoint = `api/corporate/rider`;
  apiObject.body = {
    fname: riderObj.riderFirstName,
    lname: riderObj.riderLastName
  };
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default { postCorporateRider };
