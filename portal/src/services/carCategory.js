import ApiService from './apiService';

async function getCarCategories(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = 'api/carcategories';
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default { getCarCategories };
