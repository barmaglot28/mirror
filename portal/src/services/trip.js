import Moment from 'moment';
import ApiService from './apiService';
import bookingConst from '../constants/booking';

async function list(token, params) {
  if (!token) {
    return new Promise((resolve, reject) => {
      reject({
        message: 'Unauthorized',
      });
    });
  }
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/corporate/trip?pageNo=${params.pageNo}&filter=${params.filter}&limit=5&status=${params.status}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function post(token, params, tripObj, carCategory) {
  // TODO transform request body
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.endpoint = `api/corporate/rider/${params.riderId}/trip`;
  apiObject.body = {
    trip: {
      srcLoc: tripObj.srcLoc,
      destLoc: tripObj.destAddress ? tripObj.destLoc : null,
      pickUpAddress: tripObj.pickUpAddress,
      destAddress: tripObj.destAddress,
      latitudeDelta: tripObj.latitudeDelta,
      longitudeDelta: tripObj.longitudeDelta,
      taxiType: tripObj.taxiType,
      preBookedTime: tripObj.preBooking === bookingConst.later ? tripObj.dateTime : null,
      fareDetails: tripObj.fareDetails,
      tripAmt: tripObj.tripAmt,
    },
    carCategory: carCategory
  };
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function cancel(token, params) {
  // TODO transform request body
  const apiObject = {};
  apiObject.method = 'PUT';
  apiObject.authentication = token;
  apiObject.endpoint = `api/corporate/trip/${params.id}`;
  apiObject.body = { tripStatus: 'cancelled' };

  const response = await ApiService.callApi(apiObject);
  return response;
}

async function remove(token, params) {
  // TODO transform request body
  const apiObject = {};
  apiObject.method = 'DELETE';
  apiObject.authentication = token;
  apiObject.endpoint = `api/corporate/trip/${params.id}`;
  apiObject.body = {};

  const response = await ApiService.callApi(apiObject);
  return response;
}

async function exportCompleted(token, ranges) {
  const apiObject = {};
  apiObject.headers = {
    Accept: '*/*',
    'Content-Type': 'application/json',
  };
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.endpoint = 'api/corporate/trip/exportCompleted';
  apiObject.body = {
    startRange: ranges.startRange,
    endRange: ranges.endRange,
    zone: Moment().zone()
  };
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function estimateFare(token, origin, destination) {
  const apiObject = {};
  apiObject.headers = {
    Accept: '*/*',
    'Content-Type': 'application/json',
  };
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.endpoint = 'api/payment/fare/calculate';
  apiObject.body = {
    origin,
    destination
  };
  const response = await ApiService.callApi(apiObject);
  return response;
}


export default { list, post, cancel, remove, exportCompleted, estimateFare };
