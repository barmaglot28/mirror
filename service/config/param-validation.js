import Joi from 'joi';

export default {
  // POST /api/users/register
  createUser: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      phoneNo: Joi.string().required()
    }
  },

  // UPDATE /api/users
  updateUser: {
    body: {
      fname: Joi.string().required(),
      lname: Joi.string().required(),
      phoneNo: Joi.string().required()
    }
  },

  // PUT /api/users/changePass
  changePass: {
    body: {
      oldPass: Joi.string().required(),
      newPass: Joi.string().required()
    }
  },

  // PUT /api/users/feedback
  feedback: {
    body: {
      reason: Joi.string().required(),
      query: Joi.string().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required()
    }
  },

  // PUT /api/auth/setup
  setup: {
    body: {
      jwtAccessToken: Joi.string().required()
    }
  },

  // GET /api/admin/user
  userList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
      type: Joi.string().required(),
    }
  },

  // PUT /api/admin/user: userId

  updateUserByAdmin: {
    body: {
      _id: Joi.string().alphanum().required(),
      userType: Joi.string().valid('rider', 'driver', 'corporate').required()
    }
  },

  // DELETE /api/admin/user: userId

  removeUserByAdmin: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },


  // GET /api/admin/tripDetails
  // GET /api/corporate/trip
  tripList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
      status: Joi.any().valid('booked', 'active', 'completed').optional()
    }
  },

  // GET /api/admin/getTripsPerDay
  userTripsPerDay: {
    body: {
      time: Joi.string()
    }
  },

  tripPerDayRevenueGraph: {
    params: {
      date: Joi.string()
    }
  },

  // GET /api/admin/getTripsByUserPerDay
  userTripsByUserPerDay: {
    body: {
      time: Joi.string(),
      driverId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
    }
  },

  // GET /api/admin/tripDetails
  userTripList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
      filter: Joi.string(),
    }
  },

  userCouponCodeList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
      filter: Joi.string(),
    }
  },
  tripRevenueGraph: {
    params: {
      revenueYear: Joi.number().integer().min(2000),
    }
  },
  createNewTrip: {
    body: {
      riderId: Joi.string().regex(/^[0-9a-fA-F]{24}$/),
      driverId: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
    }
  },
  updateTripObject: {
    body: {
      riderId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      driverId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      pickUpAddress: Joi.string().required(),
      destAddress: Joi.string().required().allow(null).allow(''),
      paymentMode: Joi.string().required(),
      taxiType: Joi.string().required(),
      riderRatingByDriver: Joi.number().integer().required(),
      driverRatingByRider: Joi.number().integer().required(),
      tripStatus: Joi.string().required(),
      tripIssue: Joi.string().required(),
      tripAmt: Joi.number().integer().required(),
      seatBooked: Joi.number().integer().required(),
    }
  },
  createNewUser: {
    body: {
      fname: Joi.string().regex(/^[a-zA-Z]/).required(),
      lname: Joi.string().regex(/^[a-zA-Z]/).required(),
      userType: Joi.string().valid('rider', 'driver', 'corporate').required(),
      email: Joi.string().email().required(),
      password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
      phoneNo: Joi.number().required(),
    }
  },
  createNewCar: {
    body: {
      licensePlateNumber: Joi.string().regex(/[A-Z]{1,3}-[A-Z]{1,2}-[0-9]{1,4}/).required(),
      make: Joi.string().required(),
      model: Joi.string().required(),
      carCategory: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      usedBy: Joi.string().regex(/^[0-9a-fA-F]{24}$/).optional(),
      canBeUsed: Joi.boolean().required(),
    }
  },
  updateCar: {
    body: {
      licensePlateNumber: Joi.string().regex(/[A-Z]{1,3}-[A-Z]{1,2}-[0-9]{1,4}/).required(),
      make: Joi.string().required(),
      model: Joi.string().required(),
      carCategory: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
      usedBy: Joi.string().regex(/^[0-9a-fA-F]{24}$/).optional(),
      canBeUsed: Joi.boolean().required(),
    }
  },
  // GET /api/admin/availableBookings && /api/admin/acceptedBookings
  bookingsList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
      filter: Joi.string(),
    }
  },

  removeBookingByAdmin: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },

  // GET /api/admin/cars
  carList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
    }
  },

  // remove car
  removeCar: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },

  // UPDATE /api/carcategories
  updateCarcategories: {
    body: {
      name: Joi.string().required(),
      isMetered: Joi.boolean().required(),
      pricePerKilometer: Joi.string(),
      pricePerMinute: Joi.string(),
      baseFare: Joi.string(),
      minimumFare: Joi.string(),
      commissionToCabdo: Joi.string(),
      capacity: Joi.string(),
    }
  },

  // GET /api/admin/carcategories
  carcategoriesList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
    }
  },

  // PUT /api/admin/carcategories: carcategoriesId
  updateCarcategoriesByAdmin: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },

  createNewCarcategories: {
    body: {
      name: Joi.string().required(),
      isMetered: Joi.boolean().required(),
      pricePerKilometer: Joi.string(),
      pricePerMinute: Joi.string(),
      baseFare: Joi.string(),
      minimumFare: Joi.string(),
      commissionToCabdo: Joi.string(),
      capacity: Joi.string(),
    }
  },

  removeCarcategoriesByAdmin: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },

// UPDATE /api/couponcodes
  updateCouponcodes: {
    body: {
      code: Joi.string(),
      discount: Joi.number().integer().min(1),
    }
  },

  // GET /api/admin/couponcodes
  couponcodesList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
    }
  },

  // PUT /api/admin/couponcodes: couponcodesId
  updateCouponcodesByAdmin: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },

  createNewCouponcodes: {
    body: {
      code: Joi.string(),
      discount: Joi.number().integer().min(1),
    }
  },

  removeCouponcodesByAdmin: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },


  // GET /api/admin/referralcodes
  referralcodesList: {
    query: {
      limit: Joi.number().integer().min(1),
      pageNo: Joi.number().integer().min(1),
    }
  },


  createNewReferralcodes: {
    body: {
      userIdTo: Joi.string(),
      userIdFrom: Joi.string(),
      code: Joi.string(),
      countMax: Joi.number().integer().min(1),
    }
  },

  removeReferralcodesByAdmin: {
    body: {
      _id: Joi.string().alphanum().required()
    }
  },

  // POST /api/corporate/rider
  createCorporateRider: {
    body: {
      // nothing required, all data will be taken from Corporate user
    }
  },

  // POST /api/corporate/rider/:id/trip
  createCorporateTrip: {
    body: {
      trip: {
        srcLoc: Joi.array().items(Joi.number()).length(2).required()
          .label('Pickup coordinates').options({
            language: {
              array: { base: 'are required' }
            }
          }),
        destLoc: Joi.array().items(Joi.number()).length(2).required().allow(null)
          .label('Destination coordinates').options({
            language: {
              array: { base: 'are required' }
            }
          }),
        pickUpAddress: Joi.string().required()
          .label('Pickup address').options({
            language: {
              string: { base: 'is required' }
            }
          }),
        destAddress: Joi.string().required().allow(null).allow('')
          .label('Destination address').options({
            language: {
              string: { base: 'is required' }
            }
          }),
        latitudeDelta: Joi.number().allow(null),
        longitudeDelta: Joi.number().allow(null),
        taxiType: Joi.string().required()
          .label('Taxi type').options({
            language: {
              string: { base: 'is required' }
            }
          }),
        preBookedTime: Joi.date().iso().allow(null).required(),
        tripAmt: Joi.number().allow(null).required()
      },
      carCategory: Joi.object().required()
        .label('Car Category').options({
          language: {
            string: { base: 'is required' }
          }
        })
    }
  },

  // PUT /api/corporate/trip/:id
  updateCorporateTrip: {
    body: {
      tripStatus: Joi.string().valid('cancelled').required(),
    }
  },

  // POST api/trip/user/:userId/exportCompleted
  // POST api/corporate/trip/exportCompleted
  exportCompletedTrips: {
    body: {
      startRange: Joi.date().iso().allow(null).required(),
      endRange: Joi.date().iso().allow(null).required(),
      zone: Joi.number().allow(null).required()
    }
  }
};
