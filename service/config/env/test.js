export default {
  env: 'test',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://mongo:27017/cabdo-test',
  port: 4123,
  passportOptions: {
    session: false
  },
  radius: 20,
  maxDistance: 20 / 111.12,
  maxBookingDistance: 20 / 111.12,
  driverResponseTime: 20,
  arrivedDistance: 200,
  arrivingDistance: 1000,
  limit: 10,
  skip: 0,
  tripFilter: 'All',
  stripeSecretKey: 'sk_test_HBVUVwrO3sRWbb4ww7upfHbN',
  googleAPIkey: 'AIzaSyA-rEfh3RHxj4utD1nd8sfu8bXpv8cCOho',
  twilio_SID: 'AC1ab0205c0efe67b0b5f3d3dd86060c99',
  twilio_token: 'af7d3c7a806ffe0d7a97ac0ce289faa9',
  sendgridAPIkey: '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'
};
