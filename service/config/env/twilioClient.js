import Twilio from 'twilio';
import config from '../env';

let twilioClient;
twilioClient = new Twilio(
  config.twilio_SID,
  config.twilio_token
);

export default twilioClient;
