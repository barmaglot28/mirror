import sockeHandler from '../server/socketHandler';
import jwt from 'jsonwebtoken';
import config from './env';
import SocketStore from '../server/service/socket-store';

function startSocketServer(server) {
  const io = require('socket.io').listen(server);

  io.on('connection', (socket) => {
    if (!socket.handshake.query.token) {
      socket.disconnect();
      return;
    }

    const authToken = socket.handshake.query.token.replace('JWT ', '');     // check for authentication of the socket
    jwt.verify(authToken, config.jwtSecret, (err, userDtls) => {
      if (err) {
        socket.disconnect();
      } else if (userDtls) {
        socket.userId = userDtls._doc._id;
        socket.userType = userDtls._doc.userType;
        SocketStore.addByUserId(socket.userId, socket.userType, socket);
        sockeHandler(socket);         // call socketHandler to handle different socket scenario
      }
    });
  });
}

export default { startSocketServer };
