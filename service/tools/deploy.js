import GitRepo from 'git-repository';
import fs from 'fs';

const remote = {
  name: 'origin',
  url: 'git@gitlab.com:cabdo/service-deployment.git',
  branch: 'master',
};
/**
 * Deploy the contents of the `/dist` folder to a git repository
 */
async function deployToGitHubPages() {
  if (!fs.existsSync('dist')) {
    fs.mkdirSync('dist');
  }
  const repo = await GitRepo.open('dist', { init: true });
  await repo.setRemote(remote.name, remote.url);
  const isRefExists = await repo.hasRef(remote.url, remote.branch);
  if (isRefExists) {
    await repo.fetch(remote.name);
    await repo.reset(`${remote.name}/${remote.branch}`, { hard: true });
    await repo.clean({ force: true });
  }

  require('child_process').execSync('npm run build', { stdio: [0, 1, 2] });

  await repo.add('--all .');
  await repo.commit(`Update ${new Date().toISOString()}`);
  await repo.push(remote.name, `master:${remote.branch}`);
}

export default { default: deployToGitHubPages };
