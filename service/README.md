## React Native Taxi App Theme - Backend v4.0.0

Thanks for purchasing the React Native Taxi App Theme.

Follow the documentation to install and get started with the development:

-   [Documentation](http://strapmobile.com/docs/react-native-uber-like-app-backend/master)
-   [Product Page](http://strapmobile.com/react-native-uber-like-app-backend/)

Happy coding!
