import Promise from 'bluebird';
import mongoose from 'mongoose';
import config from './config/env';
import app from './config/express';
import socketServer from './config/socket-server';
import { users, testAdmin, corporateUsers } from './server/dev-users';
import { admin as liveAdmin } from './server/live-users';
import { carCategories } from './server/live-categories';
import { referralAmount } from './server/live-refamount';
import { create as createUser } from './server/controllers/user';
import { createNewCarcategories as createCarCategory } from './server/controllers/admin-carcategories';
import { saveReferralAmt as createReferralAmt } from './server/controllers/admin-referralcodes';
import cronJobs from './server/cron-jobs';

// promisify mongoose
Promise.promisifyAll(mongoose);
mongoose.Promise = Promise;

// connect to mongo db
mongoose.connect(config.db, { server: { socketOptions: { keepAlive: 1 } } }, () => {
  const noop = () => {
  };
  if (config.env === 'test') {
    mongoose.connection.db.dropDatabase().then(() => {
      console.log(`Creating admin ${testAdmin.email} of type ${testAdmin.userType} with password ${testAdmin.password}`);
      createUser({ body: { ...testAdmin, requireSms: false } }, { send: noop }, noop);
    });
  } else {
    if (config.env === 'development') {
      users.forEach(user => {
        console.log(`Creating user ${user.email} of type ${user.userType} with password ${user.password}`);
        createUser({ body: { ...user, requireSms: false } }, { send: noop }, noop);
      });
      corporateUsers.forEach(user => {
        console.log(`Creating user ${user.email} of type ${user.userType} with password ${user.password}`);
        createUser({ body: { ...user, requireSms: false } }, { send: noop }, noop);
      });
    }
    createUser({ body: { ...liveAdmin, requireSms: false } }, { send: noop }, noop);
    carCategories.forEach(category => {
      createCarCategory({ body: category }, { send: noop }, noop);
    });
    createReferralAmt({ body: referralAmount }, { send: noop }, noop);
  }

  cronJobs();
});
mongoose.connection.on('error', () => {
  throw new Error(`unable to connect to database: ${config.db}`);
});

socketServer.startSocketServer(app);

// listen on port config.port
app.listen(config.port, () => {
  console.log(`Server started on port ${config.port} (${config.env})`);
});

export default app;
