import Car from '../models/car';
import User from '../models/user';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import SendNotification from '../service/pushExpo';
import SocketStore from '../service/socket-store.js';
import I18n from '../i18n/i18n';

/**
 * Get car list
 * @returns {Cars}
 */
function all(req, res, next) {
  const params = { skip: req.params.skip || 0, count: req.params.count || 1000 };
  Car.list(params)
    .then((cars) => {
      return res.send({ success: true, message: 'cars found', data: cars });
    }).error((e) => next(e));
}

/**
 * Load car.
 */
function load(req, res, next) {
  Car.get(req.params.carId)
    .then((car) => {
      return res.send({ success: true, message: 'car loaded', data: car.toObject() });
    }).error((e) => next(e));
}

/**
 * Lock cars.
 */
function lock(req, res, next) {
  Car.findOneAsync({ _id: req.params.carId })
    .then(carDoc => {
      if (carDoc) {
        const oldUserId = carDoc.usedBy;
        carDoc.usedBy = req.user._id;
        Promise.all([
          carDoc.saveAsync(),
          Car.findOneAndUpdate({ usedBy: req.user._id }, { $set: { usedBy: null } }),
          User.findOneAndUpdateAsync({ _id: req.user._id }, { $set: { carDetails: carDoc } }, { new: true }),
          User.findOneAndUpdateAsync({ _id: oldUserId }, { $set: { carDetails: {} } }, { new: true })
        ])
          .then(([updatedCar]) => {
            if (oldUserId) {
              SocketStore.emitByUserId(oldUserId, 'carTakenByOther');
              SendNotification(oldUserId, I18n.t('carTakenByOther', { driver: req.user.fname }), 'carTakenByOther');
            }
            return res.send({ success: true, message: 'car locked', data: updatedCar });
          })
          .catch((e) => {
            const err = new APIError(`error in updating locking a car ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
      } else {
        const err = new APIError('No such car exists!', httpStatus.NOT_FOUND);
        next(err);
      }
    });
}

/**
 * Unlock car.
 */
function unlock(req, res, next) {
  Car.findOneAsync({ _id: req.params.carId, usedBy: req.user._id })
    .then((carDoc) => {
      if (carDoc) {
        if (carDoc.usedBy) {
          carDoc.usedBy = undefined;
          Promise.all([
            carDoc.saveAsync(),
            User.findOneAndUpdateAsync({ _id: req.user._id }, { $set: { carDetails: {} } }, { new: true })
          ])
            .then(([updatedCar]) => {
              return res.send({ success: true, message: 'car unlocked', data: updatedCar.toObject() });
            })
            .catch((e) => {
              const err = new APIError(`error in updating locking a car ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
              next(err);
            });
        } else {
          return res.send({ success: false, message: 'car not locked', data: carDoc.toObject() });
        }
      } else {
        const err = new APIError('No such car exists!', httpStatus.NOT_FOUND);
        next(err);
      }
    });
}

export default { load, all, lock, unlock };
