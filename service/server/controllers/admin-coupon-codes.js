import CouponCodeSchema from '../models/coupon-codes';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-carcategories');

function getAllCouponcodes(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);
  CouponCodeSchema.countAsync()
    .then((totalCouponcodesRecord) => {
      const returnObj = {
        success: true,
        message: 'no of coupon codes are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalCouponcodesRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (totalCouponcodesRecord < 1) {
        return res.send(returnObj);
      }
      if (skip > totalCouponcodesRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      CouponCodeSchema.list({ skip, limit })
        .then((couponcodesData) => {
          returnObj.data = transformReturnObj(couponcodesData);
          returnObj.message = 'coupon codes found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting couponcodes data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllCouponcodes when getting couponcodes data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of couponcodes ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllCouponcodes records`);
      next(err);
    });
}

function getCouponcodesDetails(req, res, next) {
  const couponcodesId = req.params.couponcodesId;
  const returnObj = {
    success: false,
    message: 'couponcodes Id is not defined',
    data: null,
  };
  if (couponcodesId) {
    CouponCodeSchema.findByIdAsync(couponcodesId)
      .then((couponcodesData) => {
        if (couponcodesData) {
          returnObj.success = true;
          returnObj.message = 'couponcodes found and its corresponding details';
          returnObj.data = couponcodesData;
        } else {
          returnObj.success = false;
          returnObj.message = 'couponcodes not found with the given id';
          returnObj.data = null;
        }
        res.send(returnObj);
      })
      .error((e) => {
        const err = new APIError(`Error occured while finding the couponcodes details ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        next(err);
      });
  } else {
    res.send(returnObj);
  }
}

function updateCouponcodesDetails(req, res, next) {
  const couponcodesId = req.body._id;
  const updateCouponcodesObj = Object.assign({}, req.body);

  CouponCodeSchema.findOneAsync({ _id: couponcodesId })
    .then((couponcodesDoc) => {
      if (couponcodesDoc) {
        couponcodesDoc.userId = updateCouponcodesObj.userId ? updateCouponcodesObj.userId : couponcodesDoc.userId;
        couponcodesDoc.name = updateCouponcodesObj.name ? updateCouponcodesObj.name : couponcodesDoc.name;
        couponcodesDoc.countMax = updateCouponcodesObj.countMax ? updateCouponcodesObj.countMax : couponcodesDoc.countMax;
        couponcodesDoc.discount = updateCouponcodesObj.discount ? updateCouponcodesObj.discount : couponcodesDoc.discount;
        const returnObj = {
          success: false,
          message: 'unable to find the object',
          data: null,
          meta: null
        };
        couponcodesDoc.saveAsync()
        .then((savedDoc) => {
          returnObj.success = true;
          returnObj.message = 'couponcodes document saved';
          returnObj.data = savedDoc;
          res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `Error occured while updating the couponcodes details ${e}`,
             httpStatus.INTERNAL_SERVER_ERROR
           );
          next(err);
        });
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while searching for the couponcodes ${e}`,
         httpStatus.INTERNAL_SERVER_ERROR
       );
      next(err);
    });
}

function removeCouponcodes(req, res, next) {
  const couponcodesId = req.body._id;
  CouponCodeSchema.findOneAsync({ _id: couponcodesId })
    .then((couponcodesDoc) => {
      if (couponcodesDoc) {
        const returnObj = {
          success: false,
          message: 'unable to find the object',
          data: null,
          meta: null
        };

        couponcodesDoc.removeAsync()
          .then((removedDoc) => {
            returnObj.success = true;
            returnObj.message = 'couponcodes document removed';
            returnObj.data = removedDoc;
            res.send(returnObj);
          })
          .error((e) => {
            const err = new APIError(
              `Error occured while removing the couponcodes details ${e}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            next(err);
          });
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while searching for the couponcodes ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

function transformReturnObj(couponcodesData) {
  for (let i = 0; i < couponcodesData.length; i++) {
    if (couponcodesData[i].couponDetails) {
      delete couponcodesData[i].couponDetails;
    }
  }
  return couponcodesData;
}

function createNewCouponcodes(req, res, next) {
  const couponcodesData = Object.assign({}, req.body);
  CouponCodeSchema.findOneAsync({ code: couponcodesData.code })
  .then(() => {
    const returnObj = {
      success: false,
      message: '',
      data: null,
    };
    const couponcodesObj = new CouponCodeSchema(couponcodesData);
    couponcodesObj.saveAsync()
    .then((savedCouponcodes) => {
      returnObj.success = true;
      returnObj.message = 'couponcodes generated successfully';
      returnObj.data = savedCouponcodes;
      return res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(`Error while generating new couponcodes ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    });
  })
  .error((e) => {
    const err = new APIError(`Error while Searching the couponcodes ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
    return next(err);
  });
}


export default { getCouponcodesDetails, getAllCouponcodes, createNewCouponcodes, updateCouponcodesDetails, removeCouponcodes };
