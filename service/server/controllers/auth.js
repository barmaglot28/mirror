import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import UserSchema from '../models/user.js';
import config from '../../config/env';
import I18n from '../i18n/i18n';
import Wallet from '../models/wallet';

/**
 * Returns jwt token  and user object if valid email and password is provided
 * @param req (email, password, userType)
 * @param res
 * @param next
 * @returns {jwtAccessToken, user}
 */
function login(req, res, next) {
  const userObj = {
    email: req.body.email.toLowerCase()
  };
  let updatedUser = null;
  UserSchema.findOneAsync(userObj, '+password')
    .then((user) => {
      if (!user) {
        const err = new APIError('User not found with the given email id', httpStatus.NOT_FOUND);
        return next(err);
      }
      user.comparePassword(req.body.password, (passwordError, isMatch) => {
        if (passwordError || !isMatch) {
          const err = new APIError('Incorrect password', httpStatus.UNAUTHORIZED);
          return next(err);
        }
        user.loginStatus = true;
        const token = jwt.sign(user, config.jwtSecret);
        UserSchema.findOneAndUpdate({ _id: user._id }, { $set: { loginStatus: true } }, { new: true })
          .populate('avatar')
          .execAsync()
          .then(async (resultUser) => {
            const restoredWallet = await restoreWallet(resultUser);
            updatedUser = (resultUser).toObject();
            updatedUser.avatar = resultUser.avatar && resultUser.avatar.base64 ? new Buffer(resultUser.avatar.base64, 'base64').toString('base64') : null;
            updatedUser.gpsLoc = [resultUser.gpsLoc[1], resultUser.gpsLoc[0]];
            const returnObj = {
              success: true,
              message: 'user successfully logged in',
              data: {
                jwtAccessToken: `JWT ${token}`,
                user: updatedUser,
                maxDistance: config.maxDistance,
                restoredWallet: restoredWallet.success
              }
            };
            I18n.locale = updatedUser.language;
            res.json(returnObj);
          })
          .error((err123) => {
            const err = new APIError(`error in updating user details while login ${err123}`, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
      });
    })
    .error((e) => {
      const err = new APIError(`error while finding user ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      next(err);
    });
}

function setup(req, res, next) {
  const jwtAccessToken = req.body.jwtAccessToken.slice(4);
  const decoded = jwt.decode(jwtAccessToken, config.jwtSecret);
  const user = decoded._doc;
  user.loginStatus = true;
  UserSchema.findOneAndUpdate({ _id: user._id }, { $set: { loginStatus: true } }, { new: true })
    .populate('avatar')
    .execAsync()
    .then(async (resultUser) => {
      if (!resultUser) {
        res.status(404);
        res.json({
          success: false,
          message: 'user not found'
        });
        return;
      }
      const restoredWallet = await restoreWallet(resultUser);
      const updatedUser = (resultUser).toObject();
      updatedUser.avatar = resultUser.avatar && resultUser.avatar.base64 ? new Buffer(resultUser.avatar.base64, 'base64').toString('base64') : null;
      updatedUser.gpsLoc = [resultUser.gpsLoc[1], resultUser.gpsLoc[0]];

      I18n.locale = updatedUser.language;
      res.json({
        success: true,
        message: 'user successfully setup',
        data: {
          jwtAccessToken: `JWT ${jwtAccessToken}`,
          user: updatedUser,
          maxDistance: config.maxDistance,
          restoredWallet: restoredWallet.success
        }
      });
    })
    .error((err123) => {
      const err = new APIError(`error in updating user details while setup ${err123}`, httpStatus.INTERNAL_SERVER_ERROR);
      next(err);
    });
}

/**
 * function restore wallet only need to restore deleted wallet, (fixed bug with deleted wallet)
 */
function restoreWallet(user) {
  const returnObj = {
    success: true,
    message: 'Successfully created',
  };
  return new Promise((resolve) => {
    Wallet.findOneAsync({ userEmail: user.email }).then(async (wallet) => {
      if (!wallet) {
        const walletObj = new Wallet({
          walletBalance: 0,
          userId: user._id,
          userType: user.userType,
          userEmail: user.email,
        });
        await walletObj.saveAsync();
        resolve(returnObj);
      } else {
        returnObj.success = false;
        returnObj.message = 'wallet exist';
        resolve(returnObj);
      }
    });
  });
}

/** This is a protected route. Change login status to false and send success message.
* @param req
* @param res
* @param next
* @returns success message
*/

function logout(req, res, next) {
  const userObj = req.user;

  if (userObj === undefined || userObj === null) {
    res.sendStatus(400);
    return;
  }
  UserSchema.findOneAndUpdate({ _id: userObj._id, loginStatus: true }, { $set: { loginStatus: false } }, { new: true }, (err, userDoc) => {
    if (err) {
      const error = new APIError('error while updating login status', httpStatus.INTERNAL_SERVER_ERROR);
      return next(error);
    }

    if (!userDoc) {
      const error = new APIError('user not found', httpStatus.NOT_FOUND);
      next(error);
    }

    res.json({
      success: true,
      message: 'user logout successfully'
    });
  });
}

export default { login, logout, setup };
