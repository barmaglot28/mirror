import Moment from 'moment';
import UserSchema from '../models/user';
import TripSchema from '../models/trip';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import { transformReturnObj } from '../service/transform-return-object';
import Utils from '../helpers/utils';

const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-trip-user');

function userTripDetails(req, res, next) {
  const userId = req.params.userId;
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo ? req.query.pageNo : 1;
  const skip = pageNo ? (pageNo - 1) * limit : config.skip;

  UserSchema.findByIdAsync(userId)
    .then((userObject) => {
      const returnObj = {
        success: false,
        message: 'user not found with the given id',
        data: [],
        meta: {
          totalNoOfPages: null,
          limit,
          currPageNo: pageNo,
          totalRecords: null
        }
      };
      if (userObject === null) {
        return res.send(returnObj);
      }

      const filter = {};
      switch (userObject.userType) {
        case 'driver':
          filter.driverId = userObject._id;
          break;
        case 'rider':
          filter.riderId = userObject._id;
          break;
        case 'corporate':
          filter.corporateUserId = userObject._id;
          break;
        default:
      }

      TripSchema.countAsync(filter)
        .then((totalUserTripRecords) => {
          returnObj.meta.totalNoOfPages = Math.ceil(totalUserTripRecords / limit);
          returnObj.meta.totalRecords = totalUserTripRecords;

          if (totalUserTripRecords < 1) {
            returnObj.success = true;
            returnObj.message = 'user has zero trip records';
            return res.send(returnObj);
          }
          if (skip > totalUserTripRecords) {
            const err = new APIError('Request Page No does not exists', httpStatus.NOT_FOUND);
            return next(err);
          }

          TripSchema.find(filter)
            .sort({ _id: -1 })
            .skip(skip || 0)
            .limit(limit || 20)
            .populate('riderId', 'fname lname')
            .populate('driverId', 'fname lname')
            .populate('corporateUserId', 'fname lname')
            .select('-__v')
            .execAsync()
            .then((userTripData) => {
              for (let i = 0; i < userTripData.length; i++) {
                userTripData[i] = transformReturnObj(userTripData[i]);
              }
              returnObj.success = true;
              returnObj.message = 'user trip records';
              returnObj.data = userTripData;
              res.send(returnObj);
            })
            .error((e) => {
              const err = new APIError(
                `Error occured while fetching user trip records ${e}`,
                httpStatus.INTERNAL_SERVER_ERROR
              );
              next(err);
            });
        })
        .error((e) => {
          const err = new APIError(
            `Error occured counting user trip records ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `Error occured searching for user object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

function userTripRequestStatics(req, res, next) {
  const userId = req.params.userId;
  debug(`user id ${userId}`);
  debug(`limit value ${req.query.limit}`);
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  UserSchema.findByIdAsync(userId)
    .then((userObject) => {
      const returnObj = {
        success: false,
        message: 'user not found with the given id',
        data: null,
        meta: {
          totalNoOfPages: null,
          limit,
          currPageNo: pageNo,
          totalRecords: null
        }
      };
      if (userObject === null || userObject === undefined) {
        return res.send(returnObj);
      }
      const userType = userObject.userType;
      let searchObj = {};
      let groupBy = null;
      if (userType === 'rider') {
        searchObj = {};
        groupBy = 'riderId';
        searchObj.riderId = userObject._id;
      }
      if (userType === 'driver') {
        groupBy = 'driverId';
        searchObj = {};
        searchObj.driverId = userObject._id;
      }
      TripSchema.aggregateAsync([
        { $match: searchObj },
        { $group: {
          _id: `$${groupBy}`,
          completed: { $sum: { $cond: [{ $eq: ['$tripRequestStatus', 'completed'] }, 1, 0] } },
          inQueue: { $sum: { $cond: [
                    { $anyElementTrue: { $map: {
                      input: ['enRoute', 'arriving', 'arrived', 'request'],
                      as: 'status',
                      in: { $eq: ['$$status', '$tripRequestStatus'] }
                    } } },
            1,
            0
          ] } },
          cancelled: { $sum: { $cond: [{ $or: [{ $eq: ['$tripRequestStatus', 'cancelled'] }, { $eq: ['$tripRequestStatus', 'rejected'] }] }, 1, 0] } },
          totalRequest: { $sum: 1 },
        } },
      ])
      .then((chartStats) => {
        returnObj.success = true;
        returnObj.message = 'user trip request statistic';
        returnObj.data = chartStats;
        res.send(returnObj);
      })
        .error((e) => {
          const err = new APIError(
            `Error occured while grouping the _id ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `Error occured searching for user object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

function exportUserCompletedTrips(req, res, next) {
  const userId = req.params.userId;

  UserSchema.findByIdAsync(userId)
    .then((userObject) => {
      const returnObj = {
        success: false,
        message: 'user not found with the given id',
        data: null,
        meta: null
      };
      if (userObject === null) {
        return res.send(returnObj);
      }

      const filter = {
        tripStatus: 'endTrip',
        endTime: { $ne: null }
      };
      switch (userObject.userType) {
        case 'driver':
          filter.driverId = userObject._id;
          break;
        case 'rider':
          filter.riderId = userObject._id;
          break;
        case 'corporate':
          filter.corporateUserId = userObject._id;
          break;
        default:
      }

      if (req.body.startRange) {
        filter.endTime.$gte = req.body.startRange;
      }
      if (req.body.endRange) {
        filter.endTime.$lte = req.body.endRange;
      }

      TripSchema.find(filter)
        .sort({ _id: -1 })
        .populate('riderId', 'fname lname')
        .populate('driverId', 'fname lname')
        .populate('corporateUserId', 'fname lname')
        .select('-__v')
        .execAsync()
        .then((userTripData) => {
          const output = [];
          userTripData.forEach(trip => {
            output.push({
              id: trip._id,
              bookingTime: Moment.utc(trip.bookingTime).zone(req.body.zone === null ? 0 : req.body.zone).toString(),
              endTime: Moment.utc(trip.endTime).zone(req.body.zone === null ? 0 : req.body.zone).toString(),
              tripAmount: trip.tripAmt,
              paymentMode: trip.paymentMode,
              pickUpAddress: trip.pickUpAddress,
              destAddress: trip.destAddress,
              corporate: Utils.getName(trip.corporateUserId),
              driver: Utils.getName(trip.driverId),
              rider: Utils.getName(trip.riderId)
            });
          });
          if (output.length === 0) {
            output.push({ result: `there are no completed trips for ${userObject.userType} ${Utils.getName(userObject)}` });
          }
          res.xls('completed-trips.xlsx', output);
        })
        .error((e) => {
          const err = new APIError(
            `Error occured while fetching user trip records ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `Error occured searching for user object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

export default { userTripDetails, userTripRequestStatics, exportUserCompletedTrips };
