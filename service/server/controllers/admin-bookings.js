import TripSchema from '../models/trip';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-bookings');

function getAllAvailableBookings(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);
  TripSchema.find({
    driverId: null,
    tripStatus: 'booking',
    preBookedTime: {
      $gte: new Date()
    },
  }).countAsync()
    .then((totalAvailableBookingsRecord) => {
      const returnObj = {
        success: true,
        message: 'no of available bookings are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalAvailableBookingsRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (totalAvailableBookingsRecord < 1) {
        return res.send(returnObj);
      }
      if (skip > totalAvailableBookingsRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      TripSchema.find({
        driverId: null,
        tripStatus: 'booking',
        preBookedTime: {
          $gte: new Date()
        },
      }).populate('riderId driverId')
        .skip(skip).limit(limit)
        .then((availableBookingsData) => {
          for (let i = 0; i < availableBookingsData.length; i++) {
            availableBookingsData[i] = transformReturnObj(availableBookingsData[i]);
          }
          returnObj.data = availableBookingsData;
          returnObj.message = 'available bookings found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting available bookings data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllAvailableBookings when getting available bookings data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of available bookings ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllAvailableBookings records`);
      next(err);
    });
}

function getAllAcceptedBookings(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);
  TripSchema.find({
    driverId: { $ne: null },
    tripStatus: 'booked',
    preBookedTime: {
      $gte: new Date()
    },
  }).countAsync()
    .then((totalAcceptedBookingsRecord) => {
      const returnObj = {
        success: true,
        message: 'no of accepted bookings are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalAcceptedBookingsRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (totalAcceptedBookingsRecord < 1) {
        return res.send(returnObj);
      }
      if (skip > totalAcceptedBookingsRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      TripSchema.find({
        driverId: { $ne: null },
        tripStatus: 'booked',
        preBookedTime: {
          $gte: new Date()
        },
      }).populate('riderId driverId')
        .skip(skip).limit(limit)
        .then((acceptedBookingsData) => {
          for (let i = 0; i < acceptedBookingsData.length; i++) {
            acceptedBookingsData[i] = transformReturnObj(acceptedBookingsData[i]);
          }
          returnObj.data = acceptedBookingsData;
          returnObj.message = 'accepted bookings found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting accepted bookings data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllAcceptedBookings when getting accepted bookings data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of accepted bookings ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllAcceptedBookings records`);
      next(err);
    });
}

function unpinAcceptedBooking(req, res, next) {
  const bookingId = req.body.bookingId;
  TripSchema.findOneAndUpdateAsync({ _id: bookingId }, { $set: { loginStatus: true } })
    .then((bookingDoc) => {
      if (bookingDoc) {
        const returnObj = {
          success: false,
          message: 'unable to find the object',
          data: null,
          meta: null
        };

        bookingDoc.removeAsync()
          .then((removedDoc) => {
            returnObj.success = true;
            returnObj.message = 'prebooking document removed';
            returnObj.data = removedDoc;
            res.send(returnObj);
          })
          .error((e) => {
            const err = new APIError(
              `Error occured while removing the prebooking ${e}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            next(err);
          });
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while searching for the prebooking ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

function transformReturnObj(tripData) {
  if (tripData instanceof Object) {
    tripData = tripData.toObject();
    if (tripData.riderId) {
      tripData.rider = tripData.riderId;
      tripData.riderId = tripData.rider._id ? tripData.rider._id : null;
    }
    if (tripData.driverId) {
      tripData.driver = tripData.driverId;
      tripData.driverId = tripData.driver._id ? tripData.driver._id : null;
    }
  }
  return tripData;
}

function getAllCancelledBookings(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;

  const now = new Date();
  const deletedTimeFrom = new Date(now.setDate(now.getDate() - 7));

  debug(`skip value: ${req.query.pageNo}`);
  TripSchema.find({
    tripStatus: 'deleted',
    deletedTime: { $gte: deletedTimeFrom }
  }).countAsync()
    .then((totalCancelledBookingsRecord) => {
      const returnObj = {
        success: true,
        message: 'no of cancelled bookings are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalCancelledBookingsRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (totalCancelledBookingsRecord < 1) {
        return res.send(returnObj);
      }
      if (skip > totalCancelledBookingsRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      TripSchema.find({
        tripStatus: 'deleted',
        deletedTime: { $gte: deletedTimeFrom }
      }).populate('riderId driverId')
        .skip(skip).limit(limit)
        .then((cancelledBookingsData) => {
          for (let i = 0; i < cancelledBookingsData.length; i++) {
            cancelledBookingsData[i] = transformReturnObj(cancelledBookingsData[i]);
          }
          returnObj.data = cancelledBookingsData;
          returnObj.message = 'cancelled bookings found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting cancelled bookings data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllCancelledBookings when getting cancelled bookings data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of cancelled bookings ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllCancelledBookings records`);
      next(err);
    });
}

export default { getAllAvailableBookings, getAllAcceptedBookings, unpinAcceptedBooking, getAllCancelledBookings };
