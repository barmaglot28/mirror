import Transaction from '../models/transaction';
import CarCategory from '../models/car-category';
import Wallet from '../models/wallet';
import cfg from '../../config/env';
import request from 'request';
import config from '../../config/env';

const stripe = require('stripe')(cfg.stripeSecretKey);

function getBalance(req, res) {
  Wallet.findOneAsync({ userEmail: req.body.email })
    .then((foundWallet) => {
      if (foundWallet !== null) {
        const returnObj = {
          success: true,
          message: 'Wallet Present for this account',
          data: foundWallet
        };
        res.send(returnObj);
      } else {
        const returnObj = {
          success: false,
          message: 'No wallet Present for this account',
          data: {}
        };
        res.send(returnObj);
      }
    });
}

export function payAll(tripObj) {
  Wallet.findOneAndUpdateAsync({ userEmail: tripObj.rider.email }, { $inc: { walletBalance: - Number(tripObj.tripAmt) * 100 } })
    .then((updateWalletObj) => {
      if (updateWalletObj) {
        // transaction insert
        const transactionOwner = new Transaction({
          userIdFrom: tripObj.riderId,
          tripId: tripObj._id,
          amount: Number(tripObj.tripAmt) * 20,  // couz value is in cents
          walletIdFrom: tripObj.rider.email,
        });
        transactionOwner.saveAsync()
          .then((transactionRider) => {
            const returnObj = {
              success: true,
              message: '',
              data: {}
            };
            returnObj.data.user = transactionRider;
            returnObj.message = 'transaction created successfully wallet was present';
            console.log('return object', returnObj);
          });
        Wallet.findOneAndUpdateAsync({ userEmail: tripObj.driver.email }, { $inc: { walletBalance: Number(tripObj.tripAmt) * 80 } })
          .then((WalletObjDriver) => {
            console.log('wallet object driver', WalletObjDriver);
            const transactionDriver = new Transaction({
              userIdTo: tripObj.driverId,
              userIdFrom: tripObj.riderId,
              amount: Number(tripObj.tripAmt) * 80,
              tripId: tripObj._id,
              walletIdFrom: tripObj.rider.email,
              walletIdTo: tripObj.driver.email
            });
            transactionDriver.saveAsync()
              .then((transactionRider) => {
                const returnObj = {
                  success: true,
                  message: '',
                  data: {}
                };
                returnObj.data.user = transactionRider;
                returnObj.message = 'transaction created successfully wallet was not present';
                console.log('return object', returnObj);
              });
          });
      } else {
        const returnObj = {
          success: false,
          message: '',
          data: {}
        };
        returnObj.data.user = updateWalletObj;
        returnObj.message = 'walletBalance updatation failed';
        returnObj.success = false;
        console.log('return object', returnObj);
      }
    });
}

export function getUserBalanceById(userId) {
  let retObject = null;
  return new Promise((resolve) => {
    Wallet.findOneAsync({ userId })
      .then((foundWallet) => {
        if (foundWallet !== null) {
          retObject = foundWallet;
        }
        resolve(retObject);
      });
  });
}

export function processWithWallet(trip) {
  const userId = trip.riderId;
  const costForTrip = trip.tripAmt; //  + riderWallet.commissionToCabdo;
  let restForPayWithCash = 0;
  return new Promise((resolve) => {
    getUserBalanceById(userId)
      .then((wallet) => {
        // if wallet not exist create transaction and return amount of trip to rider to pay cash
        if (!wallet) {
          resolve({ error: true, tripAmt: costForTrip });
        }
        return wallet;
      })
      .then((wallet) => {
        const availableQuantity = wallet.walletBalance - costForTrip;
        if (wallet.walletBalance > 0) {
          const transactionOwner = new Transaction({
            userIdFrom: trip.riderId,
            tripId: trip._id,
            amount: availableQuantity >= 0 ? Number(costForTrip) : Number(wallet.walletBalance),
            walletIdFrom: trip.riderId,
          });
          transactionOwner.saveAsync();
        }
        if (availableQuantity >= 0) {
          Wallet.findOneAndUpdateAsync({ userId }, { $inc: { walletBalance: -costForTrip } });
        } else {
          restForPayWithCash = Math.abs(availableQuantity);
          Wallet.findOneAndUpdateAsync({ userId }, { $set: { walletBalance: 0 } });
        }
        resolve({ error: false, tripAmt: restForPayWithCash });
      })
      .catch((err) => {
        console.log('Error', err);
      });
  });
}

export async function processWithCard(tripObj) {
  let restForPayWithCard = tripObj.tripAmt;
  if (tripObj.taxiType === 'CabdoBlack') {
    const resultForPayWithCard = await processWithWallet(tripObj);
    restForPayWithCard = resultForPayWithCard.tripAmt;
  }

  return new Promise((resolve) => {
    if (restForPayWithCard > 0) {
      Wallet.findOneAsync({ userId: tripObj.riderId })
        .then(wallet => {
          stripe.charges.create({
            amount: restForPayWithCard * 100,
            currency: 'eur',
            customer: wallet.stripeAccountId,
          }, (err, result) => {
            if (result && result.paid) {
              resolve({ error: false, tripAmt: restForPayWithCard });
            } else {
              resolve({ error: true, tripAmt: restForPayWithCard });
            }
          });
        });
    } else {
      resolve({ error: false, tripAmt: 0 });
    }
  });
}

function addBalance(req, res, next) {
  Wallet.findOneAndUpdateAsync({ userEmail: req.body.riderEmail }, { $inc: { walletBalance: Number(req.body.amount) } })
    .then((updateWalletObj) => {
      if (updateWalletObj) {
        // transaction insert
        const transactionOwner = new Transaction({
          userIdFrom: req.body.riderEmail,
          tripId: req.body.tripId,
          amount: Number(req.body.amount),
          walletIdFrom: req.body.riderEmail,
        });
        transactionOwner.saveAsync()
          .then((transactionRider) => {
            const returnObj = {
              success: true,
              message: '',
              data: {}
            };
            returnObj.data.user = transactionRider;
            returnObj.message = 'transaction created successfully';
            res.send(returnObj);
          });
      } else {
        const wallet = new Wallet({
          userEmail: req.body.riderEmail,
          walletBalance: req.body.amount,
        });
        wallet.saveAsync()
          .then((savedWallet) => {
            console.log('saved wallet object', savedWallet);
            const transactionOwner = new Transaction({
              userIdFrom: req.body.riderEmail,
              tripId: req.body.tripId,
              amount: Number(req.body.amount),
              walletIdFrom: req.body.riderEmail,
            });
            transactionOwner.saveAsync()
              .then((transactionRider) => {
                const returnObj = {
                  success: true,
                  message: '',
                  data: {}
                };
                returnObj.data.user = transactionRider;
                returnObj.message = 'transaction created successfully';
                res.send(returnObj);
              })
              .error((e) => { console.log('error', e); });
          });
      }
    }).error((e) => {
      next(e);
    });
}

function addCard(req, res, next) {
  const returnObj = {
    success: false,
    message: '',
    data: {}
  };
  if (!req.body.riderEmail || !req.body.riderId || !req.body.stripeToken) {
    returnObj.message = 'Check wallet data! Missing argument(s) - riderEmail, riderId, stripeToken.';
    res.send(returnObj);
  }
  const custometObj = {
    description: `Customer for ${req.body.riderEmail}`,
    source: req.body.stripeToken,
    email: req.body.riderEmail
  };
  stripe.customers.create(custometObj, (err, customer) => {
    if (err) {
      console.log('Error on customer creating', err);
      returnObj.message = `Error in stripe on customer creating. ${err.message}`;
      res.send(returnObj);
    } else {
      Wallet.findOneAsync({ userEmail: req.body.riderEmail }).then(wallet => {
        if (wallet) {
          wallet.stripeAccountId = customer.id;
        } else {
          wallet = new Wallet({
            userEmail: req.body.riderEmail,
            userId: req.body.riderId,
            stripeAccountId: customer.id
          });
        }
        wallet.saveAsync()
          .then((savedWallet) => {
            returnObj.success = true;
            returnObj.data = savedWallet.toJSON();
            returnObj.data.card = customer.sources.data[0];
            res.send(returnObj);
          })
          .error((e) => {
            console.log('error', e);
            next(e);
          });
      });
    }
  });
}

function getCard(req, res, next) {
  const returnObj = {
    success: false,
    message: '',
    data: {}
  };
  if (!req.params.userId) {
    returnObj.message = 'Missing argument rider id';
    res.send(returnObj);
  }
  Wallet.findOne({ userId: req.params.userId })
    .then((foundWallet) => {
      if (foundWallet && foundWallet.stripeAccountId) {
        stripe.customers.retrieve(foundWallet.stripeAccountId, (err, customer) => {
          if (err) {
            console.error('Can\'t get stripe customer by id: ', err.Error);
            return res.send(returnObj);
          }

          returnObj.success = true;
          returnObj.message = 'wallet found';
          returnObj.data = foundWallet.toJSON();
          returnObj.data.card = customer.sources.data[0];
          res.send(returnObj);
        });
      } else {
        returnObj.message = 'card not found';
        res.send(returnObj);
      }
    }).error(e => next(e));
}

function updateCard(req, res) {
  const returnObj = {
    success: false,
    message: '',
    data: {}
  };
  if (!req.body.riderId || !req.body.stripeToken || !req.body.stripeAccountId) {
    returnObj.message = 'Check wallet data! Missing argument(s) - riderId, stripeToken, stripeAccountId.';
    res.send(returnObj);
  }
  stripe.customers.update(req.body.stripeAccountId, { source: req.body.stripeToken }, (err, customer) => {
    if (err) {
      console.error('Error on customer updating: ', err.Error);
      res.send(returnObj);
    } else {
      returnObj.success = true;
      returnObj.data = customer.sources.data[0];
      res.send(returnObj);
    }
  });
}

function updateMethod(req, res, next) {
  const returnObj = {
    success: false,
    message: '',
    data: {}
  };
  if (!req.body.userId || !req.body.paymentMethod) {
    returnObj.message = 'Check payment data! Missing argument(s) - userId, paymentMethod.';
    res.send(returnObj);
  }
  Wallet.findOneAndUpdate({ userId: req.body.userId }, { paymentMethod: req.body.paymentMethod }, { new: true, upsert: true, returnNewDocument: true })
    .then((foundWallet) => {
      if (foundWallet !== null) {
        res.send({ success: true, message: 'wallet found', data: foundWallet });
      } else {
        res.send({ success: false, message: 'wallet not found', data: {} });
      }
    }).error(e => next(e));
}

function removeCard(req, res, next) {
  const returnObj = {
    success: false,
    message: '',
    data: {}
  };
  if (!req.body.userId || !req.body.stripeAccountId) {
    returnObj.message = 'Check data! Missing argument(s) - userId, stripeAccountId.';
    res.send(returnObj);
  }
  stripe.customers.del(req.body.stripeAccountId, (err, confirmation) => {
    if (err) {
      console.error('Error on customer deleting: ', err.Error);
      res.send(returnObj);
      return;
    }

    if (confirmation.deleted) {
      Wallet.findOneAndUpdateAsync({ userId: req.body.userId }, { $set: { stripeAccountId: null, updatedAt: Date.now() } }, { new: true })
        .then((updatedWallet) => {
          returnObj.success = true;
          returnObj.data = updatedWallet;
          res.send(returnObj);
        }).error(e => next(e));
      return;
    }
    res.send(returnObj);
  });
}

function calculateFare(req, res, next) {
  const returnObj = {
    success: false,
    message: 'Unknown error',
    data: {}
  };
  const { origin, destination } = req.body;
  if (!origin || !destination) {
    returnObj.message = 'Check data! Missing argument(s) - origin, destination.';
    return res.send(returnObj);
  }
  request.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${encodeURI(origin)}&destinations=${encodeURI(destination)}&key=${config.googleAPIkey}`, (err, resp, body) => {
    if (err) {
      const error = `Error in fetching google map api ${err ? err.message : ''} ${err}`;
      console.error(error);
      returnObj.message = error;
      return res.send(returnObj);
    }

    body = JSON.parse(body);

    if (body.status === 'OK' && body.rows[0].elements[0].distance && body.rows[0].elements[0].duration) {
      const distance = body.rows[0].elements[0].distance.value / 1000;
      const duration = body.rows[0].elements[0].duration.value / 60;
      CarCategory.findAsync().then(categories => {
        const cost = {
          CabdoBlack: {},
          Taxi: {},
          TaxiXL: {}
        };
        const cats = [];
        const today = new Date();
        const day = today.getDay();
        const hours = today.getHours();
        const isDay = day > 0 && day < 7 && hours > 6 && hours < 22;
        categories.map(category => {
          const { name, baseFareDay, baseFareNight, priceFirstKilometerDay, pricePerKilometerDay, priceFirstKilometerNight, pricePerKilometerNight, minimumFare, extraFare } = category;
          const baseFare = isDay ? baseFareDay : baseFareNight;
          const priceFirstKilometer = isDay ? priceFirstKilometerDay : priceFirstKilometerNight;
          const pricePerKilometer = isDay ? pricePerKilometerDay : pricePerKilometerNight;

          let firstDistance = 1;
          let remainingDistance = 0;
          if (distance > 1) {
            remainingDistance = distance - firstDistance;
          } else {
            firstDistance = distance;
            remainingDistance = 0;
          }
          cost[name].baseFare = round(baseFare);
          cost[name].distanceFare = round(priceFirstKilometer * firstDistance + pricePerKilometer * remainingDistance);
          cost[name].totalFare = cost[name].baseFare + cost[name].distanceFare;
          cost[name].minimumFare = minimumFare;
          if (category.default && cost[name].totalFare < minimumFare) {
            cost[name].totalFare = minimumFare;
          }
          if (name === 'TaxiXL') {
            cost[name].totalFare += extraFare;
          }
          cost[name].totalFare = roundUp(parseFloat(cost[name].totalFare.toFixed(2)));
          cats.push({ id: category._id, name: category.name, cost: cost[name].totalFare, isDefaultCat: category.default });
        });
        returnObj.success = true;
        returnObj.data = { duration, distance, cost, categories: cats };
        returnObj.message = 'Fare successfully calculated!';
        return res.send(returnObj);
      }).error(e => next(e));
    } else {
      res.send(returnObj);
    }
  });
}

// To make the cost round to 10
function roundUp(num) {
  return Math.ceil(num * 10) / 10;
}

/** To make the cost have two digits after dot, and only 5 or 0 at the end */
function round(cost) {
  const val = Math.round(cost * 100);
  return (val - val % 5) / 100;
}

function fetchUserTransactions(req, res) {
  const userId = req.params.userId;

  Transaction.findAsync({ $or: [{ userIdFrom: userId }, { userIdTo: userId }] })
    .then(data => res.send(data))
    .error(e => console.log('errorTransactions', e));
}

export default { payAll, getBalance, addBalance, addCard, getCard, updateCard, updateMethod, removeCard, calculateFare, fetchUserTransactions };
