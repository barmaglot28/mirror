import TripSchema from '../models/trip';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { fetchReturnObj } from '../service/transform-response';
import tripStatusConsts from '../constants/tripStatus';
/**
 * Return the trip details of the user.
 * @param req
 * @param res
 * @param next
 * @returns { trip: historyObjArray[{ tripObj }]  }
 */

function getSyncData(req, res, next) {
  const currTripId = req.user.currTripId;
  const currTripState = req.user.currTripState;
  const returnObj = {
    success: true,
    message: 'user is not in any trip or tripRequest',
    data: {
      trip: null
    }
  };
  if (currTripId === null || currTripId === undefined || currTripState === null || currTripState === undefined) {
    res.send(returnObj);
  } else {
    TripSchema.findOneAsync({ $and: [{ _id: currTripId }, { $or: [{ tripStatus: tripStatusConsts.enRoute }, { tripStatus: tripStatusConsts.arriving }, { tripStatus: tripStatusConsts.arrived }, { tripStatus: tripStatusConsts.feedback }, { tripStatus: tripStatusConsts.endTrip }] }] })
      .then((tripObj) => {
        if (tripObj) {
          fetchReturnObj(tripObj).then((transformedTripObj) => {
            returnObj.message = `user is in ${tripObj.tripStatus} state`;
            returnObj.data.trip = transformedTripObj;
            res.send(returnObj);
          })
            .error((e) => {
              const err = new APIError(`error occurred when transforming tripObj ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
              return next(err);
            });
        } else {
          returnObj.message = 'no trip request object found for the current trip state for the corresponding user';
          res.send(returnObj);
        }
      })
      .error((e) => {
        const err = new APIError(`error occurred when feteching user data from trip schema ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        return next(err);
      });
  }
}

export default { getSyncData };
