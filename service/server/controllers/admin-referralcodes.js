import ReferralCodeAmountSchema from '../models/referral-code-amount';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-referralcodes');

function fetchReferralAmt(req, res, next) {
  const returnObj = {
    success: false,
    message: '',
    data: {},
  };
  ReferralCodeAmountSchema.findOneAsync()
    .then((referralAmount) => {
      if (referralAmount) {
        returnObj.success = true;
        returnObj.message = 'Referral Amount found';
        returnObj.data = referralAmount;
        return res.send(returnObj);
      } else {
        returnObj.success = false;
        returnObj.message = 'No Referral Amount found';
        returnObj.data = {};
        return res.send(returnObj);
      }
    })
    .error((e) => {
      const err = new APIError(
        `error occured while finding referralAmount ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside referralAmount records`);
      next(err);
    });
}

function saveReferralAmt(req, res, next) {
  const referralAmtsData = Object.assign({}, req.body);
  const { amountToSenderWallet, amountToRecipientWallet } = referralAmtsData;
  const returnObj = {
    success: false,
    message: '',
    data: {},
  };
  let referralAmtObj;
  ReferralCodeAmountSchema.findOneAsync()
    .then((referralAmount) => {
      if (referralAmount) {
        referralAmount.amountToSenderWallet = amountToSenderWallet;
        referralAmount.amountToRecipientWallet = amountToRecipientWallet;
        referralAmtObj = referralAmount;
      } else {
        referralAmtObj = new ReferralCodeAmountSchema({ amountToSenderWallet, amountToRecipientWallet });
      }
      referralAmtObj.saveAsync()
        .then((savedReferralAmt) => {
          returnObj.success = true;
          returnObj.message = 'referral amount saved successfully';
          returnObj.data = savedReferralAmt;
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(`Error while Saving new referralAmount ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
          return next(err);
        });
    })
    .error((e) => {
      const err = new APIError(`Error while Searching the referralAmount ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    });
}

export default { fetchReferralAmt, saveReferralAmt };
