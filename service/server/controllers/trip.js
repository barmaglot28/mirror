import TripSchema from '../models/trip';
import UserSchema from '../models/user';
import CarCategorySchema from '../models/car-category';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import { fetchReturnObj } from '../service/transform-response';
import moment from 'moment';
import { sendTripForDashboard } from '../socketHandler/story/request-trip';
/**
 * Return the trip details of the user.
 */

function getHistory(req, res, next) {
  const historyObjArray = [];
  const userID = req.user._id;
  const userType = req.user.userType;
  const searchObj = {};
  if (userType === 'rider') {
    searchObj.riderId = userID;
    searchObj.showItemForRider = true;
  } else if (userType === 'driver') {
    searchObj.driverId = userID;
    searchObj.showItemForDriver = true;
  }
  TripSchema.find({ $and: [searchObj, { tripStatus: 'endTrip' }] }, (tripErr, tripObj) => {
    if (tripErr) {
      const err = new APIError(`error while finding trip history for the user  ${ tripErr }`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    }
    if (tripObj.length !== 0) {
      tripObj.forEach((obj, index) => {
        fetchReturnObj(obj).then((transformedReturnObj) => {
          historyObjArray.push(transformedReturnObj);
          if (index === tripObj.length - 1) {
            const returnObj = {
              success: true,
              message: 'user trip history',
              data: historyObjArray
            };
            res.send(returnObj);
          }
        });
      });
    } else {
      const returnObj = {
        success: true,
        message: 'no history available',
        data: []
      };
      res.send(returnObj);
    }
  });
}

function removeHistory(req, res, next) {
  const userType = req.user.userType;
  let returnObj = {
    success: false,
    message: 'no items to delete',
  };
  const dataForRemove = {};
  if (userType === 'rider') {
    dataForRemove.showItemForRider = false;
  } else if (userType === 'driver') {
    dataForRemove.showItemForDriver = false;
  }
  TripSchema.update({ _id: { $in: req.body } }, { $set: dataForRemove }, { multi: true }, (removeErr, tripArr) => {
    if (removeErr) {
      const err = new APIError(`error while remove trip history for the user  ${ removeErr }`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    }
    if (tripArr.length !== 0) {
      returnObj = {
        success: true,
        message: 'user trip history removed',
      };
    }
    res.send(returnObj);
  });
}

export function getDriverAcceptedBookings(driverId) {
  return new Promise((resolve) =>
    TripSchema.find({ driverId, tripStatus: 'booked', preBookedTime: { $gte: new Date() } })
      .populate('riderId', 'fname lname phoneNo')
      .then(acceptedBookingsArr => {
        if (acceptedBookingsArr) {
          const returnObj = {
            success: true,
            message: 'driver accepted bookings',
            data: acceptedBookingsArr
          };
          return resolve(returnObj);
        } else {
          const returnObj = {
            success: true,
            message: 'no driver accepted bookings',
            data: []
          };
          return resolve(returnObj);
        }
      })
  );
}

export function getRiderBookings(riderId) {
  return new Promise((resolve) =>
    TripSchema.find({ riderId, tripStatus: { $in: ['booked', 'booking'] }, preBookedTime: { $gte: new Date() } })
      .populate('driverId', 'fname lname phoneNo')
      .then(bookedTripsArr => {
        if (bookedTripsArr) {
          const returnObj = {
            success: true,
            message: 'rider booked trips',
            data: bookedTripsArr
          };
          return resolve(returnObj);
        } else {
          const returnObj = {
            success: true,
            message: 'no rider booked trips',
            data: []
          };
          return resolve(returnObj);
        }
      })
  );
}

export function getAvailableBookings(driver, carCategory, maxDistance) {
  return new Promise((resolve) => {
    CarCategorySchema.findOneAsync({ _id: carCategory }).then(category => {
      console.log(`getAvailableBookings lng/lat: ${driver.gpsLoc[0]}/${driver.gpsLoc[1]}`);
      TripSchema.find({
        driverId: null,
        tripStatus: 'booking',
        preBookedTime: {
          $gte: new Date()
        },
        srcLoc: { $near: driver.gpsLoc, $maxDistance: maxDistance },
        taxiType: category.name
      })
        .then(availableBookingsArr => {
          if (availableBookingsArr) {
            const returnObj = {
              success: true,
              message: 'all available bookings',
              data: availableBookingsArr
            };
            return resolve(returnObj);
          } else {
            const returnObj = {
              success: true,
              message: 'no available bookings',
              data: []
            };
            return resolve(returnObj);
          }
        });
    });
  });
}

export async function setDriverToBooking(payload) {
  return new Promise((resolve) =>
    UserSchema.findOneAsync({ _id: payload.driverId })
      .then(driver => {
        TripSchema.findOneAndUpdate({ _id: payload.trip._id, preBookedTime: { $gte: new Date() } }, { $set: { driverId: payload.driverId, tripStatus: 'booked', carDetails: driver.carDetails } }, { new: true })
          .then(tripDoc => {
            sendTripForDashboard('tripUpdatedDashboard', tripDoc);
            if (tripDoc) {
              return TripSchema.find({ driverId: null, tripStatus: 'booking', preBookedTime: { $gte: new Date() } })
                .then(availableBookingsArr => {
                  if (availableBookingsArr) {
                    return resolve({
                      success: true,
                      message: 'Approved successfully! Check this trip in "My Bookings" screen.',
                      data: availableBookingsArr
                    });
                  } else {
                    return resolve({
                      success: true,
                      message: 'Approved successfully! Check this trip in "My Bookings" screen.',
                      data: []
                    });
                  }
                });
            } else {
              return resolve({
                success: false,
                message: 'No such trip exists or approved by another driver!'
              });
            }
          });
      })
  );
}

export function removeDriverFromBooking(trip) {
  return new Promise((resolve) =>
    TripSchema.findOneAndUpdate({ _id: trip._id, preBookedTime: { $gte: new Date() } }, { $set: { driverId: null, tripStatus: 'booking', carDetails: null } }, { new: true })
      .then(tripDoc => {
        sendTripForDashboard('tripUpdatedDashboard', tripDoc);
        if (tripDoc) {
          return TripSchema.find({ driverId: null, tripStatus: 'booking', preBookedTime: { $gte: new Date() } })
            .then(availableBookingsArr => {
              if (availableBookingsArr) {
                return resolve({
                  success: true,
                  data: availableBookingsArr
                });
              } else {
                return resolve({
                  success: true,
                  data: []
                });
              }
            });
        } else {
          return resolve({
            success: false
          });
        }
      })
  );
}

export function findSoonTrips(driverId, newBookedTime, trip) {
  const ltTime = moment(newBookedTime).add(20, 'm');
  const gtTime = moment(newBookedTime).subtract(20, 'm');
  return new Promise((resolve) => {
    return trip.driverId ? resolve({ success: true, data: [] }) : TripSchema.find({
      driverId,
      tripStatus: 'booked',
      preBookedTime: { $gt: gtTime, $lt: ltTime } })
      .then(bookedArr => {
        if (bookedArr && bookedArr.length > 0) {
          return resolve({
            success: false,
            data: bookedArr
          });
        } else {
          return resolve({
            success: true,
            data: []
          });
        }
      });
  });
}

export default { getHistory, removeHistory, getDriverAcceptedBookings };
