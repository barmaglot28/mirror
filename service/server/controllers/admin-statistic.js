import UserSchema from '../models/user';
import TripSchema from '../models/trip';
import TransactionSchema from '../models/transaction';
import mongoose from 'mongoose';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-user');

function getGeneralStatistic(req, res) {
  const returnObj = {
    success: true,
    message: 'general statistic',
    data: {
      riders: 0,
      drivers: 0,
      taxiCompanies: 0,
      totalEarnings: 0,
      acceptedRides: 0,
      notAcceptedRides: 0,
    }
  };
  return totalDriversCount()
    .then((resTotalDrivers) => {
      returnObj.data.drivers = resTotalDrivers;
      return totalRiders()
        .then((resTotalRiders) => {
          returnObj.data.riders = resTotalRiders;
          return totalCorporate()
            .then((resTotalCorporate) => {
              returnObj.data.taxiCompanies = resTotalCorporate;
              return totalEarnings()
                .then((resTotalEarnings) => {
                  returnObj.data.totalEarnings = resTotalEarnings;
                  return acceptedRides()
                    .then((resAcceptedRides) => {
                      returnObj.data.acceptedRides = resAcceptedRides;
                      return notAcceptedRides()
                        .then(resNotAcceptedRides => {
                          returnObj.data.notAcceptedRides = resNotAcceptedRides;
                          return res.send(returnObj);
                        });
                    });
                });
            });
        });
    });
}

function totalDriversCount() {
  return UserSchema.find({ userType: 'driver' })
    .countAsync()
    .then((resTotalDrivers) => {
      return resTotalDrivers;
    })
    .error((e) => {
      debug(`Error inside totalDrivers records. Error occured while counting the no of drivers ${e}`);
      return [];
    });
}

function totalDrivers() {
  return UserSchema.findAsync({ userType: 'driver' })
    .then((resTotalDrivers) => {
      return resTotalDrivers;
    })
    .error((e) => {
      debug(`Error inside totalDrivers records. Error occured while counting the no of drivers ${e}`);
      return [];
    });
}

function totalRiders() {
  return UserSchema.find({ userType: 'rider', corporateUserId: null })
    .countAsync()
    .then((resTotalRiders) => {
      return resTotalRiders;
    })
    .error((e) => {
      debug(`Error inside totalRiders records. Error occured while counting the no of riders ${e}`);
      return 0;
    });
}

function totalCorporate() {
  return UserSchema.find({ userType: 'corporate' })
    .countAsync()
    .then((resTotalCorporate) => {
      return resTotalCorporate;
    })
    .error((e) => {
      debug(`Error inside totalCorporate records. Error occured while counting the no of riders ${e}`);
      return 0;
    });
}
function totalEarnings() {
  return TransactionSchema.aggregateAsync([
    {
      $match: {
        walletIdTo: 'owner'
      }
    }, {
      $group: {
        _id: null,
        total: {
          $sum: '$amount'
        }
      }
    }])
    .then((earnings) => {
      if (earnings.length) {
        return earnings[0].total;
      }
      return 0;
    })
    .error((e) => {
      debug(`Error inside totalEarnings records. Error occured while summ earnings in Transactions ${e}`);
      return 0;
    });
}

function acceptedRides() {
  return TripSchema.find({ driverId: { $ne: null } })
    .countAsync()
    .then((resAcceptedRides) => {
      return resAcceptedRides;
    })
    .error((e) => {
      debug(`Error inside acceptedRides records. Error occured while counting the no of accepted rides ${e}`);
      return 0;
    });
}

function notAcceptedRides() {
  return TripSchema.find({ driverId: null })
    .countAsync()
    .then((resNotAcceptedRides) => {
      return resNotAcceptedRides;
    })
    .error((e) => {
      debug(`Error inside notAcceptedRides records. Error occured while counting the no of accepted rides ${e}`);
      return 0;
    });
}

function getRideTodayStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: {
      total: 0,
      ongoing: 0,
      completed: 0,
      cancelled: 0,
      accepted: 0,
      revenue: 0,
    }
  };
  return todayRides()
    .then((resTodayRides) => {
      returnObj.data.total = resTodayRides.length;
      returnObj.data.ongoing = resTodayRides.filter(ride => {
        return ride.tripStatus === 'enRoute' || ride.tripStatus === 'arriving' || ride.tripStatus === 'arrived';
      }).length;
      const endedTrips = resTodayRides.filter(ride => {
        return ride.tripStatus === 'endTrip';
      });
      returnObj.data.completed = endedTrips.length;
      returnObj.data.cancelled = resTodayRides.filter(ride => {
        return ride.tripStatus === 'cancelled';
      }).length;
      returnObj.data.accepted = resTodayRides.filter(ride => {
        return ride.driverId !== null;
      }).length;
      returnObj.data.cancelled = resTodayRides.filter(ride => {
        return ride.tripStatus === 'cancelled';
      }).length;
      endedTrips.forEach(trip => {
        returnObj.data.revenue += trip.tripAmt;
      });
      return res.send(returnObj);
    });
}

function todayRides() {
  const nowDate = new Date();
  const today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0);
  return TripSchema.findAsync({ bookingTime: { $gte: today } })
    .then((resTodayRides) => {
      return resTodayRides;
    })
    .error((e) => {
      debug(`Error inside todayRides records. Error occured while getting today rides ${e}`);
      return [];
    });
}

function monthRides() {
  const nowDate = new Date();
  const month = new Date(nowDate.getFullYear(), nowDate.getMonth(), 1, 0, 0, 0);
  return TripSchema.findAsync({ bookingTime: { $gte: month } })
    .then((resMonthRides) => {
      return resMonthRides;
    })
    .error((e) => {
      debug(`Error inside monthRides records. Error occured while getting month rides ${e}`);
      return [];
    });
}

function getCompletedRideStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: {
      today: 0,
      month: 0,
      blackToday: 0,
      blackMonth: 0
    }
  };
  return monthRides()
    .then((resMonthRides) => {
      returnObj.data.month = resMonthRides.length;
      returnObj.data.blackMonth = resMonthRides.filter(ride => {
        return ride.taxiType === 'CabdoBlack';
      }).length;
      return todayRides()
        .then((resTodayRides) => {
          returnObj.data.today = resTodayRides.length;
          returnObj.data.blackToday = resTodayRides.filter(ride => {
            return ride.taxiType === 'CabdoBlack';
          }).length;
          return res.send(returnObj);
        });
    });
}

function getDriversStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: {
      registered: 0,
      online: 0,
      offline: 0,
      onTrip: 0,
    }
  };
  return totalDrivers()
    .then((resTotalDrivers) => {
      returnObj.data.registered = resTotalDrivers.length;
      returnObj.data.online = resTotalDrivers.filter(ride => {
        return ride.loginStatus === true;
      }).length;
      returnObj.data.offline = resTotalDrivers.filter(ride => {
        return ride.loginStatus === false;
      }).length;
      returnObj.data.onTrip = resTotalDrivers.filter(ride => {
        return ride.currTripState !== null;
      }).length;
      return res.send(returnObj);
    });
}

function getRatingCommentsStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: []
  };
  return TripSchema.find(
    {
      tripStatus: 'endTrip',
      $or: [
        {
          driverRatingByRider: {
            $gt: 0
          }
        },
        {
          riderRatingByDriver: {
            $gt: 0
          }
        },
        {
          riderComment: {
            $ne: null
          }
        }
      ]
    })
    .sort({ endTime: -1 })
    .limit(5)
    .populate('driverId')
    .populate('riderId')
    .execAsync()
    .then((tripsArr) => {
      returnObj.data = tripsArr;
      return res.send(returnObj);
    })
    .error((e) => {
      debug(`Error inside getRatingCommentsStatistic records. Error occured while getting rating comments of trips ${e}`);
      returnObj.success = false;
      return res.send(returnObj);
    });
}

function getCancelledCommentsStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: []
  };
  return TripSchema.find({ tripStatus: 'cancelled', tripIssueComment: { $ne: '' } })
    .sort({ bookingTime: -1 })
    .limit(5)
    .populate('driverId')
    .populate('riderId')
    .execAsync()
    .then((tripsArr) => {
      returnObj.data = tripsArr;
      return res.send(returnObj);
    })
    .error((e) => {
      debug(`Error inside getCancelledCommentsStatistic records. Error occured while getting cancelled trips comments ${e}`);
      returnObj.success = false;
      return res.send(returnObj);
    });
}

function getDriverWorklistStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: []
  };
  let totalDriversArr = [];
  return totalDrivers()
    .then((resTotalDrivers) => {
      const promiseArr = [];
      totalDriversArr = JSON.parse(JSON.stringify(resTotalDrivers));
      totalDriversArr.forEach((driver) => {
        promiseArr.push(new Promise((resolve) => {
          TripSchema.aggregateAsync([
            {
              $match: {
                driverId: mongoose.Types.ObjectId(driver._id),
                tripStatus: 'endTrip'
              }
            }, {
              $sort: {
                endTime: 1
              }
            }, {
              $group: {
                _id: null,
                count: {
                  $sum: 1
                },
                revenue: {
                  $sum: '$tripAmt'
                },
                time: {
                  $last: '$endTime'
                }
              },
            }])
            .then((trips) => {
              if (trips.length > 0) {
                driver.completedTrips = trips[0].count;
                driver.totalRevenue = trips[0].revenue;
                driver.lastTime = trips[0].time;
              }
              resolve(trips);
            });
        }));
      });
      return Promise.all(promiseArr);
    })
    .then(() => {
      returnObj.data = totalDriversArr;
      return res.send(returnObj);
    })
    .error((e) => {
      debug(`Error inside getDriverWorklistStatistic records. Error occured while getting drivers worklist ${e}`);
      returnObj.success = false;
      return res.send(returnObj);
    });
}

function getTripsStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: []
  };
  return TripSchema.find()
    .sort({ bookingTime: -1 })
    .limit(100)
    .populate('driverId')
    .populate('riderId')
    .execAsync()
    .then((tripsArr) => {
      returnObj.data = tripsArr;
      return res.send(returnObj);
    })
    .error((e) => {
      debug(`Error inside getTripsStatistic records. Error occured while getting trips ${e}`);
      returnObj.success = false;
      return res.send(returnObj);
    });
}

function getVehicleStatistic(req, res) {
  const returnObj = {
    success: true,
    message: '',
    data: {
      requested: [],
      accepted: [],
      completed: [],
      revenue: [],
    }
  };
  return getRequestedVehicle()
    .then((tripsArr) => {
      returnObj.data.requested = tripsArr;
      return getAcceptedVehicle()
        .then((acceptedArr) => {
          returnObj.data.accepted = acceptedArr;
          return getCompletedVehicle()
            .then((completedArr) => {
              returnObj.data.completed = completedArr;
              return getRevenueVehicle()
                .then((revenueArr) => {
                  returnObj.data.revenue = revenueArr;
                  return res.send(returnObj);
                });
            });
        });
    })
    .error((e) => {
      debug(`Error inside getVehicleStatistic records. Error occured while getting vehicle statistic ${e}`);
      returnObj.success = false;
      return res.send(returnObj);
    });
}

function getRequestedVehicle() {
  return TripSchema.aggregateAsync([
    {
      $group: {
        _id: '$taxiType',
        count: {
          $sum: 1
        }
      },
    }, {
      $project: {
        _id: 0,
        name: '$_id',
        value: '$count'
      }
    }])
    .then((requested) => {
      return requested;
    })
    .error((e) => {
      debug(`Error inside getRequestedVehicle records. Error occured while getting requested trips ${e}`);
      return [];
    });
}

function getCompletedVehicle() {
  return TripSchema.aggregateAsync([
    {
      $match: {
        tripStatus: 'endTrip'
      }
    },
    {
      $group: {
        _id: '$taxiType',
        count: {
          $sum: 1
        }
      },
    }, {
      $project: {
        _id: 0,
        name: '$_id',
        value: '$count'
      }
    }])
    .then((completed) => {
      return completed;
    })
    .error((e) => {
      debug(`Error inside getCompletedVehicle records. Error occured while getting completed trips ${e}`);
      return [];
    });
}

function getAcceptedVehicle() {
  return TripSchema.aggregateAsync([
    {
      $match: {
        driverId: { $ne: null }
      }
    },
    {
      $group: {
        _id: '$taxiType',
        count: {
          $sum: 1
        }
      },
    }, {
      $project: {
        _id: 0,
        name: '$_id',
        value: '$count'
      }
    }])
    .then((completed) => {
      return completed;
    })
    .error((e) => {
      debug(`Error inside getAcceptedVehicle records. Error occured while getting accepted trips ${e}`);
      return [];
    });
}

function getRevenueVehicle() {
  return TripSchema.aggregateAsync([
    {
      $match: {
        tripStatus: 'endTrip'
      }
    },
    {
      $group: {
        _id: '$taxiType',
        count: {
          $sum: '$tripAmt'
        }
      },
    }, {
      $project: {
        _id: 0,
        name: '$_id',
        value: '$count'
      }
    }])
    .then((revenue) => {
      return revenue;
    })
    .error((e) => {
      debug(`Error inside getRevenueVehicle records. Error occured while getting revenue trips ${e}`);
      return [];
    });
}

export default { getGeneralStatistic, getRideTodayStatistic, getCompletedRideStatistic, getDriversStatistic,
  getRatingCommentsStatistic, getCancelledCommentsStatistic, getDriverWorklistStatistic, getTripsStatistic,
  getVehicleStatistic };
