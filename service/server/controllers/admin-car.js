import CarSchema from '../models/car';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-user');

function getAllCars(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);
  CarSchema.countAsync()
      .then((totalCarRecord) => {
        const returnObj = {
          success: true,
          message: 'no of car are zero',
          data: null,
          meta: {
            totalNoOfPages: Math.ceil(totalCarRecord / limit),
            limit,
            currPageNo: pageNo,
            currNoOfRecord: 20,
          }
        };
        if (totalCarRecord < 1) {
          return res.send(returnObj);
        }
        if (skip > totalCarRecord) {
          const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
          return next(err);
        }
        CarSchema.list({ skip, limit })
            .then((carData) => {
              returnObj.data = transformReturnObj(carData);
              returnObj.message = 'car found';
              returnObj.meta.currNoOfRecord = returnObj.data.length;
              debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
              return res.send(returnObj);
            })
            .error((e) => {
              const err = new APIError(
                  `error occured while getting car data ${e}`,
                  httpStatus.INTERNAL_SERVER_ERROR
              );
              debug(`error inside getAllCars when getting car data ${e}`);
              next(err);
            });
      })
      .error((e) => {
        const err = new APIError(
            `error occured while counting the no of cars ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
        );
        debug(`error inside getAllCars records`);
        next(err);
      });
}

function getCarsDetails(req, res, next) {
  const carId = req.params.carId;
  const returnObj = {
    success: false,
    message: 'car Id is not defined',
    data: null,
  };
  if (carId) {
    CarSchema
        .get(carId)
        .then((carData) => {
          if (carData) {
            returnObj.success = true;
            returnObj.message = 'car found and its corresponding details';
            returnObj.data = carData;
          } else {
            returnObj.success = false;
            returnObj.message = 'car not found with the given id';
            returnObj.data = null;
          }
          res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(`Error occured while findind the car details ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
          next(err);
        });
  } else {
    res.send(returnObj);
  }
}

function updateCarDetails(req, res, next) {
  const carId = req.body._id;
  const updateCarObj = Object.assign({}, req.body);

  CarSchema.findOneAsync({ _id: carId })
      .then((carDoc) => {
        if (carDoc) {
          carDoc.licensePlateNumber = updateCarObj.licensePlateNumber ? updateCarObj.licensePlateNumber : carDoc.licensePlateNumber;
          carDoc.make = updateCarObj.make ? updateCarObj.make : carDoc.make;
          carDoc.model = updateCarObj.model ? updateCarObj.model : carDoc.model;
          carDoc.usedBy = updateCarObj.usedBy ? updateCarObj.usedBy : carDoc.usedBy;
          carDoc.carCategory = updateCarObj.carCategory ? updateCarObj.carCategory : carDoc.carCategory;
          carDoc.canBeUsed = updateCarObj.canBeUsed || false;
          const returnObj = {
            success: false,
            message: 'unable to find the object',
            data: null,
            meta: null
          };

          carDoc.saveAsync()
              .then((savedDoc) => {
                returnObj.success = true;
                returnObj.message = 'car document saved';
                returnObj.data = savedDoc;
                res.send(returnObj);
              })
              .error((e) => {
                const err = new APIError(
                    `Error occured while updating the car details ${e}`,
                    httpStatus.INTERNAL_SERVER_ERROR
                );
                next(err);
              });
        }
      })
      .error((e) => {
        const err = new APIError(
            `Error occured while searching for the car ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
        );
        next(err);
      });
}

function transformReturnObj(carData) {
  for (let i = 0; i < carData.length; i++) {
    if (carData[i].carType === 'rider' && carData[i].carDetails) {
      delete carData[i].carDetails;
    }
  }
  return carData;
}

function createNewCar(req, res, next) {
  const carData = Object.assign({}, req.body);
  CarSchema.findOneAsync({ licensePlateNumber: carData.licensePlateNumber })
      .then((foundCar) => {
        const returnObj = {
          success: false,
          message: '',
          data: null,
        };
        if (foundCar !== null) {
          const err = new APIError('License Plate Number Already Exist', httpStatus.CONFLICT);
          return next(err);
        }
        const carObj = new CarSchema({
          licensePlateNumber: carData.licensePlateNumber,
          make: carData.make,
          model: carData.model,
          usedBy: carData.usedBy,
          carCategory: carData.carCategory,
          canBeUsed: carData.canBeUsed,
        });

        carObj.saveAsync()
            .then((savedCar) => {
              returnObj.success = true;
              returnObj.message = 'car created successfully';
              returnObj.data = savedCar;
              return res.send(returnObj);
            })
            .error((e) => {
              const err = new APIError(`Error while Creating new Car ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
              return next(err);
            });
      })
      .error((e) => {
        const err = new APIError(`Error while Searching the car ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        return next(err);
      });
}

function removeCar(req, res, next) {
  const carId = req.body._id;

  CarSchema.findOneAsync({ _id: carId })
      .then((carDoc) => {
        if (carDoc) {
          const returnObj = {
            success: false,
            message: 'unable to find the object',
            data: null,
            meta: null
          };

          carDoc.removeAsync()
              .then((removedDoc) => {
                returnObj.success = true;
                returnObj.message = 'car document removed';
                returnObj.data = removedDoc;
                res.send(returnObj);
              })
              .error((e) => {
                const err = new APIError(
                    `Error occured while removing the car details ${e}`,
                    httpStatus.INTERNAL_SERVER_ERROR
                );
                next(err);
              });
        }
      })
      .error((e) => {
        const err = new APIError(
            `Error occured while searching for the car ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
        );
        next(err);
      });
}

export default { getAllCars, getCarsDetails, updateCarDetails, createNewCar, removeCar };
