import WalletSchema from '../models/wallet';
import UserSchema from '../models/user';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-user');

function getAllWallets(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);
  WalletSchema.countAsync()
    .then((totalWalletRecord) => {
      const returnObj = {
        success: true,
        message: 'no of wallets found',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalWalletRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (!totalWalletRecord) {
        return res.send(returnObj);
      }
      if (skip > totalWalletRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      WalletSchema.list({ skip, limit })
        .then((walletData) => {
          returnObj.data = walletData;
          returnObj.message = 'wallets found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting wallets data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllWallets when getting wallets data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of wallets ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllWallets records`);
      next(err);
    });
}

function addWalletBalance(req, res, next) {
  const returnObj = {
    success: false,
    message: 'no of wallets found',
  };
  WalletSchema.findOneAndUpdate({ _id: req.body.id }, { $inc: { walletBalance: req.body.value }, $set: { updatedAt: Date.now() } }, { new: true })
    .then(updatedWallet => {
      if (updatedWallet) {
        returnObj.success = true;
        returnObj.message = 'updated successful';
      }
      return res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(
        `error occured while update wallet balance ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside addWalletBalance records`);
      next(err);
    });
}

function getFilteredWallets(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  const regex = new RegExp(req.query.filter, 'g');
  const filter = { $or: [{ fname: regex }, { lname: regex }] };
  const returnObj = {
    success: true,
    message: 'no of filtered users found',
    data: null,
    meta: {}
  };
  debug(`skip value: ${req.query.pageNo}`);
  UserSchema.find(filter)
    .countAsync()
    .then((totalFilteredUsersRecord) => {
      returnObj.meta = {
        totalNoOfPages: Math.ceil(totalFilteredUsersRecord / limit),
        limit,
        currPageNo: pageNo,
        currNoOfRecord: 20,
      };
      if (!totalFilteredUsersRecord) {
        return res.send(returnObj);
      }
      if (skip > totalFilteredUsersRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      UserSchema.find(filter, { _id: 1 })
        .sort({ _id: -1 })
        .then((userIds) => {
          WalletSchema.list({ skip, limit, filter: userIds })
            .then((filteredWallets) => {
              /*
               * restore wallet only need to restore deleted wallet, (fixed bug with deleted wallet)
               */
              if (filteredWallets.length !== userIds.length) {
                const walletUserIds = [];
                for (const wallet of filteredWallets) {
                  walletUserIds.push(wallet.userId._id.toString());
                }
                const userIdsWithoutWallet = userIds.filter((item) => { return walletUserIds.indexOf(item._id.toString()) === -1; });
                UserSchema.find({ _id: { $in: userIdsWithoutWallet } })
                  .then(async users => {
                    for (const user of users) {
                      const walletObj = new WalletSchema({
                        walletBalance: 0,
                        userId: user._id,
                        userType: user.userType,
                        userEmail: user.email,
                      });
                      await walletObj.saveAsync();
                    }
                    WalletSchema.list({ skip, limit, filter: userIds })
                      .then((wallets) => {
                        returnObj.data = wallets;
                        returnObj.message = 'filtered wallets found';
                        returnObj.meta.currNoOfRecord = returnObj.data.length;
                        debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
                        return res.send(returnObj);
                      });
                  });
              } else {
                returnObj.data = filteredWallets;
                returnObj.message = 'filtered wallets found';
                returnObj.meta.currNoOfRecord = returnObj.data.length;
                debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
                return res.send(returnObj);
              }
            })
            .error((e) => {
              const err = new APIError(
                `error occured while getting filtered wallets data ${e}`,
                httpStatus.INTERNAL_SERVER_ERROR
              );
              debug(`error inside getFilteredWallets when getting wallets with filter data ${e}`);
              next(err);
              returnObj.message = err;
              return res.send(returnObj);
            });
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of wallets ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getFilteredWallets records`);
      next(err);
      returnObj.message = err;
      return res.send(returnObj);
    });
}

export default { getAllWallets, addWalletBalance, getFilteredWallets };
