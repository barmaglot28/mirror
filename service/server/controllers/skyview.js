import CarCategories from '../models/car-category';
import User from '../models/user';

/**
 * Get carcategories
 * @returns {CarCategories}
 */
async function getDrivers(req, res) {
  // the first item will be category with { default: true }
  const data = await fetchCarCategoriesData();
  // define default car cat
  let defaultCategory = null;
  data.forEach((carCat) => {
    if (carCat.default) {
      defaultCategory = carCat;
    }
  });

  return res.send({ success: true, message: 'carcategories found', data, defaultCategory });
}

/**
 * we need local function to find drivers for request-trip - start automatic request page in available books
 * @returns all {CarCategories} with drivers
 */

export function fetchCarCategoriesData() {
  let retObj;
  return new Promise((resolve) => {
    CarCategories.find({}).sort({ default: -1 })
      .execAsync()
      .then((data) => {
        retObj = data;
        User.findAsync({
          userType: 'driver',
          availability: true,
          'carDetails.carCategory': { $in: data.map((cat) => (cat._id.toString())) }
        }, 'email gpsLoc carDetails currTripId')
          .then((drivers) => {
            // include drivers to car cats
            retObj.forEach((carCat) => {
              carCat.setValue('drivers', []);
              drivers.forEach((driver) => {
                if (carCat._id.toString() === driver.carDetails.carCategory.toString()) {
                  carCat.getValue('drivers').push(driver);
                }
              });
            });
            resolve(retObj);
          });
      })
      .catch((err) => {
        console.error('Error', err);
      });
  });
}

export default { getDrivers };
