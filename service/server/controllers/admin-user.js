import UserSchema from '../models/user';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import twilioClient from '../../config/env/twilioClient';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-user');

import { sendMail } from './user.js';

function getAllDrivers(req, res, next) {
  UserSchema.find({ userType: 'driver' })
    .populate('carDetails.carCategory')
    .execAsync()
    .then(result => {
      const returnObj = {
        success: true,
        message: result.length ? 'user found' : 'no of user are zero',
        data: result.length ? transformReturnObj(result) : null
      };
      res.send(returnObj);
    }).error(err => {
      err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
      return next(err);
    });
}

function getAllUsers(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);
  UserSchema.find({
    userType: req.query.type,
    corporateUserId: null
  }).countAsync()
    .then((totalUserRecord) => {
      const returnObj = {
        success: true,
        message: 'no of user are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalUserRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (totalUserRecord < 1) {
        return res.send(returnObj);
      }
      if (skip > totalUserRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      UserSchema.list({ skip, limit, corporateUserId: null, userType: req.query.type })
        .then((userData) => {
          returnObj.data = transformReturnObj(userData);
          returnObj.message = 'user found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting user data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllUsers when getting user data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of users ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllUsers records`);
      next(err);
    });
}

function getFilteredUsers(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  const regex = new RegExp(req.query.value, 'g');
  debug(`skip value: ${req.query.pageNo}`);
  UserSchema.find({
    userType: req.query.type,
    corporateUserId: null,
    $or: [{ fname: regex }, { lname: regex }]
  }).countAsync()
    .then((totalFilteredUsersRecord) => {
      const returnObj = {
        success: true,
        message: 'no of user are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalFilteredUsersRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (!totalFilteredUsersRecord) {
        return res.send(returnObj);
      }
      if (skip > totalFilteredUsersRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      UserSchema.list({ skip, limit, corporateUserId: null, userType: req.query.type, value: req.query.value })
        .then((userData) => {
          returnObj.data = transformReturnObj(userData);
          returnObj.message = 'user found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting filtered user data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getFilteredUsers when getting user data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of users ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getFilteredUsers records`);
      next(err);
    });
}

function sendSmsToAll(req, res, next) {
  const returnObj = {
    data: [],
    success: false,
    message: 'cannot send sms'
  };
  debug(`sendSmsToAllUsers: ${req.body.messageText}`);
  UserSchema.find()
    .select('-__v')
    .execAsync()
    .then(async (users) => {
      if (!users) {
        returnObj.message = 'no of users';
        return res.send(returnObj);
      }

      const usersNumbers = [];
      for (const item of users) {
        if (item.phoneNo !== '9876543210') {
          usersNumbers.push(item.phoneNo);
        }
      }

      const smsStatuses = await sendGeneralSmsToAll(usersNumbers, req.body.messageText);

      returnObj.success = true;
      returnObj.message = 'Sms was tried to send';
      returnObj.data = { smsStatuses };

      return res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(
        `error occured while sendSmsToAll ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside sendSmsToAll records`);
      next(err);
    });
}

function sendEmailToAll(req, res, next) {
  const returnObj = {
    data: [],
    success: false,
    message: 'cannot send email'
  };

  debug(`sendEmailToAllUsers: ${req.body.messageText}`);
  UserSchema.find()
    .select('-__v')
    .execAsync()
    .then(async (users) => {
      if (!users) {
        returnObj.message = 'no users';
        return res.send(returnObj);
      }

      for (const user of users) {
        sendMail(user.email, 'Cabdo notification', req.body.messageText);
      }

      returnObj.success = true;
      returnObj.message = 'Email was tried to send';
      returnObj.data = {};

      return res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(
        `error occured while sendEmailToAll ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside sendEmailToAll records`);
      next(err);
    });
}

async function sendGeneralSmsToAll(usersNumbers, message) {
  if (config.env === 'test' || config.env === 'development') {
    return { status: true };
  }
  return await smsEveryOneSec(0, usersNumbers, message);
}

function smsEveryOneSec(i, usersNumbers, message) {
  if (i < usersNumbers.length) {
    twilioClient.messages.create({
      body: message,
      to: usersNumbers[i],
      from: config.fromTelNumber
    }).then(result => {
      return result;
    }).catch(() => {
      return { status: false };
    });
    i++;
    if (i % 10 === 0) {
      const timer = setTimeout(() => {
        smsEveryOneSec(i, usersNumbers, message);
        clearTimeout(timer);
      }, 1000);
    } else {
      smsEveryOneSec(i, usersNumbers, message);
    }
  }
}

function getUsersDetails(req, res, next) {
  const userId = req.params.userId;
  const returnObj = {
    success: false,
    message: 'user Id is not defined',
    data: null,
  };
  if (userId) {
    UserSchema.findByIdAsync(userId)
      .then((userData) => {
        if (userData) {
          returnObj.success = true;
          returnObj.message = 'user found and its corresponding details';
          returnObj.data = userData;
        } else {
          returnObj.success = false;
          returnObj.message = 'user not found with the given id';
          returnObj.data = null;
        }
        res.send(returnObj);
      })
      .error((e) => {
        const err = new APIError(`Error occured while findind the user details ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        next(err);
      });
  } else {
    res.send(returnObj);
  }
}

function updateUserDetails(req, res, next) {
  const userId = req.body._id;
  const updateUserObj = Object.assign({}, req.body);

  UserSchema.findOneAsync({ _id: userId })
    .then((userDoc) => {
      if (userDoc) {
        userDoc.fname = updateUserObj.fname ? updateUserObj.fname : userDoc.fname;
        userDoc.lname = updateUserObj.lname ? updateUserObj.lname : userDoc.lname;
        userDoc.phoneNo = updateUserObj.phoneNo ? updateUserObj.phoneNo : userDoc.phoneNo;
        userDoc.address = updateUserObj.address ? updateUserObj.address : userDoc.address;
        userDoc.city = updateUserObj.city ? updateUserObj.city : userDoc.city;
        userDoc.state = updateUserObj.state ? updateUserObj.state : userDoc.state;
        userDoc.country = updateUserObj.country ? updateUserObj.country : userDoc.country;
        userDoc.fullAddress = updateUserObj.fullAddress ? updateUserObj.fullAddress : userDoc.fullAddress;
        userDoc.gpsLoc = updateUserObj.gpsLoc ? updateUserObj.gpsLoc : userDoc.gpsLoc;
        const returnObj = {
          success: false,
          message: 'unable to find the object',
          data: null,
          meta: null
        };

        userDoc.saveAsync()
        .then((savedDoc) => {
          if (savedDoc.password) {
            debug(`inside password delete function`);
            savedDoc = savedDoc.toObject();
            delete savedDoc.password;
          }
          returnObj.success = true;
          returnObj.message = 'user document saved';
          returnObj.data = savedDoc;
          res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `Error occured while updating the user details ${e}`,
             httpStatus.INTERNAL_SERVER_ERROR
           );
          next(err);
        });
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while searching for the user ${e}`,
         httpStatus.INTERNAL_SERVER_ERROR
       );
      next(err);
    });
}

function userStats(req, res, next) {
  const returnObj = {
    success: false,
    message: 'no data available',
    data: null
  };
  UserSchema.aggregateAsync([
    { $match: { $or: [{ userType: 'driver' }, { userType: 'rider' }] } },
    { $group: {
      _id: 'riderDriverRatio',
      rider: { $sum: { $cond: [{ $eq: ['$userType', 'rider'] }, 1, 0] } },
      driver: { $sum: { $cond: [{ $eq: ['$userType', 'driver'] }, 1, 0] } },
      totalUser: { $sum: 1 }
    } }
  ])
  .then((userStatsData) => {
    returnObj.success = true;
    returnObj.message = 'user chart data';
    returnObj.data = userStatsData;
    return res.send(returnObj);
  })
  .error((e) => {
    const err = new APIError(`Error occurred while computing statistic for user ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
    next(err);
  });
}

// this function removes carDetails from the rider object and for driver object add car details a object
function transformReturnObj(userData) {
  for (let i = 0; i < userData.length; i++) {
    if (userData[i].userType === 'rider' && userData[i].carDetails) {
      delete userData[i].carDetails;
    }
  }
  return userData;
}

function createNewUser(req, res, next) {
  const userData = Object.assign({}, req.body);
  UserSchema.findOneAsync({ email: userData.email, userType: userData.userType })
  .then((foundUser) => {
    const returnObj = {
      success: false,
      message: '',
      data: null,
    };
    if (foundUser !== null) {
      const err = new APIError('Email Id Already Exist', httpStatus.CONFLICT);
      return next(err);
    }

    const userObj = new UserSchema({
      email: userData.email,
      password: userData.password,
      userType: userData.userType,
      fname: userData.fname,
      lname: userData.lname,
      phoneNo: userData.phoneNo,
      fullAddress: userData.fullAddress,
      address: userData.address,
      city: userData.city,
      state: userData.state,
      country: userData.country,
      gpsLoc: userData.gpsLoc || [19.02172902354515, 72.85368273308545],
      carDetails: userData.userType === 'driver' ? [{ type: 'sedan' }] : [],
      loginStatus: true
    });

    userObj.saveAsync()
    .then((savedUser) => {
      returnObj.success = true;
      returnObj.message = 'user created successfully';
      returnObj.data = savedUser;
      return res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(`Error while Creating new User ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    });
  })
  .error((e) => {
    const err = new APIError(`Error while Searching the user ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
    return next(err);
  });
}

function removeUser(req, res, next) {
  const userId = req.body._id;

  UserSchema.findOneAsync({ _id: userId })
    .then((userDoc) => {
      if (userDoc) {
        const returnObj = {
          success: false,
          message: 'unable to find the object',
          data: null,
          meta: null
        };

        userDoc.removeAsync()
          .then((removedDoc) => {
            returnObj.success = true;
            returnObj.message = 'user document removed';
            returnObj.data = removedDoc;
            res.send(returnObj);
          })
          .error((e) => {
            const err = new APIError(
              `Error occured while removing the user details ${e}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            next(err);
          });
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while searching for the user ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

export default { getAllUsers, getAllDrivers, getUsersDetails, updateUserDetails, userStats, createNewUser, removeUser, sendSmsToAll, sendEmailToAll, getFilteredUsers };
