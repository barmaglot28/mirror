import httpStatus from 'http-status';
import Moment from 'moment';
import moment from 'moment';
import Trip from '../models/trip';
import User from '../models/user';
import EventService from '../service/event-service';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import twilioClient from '../../config/env/twilioClient';
import phones from '../../config/env/phonesDB';
import Utils from '../helpers/utils';
import request from 'request';

const debug = require('debug')('Taxi-app-backend-web-dashboard: corporate-trip');
// const ObjectId = mongoose.Schema.Types.ObjectId;

// this function rename properties: riderId -> rider & driverId -> driver
function transformResponse(listing) {
  listing.forEach((item) => {
    item.setValue('rider', item.riderId);
    item.setValue('riderId', undefined);

    item.setValue('driver', item.driverId);
    item.setValue('driverId', undefined);
  });
  return listing;
}

async function getDistance(origin, destination) {
  return new Promise((resolve) =>
    request.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${encodeURI(origin)}&destinations=${encodeURI(destination)}&key=${config.googleAPIkey}`, (err, resp, body) => {
      if (err || resp.status !== 200) {
        return resolve(NaN);
      }
      body = JSON.parse(body);
      if (body.status === 'OK' && body.rows[0].elements[0].distance) {
        return resolve(body.rows[0].elements[0].distance);
      }
      resolve(NaN);
    })
  );
}

/**
 * Emits event that trigger socket listener to create the trip
 * -
 * @param req
 * @param res
 * @param next
 */
function create(req, res, next) {
  User.findOneAsync({
    _id: req.params.riderId,
    userType: 'rider',
    corporateUserId: req.user._id
  })
    .then(async (foundUser) => {
      if (foundUser === null) {
        const err = new APIError('user not found', httpStatus.NOT_FOUND);
        return next(err);
      }

      const payload = Object.assign({}, {
        trip: req.body.trip,
        carCategory: req.body.carCategory,
        corporateUserId: req.user._id
      }, { rider: foundUser });

      // adapt to format that mobile does
      payload.trip.srcLoc = [payload.trip.srcLoc[1], payload.trip.srcLoc[0]];
      payload.trip.isMetered = !(payload.trip.taxiType === 'CabdoBlack');
      if (payload.trip.destLoc) {
        const origin = `${payload.trip.srcLoc[0]},${payload.trip.srcLoc[1]}`;
        const destination = payload.trip.destLoc ? `${payload.trip.destLoc[0]},${payload.trip.destLoc[1]}` : null;
        payload.trip.distance = destination ? await getDistance(origin, destination) : null;
      }


      // call socket handlers that create trip request
      if (payload.trip.preBookedTime) {
        EventService.emit('corporateRequestBookingTrip', payload);
      } else {
        EventService.emit('corporateRequestTrip', payload);
      }

      const smsStatuses = await sendSmsFromPortal(payload);
      console.log('[create corporate trip] sendSms()', smsStatuses);

      res.send({
        success: true,
        message: 'The request for a trip is in processing',
        smsStatuses: { smsStatuses }
      });
    })
    .error((e) => {
      const err = new APIError(`Error while Searching the user ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    });
}

function sendSmsFromPortal(data) {
  if (config.env === 'test' || config.env === 'development') {
    return { status: true };
  }

  let smsBody;
  let fullName = '';
  if (data.rider.fname && data.rider.lname) {
    fullName = fullName.concat(` ${data.rider.fname}, ${data.rider.lname}`);
  }
  if (data.trip.preBookedTime) {
    smsBody = `Vorbestellung:${fullName} von ${data.trip.pickUpAddress}`;
    smsBody = smsBody.concat(` am ${moment(data.trip.preBookedTime).local().format('Do MMM [um] HH:mm')}`);
    if (data.trip.destAddress) {
      smsBody = smsBody.concat(` zu ${data.trip.destAddress}`);
      smsBody = smsBody.concat(`. ${data.trip.tripAmt.toFixed(2).toString().replace('.', ',')} €`);
    }
  } else {
    smsBody = `Jetzt Fahren:${fullName} von ${data.trip.pickUpAddress}`;
    if (data.trip.destAddress) {
      smsBody = smsBody.concat(` zu ${data.trip.destAddress}`);
      smsBody = smsBody.concat(`. ${data.trip.tripAmt.toFixed(2).toString().replace('.', ',')} €`);
    }
  }
  smsBody = smsBody.concat(`. Kategorie: ${data.carCategory.name}`);
  const retArray = phones.map((item) => {
    return twilioClient.messages.create({
      body: smsBody,
      to: item,
      from: config.fromTelNumber
    }).then(result => {
      return result;
    }).catch(() => {
      return { status: false };
    });
  });
  return retArray;
}

/**
 * fetch Trip Requests (booked, active or completed) that belongs to current Corporate user
 * -
 * @param req
 * @param res
 * @param next
 */
function list(req, res, next) {
  const statuses = [];
  const corporateUserId = req.user._id;
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);

  switch (req.query.status) {
    case 'booked':
      statuses.push('booked');
      break;
    case 'active':
      statuses.push('request', 'enRoute', 'arriving', 'arrived', 'onTrip');
      break;
    case 'completed':
      statuses.push('endTrip');
      break;
    default:
      statuses.push('booked', 'request', 'enRoute', 'arriving', 'arrived', 'onTrip', 'endTrip');
  }
  const filter = {
    corporateUserId,
    tripStatus: { $in: statuses }
  };

  Trip.countAsync(filter)
    .then((totalTripRecord) => {
      const returnObj = {
        success: true,
        message: 'count of trips are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalTripRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };

      if (totalTripRecord < 1) {
        return res.send(returnObj);
      }

      if (skip > totalTripRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }

      Trip.find(filter)
        .sort({ _id: -1 })
        .skip(skip || 0)
        .limit(limit || 20)
        .populate('riderId', 'fname lname')
        .populate('driverId', 'fname lname')
        .select('-__v')
        .execAsync()
        .then((tripList) => {
          returnObj.data = transformResponse(tripList);
          returnObj.message = 'trips found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`count of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting trip data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside Trip.list when getting trip request data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of trip requests ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside Trip.countAsync records`);
      next(err);
    });
}

/**
 * Emits event that trigger socket listener to update the trip request  status
 * -
 * @param req
 * @param res
 * @param next
 */
function update(req, res, next) {
  Trip.findOneAsync({
    _id: req.params.tripId,
    corporateUserId: req.user._id
  })
    .then((trip) => {
      if (trip === null) {
        const err = new APIError('trip not found', httpStatus.NOT_FOUND);
        return next(err);
      }

      // trip status changed ?
      if (trip.tripStatus === req.body.trip) {
        const returnObj = {
          success: true,
          message: 'trip already up-to-date',
          data: trip,
          meta: null
        };
        return res.send(returnObj);
      }

      const payload = Object.assign({}, {
        _id: trip._id,
        riderId: trip.riderId,
        driverId: trip.driverId,
        tripStatus: req.body.tripStatus
      });

      // call socket handlers that update trip
      EventService.emit('corporateUpdateTrip', payload, (e, data) => {
        if (e) {
          const err = new APIError(`Error while Updating the trip ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
          return next(err);
        }
        const returnObj = {
          success: true,
          message: 'trip updated',
          data,
          meta: null
        };
        res.send(returnObj);
      });
    })
    .catch((e) => {
      const err = new APIError(`Error while Searching the trip ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    });
}

/**
 * Remove trip by id
 * -
 * @param req
 * @param res
 * @param next
 */
function remove(req, res, next) {
  Trip.findByIdAndRemoveAsync({
    _id: req.params.tripId,
    corporateUserId: req.user._id,
    tripStatus: 'booked'
  })
    .then((trip) => {
      if (trip) {
        const returnObj = {
          success: true,
          message: 'trip request deleted',
          data: null,
          meta: null
        };
        res.send(returnObj);
      } else {
        const err = new APIError('trip request not found', httpStatus.NOT_FOUND);
        return next(err);
      }
    })
    .catch((e) => {
      const err = new APIError(`Error while Searching the trip request ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    });
}

function exportCompleted(req, res, next) {
  const filter = {
    tripStatus: 'endTrip',
    corporateUserId: req.user._id,
    endTime: { $ne: null }
  };

  if (req.body.startRange) {
    filter.endTime.$gte = req.body.startRange;
  }
  if (req.body.endRange) {
    filter.endTime.$lte = req.body.endRange;
  }

  Trip.find(filter)
    .sort({ _id: -1 })
    .populate('riderId', 'fname lname')
    .populate('driverId', 'fname lname')
    .populate('corporateUserId', 'fname lname')
    .select('-__v')
    .execAsync()
    .then((tripData) => {
      const output = [];
      tripData.forEach(trip => {
        output.push({
          id: trip._id,

          bookingTime: Moment.utc(trip.bookingTime).zone(req.body.zone === null ? 0 : req.body.zone).toString(),
          endTime: Moment.utc(trip.endTime).zone(req.body.zone === null ? 0 : req.body.zone).toString(),

          tripAmount: trip.tripAmt,
          paymentMode: trip.paymentMode,
          pickUpAddress: trip.pickUpAddress,
          destAddress: trip.destAddress,
          driver: Utils.getName(trip.driverId),
          rider: Utils.getName(trip.riderId)
        });
      });
      if (output.length === 0) {
        output.push({ result: 'there are no completed trips' });
      }
      res.xls('completed-trips.xlsx', output);
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while fetching corporate trip records ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

export default { create, list, update, remove, exportCompleted };
