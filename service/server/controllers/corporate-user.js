import UserSchema from '../models/user';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
const debug = require('debug')('Taxi-app-backend-web-dashboard: corporate-user');

// this function removes carDetails from the rider object and for driver object add car details a object
function transformReturnObj(userData) {
  for (let i = 0; i < userData.length; i++) {
    if (userData[i].userType === 'rider' && userData[i].carDetails) {
      delete userData[i].carDetails;
    }
  }
  return userData;
}

/**
 * Creates new one-trip-rider.
 * Auto email format - email+timestamp@corp.com
 * Auto password format - timestamp
 * -
 * @param req
 * @param res
 * @param next
 */
async function create(req, res) {
  const returnObj = await createCorporate(req.user, req.body);
  return res.send(returnObj);
}

export function createCorporate(userPool, user) {
  console.log('createCorporate userPool', JSON.stringify(userPool));
  console.log('createCorporate user', JSON.stringify(user));
  const timestamp = parseInt(new Date().getTime() / 1000, 10);

  const userData = Object.assign({}, user, {
    email: `${userPool.email.split('@')[0]}+${timestamp}@${userPool.email.split('@')[1]}`,
    password: timestamp,
    userType: 'rider',
    corporateUserId: userPool._id,
    phoneNo: userPool.phoneNo // get corporate number
  });

  const returnObj = {
    success: false,
    message: '',
    data: null,
  };

  return new Promise((resolve, reject) => {
    UserSchema.findOneAsync({ email: userData.email, userType: 'rider' })
      .then((foundUser) => {
        if (foundUser !== null) {
          const err = new APIError('Email Id Already Exist', httpStatus.CONFLICT);
          reject(err);
        }
        const userObj = new UserSchema({
          email: userData.email,
          password: userData.password,
          userType: userData.userType,
          fname: userData.fname,
          lname: userData.lname,
          phoneNo: userData.phoneNo,
          gpsLoc: userPool.gpsLoc,
          carDetails: null,
          loginStatus: true,
          corporateUserId: userData.corporateUserId
        });
        console.log('createCorporate userObj', JSON.stringify(userObj));
        return userObj.saveAsync();
      })
      .then((savedUser) => {
        console.log('createCorporate savedUser', JSON.stringify(savedUser));
        returnObj.success = true;
        returnObj.message = 'user created successfully';
        returnObj.data = savedUser;
        resolve(returnObj);
        return returnObj;
      })
      .catch((e) => {
        const err = new APIError(`Error while Creating new User ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        reject(err);
        return returnObj;
      })
      .then(() => {
        resolve(returnObj);
        return returnObj;
      });
  });
}

/**
 * fetch all Riders that belongs to current Corporate user
 * -
 * @param req
 * @param res
 * @param next
 */
function list(req, res, next) {
  const corporateUserId = req.user._id;
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);

  UserSchema.countAsync({ corporateUserId })
    .then((totalUserRecord) => {
      const returnObj = {
        success: true,
        message: 'no of user are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalUserRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };

      if (totalUserRecord < 1) {
        return res.send(returnObj);
      }

      if (skip > totalUserRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }

      UserSchema.list({ skip, limit, corporateUserId })
        .then((userData) => {
          returnObj.data = transformReturnObj(userData);
          returnObj.message = 'user found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting user data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllUsers when getting user data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of users ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllUsers records`);
      next(err);
    });
}

export default { create, list };
