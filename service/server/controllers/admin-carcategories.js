import CarCategorySchema from '../models/car-category';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
const debug = require('debug')('Taxi-app-backend-web-dashboard: admin-carcategories');


function getAllCarcategories(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo;
  const skip = pageNo ? ((pageNo - 1) * limit) : config.skip;
  debug(`skip value: ${req.query.pageNo}`);
  CarCategorySchema.countAsync()
    .then((totalCarcategoriesRecord) => {
      const returnObj = {
        success: true,
        message: 'no of carcategories are zero',
        data: null,
        meta: {
          totalNoOfPages: Math.ceil(totalCarcategoriesRecord / limit),
          limit,
          currPageNo: pageNo,
          currNoOfRecord: 20,
        }
      };
      if (totalCarcategoriesRecord < 1) {
        return res.send(returnObj);
      }
      if (skip > totalCarcategoriesRecord) {
        const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
        return next(err);
      }
      CarCategorySchema.list({ skip, limit })
        .then((carcategoriesData) => {
          returnObj.data = transformReturnObj(carcategoriesData);
          returnObj.message = 'carcategories found';
          returnObj.meta.currNoOfRecord = returnObj.data.length;
          debug(`no of records are ${returnObj.meta.currNoOfRecord}`);
          return res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `error occured while getting carcategories data ${e}`,
            httpStatus.INTERNAL_SERVER_ERROR
          );
          debug(`error inside getAllCarcategories when getting carcategories data ${e}`);
          next(err);
        });
    })
    .error((e) => {
      const err = new APIError(
        `error occured while counting the no of carcategories ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      debug(`error inside getAllCarcategories records`);
      next(err);
    });
}


function getCarcategoriesDetails(req, res, next) {
  const carcategoriesId = req.params.carcategoriesId;
  const returnObj = {
    success: false,
    message: 'carcategories Id is not defined',
    data: null,
  };
  if (carcategoriesId) {
    CarCategorySchema.findByIdAsync(carcategoriesId)
      .then((carcategoriesData) => {
        if (carcategoriesData) {
          returnObj.success = true;
          returnObj.message = 'carcategories found and its corresponding details';
          returnObj.data = carcategoriesData;
        } else {
          returnObj.success = false;
          returnObj.message = 'carcategories not found with the given id';
          returnObj.data = null;
        }
        res.send(returnObj);
      })
      .error((e) => {
        const err = new APIError(`Error occured while findind the carcategories details ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        next(err);
      });
  } else {
    res.send(returnObj);
  }
}

function updateCarcategoriesDetails(req, res, next) {
  const carcategoriesId = req.body._id;
  const updateCarcategoriesObj = Object.assign({}, req.body);

  CarCategorySchema.findOneAsync({ _id: carcategoriesId })
    .then((carcategoriesDoc) => {
      if (carcategoriesDoc) {
        carcategoriesDoc.name = updateCarcategoriesObj.name ? updateCarcategoriesObj.name : carcategoriesDoc.name;
        carcategoriesDoc.isMetered = updateCarcategoriesObj.isMetered || false;
        carcategoriesDoc.type = updateCarcategoriesObj.type;
        carcategoriesDoc.pricePerKilometer = updateCarcategoriesObj.pricePerKilometer ? updateCarcategoriesObj.pricePerKilometer : carcategoriesDoc.pricePerKilometer;
        carcategoriesDoc.pricePerMinute = updateCarcategoriesObj.pricePerMinute ? updateCarcategoriesObj.pricePerMinute : carcategoriesDoc.pricePerMinute;
        carcategoriesDoc.baseFare = updateCarcategoriesObj.baseFare ? updateCarcategoriesObj.baseFare : carcategoriesDoc.baseFare;
        carcategoriesDoc.minimumFare = updateCarcategoriesObj.minimumFare ? updateCarcategoriesObj.minimumFare : carcategoriesDoc.minimumFare;
        carcategoriesDoc.commissionToCabdo = updateCarcategoriesObj.commissionToCabdo ? updateCarcategoriesObj.commissionToCabdo : carcategoriesDoc.commissionToCabdo;
        carcategoriesDoc.capacity = updateCarcategoriesObj.capacity ? updateCarcategoriesObj.capacity : carcategoriesDoc.capacity;
        const returnObj = {
          success: false,
          message: 'unable to find the object',
          data: null,
          meta: null
        };

        carcategoriesDoc.saveAsync()
        .then((savedDoc) => {
          returnObj.success = true;
          returnObj.message = 'carcategories document saved';
          returnObj.data = savedDoc;
          res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(
            `Error occured while updating the carcategories details ${e}`,
             httpStatus.INTERNAL_SERVER_ERROR
           );
          next(err);
        });
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while searching for the carcategories ${e}`,
         httpStatus.INTERNAL_SERVER_ERROR
       );
      next(err);
    });
}

function removeCarcategories(req, res, next) {
  const carcategoriesId = req.body._id;

  CarCategorySchema.findOneAsync({ _id: carcategoriesId })
    .then((carcategoriesDoc) => {
      if (carcategoriesDoc) {
        const returnObj = {
          success: false,
          message: 'unable to find the object',
          data: null,
          meta: null
        };

        carcategoriesDoc.removeAsync()
          .then((removedDoc) => {
            returnObj.success = true;
            returnObj.message = 'carcategories document removed';
            returnObj.data = removedDoc;
            res.send(returnObj);
          })
          .error((e) => {
            const err = new APIError(
              `Error occured while removing the carcategories details ${e}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            next(err);
          });
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while searching for the carcategories ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

// this function removes carDetails from the rider object and for driver object add car details a object
function transformReturnObj(carcategoriesData) {
  for (let i = 0; i < carcategoriesData.length; i++) {
    if (carcategoriesData[i].carDetails) {
      delete carcategoriesData[i].carDetails;
    }
  }
  return carcategoriesData;
}

function createNewCarcategories(req, res, next) {
  const carcategoriesData = Object.assign({}, req.body);
  let isFullCopyCategory = true;
  CarCategorySchema.findOneAsync({ name: carcategoriesData.name })
  .then((foundCarcategories) => {
    const returnObj = {
      success: false,
      message: '',
      data: null,
    };

    if (foundCarcategories) {
      for (const key in carcategoriesData) {
        if (carcategoriesData[key] !== foundCarcategories[key]) {
          foundCarcategories[key] = carcategoriesData[key];
          isFullCopyCategory = false;
        }
      }
      if (isFullCopyCategory) {
        const err = new APIError('Category with same name already exists', httpStatus.CONFLICT);
        return next(err);
      }
    }

    if (!foundCarcategories) {
      foundCarcategories = new CarCategorySchema(carcategoriesData);
    }

    foundCarcategories.saveAsync()
      .then((savedCarcategories) => {
        returnObj.success = true;
        returnObj.message = 'carcategories updated successfully';
        returnObj.data = savedCarcategories;
        return res.send(returnObj);
      })
      .error((e) => {
        const err = new APIError(`Error while updating carcategories ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        return next(err);
      });
  })
  .error((e) => {
    const err = new APIError(`Error while Searching the carcategories ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
    return next(err);
  });
}

export default { getAllCarcategories, getCarcategoriesDetails, updateCarcategoriesDetails, createNewCarcategories, removeCarcategories };
