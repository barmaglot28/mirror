import User from '../models/user';
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import Wallet from '../models/wallet';
import ReferralCode from '../models/referral-code';
import ReferralCodeAmountSchema from '../models/referral-code-amount';
import UserAvatar from '../models/user-avatar';
import I18n from '../i18n/i18n';
import twilioClient from '../../config/env/twilioClient';
import config from '../../config/env';

import { format } from 'util';

const helper = require('sendgrid').mail;
const sg = require('sendgrid')(config.sendgridAPIkey);

/**
 * Get user
 * @returns {User}
 */
function get(req, res) {
  return res.send({ success: true, message: 'user found', data: req.user });
}
/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function create(req, res, next) {
  const returnObj = {
    success: true,
    SMSsuccess: false,
    message: '',
    data: {},
    referralCodeResult: {},
    walletResult: {},
  };
  User.findOneAsync({ $or: [{ email: req.body.email.toLowerCase() }, { phoneNo: req.body.phoneNo }], userType: req.body.userType })
    .then((foundUser) => {
      if (foundUser !== null) {
        User.findOneAndUpdateAsync({ _id: foundUser._id }, { $set: { loginStatus: true } }, { new: true })
          .then((updateUserObj) => {
            if (updateUserObj) {
              const jwtAccessToken = jwt.sign(updateUserObj, config.jwtSecret);
              returnObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
              returnObj.data.user = updateUserObj;
              returnObj.data.maxDistance = config.maxDistance;
              returnObj.message = (foundUser.email === req.body.email) ? 'email' : 'phone';
              returnObj.success = false;
              I18n.locale = updateUserObj.language;
              return res.send(returnObj);
            }
          })
          .error((e) => {
            const err = new APIError(`error in updating user details while login ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
      } else {
        const user = new User({
          email: req.body.email.toLowerCase(),
          password: req.body.password,
          userType: req.body.userType,
          fname: req.body.fname,
          lname: req.body.lname,
          phoneNo: req.body.phoneNo,
          gpsLoc: [19.02172902354515, 72.85368273308545],
          loginStatus: true,
          verifyCode: Math.floor((Math.random() * 999) + 1111),
        });
        user.saveAsync()
          .then(async (savedUser) => {
            const requiredSms = req.body.requireSms === undefined;
            const smsStatus = await sendSms(savedUser, requiredSms);
            const jwtAccessToken = jwt.sign(user, config.jwtSecret);
            const referralResult = await createReferralCode(savedUser, req.body.referralCode);
            const walletResult = await createWallet(savedUser, referralResult.referralCodeIsValid);
            returnObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
            returnObj.data.user = user;
            returnObj.data.maxDistance = config.maxDistance;
            returnObj.message = 'user created successfully';
            returnObj.referralCodeResult = referralResult;
            returnObj.walletResult = walletResult;
            returnObj.SMSsuccess = smsStatus && smsStatus.status;
            res.send(returnObj);
          })
          .error((e) => next(e));
      }
    });
}

function createReferralCode(user, referralCodeFromInviter) {
  const returnObj = {
    success: true,
    referralCodeIsValid: true,
    message: 'Successfully created',
  };
  return new Promise(async (resolve) => {
    const inviterCodeObj = await ReferralCode.findOneAsync({ code: referralCodeFromInviter });
    if (!inviterCodeObj || user.userType === 'driver') {
      const code = ReferralCode.generate();
      const referralCode = new ReferralCode({
        code,
        userId: user._id,
        inviterCodeActive: false,
      });
      const savedObj = await referralCode.saveAsync();
      if (savedObj) {
        returnObj.success = true;
        returnObj.referralCodeIsValid = false;
        returnObj.message = referralCodeFromInviter ? 'invalidCode' : 'emptyCode';
        resolve(returnObj);
      }
    } else {
      const code = ReferralCode.generate();
      const referralCode = new ReferralCode({
        code,
        userId: user._id,
        inviterId: inviterCodeObj.userId,
        codeFromInviter: referralCodeFromInviter,
        inviterCodeActive: true,
      });
      const savedObj = await referralCode.saveAsync();
      if (savedObj) {
        resolve(returnObj);
      }
    }
  });
}

function createWallet(user, referralCodeIsValid) {
  const returnObj = {
    success: true,
    bonusAdded: 0,
    message: 'Successfully created',
  };
  return new Promise((resolve) => {
    Wallet.findOneAsync({ userEmail: user.email }).then(async (wallet) => {
      if (!wallet) {
        let walletBalance = 5;
        const amount = await ReferralCodeAmountSchema.findOneAsync();
        if (amount && referralCodeIsValid) {
          walletBalance = amount.amountToRecipientWallet;
        }
        const walletObj = new Wallet({
          walletBalance,
          userId: user._id,
          userType: user.userType,
          userEmail: user.email,
        });
        const savedWalletObj = await walletObj.saveAsync();
        if (savedWalletObj) {
          returnObj.bonusAdded = savedWalletObj.walletBalance;
          resolve(returnObj);
        }
      } else {
        returnObj.success = false;
        returnObj.message = 'duplicatedEmail';
        resolve(returnObj);
      }
    });
  });
}
/**
 * Update existing user
 * @property {Object} req.body.user - user object containing all fields.
 * @returns {User}
 */
function update(req, res, next) {
  const user = req.user;
  user.fname = req.body.fname ? req.body.fname : user.fname;
  user.lname = req.body.lname ? req.body.lname : user.lname;
  user.email = req.body.email ? req.body.email : user.email;
  user.phoneNo = req.body.phoneNo ? req.body.phoneNo : user.phoneNo;
  user.deviceId = req.body.deviceId ? req.body.deviceId : user.deviceId;
  user.pushToken = req.body.pushToken ? req.body.pushToken : user.deviceId;
  user.tokenId = req.body.tokenId ? req.body.tokenId : user.tokenId;
  user.language = req.body.language ? req.body.language : user.language;
  if (req.body.language) {
    I18n.locale = req.body.language;
  }
  user.saveAsync()
    .then((savedUser) => {
      const returnObj = {
        success: true,
        message: 'user details updated successfully',
        data: savedUser
      };
      res.send(returnObj);
    })
    .error((e) => next(e));
}

/**
 * Send sms. Local function
 */
function sendSms(user, requireSms = true) {
  if (config.env === 'test' || config.env === 'development' || requireSms === false) {
    return { status: true };
  }

  return twilioClient.messages.create({
    body: `Dein Cabdo Verifizierungscode lautet: ${user.verifyCode}`,
    to: user.phoneNo,
    from: config.fromTelNumber
  }).then(result => {
    return result;
  }).catch(() => {
    return { status: false };
  });
}

/**
 * Resend sms. Route function
 * @returns {User}
 */

function resendSms(req, res, next) {
  const user = req.user;
  user.verifyCode = Math.floor((Math.random() * 999) + 1111);

  const returnObj = {
    success: false,
    SMSsuccess: false,
    message: '',
    data: {}
  };

  user.saveAsync()
    .then((savedUser) => {
      returnObj.success = true;
      returnObj.data = savedUser;
      return sendSms(savedUser);
    })
    .then((smsStatus) => {
      if (smsStatus && smsStatus.status) {
        returnObj.SMSsuccess = true;
        returnObj.message = 'sms successfully sent.';
      } else {
        returnObj.message = 'sms failed';
      }
      res.send(returnObj);
    })
    .error((e) => next(e));
}

/**
 * Update user phone number. Route function
 * @property {number} req.body.phoneNo - phone number for update
 * @returns {User}
 */

function updatePhone(req, res, next) {
  const user = req.user;
  user.phoneNo = req.body.phoneNo;

  const returnObj = {
    success: false,
    message: '',
    data: {}
  };

  user.saveAsync()
    .then((savedUser) => {
      returnObj.success = true;
      returnObj.message = '[updatePhone] user phone updated successfully';
      returnObj.data = savedUser;

      res.send(returnObj);
    })
    .error((e) => next(e));
}

/**
 * Update verify status. Route function
 * @returns {User}
 */

function changeVerifyStatus(req, res, next) {
  const user = req.user;
  user.verified = true;

  const returnObj = {
    success: false,
    message: '',
    data: {}
  };

  user.saveAsync()
    .then((savedUser) => {
      returnObj.success = true;
      returnObj.message = '[updatePhone] user phone updated successfully';
      returnObj.data = savedUser;

      res.send(returnObj);
    })
    .error((e) => next(e));
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.user;
  user.removeAsync()
    .then((deletedUser) => {
      const returnObj = {
        success: true,
        message: 'user deleted successfully',
        data: deletedUser
      };
      res.send(returnObj);
    })
    .error((e) => next(e));
}
/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  User.get(id).then((user) => {
    req.user = user; // eslint-disable-line no-param-reassign
    return next();
  }).error((e) => next(e));
}

/**
 * Update user avatar by base64.
 * @property {string} req.body.base64
 */
function updateAvatar(req, res, next) {
  const user = req.user;
  if (req.body.base64) {
    UserAvatar.findOneAsync({ userId: user._id })
      .then((avatarObjRet) => {
        const base64 = req.body.base64.toString('base64');
        const avatar = new Buffer(base64, 'base64');
        let avatarObj;
        if (avatarObjRet) {
          avatarObj = avatarObjRet;
          avatarObj.base64 = avatar;
        } else {
          avatarObj = new UserAvatar({
            base64: avatar,
            userId: user._id
          });
        }
        avatarObj.saveAsync()
          .then(() => {
            user.avatar = avatarObj._id;
            user.saveAsync()
              .then((savedUser) => {
                const returnObj = {
                  success: true,
                  message: 'user avatar updated successfully',
                  data: savedUser
                };
                res.send(returnObj);
              })
              .error((e) => next(e));
          })
          .catch((e) => next(e));
      })
      .catch((e) => {
        next(e);
      });
  } else {
    UserAvatar.deleteOneAsync({ userId: user._id })
      .then(() => {
        user.avatar = null;
        user.saveAsync()
          .then((savedUser) => {
            const returnObj = {
              success: true,
              message: 'user avatar updated successfully',
              data: savedUser
            };
            res.send(returnObj);
          })
          .error((e) => next(e));
      })
      .catch((e) => next(e));
  }
}

/**
 * Update existing driver's availability
 * @property {boolean} req.body.availability
 * @returns {User}
 */
function changeDriverAvailability(req, res, next) {
  const user = req.user;
  const availability = req.body.availability;
  User.findOneAndUpdateAsync({ _id: user._id }, { $set: { availability } }, { new: true })
    .then(updatedUserObj => {
      const returnObj = {
        success: true,
        message: 'user availability updated successfully',
        data: updatedUserObj
      };
      res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(`error in updating user availability ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      next(err);
    });
}

function generateRandomPassword() {
  return Math.random().toString(36).slice(-8);
}

function passwordRecovery(req, res) {
  User.findOneAsync({ email: req.body.email })
    .then(foundUser => {
      if (foundUser !== null) {
        const newPassword = generateRandomPassword();

        const emailText = format(req.body.emailText, newPassword);
        foundUser.password = newPassword;
        foundUser.save(err => {
          if (err) {
            console.error(err);
          }

          res.send(!err);
        });
        sendMail(req.body.email, req.body.emailSubject, emailText);
      } else {
        res.send(false);
      }
    })
    .error(e => {
      console.error(e);
      res.send(false);
    });
}

function sendMail(email, subject, content) {
  return new Promise((resolve, reject) => {
    const fromEmail = new helper.Email('service@cabdo.de');
    const toEmail = new helper.Email(email);
    const text = new helper.Content('text/plain', content);
    const mail = new helper.Mail(fromEmail, subject, toEmail, text);
    const request = sg.emptyRequest({
      method: 'POST',
      path: '/v3/mail/send',
      body: mail.toJSON()
    });

    sg.API(request, (error) => {
      if (error) {
        reject(error);
        return;
      }

      resolve();
    });
  });
}

function changePass(req, res, next) {
  let returnObj = {
    success: false,
    message: '',
  };
  User.findOneAsync(req.user, '+password')
    .then(foundUser => {
      if (!foundUser) {
        return next(new APIError('User not found', httpStatus.NOT_FOUND));
      }
      foundUser.comparePassword(req.body.oldPass, (passwordError, isMatch) => {
        if (passwordError || !isMatch) {
          returnObj.message = 'wrong password';
          res.send(returnObj);
        } else {
          foundUser.password = req.body.newPass;
          foundUser.save(err => {
            if (err) {
              console.error(err);
            }
            returnObj = {
              success: true,
              message: 'change password success',
            };
            res.send(returnObj);
          });
        }
      });
    })
    .error(e => {
      console.error(e);
      res.send(false);
    });
}


function feedback(req, res) {
  const retObj = {
    success: false,
    message: '',
    data: {}
  };
  User.findOneAsync({ email: req.user.email })
    .then(foundUser => {
      if (foundUser !== null) {
        const fromEmail = new helper.Email(req.user.email);
        const toEmail = new helper.Email('info@cabdo.de');
        const text = new helper.Content('text/plain', req.body.query);
        const mail = new helper.Mail(fromEmail, req.body.reason, toEmail, text);
        const request = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: mail.toJSON()
        });

        sg.API(request, (error) => {
          if (error) {
            res.send(false);
          }

          retObj.success = true;
          retObj.message = 'feedback sent successful';
          res.send(retObj);
        });
      } else {
        res.send(false);
      }
    })
    .error(e => {
      console.error(e);
      res.send(false);
    });
}

function hidePermanentBadge(req, res) {
  const retObj = {
    success: false,
    message: '',
    data: {}
  };
  User.findOneAndUpdateAsync({ _id: req.user._id }, { $set: { showPermanentBadge: false } }, { new: true })
    .then(updatedUser => {
      if (updatedUser !== null) {
        retObj.success = true;
        retObj.message = 'permanent badge updated successful';
        res.send(retObj);
      } else {
        res.send(false);
      }
    })
    .error(e => {
      console.error(e);
      res.send(false);
    });
}

export default { load, get, create, update, remove, changeDriverAvailability, updateAvatar, passwordRecovery, resendSms, updatePhone, changeVerifyStatus, changePass, feedback, hidePermanentBadge, sendMail };
