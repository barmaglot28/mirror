import UserSchema from '../models/user';
import TripSchema from '../models/trip';
import TripRequestSchema from '../models/trip';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import moment from 'moment';
import request from 'request';

function getTripsPerDay(req, res, next) {
  const filter = { bookingTime: req.body.time };
  const returnObj = {};
  TripSchema.find(
    {
      bookingTime: {
        $gte: moment(filter.bookingTime).startOf('day'),
        $lt: moment(filter.bookingTime).add(1, 'days')
      },
      tripStatus: { $nin: ['cancelled', 'noNearbyDriver'] }
    })
    .sort({ _id: -1 })
    .limit(250)
    .select('-__v')
    .populate('riderId driverId')
    .execAsync()
    .then((tripData) => {
      if (tripData.length) {
        for (let i = 0; i < tripData.length; i++) {
          tripData[i] = transformReturnObj(tripData[i]);
        }
        returnObj.success = true;
        returnObj.message = 'trip object retrieved';
        returnObj.data = tripData;
        res.send(returnObj);
      } else {
        returnObj.success = true;
        returnObj.message = 'no trip details available';
        res.send(returnObj);
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while retrieving trip object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

function tripPerDayRevenueGraph(req, res, next) {
  const currentISODate = new Date(req.params.date);
  const nextISODate = new Date(req.params.date);
  nextISODate.setHours(23, 59, 59, 59);
  const returnObj = {
    success: false,
    message: 'no of trips available',
    data: [],
    currentISODate
  };
  TripSchema.aggregateAsync([
    { $match: { endTime: { $gte: currentISODate, $lte: nextISODate } } },
    { $project: {
      date: '$endTime',
      endTime: '$endTime',
      tripAmt: '$tripAmt',
      tripStatus: '$tripStatus',
      paymentMode: '$paymentMode'
    } },
    { $match: { tripStatus: 'endTrip' } },
    {
      $group: {
        _id: {
          date: '$date',
          paymentMode: '$paymentMode'
        },
        tripAmt: { $sum: '$tripAmt' }
      }
    },
    {
      $project: {
        _id: null,
        date: '$_id.date',
        paymentMode: '$_id.paymentMode',
        tripAmt: '$tripAmt'
      }
    }
  ])
    .then((revenueGraphDocs) => {
      returnObj.success = true;
      returnObj.message = 'revenue graph for the trips';
      returnObj.data = revenueGraphDocs;
      res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while computing revenue graph ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

function exportToExcel(req, res, next) {
  const filter = { bookingTime: req.params.date };
  const returnObj = {};
  const rows = [[
    'ID',
    'Rider',
    'Created Date',
    'End Date',
    'Driver',
    'Pick-up Address',
    'Destination Address',
    'Distance',
    'Fare',
    'Payment',
    'Drivers Rating',
    'Trip Issue',
    'Trip Status',
    'Car number'
  ]];

  TripSchema.find(
    {
      bookingTime: {
        $gte: moment(moment(filter.bookingTime).subtract(2, 'weeks')).startOf('day'),
        $lt: moment(filter.bookingTime).add(1, 'days')
      },
      tripStatus: { $nin: ['cancelled', 'noNearbyDriver'] }
    })
    .sort({ _id: -1 })
    .populate('riderId driverId')
    .execAsync()
    .then(async (tripData) => {
      if (tripData.length) {
        for (let i = 0; i < tripData.length; i++) {
          tripData[i] = transformReturnObj(tripData[i]);
          const item = tripData[i];

          let distance = {};
          distance = item.distance && item.distance.text ? item.distance.text : 0;
          rows.push([
            item.tripNumber,
            item.rider ? `${item.rider.fname} ${item.rider.lname}` : '------',
            item.bookingTime ? moment(item.bookingTime).format('Do MMM [at] HH:mm') : '------',
            item.endTime ? moment(item.endTime).format('Do MMM [at] HH:mm') : '------',
            item.driver ? `${item.driver.fname} ${item.driver.lname}` : '------',
            item.pickUpAddress,
            item.destAddress,
            distance,
            (item.tripAmt ? item.tripAmt.toFixed(2) : 0.00),
            item.paymentMode,
            item.driverRatingByRider,
            item.tripIssue,
            item.tripStatus,
            item.carDetails && item.carDetails.licensePlateNumber !== 'AB-CD-1234' ? item.carDetails.licensePlateNumber : '------'
          ]);
        }
        res.xls(`${filter.bookingTime}.xlsx`, rows);
      } else {
        returnObj.success = true;
        returnObj.message = 'no trip details available';
        res.send(returnObj);
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while retrieving trip object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

async function getDistance(origin, destination) {
  return new Promise((resolve) =>
    request.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${encodeURI(origin)}&destinations=${encodeURI(destination)}&key=${config.googleAPIkey}`, (err, resp, body) => {
      if (err || resp.status !== 200) {
        return resolve(NaN);
      }
      body = JSON.parse(body);
      if (body.status === 'OK' && body.rows[0].elements[0].distance) {
        return resolve(body.rows[0].elements[0].distance);
      }
      resolve(NaN);
    })
  );
}

function tripPerDayGraph(req, res, next) {
  const currentISODate = new Date(req.params.date);
  const nextISODate = new Date(req.params.date);
  nextISODate.setHours(23, 59, 59, 59);
  const returnObj = {
    success: false,
    message: 'no of trips avaliable',
    data: [],
    currentISODate
  };
  TripSchema.aggregateAsync([
    { $match: { bookingTime: { $gte: currentISODate, $lte: nextISODate } } },
    {
      $project: {
        date: '$bookingTime',
        bookingTime: '$bookingTime',
        tripAmt: '$tripAmt',
        tripStatus: '$tripStatus',
        taxiType: '$taxiType'
      }
    },
    {
      $group: {
        _id: {
          date: '$date',
          taxiType: '$taxiType',
          tripStatus: '$tripStatus'
        },
        count: {
          $sum: 1
        }
      }
    },
    {
      $project: {
        _id: null,
        date: '$_id.date',
        taxiType: '$_id.taxiType',
        tripStatus: '$_id.tripStatus',
        count: '$count'
      }
    }
  ])
    .then((tripsGraphDocs) => {
      returnObj.success = true;
      returnObj.message = 'trips graph';
      returnObj.data = tripsGraphDocs;
      res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while computing trips graph ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}

function getTripsByUserPerDay(req, res, next) {
  const filter = { bookingTime: req.body.time, driverId: req.body.driverId };
  const returnObj = {};

  TripSchema.list({ dateFilter: filter })
    .then((tripData) => {
      if (tripData.length) {
        for (let i = 0; i < tripData.length; i++) {
          tripData[i] = transformReturnObj(tripData[i]);
        }
        returnObj.success = true;
        returnObj.message = 'trip object retrieved';
        returnObj.data = tripData;
      } else {
        returnObj.success = true;
        returnObj.message = 'no trip details available';
      }
      res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while retreiving trip object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}


function tripDetails(req, res, next) {
  const limit = req.query.limit ? req.query.limit : config.limit;
  const pageNo = req.query.pageNo ? req.query.pageNo : 1;
  const skip = pageNo ? (pageNo - 1) * limit : config.skip;
  const filter = req.query.filter ? req.query.filter : config.tripFilter;
  TripSchema.getCount(filter)
  .then((totalTripRecords) => {
    const returnObj = {
      success: false,
      message: 'no of trips are zero',
      data: null,
      meta: {
        totalNoOfPages: Math.ceil(totalTripRecords / limit),
        limit,
        currPageNo: pageNo,
        totalRecords: totalTripRecords.length
      }
    };
    if (totalTripRecords < 1) {
      returnObj.success = true;
      returnObj.data = [];
      returnObj.meta.totalNoOfPages = 0;
      returnObj.meta.limit = limit;
      returnObj.meta.currPageNo = 0;
      returnObj.meta.totalRecords = 0;
      return res.send(returnObj);
    }
    if (skip > totalTripRecords) {
      const err = new APIError('Request Page does not exists', httpStatus.NOT_FOUND);
      return next(err);
    }

    TripSchema.list({ skip, limit, filter })
    .then((tripData) => {
      if (tripData.length !== 0) {
        for (let i = 0; i < tripData.length; i++) {
          tripData[i] = transformReturnObj(tripData[i]);
        }
        returnObj.success = true;
        returnObj.message = 'trip object retrieved';
        returnObj.data = tripData;
      } else {
        returnObj.success = true;
        returnObj.message = 'no trip details available';
      }
      res.send(returnObj);
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while retreiving trip object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
  })
  .error((e) => {
    const err = new APIError(
      `Error occured while counting trip object ${e}`,
      httpStatus.INTERNAL_SERVER_ERROR
    );
    next(err);
  });
}


function createNewTrip(req, res, next) {
  const riderId = req.body.riderId;
  const driverId = req.body.driverId;
  UserSchema.findAsync({ $or: [{ $and: [{ userType: 'rider' }, { _id: riderId }] }, { $and: [{ userType: 'driver' }, { _id: driverId }] }] })
  .then(async (foundUserData) => {
    if (foundUserData.length !== 2) {
      const err = new APIError('rider or driver does not exist', httpStatus.BAD_REQUEST);
      return next(err);
    }

    if (foundUserData[0].currTripId !== null || foundUserData[1].currTripId !== null) {
      let errMsg = '';
      if (foundUserData[0].userType === 'rider' && foundUserData[0].currTripId === null) {
        errMsg += 'Rider is On Trip';
      }
      if (foundUserData[1].userType === 'driver' && foundUserData[1].currTripId === null) {
        errMsg += 'Driver is On Trip';
      }
      const err = new APIError(errMsg, httpStatus.BAD_REQUEST);
      return next(err);
    }

    const srcLoc = req.body.srcLoc ? req.body.srcLoc : [1, 2];
    const destLoc = req.body.destLoc ? req.body.destLoc : [3, 4];

    const origin = `${srcLoc[0]},${srcLoc[1]}`;
    const destination = `${destLoc[0]},${destLoc[1]}`;
    const distance = await getDistance(origin, destination);

    const tripObj = new TripSchema({
      riderId: req.body.riderId,
      driverId: req.body.driverId,
      srcLoc,
      destLoc,
      pickUpAddress: req.body.pickUpAddress,
      destAddress: req.body.destAddress,
      distance
    });
    tripObj.saveAsync()
    .then((newTripObj) => {
      const returnObj = {
        success: true,
        message: 'trip object created',
        data: newTripObj,
        meta: null
      };
      const tripRequest = new TripRequestSchema({
        riderId: newTripObj.riderId,
        driverId: newTripObj.driverId,
        tripId: newTripObj._id,
        srcLoc: newTripObj.srcLoc,
        destLoc: newTripObj.destLoc,
        pickUpAddress: newTripObj.pickUpAddress,
        destAddress: newTripObj.destAddress,
        tripRequestStatus: 'completed',
        tripRequestIssue: 'noIssue',
      });
      tripRequest.saveAsync()
      .then(() => {
        UserSchema.updateAsync({ $or: [{ _id: newTripObj.riderId }, { _id: newTripObj.driverId }] }, { $set: { currTripId: newTripObj._id, currTripState: 'trip' } }, { multi: true })
        .then(() => {
          res.send(returnObj);
        })
        .error((e) => {
          const err = new APIError(`Error occured while Updating User Object ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
          next(err);
          return;
        });
      })
      .error((e) => {
        const err = new APIError(`Error occured while Saving Trip Request Object ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
        next(err);
        return;
      });
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while saving trip object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
  })
  .error((e) => {
    const err = new APIError(
      `Error occured while finding rider or driver ${e}`,
      httpStatus.INTERNAL_SERVER_ERROR
    );
    next(err);
  });
}


function updateTrip(req, res, next) {
  const tripId = req.body._id;
  const tripObj = {
    riderId: req.body.riderId,
    driverId: req.body.driverId,
    srcLoc: req.body.srcLoc ? req.body.srcLoc : [1, 2],
    destLoc: req.body.destLoc ? req.body.destLoc : [2, 2],
    pickUpAddress: req.body.pickUpAddress ? req.body.pickUpAddress : 'new Dehli',
    destAddress: req.body.destAddress ? req.body.destAddress : 'mumbai',
    tripAmt: req.body.tripAmt ? req.body.tripAmt : 0,
    tripIssue: req.body.tripIssue ? req.body.tripIssue : 'noIssue',
    tripStatus: req.body.tripStatus ? req.body.tripStatus : 'OnTrip',
    paymentMode: req.body.paymentMode ? req.body.paymentMode : 'cash',
    taxiType: req.body.taxiType ? req.body.taxiType : 'taxiMini',
    riderRatingByDriver: req.body.riderRatingByDriver ? req.body.riderRatingByDriver : 0,
    driverRatingByRider: req.body.driverRatingByRider ? req.body.driverRatingByRider : 0,
    seatBooked: req.body.seatBooked ? req.body.seatBooked : 1,
    endTime: req.body.tripStatus === 'endTrip' ? (new Date).toISOString() : null
  };

  TripSchema.findOneAndUpdateAsync({ _id: tripId }, { $set: tripObj }, { new: 1, runValidators: true })
    .then((updatedTripObj) => {
      const returnObj = {
        success: false,
        message: 'unable to update trip object as trip id provided didnt match',
        data: null,
        meta: null
      };
      if (updatedTripObj) {
        returnObj.success = true;
        returnObj.message = 'trip object updated';
        returnObj.data = updatedTripObj;
        if (updatedTripObj.tripStatus === 'endTrip') {
          UserSchema.updateAsync({ $or: [{ _id: updatedTripObj.riderId }, { _id: updatedTripObj.driverId }] }, { $set: { currTripId: null, currTripState: null } }, { new: true, multi: true })
          .then(() => res.send(returnObj)) // sending the updated tripObj in the fronted
          .error((e) => {
            const err = new APIError(
              `Error occured while updatating User Object ${e}`,
              httpStatus.INTERNAL_SERVER_ERROR
            );
            return next(err);
          });
        }
      } else {
        const err = new APIError(
          `Trip Id did not matched`,
          httpStatus.BAD_REQUEST
        );
        return next(err);
      }
    })
    .error((e) => {
      const err = new APIError(
        `Error occured while updatating trip object ${e}`,
        httpStatus.INTERNAL_SERVER_ERROR
      );
      next(err);
    });
}


function loadTripDetails(req, res, next) {
  const tripId = req.params.tripId;
  TripSchema.get(tripId)
    .then((tripData) => {
      const returnObj = {
        success: true,
        message: 'trip object found',
        data: transformReturnObj(tripData)
      };
      res.send(returnObj);
    })
    .error((e) => next(e));
}

function tripRevenueGraph(req, res, next) {
  let lastYearDate = new Date();
  lastYearDate.setDate(1);
  lastYearDate.setMonth(lastYearDate.getMonth() - 11);
  lastYearDate = new Date(lastYearDate);
  const returnObj = {
    success: false,
    message: 'no of trips avaliable',
    data: [],
    lastYearDate
  };
  TripSchema.aggregateAsync([
    { $match: { bookingTime: { $gt: lastYearDate } } },
    { $project: {
      year: { $year: '$bookingTime' },
      month: { $month: '$bookingTime' },
      tripAmt: '$tripAmt',
      tripStatus: '$tripStatus',
    } },
    { $match: { tripStatus: 'endTrip' } },
    { $group: {
      _id: 'RevenueGraph',
      1: { $sum: { $cond: [{ $eq: ['$month', 1] }, '$tripAmt', 0] } },
      2: { $sum: { $cond: [{ $eq: ['$month', 2] }, '$tripAmt', 0] } },
      3: { $sum: { $cond: [{ $eq: ['$month', 3] }, '$tripAmt', 0] } },
      4: { $sum: { $cond: [{ $eq: ['$month', 4] }, '$tripAmt', 0] } },
      5: { $sum: { $cond: [{ $eq: ['$month', 5] }, '$tripAmt', 0] } },
      6: { $sum: { $cond: [{ $eq: ['$month', 6] }, '$tripAmt', 0] } },
      7: { $sum: { $cond: [{ $eq: ['$month', 7] }, '$tripAmt', 0] } },
      8: { $sum: { $cond: [{ $eq: ['$month', 8] }, '$tripAmt', 0] } },
      9: { $sum: { $cond: [{ $eq: ['$month', 9] }, '$tripAmt', 0] } },
      10: { $sum: { $cond: [{ $eq: ['$month', 10] }, '$tripAmt', 0] } },
      11: { $sum: { $cond: [{ $eq: ['$month', 11] }, '$tripAmt', 0] } },
      12: { $sum: { $cond: [{ $eq: ['$month', 12] }, '$tripAmt', 0] } },
    } },
  ])
  .then((revenueGraphDocs) => {
    returnObj.success = true;
    returnObj.message = 'revenue graph for the trip';
    returnObj.data = revenueGraphDocs;
    res.send(returnObj);
  })
  .error((e) => {
    const err = new APIError(
      `Error occured while computing revenue graph ${e}`,
      httpStatus.INTERNAL_SERVER_ERROR
    );
    next(err);
  });
}

function transformReturnObj(tripData) {
  if (tripData instanceof Object) {
    tripData = tripData.toObject();
    if (tripData.riderId) {
      tripData.rider = tripData.riderId;
      tripData.riderId = tripData.rider._id ? tripData.rider._id : null;
    }
    if (tripData.driverId) {
      tripData.driver = tripData.driverId;
      tripData.driverId = tripData.driver._id ? tripData.driver._id : null;
    }
  }
  return tripData;
}
export default {
  tripDetails,
  createNewTrip,
  updateTrip,
  loadTripDetails,
  tripRevenueGraph,
  tripPerDayGraph,
  getTripsPerDay,
  tripPerDayRevenueGraph,
  getTripsByUserPerDay,
  exportToExcel
};
