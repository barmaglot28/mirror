import ReferralCode from '../models/referral-code';

function getReferralCode(req, res, next) {
  const user = req.user;
  const returnObj = {
    success: false,
    message: '',
    data: {}
  };
  ReferralCode.findOneAsync({ userId: user._id })
    .then(referralCodeObj => {
      if (referralCodeObj) {
        returnObj.success = true;
        returnObj.message = 'referralcode found';
        returnObj.data = referralCodeObj;
        return res.send(returnObj);
      } else {
        const code = ReferralCode.generate();
        const referralCode = new ReferralCode({
          code,
          userId: user._id,
          inviterId: null,
          codeFromInviter: null,
          inviterCodeActive: false,
        });
        referralCode.saveAsync().then(savedObj => {
          returnObj.success = true;
          returnObj.message = 'referralcode generated';
          returnObj.data = savedObj;

          return res.send(returnObj);
        });
      }
    })
    .catch(err => next(err));
}

function checkReferralCode(req, res, next) {
  const referralCode = req.body.referralCode;
  const returnObj = {
    success: false,
    message: '',
    data: {}
  };
  ReferralCode.findOneAsync({ code: referralCode })
    .then(referralCodeObj => {
      if (referralCodeObj) {
        returnObj.success = true;
        returnObj.message = 'referralcode checked successfuly';
        returnObj.data = {
          isValid: true
        };
      } else {
        returnObj.success = true;
        returnObj.message = 'referralcode checked successfuly';
        returnObj.data = {
          isValid: false
        };
      }
      return res.send(returnObj);
    })
    .catch(err => next(err));
}

export default { getReferralCode, checkReferralCode };
