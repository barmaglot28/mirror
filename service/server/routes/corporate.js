import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import { validateCorporate } from './jwt-validation';

import corporateUser from '../controllers/corporate-user';
import corporateTrip from '../controllers/corporate-trip';

const router = express.Router();

router.use(validateCorporate);

// /api/corporate/rider
router.route('/rider')
  .post(validate(paramValidation.createCorporateRider), corporateUser.create)
  .get(validate(paramValidation.userList), corporateUser.list);

// /api/corporate/rider/:id/trip
router.route('/rider/:riderId/trip')
  .post(validate(paramValidation.createCorporateTrip), corporateTrip.create);

// /api/corporate/trip/:id?
router.route('/trip/:tripId?')
  .get(validate(paramValidation.tripList), corporateTrip.list)
  .put(validate(paramValidation.updateCorporateTrip), corporateTrip.update)
  .delete(corporateTrip.remove);

router.route('/trip/exportCompleted')
  .post(validate(paramValidation.exportCompletedTrips), require('json2xls').middleware, corporateTrip.exportCompleted);

export default router;
