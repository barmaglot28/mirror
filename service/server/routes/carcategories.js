import express from 'express';
import carcategoriesCtrl from '../controllers/carcategories';
import { validateUser } from './jwt-validation';

const router = express.Router();	// eslint-disable-line new-cap

router.use(validateUser);

router.route('/')
  /** GET /api/carcategories - Get carcategories */
  .get(carcategoriesCtrl.get);

/** Load carcategories when API with carcategoriesId route parameter is hit */
router.param('carcategoriesId', carcategoriesCtrl.load);

export default router;
