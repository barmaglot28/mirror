import express from 'express';
import carCtrl from '../controllers/car';
import { validateUser } from './jwt-validation';

const router = express.Router();	// eslint-disable-line new-cap

/**
 * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
 */
router.use(validateUser);

router.route('/:carId')
    /** GET /api/cars - Get car */
    .get(carCtrl.load)

    /** POST /api/cars - Lock car */
    .post(carCtrl.lock)

    /** DELETE /api/cars - Unlock car */
    .delete(carCtrl.unlock);

router.route('/')
    /** GET /api/cars - Get free cars */
    .get(carCtrl.all);

export default router;
