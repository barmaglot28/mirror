import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import authCtrl from '../controllers/auth';
import { validateUser } from './jwt-validation';

const router = express.Router();	// eslint-disable-line new-cap

/** POST /api/auth/login - Returns token if correct email and password is provided */
router.route('/login')
  .post(validate(paramValidation.login), authCtrl.login)
  .put(validate(paramValidation.setup), authCtrl.setup);

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/
router.use(validateUser);

router.route('/logout')
  .get(authCtrl.logout);

export default router;
