import passport from 'passport';
import config from '../../config/env';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Middleware that checks JWT and access by userType
 * @param userTypes {Array}
 * @returns {function(*=, *=, *=)}
 */
function createJwtValidation(userTypes) {
  return (req, res, next) => {
    passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => {
      if (error) {
        const err = new APIError('token not matched', httpStatus.UNAUTHORIZED);
        return next(err);
      }

      if (userDtls && userTypes.includes(userDtls.userType)) {
        req.user = userDtls;
        return next();
      }

      const err = new APIError(`token not matched and error msg ${info}`, httpStatus.UNAUTHORIZED);
      return next(err);
    })(req, res, next);
  };
}

export default {
  validateAdmin: createJwtValidation(['admin']),
  validateUser: createJwtValidation(['admin', 'rider', 'driver', 'corporate']),
  validateCorporate: createJwtValidation(['admin', 'corporate', 'driver'])

};
