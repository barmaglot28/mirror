import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import userCtrl from '../controllers/user';
import { validateUser } from './jwt-validation';

const router = express.Router();	// eslint-disable-line new-cap

/** POST /api/users/register - create new user and return corresponding user object and token*/
router.route('/register')
  .post(validate(paramValidation.createUser), userCtrl.create);

router.route('/recovery')
  .post(userCtrl.passwordRecovery);

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/
router.use(validateUser);

router.route('/')
  /** GET /api/users - Get user */
  .get(userCtrl.get)

  /** PUT /api/users - Update user */
  .put(validate(paramValidation.updateUser), userCtrl.update)

  /** DELETE /api/users - Delete user */
  .delete(userCtrl.remove)

  /** POST /api/users - Change Driver availability */
  .post(userCtrl.changeDriverAvailability);

router.route('/changePass')
/** PUT /api/users/changePass - Update user */
  .put(validate(paramValidation.changePass), userCtrl.changePass);

router.route('/avatar')
  /** PUT /api/users/avatar - Update user avatar */
  .put(userCtrl.updateAvatar);

router.route('/verify')
  /** PUT /api/users/verify - Send sms */
  .put(userCtrl.resendSms)

  /** POST /api/users/verify - Send sms */
  .post(userCtrl.changeVerifyStatus);

router.route('/updatePhone')
/** PUT /api/users/updatePhone - Update phone */
  .put(userCtrl.updatePhone);

router.route('/feedback')
  .put(validate(paramValidation.feedback), userCtrl.feedback);

router.route('/hideBadge')
  .put(userCtrl.hidePermanentBadge);

/** Load user when API with userId route parameter is hit */
router.param('userId', userCtrl.load);

export default router;
