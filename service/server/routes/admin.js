import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import { validateAdmin } from './jwt-validation';

import adminUser from '../controllers/admin-user';
import adminWallets from '../controllers/admin-wallets';
import adminCar from '../controllers/admin-car';
import adminBookings from '../controllers/admin-bookings';
import adminTrip from '../controllers/admin-trip';
import adminTripUser from '../controllers/admin-trip-user';
import adminCarcategories from '../controllers/admin-carcategories';
import adminReferralcodes from '../controllers/admin-referralcodes';
import adminCouponcodes from '../controllers/admin-coupon-codes';
import adminStatistic from '../controllers/admin-statistic';
import skyView from '../controllers/skyview';

const router = express.Router();

router.route('/trip/exportToExcel/:date')
  .get(require('json2xls').middleware, adminTrip.exportToExcel);

router.use(validateAdmin);

// /api/admin/user
router.route('/user')
  .get(validate(paramValidation.userList), adminUser.getAllUsers)
  .post(validate(paramValidation.createNewUser), adminUser.createNewUser)
  .put(validate(paramValidation.updateUserByAdmin), adminUser.updateUserDetails)
  .delete(validate(paramValidation.removeUserByAdmin), adminUser.removeUser);

// /api/admin/users/filtered
router.route('/users/filtered')
  .get(adminUser.getFilteredUsers);

// /api/admin/sendSmsToAll
router.route('/user/sendSmsToAll')
  .post(adminUser.sendSmsToAll);

// api/admin/user/sendEmailToAll
router.route('/user/sendEmailToAll')
  .post(adminUser.sendEmailToAll);

router.route('/user/alldrivers')
  .get(adminUser.getAllDrivers);

router.route('/userDetails/:userId')
  .get(adminUser.getUsersDetails);

// /api/admin/car
router.route('/car')
  .get(validate(paramValidation.carList), adminCar.getAllCars)
  .post(validate(paramValidation.createNewCar), adminCar.createNewCar)
  .put(validate(paramValidation.updateCar), adminCar.updateCarDetails)
  .delete(validate(paramValidation.removeCar), adminCar.removeCar);

router.route('/car/:carId')
  .get(adminCar.getCarsDetails);

router.route('/user/userStatsChart')
  .get(adminUser.userStats);

// /api/admin/wallets/all
router.route('/wallets/all')
  .get(adminWallets.getAllWallets);

// /api/admin/wallets/add-balance
router.route('/wallets/add-balance')
  .put(adminWallets.addWalletBalance);

// /api/admin/wallets/filtered
router.route('/wallets/filtered')
  .get(adminWallets.getFilteredWallets);

// /api/admin/trip
router.route('/trip')
  .get(validate(paramValidation.tripList), adminTrip.tripDetails)
  .post(validate(paramValidation.createNewTrip), adminTrip.createNewTrip)
  .put(validate(paramValidation.updateTripObject), adminTrip.updateTrip);
// .put(adminTrip.updateTrip);

router.route('/trip/tripsPerDay')
  .post(validate(paramValidation.userTripsPerDay), adminTrip.getTripsPerDay);

router.route('/trip/tripsPerDay/charts/:date')
  .get(validate(paramValidation.tripPerDayRevenueGraph), adminTrip.tripPerDayRevenueGraph);

router.route('/trip/tripsPerDay/requests/charts/:date')
  .get(validate(paramValidation.tripPerDayRevenueGraph), adminTrip.tripPerDayGraph);

router.route('/trip/tripsByUserPerDay')
  .post(validate(paramValidation.userTripsByUserPerDay), adminTrip.getTripsByUserPerDay);

router.route('/trip/charts')
  .get(validate(paramValidation.tripRevenueGraph), adminTrip.tripRevenueGraph);


router.route('/trip/charts/:revenueYear')
  .get(validate(paramValidation.tripRevenueGraph), adminTrip.tripRevenueGraph);

router.route('/trip/:tripId')
  .get(validate(paramValidation.userTripList), adminTrip.loadTripDetails);

router.route('/trip/user/:userId')
  .get(validate(paramValidation.userTripList), adminTripUser.userTripDetails);

router.route('/trip/user/:userId/exportCompleted')
  .post(validate(paramValidation.exportCompletedTrips), require('json2xls').middleware, adminTripUser.exportUserCompletedTrips);

router.route('/trip/user/charts/:userId')
  .get(validate(paramValidation.userTripList), adminTripUser.userTripRequestStatics);

// /api/admin/availableBookings
router.route('/availableBookings')
  .get(validate(paramValidation.bookingsList), adminBookings.getAllAvailableBookings);

// /api/admin/acceptedBookings
router.route('/acceptedBookings')
  .get(validate(paramValidation.bookingsList), adminBookings.getAllAcceptedBookings);

// /api/admin/cancelledBookings
router.route('/cancelledBookings')
  .get(validate(paramValidation.bookingsList), adminBookings.getAllCancelledBookings);

// /api/admin/carcategories
router.route('/carcategories')
  .get(validate(paramValidation.carcategoriesList), adminCarcategories.getAllCarcategories)
  .post(validate(paramValidation.createNewCarcategories), adminCarcategories.createNewCarcategories)
  .put(validate(paramValidation.updateCarcategoriesByAdmin), adminCarcategories.updateCarcategoriesDetails)
  .delete(validate(paramValidation.removeCarcategoriesByAdmin), adminCarcategories.removeCarcategories);

router.route('/carcategoriesDetails/:carcategoriesId')
  .get(adminCarcategories.getCarcategoriesDetails);

// /api/admin/referralcode
router.route('/referralcode/amount')
  .get(validate(paramValidation.referralcodesList), adminReferralcodes.fetchReferralAmt)
  // .put(adminReferralcodes.getLastReferralcode)
  .post(validate(paramValidation.createNewReferralcodes), adminReferralcodes.saveReferralAmt);

// /api/admin/couponcodes
router.route('/couponcodes')
  .get(validate(paramValidation.couponcodesList), adminCouponcodes.getAllCouponcodes)
  .post(validate(paramValidation.createNewCouponcodes), adminCouponcodes.createNewCouponcodes)
  .put(validate(paramValidation.updateCouponcodesByAdmin), adminCouponcodes.updateCouponcodesDetails)
  .delete(validate(paramValidation.removeCouponcodesByAdmin), adminCouponcodes.removeCouponcodes);

router.route('/couponcodesDetails/:couponcodesId')
  .get(adminCouponcodes.getCouponcodesDetails);

router.route('/statistic/general')
  .get(adminStatistic.getGeneralStatistic);

router.route('/statistic/todayRide')
  .get(adminStatistic.getRideTodayStatistic);

router.route('/statistic/completedRide')
  .get(adminStatistic.getCompletedRideStatistic);

router.route('/statistic/drivers')
  .get(adminStatistic.getDriversStatistic);

router.route('/statistic/ratingComments')
  .get(adminStatistic.getRatingCommentsStatistic);

router.route('/statistic/cancelledComments')
  .get(adminStatistic.getCancelledCommentsStatistic);

router.route('/statistic/driverWorklist')
  .get(adminStatistic.getDriverWorklistStatistic);

router.route('/statistic/trips')
  .get(adminStatistic.getTripsStatistic);

router.route('/statistic/vehicle')
  .get(adminStatistic.getVehicleStatistic);

router.route('/skyView/getDrivers')
  .get(skyView.getDrivers);

export default router;
