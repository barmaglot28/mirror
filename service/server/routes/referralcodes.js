import express from 'express';
import referralcodesCtrl from '../controllers/referralcodes';
import { validateUser } from './jwt-validation';

const router = express.Router();    // eslint-disable-line new-cap

router.route('/')
  /** PUT /api/referralcodes - Put referralcodes */
  .put(referralcodesCtrl.checkReferralCode);

router.use(validateUser);

router.route('/')
  /** GET /api/referralcodes - Get referralcodes */
  .get(referralcodesCtrl.getReferralCode);

export default router;
