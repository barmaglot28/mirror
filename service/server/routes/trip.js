import express from 'express';
import tripCtrl from '../controllers/trip';

import { validateUser } from './jwt-validation';

const router = express.Router();

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/
router.use(validateUser);


/** GET /api/trips/history - Returns trip history details for the user */
router.route('/history')
  .get(tripCtrl.getHistory)
  .put(tripCtrl.removeHistory);

export default router;
