import express from 'express';
import carcategoriesRoutes from './carcategories';
import userRoutes from './user';
import authRoutes from './auth';
import tripRoutes from './trip';
import carRoutes from './car';
import syncDataRoute from './sync-data';
import adminRoutes from './admin';
import paymentRoutes from './payment';
import corporateRoutes from './corporate';
import referralCodeRoutes from './referralcodes';

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount user routes at /users
router.use('/carcategories', carcategoriesRoutes);

// mount user routes at /users
router.use('/users', userRoutes);

// mount car routes at /cars
router.use('/cars', carRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

// mount trip routes at /trips
router.use('/trips', tripRoutes);

// mount sync data route at /sync-data
router.use('/syncData', syncDataRoute);

// mount admin routes at /admin
router.use('/admin', adminRoutes);

// mount payment routes at /payment
router.use('/payment', paymentRoutes);

// mount corporate routes at /corporate
router.use('/corporate', corporateRoutes);

// mount referralcode routes at /referralcodes
router.use('/referralcodes', referralCodeRoutes);


export default router;
