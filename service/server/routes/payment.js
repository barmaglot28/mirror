import express from 'express';
import paymentCtrl from '../controllers/payment';

const router = express.Router();

/** GET /api/payment - Returns wallet balance details for the user */
router.route('/')
  .post(paymentCtrl.payAll);

/** GET /api/payment/transactions - Returns user's transactions history  */
router.route('/transactions/:userId')
  .get(paymentCtrl.fetchUserTransactions);

/** GET /api/payment/wallet - Returns wallet balance details for the rider triver and owner */
router.route('/wallet')
  .post(paymentCtrl.addBalance);

  /** POST /api/payment/amount - Returns wallet balance details for the user */
router.route('/amount')
  .post(paymentCtrl.getBalance);

/** POST /api/payment/card/add - Returns wallet balance details for the user */
router.route('/card/add')
  .post(paymentCtrl.addCard);

/** POST /api/payment/card/update - Returns customer stripe details for the user */
router.route('/card/update')
  .put(paymentCtrl.updateCard);

/** GET /api/payment/user/:id - Returns wallet and card details for the user */
router.route('/user/:userId')
  .get(paymentCtrl.getCard);

/** DELETE /api/payment/card/remove - Returns delete status */
router.route('/card/remove')
  .delete(paymentCtrl.removeCard);

/** POST /api/payment/method/update - Returns wallet details for the user */
router.route('/method/update')
  .post(paymentCtrl.updateMethod);

/** POST /api/payment/fare/calculate - Returns the estimated fare for every car category */
router.route('/fare/calculate')
  .post(paymentCtrl.calculateFare);

export default router;
