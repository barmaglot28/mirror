import express from 'express';
import syncDataCtrl from '../controllers/sync-data';
import { validateUser } from './jwt-validation';

const router = express.Router();

/**
* Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
*/
router.use(validateUser);

/** GET /api/syncData - Returns tripRequest or trip  object if user is in any trip*/
router.route('/')
  .get(syncDataCtrl.getSyncData);

export default router;
