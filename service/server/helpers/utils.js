export default {
  getName(user) {
    if (user && user.fname && user.lname) {
      return `${user.fname} ${user.lname}`;
    } else if (user && user.fname) {
      return user.fname;
    } else if (user && user.lname) {
      return user.lname;
    } else {
      return 'Unknown';
    }
  }
};
