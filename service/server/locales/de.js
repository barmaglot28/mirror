export default {
  availableBookingListChanged: 'Neue Vorbestellung eingegangen',
  driverArrived: 'Dein Fahrer wartet am Abholort',
  paymentModeUndefined: 'Deine Zahlungsart ist nicht definiert',
  bonusForFirstReferralTrip: 'Dir wurden €{{amount}} als Bonus für deine Weiterempfehlung in deiner Geldbörse gutgeschrieben',
  willArrive2min: 'Dein Fahrer kommt in 2 Minuten an',
  carTakenByOther: 'Dein Fahrzeug wurde von {{driver}} besetzt',
  bonusForSwitchFromCabdo: '€{{amount}} wurden deiner Geldbörse für deine nächste CabdoBlack Fahrt gutgeschrieben',
  newBooking: 'Neue Vorbestellung',
  newTripRequest: 'Neue Fahrtanfrage',
  newTripAssigned: 'Dir wurde ein neuer Fahrauftrag zugewiesen',
  acceptedPreBookingBody: 'Dein Fahrer hat die Vorbestellung akzeptiert! Den Status kannst Du unter "Vorbestellungen" einsehen.',
};
