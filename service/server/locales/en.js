export default {
  availableBookingListChanged: 'Available booking list changed',
  driverArrived: 'Driver has arrived at pickup location',
  paymentModeUndefined: 'Your paymentMode is not defined',
  bonusForFirstReferralTrip: 'You received €{{amount}} for referral code to your wallet',
  willArrive2min: 'Your driver will arrive in 2 minutes',
  carTakenByOther: 'Your car was taken by {{driver}} driver',
  bonusForSwitchFromCabdo: '€{{amount}} was debited to your wallet for your next CabdoBlack trip',
  newBooking: 'New booking was created',
  newTripRequest: 'New trip request',
  newTripAssigned: 'You were assigned a new trip',
  acceptedPreBookingBody: 'Driver accepted your pre-booking! Check status in "My bookings"',
};
