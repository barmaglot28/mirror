import userTypes from '../constants/userTypes';

/**
* SocketStore class is used to add socket to store, remove socket from store and emit socket event.
*/

const store = []; // Store keep tracks of userId and its corresponding socket.
const adminStore = []; // Dashboard store keep tracks of userId and its corresponding socket.
class SocketStore {

  /**
  * Add socket object to the store.
  * @param userId - unique user id of the user.
  * @param socketObj- socketObj to which user is connected to.
  * @returns {success, message, data}
  */

  static addByUserId(userId, userType, socketObj) {
    const currentStore = (userType === userTypes.admin) ? adminStore : store;
    if (userId === 'undefined' || userId === null || socketObj === null || socketObj === 'undefined') {
      return returnFunc(false, 'userId or socketObj is undefine', '');
    }
    const newObj = {
      id: userId,
      socket: socketObj
    };
    for (let i = 0; i < currentStore.length; i++) {
      if (currentStore[i].id.toString() === userId.toString()) {
        currentStore.splice(i, 1);
      }
    }
    currentStore.push(newObj);
    return returnFunc(true, 'user Id and socket is successfully stored', '');
  }

  /**
  * Return socket object for the given user ID.
  * @param userId - unique user id of the user.
  * @returns {success, message, data}
  */

  static getByUserId(userId) {
    if (userId === undefined || userId === null) {
      return returnFunc(false, 'userId is undefined or null', '');
    }
    if (store.length === 0) {
      return returnFunc(false, 'socket store is empty', '');
    } else {
      for (let i = 0; i < store.length; i++) {
        if (store[i].id.toString() === userId.toString()) {
          return returnFunc(true, 'userId and its corresponding socket found', store[i].socket.id);
        }
      }
      return returnFunc(false, 'userId and its corresponding socket not found in the store', '');
    }
  }

  /**
  * Return socket object for the given user ID.
  * @param userId - unique user id of the user.
  * @returns {success, message, data}
  */

  static removeByUserId(userId, userType, socketObj) {
    const currentStore = (userType === userTypes.admin) ? adminStore : store;
    if (userId === null || userId === undefined || socketObj === null || socketObj === undefined) {
      return returnFunc(false, 'userId or socket obj is undefined or null');
    }
    if (currentStore.length === 0) {
      return returnFunc(false, 'socket store is empty', '');
    } else {
      for (let i = 0; i < currentStore.length; i++) {
        if (currentStore[i].id.toString() === userId.toString()) {
          currentStore.splice(i, 1);
          return returnFunc(true, 'userId and its corresponding socket obj', currentStore[i]);
        }
      }
      return returnFunc(false, 'socketObj not found', '');
    }
  }

  /**
  * Emit socket event to the given user ID.
  * @param userId - unique user id of the user.
  * @param eventName - event name to be emitted.
  * @param payload - data to be send to the user.
  * @returns {success, message, data}
  */

  static emitByUserId(userId, eventName, payload) {
    if (userId === undefined || userId === null) {
      return returnFunc(false, 'userId is undefined or null', '');
    }
    if (store.length === 0) {
      return returnFunc(false, 'socket store is empty', '');
    } else {
      for (let i = 0; i < store.length; i++) {
        if (store[i].id.toString() === userId.toString()) {
          const socketObj = store[i].socket;
          socketObj.emit(eventName, payload);
          return returnFunc(true, 'evenet emitted successfully', '');
        }
      }
      return returnFunc(false, 'no user found with the id.', store);
    }
  }

  static emitAdminEvent(eventName, payload) {
    if (adminStore.length === 0) {
      return returnFunc(false, 'admin socket store is empty', '');
    } else {
      for (const socketItem of adminStore) {
        socketItem.socket.emit(eventName, payload);
      }
      return returnFunc(true, 'event emitted successfully', '');
    }
  }
}

/**
* Transform return Object
*/
function returnFunc(successStatus, msg, resultData) {
  return { success: successStatus, message: msg, data: resultData };
}

export default SocketStore;
