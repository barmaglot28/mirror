import Expo from 'exponent-server-sdk';
import UserSchema from '../models/user';

const expo = new Expo();

function sendNotification(userId, notification, event, payload) {
  UserSchema.findOneAsync({ _id: userId }).then(async (userObj) => {
    try {
      if (!Expo.isExponentPushToken(userObj.pushToken)) {
        console.error('pushExpo_sendNotification: isExponentPushToken returned false');
        return;
      }

      const receipts = await expo.sendPushNotificationsAsync([{
        to: userObj.pushToken,
        sound: 'default',
        priority: 'high',
        body: notification,
        data: { event, notification, payload }
      }]);
      console.log('pushExpo_sendNotification receipts: ', receipts);
    } catch (error) {
      console.error('pushExpo_sendNotification error: ', error);
    }
  });
}
export default sendNotification;
