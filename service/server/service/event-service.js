/**
 * EventService class is used to exchange data between segmental modules.
 */

import EventEmitter from 'events';

const eventEmiter = new EventEmitter();

class EventService {

  static emit(event, ...data) {
    eventEmiter.emit(event, ...data);
  }

  static on(event, callback) {
    return eventEmiter.on(event, callback);
  }
}

export default EventService;
