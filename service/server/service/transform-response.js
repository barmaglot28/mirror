import UserSchema from '../models/user';
import WalletSchema from '../models/wallet';
import Promise from 'bluebird';

export function fetchReturnObj(obj) {
  let returnObj = {};
  returnObj = obj.toObject();
  return new Promise((resolve, reject) => {
    getRiderObj(returnObj.riderId).then((riderObj) => {
      if (riderObj) {
        returnObj.rider = riderObj;
        returnObj.rider.gpsLoc = [riderObj.gpsLoc[1], riderObj.gpsLoc[0]];
      }
      return returnObj;
    }).then(() => {
      return getDriverDtls(returnObj.driverId, reject).then((driverObj) => {
        if (driverObj) {
          returnObj.driver = driverObj;
          returnObj.driver.gpsLoc = [driverObj.gpsLoc[1], driverObj.gpsLoc[0]];
        }
        return returnObj;
      });
    }).then(() => {
      resolve(returnObj);
      return returnObj;
    });
  });
}

export function getRiderObj(riderId) {
  let fetchUser;
  return UserSchema.findOne({ _id: riderId, userType: 'rider' })
    .populate('avatar')
    .execAsync()
    .then((resultUser) => {
      if (resultUser) {
        fetchUser = resultUser.toObject();
        fetchUser.avatar = resultUser.avatar && resultUser.avatar.base64 ? new Buffer(resultUser.avatar.base64, 'base64').toString('base64') : null;
        return fetchUser;
      } else {
        return resultUser;
      }
    });
}

export function getDriverDtls(driverId, reject) {
  let fetchUser;
  return UserSchema.findOne({ _id: driverId, userType: 'driver' })
    .populate('avatar')
    .execAsync()
    .then((resultUser) => {
      if (resultUser) {
        fetchUser = resultUser.toObject();
        fetchUser.avatar = resultUser.avatar && resultUser.avatar.base64 ? new Buffer(resultUser.avatar.base64, 'base64').toString('base64') : null;
        return fetchUser;
      } else {
        return resultUser;
      }
    })
    .error((errDriverDtls) => reject(errDriverDtls));
}

export function getUserWallet(userId) {
  return WalletSchema.findOneAsync({ _id: userId, userType: 'rider' });
}
