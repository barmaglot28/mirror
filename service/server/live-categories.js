const carCategories = [
  {
    name: 'CabdoBlack',
    isMetered: false,
    type: 'unmetered',
    baseFareDay: 3.25,
    priceFirstKilometerDay: 2.1,
    pricePerKilometerDay: 1.50,
    baseFareNight: 3.75,
    priceFirstKilometerNight: 2.25,
    pricePerKilometerNight: 1.65,
    minimumFare: 8,
    commissionToCabdo: 10,
    capacity: 4,
    default: true
  },
  {
    name: 'Taxi',
    isMetered: true,
    type: 'metered',
    baseFareDay: 3.5,
    priceFirstKilometerDay: 2.1,
    pricePerKilometerDay: 1.6,
    baseFareNight: 4.0,
    priceFirstKilometerNight: 2.25,
    pricePerKilometerNight: 1.75,
    minimumFare: 3.5,
    commissionToCabdo: 0,
    capacity: 4
  },
  {
    name: 'TaxiXL',
    isMetered: true,
    type: 'metered',
    baseFareDay: 3.5,
    priceFirstKilometerDay: 2.1,
    pricePerKilometerDay: 1.6,
    baseFareNight: 4.0,
    priceFirstKilometerNight: 2.25,
    pricePerKilometerNight: 1.75,
    minimumFare: 3.5,
    commissionToCabdo: 0,
    capacity: 6,
    extraFare: 5.1
  }
];

export default { carCategories };
