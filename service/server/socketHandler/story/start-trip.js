import SocketStore from '../../service/socket-store.js';
import { fetchReturnObj } from '../../service/transform-response';
import moment from 'moment';
import TripSchema from '../../models/trip';
import UserSchema from '../../models/user.js';
import ReferralCodeSchema from '../../models/referral-code';
import ReferralAmountSchema from '../../models/referral-code-amount';
import WalletSchema from '../../models/wallet';
import SendNotification from '../../service/pushExpo';
import { processWithWallet, processWithCard } from '../../controllers/payment';
import I18n from '../../i18n/i18n';
import carCategoryConsts from '../../constants/carCategories';
import tripStatusConsts from '../../constants/tripStatus';
import { sendTripForDashboard, updateDriverForDashboard } from './request-trip';
/**
* startTriphandler function create a new trip object which stores the different details related to trip.
* @param socket object
* @returns {*}
*/
const startTripHandler = (socket) => {
  /**
  * startTrip event is emitted by driver when trip get started
  * @param trip object
  * @param callback function
  * @return send tripUpdated event to the rider with all the information related to trip
  */
  socket.on('startTrip', async (tripObj, cb) => {
    const riderID = tripObj.riderId;
    const driverID = tripObj.driverId;
    tripObj.tripStatus = tripStatusConsts.enRoute;

    // TODO: ADD waiting fare caculator here by using difference between current time and arrived time.

    TripSchema.findOneAndUpdateAsync({ _id: tripObj._id }, { $set: tripObj }, { new: true })
      .then((savedTrip) => {
        return UserSchema.updateAsync({ $or: [{ _id: savedTrip.riderId }, { _id: savedTrip.driverId }] }, { $set: { currTripId: savedTrip._id, currTripState: tripStatusConsts.enRoute } }, { new: true, multi: true })
          .then(() => {
            fetchReturnObj(savedTrip).then((returnObj) => {
              cb(returnObj);
              sendTripForDashboard('tripUpdatedDashboard', returnObj);
              SocketStore.emitByUserId(riderID, 'tripUpdated', returnObj);
            });
          });
      })
      .error((e) => {
        cb(null);
        errorHandler(riderID, driverID, e);
      });
  });

  /**
  * tripUpdate emit is fired when rider or driver make any changes to trip Object
  * @param trip object
  * @return send tripUpdated event to the rider and driver with all the information related to trip
  */

  socket.on('tripUpdated', (tripObj) => {
    const riderID = tripObj.riderId;
    const driverID = tripObj.driverId;

    if (tripObj.tripStatus === tripStatusConsts.cancelled) {
      UserSchema.updateAsync({ $or: [{ _id: riderID }, { _id: driverID }] }, { $set: { currTripId: null, currTripState: null, statusTime: null } }, { new: true, multi: true })
        .then(() => {
          updateDriverForDashboard(driverID);
        })
        .error((e) => errorHandler(riderID, driverID, e));
    }

    if (tripObj.tripStatus === tripStatusConsts.arrived) {
      SendNotification(tripObj.riderId, I18n.t('driverArrived'), 'driverArrived');
    }

    if (tripObj.tripStatus === tripStatusConsts.endTrip) {
      const then = moment(tripObj.bookingTime, "YYYY-MM-DD'T'HH:mm:ss:SSSZ");
      const now = moment(new Date());
      tripObj.endTime = (new Date).toISOString();
      tripObj.travelTime = Math.abs(moment.duration(then.diff(now)));
      console.log(`end Trip ${tripObj._id} by driver ${tripObj.driver.email}`);
      UserSchema.updateAsync({ _id: tripObj.driverId }, { $set: { currTripId: null, currTripState: null, statusTime: null } })
        .then(() => {
          updateDriverForDashboard(tripObj.driverId);
          SocketStore.emitByUserId(tripObj.driverId, 'updateMyBookings', {});
        })
        .error((e) => errorHandler(tripObj.riderId, tripObj.driverId, e));
    }

    TripSchema.findOneAndUpdateAsync({ _id: tripObj._id }, { $set: tripObj }, { new: true })
      .then((updatedTripObject) => fetchReturnObj(updatedTripObject))
      .then((updatedTripObj) => {
        sendTripForDashboard('tripUpdatedDashboard', updatedTripObj);
        if (!socket || updatedTripObj.tripStatus === tripStatusConsts.endTrip) {
          return;
        }
        if (socket.userId.toString() === updatedTripObj.riderId.toString()) {
          SocketStore.emitByUserId(updatedTripObj.driverId, 'tripUpdated', updatedTripObj);
        } else if (socket.userId.toString() === updatedTripObj.driverId.toString()) {
          SocketStore.emitByUserId(updatedTripObj.riderId, 'tripUpdated', updatedTripObj);
        }
      })
      .error((e) => errorHandler(riderID, driverID, e));
  });

  socket.on('updateRate', (tripObj) => {
    UserSchema.updateAsync({ _id: tripObj.riderId }, { $set: { currTripId: null, currTripState: null, statusTime: null } })
      .error((e) => errorHandler(tripObj.riderId, tripObj.driverId, e));

    if (tripObj.driverRatingByRider === 0) {
      return;
    }

    TripSchema.findOneAndUpdateAsync({ _id: tripObj._id }, { $set: { driverRatingByRider: tripObj.driverRatingByRider, riderComment: tripObj.riderComment } }, { new: true })
      .then((updatedTripObj) =>
        TripSchema.aggregateAsync(
          [{ $match: { driverId: updatedTripObj.driverId, tripStatus: tripStatusConsts.endTrip, driverRatingByRider: { $gt: 0 } } },
          { $group: { _id: '$driverId', userRt: { $avg: '$driverRatingByRider' } } }]
        )
      )
      .then((res) => {
        if (res.length !== 0) {
          return UserSchema.findOneAndUpdateAsync({ _id: res[0]._id }, { $set: { userRating: res[0].userRt.toFixed(2) } }, { new: true });
        }
      })
      .error((e) => errorHandler(tripObj.riderId, tripObj.driverId, e));
  });

  socket.on('meteredTrip', async (tripObj) => {
    const { riderId, driverId } = tripObj;
    const meteredAmount = tripObj.tripAmt;
    let paycashAmount = null;

    console.log('tripObj.taxiType');
    if (tripObj.taxiType === carCategoryConsts.CabdoBlack) {
      checkFirstReferralTrip(tripObj);
    } else {
      checkSwitchFromCabdo(tripObj);
    }

    if (tripObj.paymentMode === 'cash') {
      paycashAmount = (tripObj.taxiType === carCategoryConsts.CabdoBlack) ?
        await processWithWallet(tripObj) : { error: false, tripAmt: meteredAmount };
    } else if (tripObj.paymentMode === 'card') {
      paycashAmount = await processWithCard(tripObj);
      if (paycashAmount.error) {
        tripObj.paymentMode = 'cash';
      }
    } else {
      SendNotification(riderId, I18n.t('paymentModeUndefined'));
    }

    tripObj.tripAmt = paycashAmount.tripAmt;
    tripObj.discount = meteredAmount - paycashAmount.tripAmt;
    tripObj.tripStatus = tripStatusConsts.feedback;
    TripSchema.findOneAndUpdateAsync({ _id: tripObj._id }, { $set: tripObj }, { new: true })
      .then((updatedTripObject) => {
        fetchReturnObj(updatedTripObject).then((updatedTripObj) => {
          console.log(`processed payment for trip ${updatedTripObj._id}: ${tripObj.tripAmt}`);
          sendTripForDashboard('tripUpdatedDashboard', updatedTripObj);
          SocketStore.emitByUserId(driverId, 'tripUpdated', updatedTripObj);
          SocketStore.emitByUserId(riderId, 'tripUpdated', updatedTripObj);
        });
      })
      .error((e) => errorHandler(riderId, driverId, e));
  });
};

function checkFirstReferralTrip(tripObj) {
  ReferralCodeSchema.findOneAsync({ userId: tripObj.riderId, inviterCodeActive: true }).then(async (referralCode) => {
    if (referralCode && referralCode.inviterId) {
      referralCode.inviterCodeActive = false;
      referralCode.saveAsync().error((e) => errorHandler(tripObj.riderId, tripObj.driverId, e));
      const amount = await ReferralAmountSchema.findOneAsync();
      if (amount && amount.amountToSenderWallet) {
        const updatedWallet = await WalletSchema.findOneAndUpdateAsync({ userId: referralCode.inviterId }, { $inc: { walletBalance: amount.amountToSenderWallet } }, { new: true });
        if (updatedWallet) {
          SendNotification(referralCode.inviterId, I18n.t('bonusForFirstReferralTrip', { amount: amount.amountToSenderWallet }), 'bonusForFirstReferralTrip');
        }
      }
    }
  });
}

function checkSwitchFromCabdo(tripObj) {
  console.log('checkSwitchFromCabdo', tripObj);
  if (!tripObj.switchedFromCabdo || tripObj.preBookedTime) {
    return;
  }

  const val = Math.round(tripObj.tripAmt * 5);
  const bonus = (val - val % 5) / 100;
  console.log('bonus', bonus);
  WalletSchema.findOneAndUpdateAsync({ userId: tripObj.riderId }, { $inc: { walletBalance: bonus } }, { new: true })
    .then((updatedWallet) => {
      console.log('updatedWallet', updatedWallet);
      if (updatedWallet) {
        SendNotification(tripObj.riderId, I18n.t('bonusForSwitchFromCabdo', { amount: bonus }), 'bonusForSwitchFromCabdo');
      }
    })
    .error(e => console.log(`error in updating wallet for trip switched from cabdo: ${e}`));
}

function errorHandler(riderId, driverId, e) {
  SocketStore.emitByUserId(riderId, 'socketError', e);
  SocketStore.emitByUserId(driverId, 'socketError', e);
}

export default startTripHandler;
