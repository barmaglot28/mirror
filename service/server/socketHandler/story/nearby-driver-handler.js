import SocketStore from '../../service/socket-store.js';
import UserSchema from '../../models/user';
import config from '../../../config/env';
import { calcArrivalTime, findUnavailableDrivers } from './request-trip';

function nearbyDriverHandler(socket) {
  socket.on('updatePickupRegion', (userRegion) => {
    const coordinates = [userRegion.longitude, userRegion.latitude];
    const userId = userRegion.userId;
    UserSchema.findAsync({
      $and: [
        { gpsLoc: { $near: coordinates, $maxDistance: config.maxDistance } },
        { currTripState: { $in: [null, 'enRoute'] } },
        { loginStatus: true },
        { availability: true },
        { userType: 'driver' },
      ]
    })
      .then(nearByDrivers => {
        if (nearByDrivers) {
          const driverIdArray = nearByDrivers.map(driver => driver._id);
          findUnavailableDrivers(driverIdArray, { preBookedTime: null }).then(unavailableDriverIds => {
            const availableDrivers = nearByDrivers.filter(driver => !unavailableDriverIds.find(id => driver._id.equals(id)));
            Promise.all(availableDrivers.map(driver =>
              calcArrivalTime(driver, { srcLoc: [userRegion.latitude, userRegion.longitude] })
              .then(time => {
                return Object.assign({}, driver.toObject(), { arrivalTime: time });
              })
            ))
              .then(results => {
                results.sort((a, b) => a.arrivalTime - b.arrivalTime).map(result => result.gpsLoc = [result.gpsLoc[1], result.gpsLoc[0]]);
                console.log('[nearbyDrivers123:]', results.map(result => `${result.email} ${result.arrivalTime}`).join(', '));
                SocketStore.emitByUserId(userId, 'nearByDriversList', results);
              });
          });
        }
      });
  });
}

export default nearbyDriverHandler;
