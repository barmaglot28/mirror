import httpStatus from 'http-status';
import Promise from 'bluebird';
import twilioClient from '../../../config/env/twilioClient';
import SocketStore from '../../service/socket-store';
import UserSchema from '../../models/user';
import APIError from '../../helpers/APIError';
import Trip from '../../models/trip';
import CarCategorySchema from '../../models/car-category';
import { fetchReturnObj } from '../../service/transform-response';
import config from '../../../config/env';
import EventService from '../../service/event-service';
import request from 'request';
import moment from 'moment';
import deferred from 'deferred';
import {
  setDriverToBooking, removeDriverFromBooking, getDriverAcceptedBookings, getRiderBookings,
  getAvailableBookings, findSoonTrips
} from '../../controllers/trip';

import { fetchCarCategoriesData } from '../../controllers/carcategories';
import { createCorporate } from '../../controllers/corporate-user';
import SendNotification from '../../service/pushExpo';
import I18n from '../../i18n/i18n';
import carCategoryConsts from '../../constants/carCategories';
import tripStatusConsts from '../../constants/tripStatus';

const watchIdObj = {};
const promObj = {};

/* handlers for endpoint API call */
EventService.on('corporateRequestTrip', requestTripCallback);
EventService.on('corporateRequestBookingTrip', requestBookingTripCallback);
EventService.on('corporateUpdateTrip', tripUpdateCallback.bind(null, null));

function requestTripHandler(socket) {
  socket.on('requestTrip', requestTripCallback);
  socket.on('requestBooking', requestBookingTripCallback);
  socket.on('requestDriverResponse', (trip) => {
    watchIdObj[trip._id] = clearInterval(watchIdObj[trip._id]);
    const driverId = trip.driver._id;
    if (promObj[driverId]) {
      deleteTripFromDashboard(trip._id);
      promObj[driverId].resolve(trip);
    }
  });
  socket.on('acceptBooking', (payload) => {
    const driverId = payload.trip.driverId;
    if (promObj[driverId]) {
      watchIdObj[payload.trip._id] = clearInterval(watchIdObj[payload.trip._id]);
      promObj[driverId].resolve(payload.trip);
    } else {
      handleAcceptBooking(payload);
    }
  });
  socket.on('cancelBookingDriver', cancelBookingDriverAsync);
  socket.on('startBookingTrip', startBookingTrip);
  socket.on('cancelBookingRider', cancelBookingRiderAsync);
  socket.on('getDriverAcceptedBookings', driverAcceptedBookings);
  socket.on('getRiderBookings', riderBookings);
  socket.on('fetchAvailableBookings', fetchAvailableBookings);
  socket.on('requestRegularTrip', requestRegularTripHandler);
  socket.on('setDestAddress', setDestAddress);

  socket.on('adminUnpinAcceptedBooking', cancelBookingDriverAsync);
  socket.on('adminCancelBooking', cancelBookingRiderAsync);
}

async function requestRegularTripHandler(payload) {
  console.log('requestRegularTripHandler trip', JSON.stringify(payload.trip));
  console.log('requestRegularTripHandler driver', JSON.stringify(payload.driver));
  console.log('requestRegularTripHandler carCategory', JSON.stringify(payload.carCategory));
  const requestObj = {
    fname: payload.driver.fname,
    lname: payload.driver.lname
  };
  const data = await createCorporate(payload.driver, requestObj);
  console.log('requestRegularTripHandler data', JSON.stringify(data));
  console.log('requestRegularTripHandler driver', JSON.stringify(payload.driver));
  const corporateUserId = payload.driver._id;
  const rider = data.data;
  const requestObject = { trip: payload.trip, corporateUserId, carCategory: payload.carCategory, rider, driver: payload.driver };
  assignTripToDriver(requestObject, requestObject.driver);
}

function assignTripToDriver(payload, driver, notification = false) {
  console.log('assignTripToDriver payload', JSON.stringify(payload));
  console.log('assignTripToDriver driver', JSON.stringify(driver));
  createTripObjAsync(payload, driver)
    .then(tripDoc => {
      console.log('assignTripToDriver tripDoc', JSON.stringify(tripDoc));
      return Trip.findOneAndUpdateAsync({ _id: tripDoc._id }, { $set: { tripStatus: tripStatusConsts.arriving } }, { new: true });
    })
    .then(savedTrip => {
      console.log('assignTripToDriver savedTrip', JSON.stringify(savedTrip));
      UserSchema.updateAsync({ $or: [{ _id: savedTrip.riderId }, { _id: savedTrip.driverId }] }, { $set: { currTripId: savedTrip._id, currTripState: tripStatusConsts.arriving } }, { new: true, multi: true })
        .then(() => {
          fetchReturnObj(savedTrip).then((updatedTripObj) => {
            sendTripForDashboard('tripUpdatedDashboard', updatedTripObj);
            updateDriverForDashboard(savedTrip.driverId);
            SocketStore.emitByUserId(savedTrip.driverId, 'tripUpdated', updatedTripObj);
            SocketStore.emitByUserId(savedTrip.riderId, 'tripUpdated', updatedTripObj);
            if (notification) {
              SendNotification(savedTrip.driverId, I18n.t('newTripAssigned'), 'newTripAssigned');
            }
          });
        });
    }
    );
}
/**
 * Trip request handler for RideNow.
 * Send trip request to all of available drivers in parallel.
 * After all drivers accept or reject or don't response, give the trip to the driver that has the shortest arrival time to the pickUp address.
 */
function requestTripCallback(payload) {
  const { trip, carCategory } = payload;
  nearByDriver(trip, carCategory, config.maxDistance).then(drivers => {
    if (!drivers.length) {
      return saveNonAcceptedTrip(payload);
    }

    Promise.all(drivers.map(driver =>
      calcArrivalTime(driver, trip).then(time => {
        payload.trip.arrivalTime = time;
        driver.arrivalTime = time;
        return dispatchHandlerAsync1(driver, payload);
      })))
      .then(results => {
        console.log('request-trip_requestTripCallback accepted: ', results.map(result => `${result.email} ${result.accepted}`).join(','));
        const acceptedDrivers = results.filter(driver => driver.accepted);
        if (!acceptedDrivers.length) {
          results.sort((a, b) => a.arrivalTime - b.arrivalTime);
          const nearestDriver = results[0];
          UserSchema.findById(nearestDriver._id).then(updatedDriver => {
            if (updatedDriver.currTripState === null && carCategory.name === carCategoryConsts.CabdoBlack) {
              assignTripToDriver(payload, nearestDriver, true);
            } else {
              saveNonAcceptedTrip(payload);
            }
          });
        } else {
          acceptedDrivers.sort((a, b) => a.arrivalTime - b.arrivalTime).map((driver, index) => {
            if (index === 0) {
              dispatchDriverAsync(driver, payload, driver.currentTripId);
            } else {
              resetTripRequestAsync(driver, driver.currentTripId).then(() => {
                SocketStore.emitByUserId(driver._id, 'assignedToOther');
              });
            }
          });
        }
      });
  });
}
/**
 * Dispatch handler for RideNow
 * If the driver is currently on a trip, then make and send the trip request as a pre-booking.
 * Returns true or false according to the response of the driver.
 */
function dispatchHandlerAsync1(driver, payload) {
  return new Promise((resolve) => {
    if (driver.currTripState === tripStatusConsts.enRoute) {
      const currentTime = new Date();
      const plusTime = payload.trip.arrivalTime + 20;
      payload.trip.preBookedTime = moment(currentTime).add(plusTime, 'm');
    } else {
      payload.trip.preBookedTime = null;
    }
    if (promObj[driver._id]) { // driver is already in process of another trip request
      driver.accepted = false;
      resolve(driver);
    } else {
      promObj[driver._id] = deferred();
      sendRequestAsync(driver, payload, promObj[driver._id])
        .then((trip) => {
          promObj[driver._id] = null; // set promise object to null so that driver can accept other trip later
          console.log(`[${trip.driver.email} driver Response]`, trip.tripStatus);
          driver.currentTripId = trip._id;
          const response = trip.tripStatus;
          if (response === 'accept' || response === 'booking') { // If the driver is on a trip he will resonse 'booking' otherwise 'accept'.
            driver.accepted = true;
            resolve(driver);
          } else if (response === 'reject') {
            driver.accepted = false;
            resetTripRequestAsync(driver, trip._id) // driver rejected so update the database to update tripRequest missed
              .then(() => resolve(driver));
          }
        },
        (driverObj) => {
          promObj[driverObj._id] = null; // set promise object to null so that driver can accept other trip later
          driverObj.accepted = false;
          resolve(driverObj);
        });
    }
  });
}

function dispatchDriverAsync(driver, payload, tripId) {
  if (driver.currTripState === tripStatusConsts.enRoute) {
    return Trip.findOneAndUpdateAsync({ _id: tripId }, { $set: { tripStatus: tripStatusConsts.booked, driverId: driver._id } }, { new: true })
      .then((tripObj) => {
        SocketStore.emitByUserId(driver._id, 'approvedBookingSoon', 'Approved successfully! Check this trip in "My Bookings" screen.');
        SendNotification(payload.rider._id, I18n.t('acceptedPreBookingBody'));
        driverAcceptedBookings(driver._id);
        fetchReturnObj(tripObj).then((returnObj) => {
          SocketStore.emitByUserId(payload.rider._id, 'tripUpdated', returnObj);
          SocketStore.emitByUserId(payload.rider._id, 'updateMyBookings', {});
        });
      })
      .error((e) => console.error(`dispatchDriverAsync failed: ${driver.currTripState} ${e}`));
  }

  Trip.findOneAndUpdateAsync({ _id: tripId }, { $set: { tripStatus: tripStatusConsts.arriving } }, { new: true })
    .then((updatedTripObject) =>
      UserSchema.updateAsync({ $or: [{ _id: updatedTripObject.riderId }, { _id: updatedTripObject.driverId }] }, { $set: { currTripId: updatedTripObject._id, currTripState: tripStatusConsts.arriving } }, { new: true, multi: true })
        .then(() => {
          fetchReturnObj(updatedTripObject).then((updatedTripObj) => {
            sendTripForDashboard('tripCreatedDashboard', updatedTripObj);
            updateDriverForDashboard(updatedTripObject.driverId);
            SocketStore.emitByUserId(updatedTripObject.riderId, 'tripUpdated', updatedTripObj);
            SocketStore.emitByUserId(updatedTripObject.driverId, 'tripUpdated', updatedTripObj);
          });
        })
    )
    .error((e) => console.error(`dispatchDriverAsync failed: ${driver.currTripState} ${e}`));
}

function sendRequestAsync(driver, payload, prom) {
  let timeout = config.driverResponseTime;
  console.log('[sending request to]', driver.email);
  createTripObjAsync(payload, driver)
    .then((tripObj) => {
      SocketStore.emitByUserId(driver._id, 'requestDriver', tripObj);
      const notiData = Object.assign({}, tripObj);
      notiData.rider.avatar = null;
      notiData.driver.avatar = null;
      SendNotification(driver._id, I18n.t('newTripRequest'), 'newTripRequest', notiData);
      watchIdObj[tripObj._id] = setInterval(() => {
        if (timeout <= 0) {
          watchIdObj[tripObj._id] = clearInterval(watchIdObj[tripObj._id]);
          resetTripRequestAsync(driver, tripObj._id) // driver did not respond so update the database to clear tripRequest made.
            .then(() => {
              SocketStore.emitByUserId(driver._id, 'responseTimedOut'); // clear tripRequest object on driver side
              driver.currentTripId = tripObj._id;
              prom.reject(driver);
            });
        }
        timeout--;
      }, 1000);
    })
    .catch((err) => prom.reject(err));
  return prom.promise;
}

// Only need for corporateTrip. It would be desirable if we can use 'tripUpdated' handler in start-trip.js
function tripUpdateCallback(socket, payload, callback) {
  Trip.findOneAndUpdateAsync({ _id: payload._id }, { $set: payload }, { new: true })
    .then((updatedTripObject) => {
      callback && callback(null, updatedTripObject);

      if (updatedTripObject.tripStatus === 'cancelled') {
        UserSchema.updateAsync({ $or: [{ _id: payload.riderId }, { _id: payload.driverId }] }, { $set: { currTripId: null, currTripState: null, statusTime: null } }, { new: true, multi: true })
          .then((updatedTrip) => {
            sendTripForDashboard('tripUpdatedDashboard', updatedTrip);
          })
          .error((e) => {
            SocketStore.emitByUserId(payload.riderId, 'socketError', { message: 'error while updating curTripId  to null in requestDriverResponse', data: e });
            SocketStore.emitByUserId(payload.driverId, 'socketError', { message: 'error while updating curTripId to null in requestDriverResponse', data: e });
          });
      }
      fetchReturnObj(updatedTripObject).then((updatedTripObj) => {
        // socket request
        if (socket) {
          if (socket.userId.toString() === updatedTripObj.riderId.toString()) {
            SocketStore.emitByUserId(updatedTripObj.driverId, 'tripUpdated', updatedTripObj);
          } else if (socket.userId.toString() === updatedTripObj.driverId.toString()) {
            SocketStore.emitByUserId(updatedTripObj.riderId, 'tripUpdated', updatedTripObj);
          }
          // api request
        } else {
          SocketStore.emitByUserId(updatedTripObj.driverId, 'tripUpdated', updatedTripObj);
        }
      });
    })
    .error((e) => {
      callback && callback(e);
      SocketStore.emitByUserId(payload.riderId, 'socketError', e);
      SocketStore.emitByUserId(payload.driverId, 'socketError', e);
    });
}
/**
 * Trip Request handler for RideLater
 * If the trip is for in an hour, send booking request to all of available drivers one by one until one accepts it.
 */
function requestBookingTripCallback(payload) {
  const carCategory = payload.carCategory || {};
  if (moment(payload.trip.preBookedTime).diff(moment(), 'hours') < 1) {
    nearByDriver(payload.trip, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
      if (nearByDriversDoc.length > 0) {
        roundRobinAsync(nearByDriversDoc, payload)
          .then((result) => {
            if (!result) {
              createTripObjAsync(payload, null).then(savedTrip => sendBookedNotification(savedTrip, carCategory));
              for (let i = 0; i < nearByDriversDoc.length; i++) {
                SocketStore.emitByUserId(nearByDriversDoc[i]._id, 'requestAvailableBookings');
              }
            }
          });
      } else {
        createTripObjAsync(payload, null).then(savedTrip => sendBookedNotification(savedTrip, carCategory));
      }
    }).catch((e) => console.error(e));
  } else {
    nearByDriver(payload.trip, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
      if (nearByDriversDoc.length > 0) {
        for (let i = 0; i < nearByDriversDoc.length; i++) {
          SocketStore.emitByUserId(nearByDriversDoc[i]._id, 'requestAvailableBookings');
        }
      }
      createTripObjAsync(payload, null).then(savedTrip => sendBookedNotification(savedTrip, carCategory));
    }).catch((e) => console.error(e));
  }
}
/**
 * Round robin algorithm for sending booking request to drivers one by one.
 * expect a response in quantum time.
 * if response is "booked" - assign that driver, break process and return true.
 * if response is "reject" - remove driver from the list and select next driver to request from queue.
 * if no response - next driver please.
 * if there is no remained driver, then return false.
 */
function roundRobinAsync(nearByDriversDoc, payload) {
  return new Promise((resolve, reject) => {
    const count = 0;
    const remain = nearByDriversDoc.length;
    const prom = deferred();
    dispatchHandlerAsync(nearByDriversDoc, remain, count, payload, prom)
      .then((result) => resolve(result))
      .catch((error) => reject(error));
  });
}

function dispatchHandlerAsync(nearByDrivers, remain, count, payload, prom) {
  if (remain <= 0) {
    prom.resolve(false);
    return prom.promise;
  }
  if (promObj[nearByDrivers[count]._id]) { // driver is already in process of another trip request
    nearByDrivers = removeDriverFromList(nearByDrivers, count);
    count = 0;
    remain--;
    setTimeout(() => {
      dispatchHandlerAsync(nearByDrivers, remain, count, payload, prom);
    }, 1000);
  } else {
    promObj[nearByDrivers[count]._id] = deferred();
    sendRequestAsync(nearByDrivers[count], payload, promObj[nearByDrivers[count]._id])
      .then((trip) => {
        promObj[nearByDrivers[count]._id] = null; // set promise object to null so that driver can accept other trip later
        const response = trip.tripStatus;
        if (response === 'booking') {
          sendBookedNotification(trip, null, false);
          handleAcceptBooking({ trip, driverId: nearByDrivers[count]._id });
          prom.resolve(true);
        } else if (response === 'reject') {
          resetTripRequestAsync(nearByDrivers[count], trip._id)
            .then(() => {
              nearByDrivers = removeDriverFromList(nearByDrivers, count);
              count = 0;
              remain--;
              setTimeout(() => {
                dispatchHandlerAsync(nearByDrivers, remain, count, payload, prom);
              }, 1000);
            });
        }
      },
      () => {
        promObj[nearByDrivers[count]._id] = null; // set promise object to null so that driver can accept other trip later
        nearByDrivers = removeDriverFromList(nearByDrivers, count);
        count = 0;
        remain--;
        setTimeout(() => {
          dispatchHandlerAsync(nearByDrivers, remain, count, payload, prom);
        }, 1000);
      });
  }
  return prom.promise;
}

function handleAcceptBooking(payload) {
  checkDriverAvailability(payload.driverId, payload.trip.preBookedTime, payload.trip).then(status => {
    if (status.success) {
      setDriverToBooking(payload).then(async (updatedBookings) => {
        SocketStore.emitByUserId(payload.driverId, 'approvedBookingSoon', updatedBookings.message);
        driverAcceptedBookings(payload.driverId);
        if (updatedBookings.success) {
          SocketStore.emitByUserId(payload.trip.riderId, 'updateMyBookings', {});
          SendNotification(payload.trip.riderId, I18n.t('acceptedPreBookingBody'));
          const categories = await fetchCarCategoriesData();
          const carCategory = categories.find((arr) => {
            return arr.name === payload.trip.taxiType;
          });
          nearByDriver(payload.trip, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
            for (let i = 0; i < nearByDriversDoc.length; i++) {
              SocketStore.emitByUserId(nearByDriversDoc[i]._id, 'setAvailableBookings', updatedBookings.data);
            }
          }).catch((e) => console.error(e));
        }
      });
    } else {
      SocketStore.emitByUserId(payload.driverId, 'foundApprovedBooking', {});
    }
  });
}

function cancelBookingDriverAsync(trip) {
  CarCategorySchema.findOneAsync({ name: trip.taxiType })
    .then(carCategory => {
      removeDriverFromBooking(trip).then(updatedBookings => {
        SocketStore.emitByUserId(trip.driverId, 'updateMyBookings', {});
        SocketStore.emitByUserId(trip.riderId, 'updateMyBookings', {});
        if (updatedBookings.success) {
          if (moment(trip.preBookedTime).diff(moment(), 'minutes') < 25) {
            nearByDriver(trip, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
              if (nearByDriversDoc.length > 0) {
                for (let i = 0; i < nearByDriversDoc.length; i++) {
                  if (nearByDriversDoc[i]._id.toString() === trip.driverId) {
                    nearByDriversDoc = removeDriverFromList(nearByDriversDoc, i);
                  }
                }
                UserSchema.findById(trip.riderId)
                  .then(rider => {
                    return { trip, rider, carCategory };
                  })
                  .then(retObj => {
                    Trip.remove({ _id: trip._id }).then(() => {
                      roundRobinAsync(nearByDriversDoc, retObj)
                        .then((result) => {
                          if (!result) {
                            createTripObjAsync(retObj, null);
                            nearByDriversDoc.forEach(driver => {
                              SocketStore.emitByUserId(driver._id, 'requestAvailableBookings');
                            });
                          }
                        });
                    });
                  });
              } else {
                nearByDriversDoc.forEach(driver => {
                  SocketStore.emitByUserId(driver._id, 'requestAvailableBookings');
                });
              }
            });
          }
        }
      });
    }).catch((e) => console.error(e));
}

function cancelBookingRiderAsync(tripObj) {
  const riderID = tripObj.riderId;
  const driverID = tripObj.driverId;
  Trip.findOneAndUpdateAsync({ _id: tripObj._id }, { $set: { tripStatus: tripStatusConsts.deleted, deletedTime: Date.now() } }, { new: true })
    .then(() => {
      deleteTripFromDashboard(tripObj._id);
      if (driverID) {
        SocketStore.emitByUserId(driverID, 'canceledBookingByRider', tripObj);
      }
      SocketStore.emitByUserId(riderID, 'updateMyBookings', {});
      CarCategorySchema.findOneAsync({ name: tripObj.taxiType })
        .then(carCategory => {
          nearByDriver(tripObj, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
            if (nearByDriversDoc.length > 0) {
              for (let i = 0; i < nearByDriversDoc.length; i++) {
                SocketStore.emitByUserId(nearByDriversDoc[i]._id, 'requestAvailableBookings');
              }
            }
          });
        });
    })
    .catch((e) => console.error(e));
}

function startBookingTrip(trip) {
  Trip.findOneAndUpdateAsync({ _id: trip._id }, { $set: { tripStatus: tripStatusConsts.arriving } }, { new: true })
    .then(tripObj => {
      if (tripObj) {
        UserSchema.updateAsync({ $or: [{ _id: tripObj.riderId }, { _id: tripObj.driverId }] }, { $set: { currTripId: tripObj._id, currTripState: tripStatusConsts.arriving } }, { new: true, multi: true })
          .then(() => {
            updateDriverForDashboard(tripObj.driverId);
            fetchReturnObj(tripObj).then((updatedTripObj) => {
              sendTripForDashboard('tripUpdatedDashboard', updatedTripObj);
              SocketStore.emitByUserId(tripObj.riderId, 'cancelTripRequest', updatedTripObj);
              SocketStore.emitByUserId(tripObj.driverId, 'tripUpdated', updatedTripObj);
              SocketStore.emitByUserId(tripObj.riderId, 'tripUpdated', updatedTripObj);
              SocketStore.emitByUserId(tripObj.driverId, 'startArrivingBookedTrip', updatedTripObj);
            });
          })
          .error((e) => {
            SocketStore.emitByUserId(tripObj.riderId, 'socketError', { message: 'error while updating curTripId in arrivingTrip', data: e });
            SocketStore.emitByUserId(tripObj.driverId, 'socketError', { message: 'error while updating curTripId in arrivingTrip', data: e });
          });
      }
    })
    .error(err => console.error('startBookingTrip failed: ', err));
}

function driverAcceptedBookings(driverId) {
  getDriverAcceptedBookings(driverId).then((bookings) => {
    SocketStore.emitByUserId(driverId, 'setMyBookings', bookings);
  });
}

function riderBookings(riderId) {
  getRiderBookings(riderId).then((bookings) => {
    SocketStore.emitByUserId(riderId, 'setMyBookings', bookings);
  });
}

function fetchAvailableBookings(driver) {
  const carCategory = driver.carDetails.carCategory;
  if (carCategory) {
    getAvailableBookings(driver, carCategory, config.maxBookingDistance).then(bookings => {
      SocketStore.emitByUserId(driver._id, 'setAvailableBookings', bookings);
    });
  } else {
    SocketStore.emitByUserId(driver._id, 'setAvailableBookings', []);
  }
}

async function updateAvailableBookings(trip) {
  const categories = await fetchCarCategoriesData();
  const carCategory = categories.find((arr) => {
    return arr.name === trip.taxiType;
  });
  nearByDriver(trip, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
    for (let i = 0; i < nearByDriversDoc.length; i++) {
      SocketStore.emitByUserId(nearByDriversDoc[i]._id, 'updateAvailableBookings', {});
    }
  }).catch((e) => console.error(e));
}

async function getDistance(origin, destination) {
  return new Promise((resolve) =>
    request.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${encodeURI(origin)}&destinations=${encodeURI(destination)}&key=${config.googleAPIkey}`, (err, resp, body) => {
      if (err || resp.status !== 200) {
        return resolve(NaN);
      }

      body = JSON.parse(body);

      if (body.status === 'OK' && body.rows[0].elements[0].distance) {
        return resolve(body.rows[0].elements[0].distance);
      }
      resolve(NaN);
    })
  );
}

function createTripObjAsync(payload, driver) {
  return new Promise(async (resolve) => {
    const riderId = payload.rider._id;
    const driverId = driver ? driver._id : null;
    const srcLoc = payload.trip.srcLoc;
    const destLoc = payload.trip.destLoc;
    const pickUpAddress = payload.trip.pickUpAddress;
    const destAddress = payload.trip.destAddress;
    const latitudeDelta = payload.trip.latitudeDelta;
    const longitudeDelta = payload.trip.longitudeDelta;
    const isMetered = payload.trip.isMetered;
    const corporateUserId = payload.corporateUserId;
    const paymentMode = payload.trip.paymentMode;
    const tripAmt = payload.trip.tripAmt;
    const travelTime = payload.trip.travelTime;
    const fareDetails = payload.trip.fareDetails;
    const taxiType = payload.carCategory.name;
    const preBookedTime = payload.trip.preBookedTime;
    const arrivalTime = payload.trip.arrivalTime;
    const tripStatus = preBookedTime ? tripStatusConsts.booking : tripStatusConsts.request;
    const switchedFromCabdo = (tripStatus === tripStatusConsts.request && taxiType !== carCategoryConsts.CabdoBlack) ? payload.trip.switchedFromCabdo : false;
    const origin = `${srcLoc[0]},${srcLoc[1]}`;
    const destination = destLoc ? `${destLoc[0]},${destLoc[1]}` : null;
    const distance = destLoc ? await getDistance(origin, destination) : null;

    const carDetails = driver ? driver.carDetails : null;
    const preTripObj = {
      riderId,
      srcLoc,
      destLoc,
      pickUpAddress,
      destAddress,
      latitudeDelta,
      longitudeDelta,
      isMetered,
      corporateUserId,
      paymentMode,
      tripAmt,
      travelTime,
      fareDetails,
      taxiType,
      preBookedTime,
      tripStatus,
      arrivalTime,
      switchedFromCabdo,
      tripId: null,
      distance,
      carDetails,
      tripNumber: new Date().getTime()
    };
    // trip Number needs because app have errors sometime this duplicate key
    const tripObj = new Trip(driver ? { ...preTripObj, driverId } : preTripObj);
    tripObj.saveAsync()
      .then((savedTrip) => {
        savedTrip.rider = null;
        savedTrip.driver = null;
        if (tripStatus === tripStatusConsts.request) {
          UserSchema.updateAsync({ $or: [{ _id: savedTrip.riderId }, { _id: savedTrip.driverId }] }, { $set: { currTripId: savedTrip._id, currTripState: tripStatusConsts.request, statusTime: new Date() } }, { new: true, multi: true })
            .then(() => {
              sendTripForDashboard('tripCreatedDashboard', savedTrip);
              fetchReturnObj(savedTrip).then((returnObj) => resolve(returnObj));
            })
            .error((e) => {
              SocketStore.emitByUserId(riderId, 'socketError', { message: 'error while updating curTripId in requestTrip', data: e });
              SocketStore.emitByUserId(driverId, 'socketError', { message: 'error while updating curTripId in requestTrip', data: e });
            });
        } else {
          sendTripForDashboard('tripCreatedDashboard', savedTrip);
          fetchReturnObj(savedTrip).then((returnObj) => {
            resolve(returnObj);
          });
          updateAvailableBookings(savedTrip);
        }
      })
      .error((e) => {
        SocketStore.emitByUserId(riderId, 'socketError', e.message);
      });
  });
}

async function saveNonAcceptedTrip(payload) {
  payload.trip.tripStatus = 'noNearByDriver';
  payload.trip.riderId = payload.rider._id;
  const origin = `${payload.trip.srcLoc[0]},${payload.trip.srcLoc[1]}`;
  const destination = `${payload.trip.destLoc[0]},${payload.trip.destLoc[1]}`;
  payload.trip.distance = await getDistance(origin, destination);
  SocketStore.emitByUserId(payload.rider._id, 'tripUpdated', payload.trip);
  const tripObj = new Trip(payload.trip);
  tripObj.saveAsync().error(e => console.error(`create nonAcceptedTrip failed: ${e}`));
}

async function resetTripRequestAsync(driverObj, tripId) {
  const promArray = [];
  promArray[0] = Trip.remove({ _id: tripId });
  if (driverObj.currTripState !== tripStatusConsts.enRoute) {
    promArray[1] = UserSchema.updateAsync({ $or: [{ _id: driverObj._id }] }, { $set: { currTripId: null, currTripState: null, statusTime: null } }, { new: true, multi: true });
  }
  return Promise.all(promArray).error((e) => console.error(`resetTripRequestAsync failed: ${e}`));
}

export function checkSocketConnection(id) {
  const res = SocketStore.getByUserId(id);
  return res.success && res.data;
}

function removeDriverFromList(drivers, index) { // test passed
  return drivers.slice(0, index).concat(drivers.slice(index + 1));
}

function checkDriverAvailability(driverId, preBookedTime, trip) {
  return findSoonTrips(driverId, preBookedTime, trip);
}

function sendBookedNotification(tripObj, carCategory, notification = true) {
  SocketStore.emitByUserId(tripObj.riderId, 'tripUpdated', tripObj);
  if (notification) {
    UserSchema.findAsync({ 'carDetails.carCategory': carCategory._id }).then(drivers => {
      drivers.map(driver => SendNotification(driver._id, I18n.t('newBooking')));
    });
  }
}

function nearByDriver(tripObj, carCategory, maxDistance) {
  return new Promise((resolve, reject) =>
    UserSchema.findAsync({
      $and: [
        { gpsLoc: { $near: [tripObj.srcLoc[1], tripObj.srcLoc[0]], $maxDistance: maxDistance } },
        { currTripState: { $in: [null, tripStatusConsts.enRoute] } },
        { loginStatus: true },
        { availability: true },
        { userType: 'driver' },
        { 'carDetails.carCategory': carCategory._id }
      ]
    })
      .then(nearByDrivers => {
        if (!nearByDrivers) {
          const err = new APIError(`no nearByDriver found`, httpStatus.INTERNAL_SERVER_ERROR);
          return reject(err);
        }
        const driverIdArray = [];
        nearByDrivers.map(driver => driverIdArray.push(driver._id));
        findUnavailableDrivers(driverIdArray, tripObj).then(unavailableDriverIds => {
          const availableDrivers = nearByDrivers.filter(driver => !unavailableDriverIds.find(id => driver._id.equals(id)));
          console.log('[availableDrivers:]', availableDrivers.map(driver => driver.email));
          return resolve(availableDrivers);
        });
      })
      .error(driverErr => reject(driverErr))
  );
}

function setDestAddress(tripId, payload) {
  return new Promise((resolve, reject) => {
    Trip.findOneAndUpdateAsync({ _id: tripId }, { $set: { destLoc: payload.destLoc, destAddress: payload.destAddress } }, { new: true })
      .then((savedTrip) => {
        sendTripForDashboard('tripUpdatedDashboard', savedTrip);
      })
    .error(error => reject(error));
  });
}
/**
 * Find drivers who has a booked trip in 20 mins from the requested time or prebooked time.
 * If the requested trip is a RideNow then add one condition for drivers that are currently on a trip but cannot finish it in 10 mins.
 * The drivers found here will be removed from the list of available drivers.
 */
export function findUnavailableDrivers(driverIdArray, tripObj) {
  const bookingTime = tripObj.preBookedTime || new Date();
  const ltTime = moment(bookingTime).add(20, 'm');
  const gtTime = moment(bookingTime);
  const limitTime = tripObj.preBookedTime ? null : 10;

  return new Promise((resolve, reject) =>
    Trip.findAsync({
      $or: [
        { driverId: { $in: driverIdArray }, tripStatus: 'booked', preBookedTime: { $lt: ltTime, $gt: gtTime } },
        { driverId: { $in: driverIdArray }, tripStatus: tripStatusConsts.enRoute, arrivalTime: { $gt: limitTime } }
      ]
    })
      .then(trips => {
        if (trips) {
          const returnArray = trips.map(trip => trip.driverId);
          return resolve(returnArray);
        } else {
          const err = new APIError(`no unavailableDrivers found`, httpStatus.INTERNAL_SERVER_ERROR);
          return reject(err);
        }
      })
      .error(driverErr => reject(driverErr))
  );
}
/**
 * Arrival time calculator from driver's current position to pickup location.
 * If the driver is currently on a trip, then arrivalTime =
 * (time from current position to destination of current trip) + (time from destination of current trip to the pickup of new trip).
 */
export async function calcArrivalTime(driver, trip) {
  let origin;
  let remainingTime = 0;
  const destination = `${trip.srcLoc[0]},${trip.srcLoc[1]}`;
  if (driver.currTripState === tripStatusConsts.enRoute) {
    const currentTrip = await Trip.findOneAsync({ tripStatus: tripStatusConsts.enRoute, driverId: driver._id });
    if (currentTrip) {
      origin = `${currentTrip.destLoc[0]},${currentTrip.destLoc[1]}`;
      remainingTime = currentTrip.arrivalTime;
    } else {
      origin = `${driver.gpsLoc[1]},${driver.gpsLoc[0]}`;
    }
  } else {
    origin = `${driver.gpsLoc[1]},${driver.gpsLoc[0]}`;
  }
  return new Promise((resolve) =>
    request.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${encodeURI(origin)}&destinations=${encodeURI(destination)}&key=${config.googleAPIkey}`, (err, resp, body) => {
      if (err || resp.status !== 200) {
        return resolve(100); // if failed in google api, return 100 min of arrivalTime so that driver has last priority.
      }
      body = JSON.parse(body);

      if (body.status === 'OK' && body.rows[0].elements[0].distance && body.rows[0].elements[0].duration) {
        let duration = Math.round(body.rows[0].elements[0].duration.value / 60);
        if (remainingTime) {
          duration = duration + remainingTime;
        }
        return resolve(duration);
      }

      resolve(100);
    })
  );
}

export async function sendBookingReminder(trip) {
  const categories = await fetchCarCategoriesData();
  const carCategory = categories.find((arr) => {
    return arr.name === trip.taxiType;
  });
  return nearByDriver(trip, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
    if (nearByDriversDoc.length > 0) {
      UserSchema.findById(trip.riderId)
        .then(rider => {
          const retObj = { trip, rider, carCategory };
          if (trip.corporateUserId) {
            retObj.corporateUserId = trip.corporateUserId;
          }
          return retObj;
        })
        .then(retObj => {
          return roundRobinAsync(nearByDriversDoc, retObj)
            .then((result) => {
              if (result === true) {
                removeEmptyBookingObjAsync(trip);
              }
            });
        });
    }
  }).catch((e) => console.error(e));
}

export async function sendSmsReminder(trip) {
  const categories = await fetchCarCategoriesData();
  const carCategory = categories.find((arr) => {
    return arr.name === trip.taxiType;
  });
  return nearByDriver(trip, carCategory, config.maxBookingDistance).then((nearByDriversDoc) => {
    if (nearByDriversDoc.length > 0) {
      nearByDriversDoc.forEach(async (driver) => {
        if (config.env === 'test' || config.env === 'development') {
          return { status: true };
        }
        if (driver.phoneNo) {
          twilioClient.messages.create({
            body: `Ausstehende Vorbestellung: ${trip.pickUpAddress} - ${moment(trip.preBookedTime).format('HH:mm')}`,
            to: driver.phoneNo,
            from: config.fromTelNumber
          }).then(() => {
            console.log('successfully sent sms');
          }).catch((err) => {
            console.error(`Can't send remind sms to ${driver.phoneNo}: ${err}`);
          });
        }
      });
    }
  }).catch((e) => console.error(e));
}

export function sendAcceptedBookingReminder(trip) {
  SocketStore.emitByUserId(trip.driverId, 'acceptedBookingReminder', trip);
}

export function removeEmptyBookingObjAsync(trip) {
  Trip.remove({ _id: trip._id, driverId: null });
}

export function sendTripForDashboard(eventName, tripObj) {
  Trip.findOne({ _id: tripObj._id })
    .populate('driverId')
    .populate('riderId')
    .execAsync()
    .then((trip) => {
      SocketStore.emitAdminEvent(eventName, trip);
    })
    .catch((e) => console.error(e));
}

export function deleteTripFromDashboard(tripId) {
  SocketStore.emitAdminEvent('tripDeletedDashboard', tripId);
}

export function updateDriverForDashboard(driverId) {
  UserSchema.findById(driverId).then((driver) => {
    SocketStore.emitAdminEvent('driverUpdatedDashboard', driver);
  })
  .catch((e) => console.error(e));
}

export default requestTripHandler;
