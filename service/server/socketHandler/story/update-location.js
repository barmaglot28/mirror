import SocketStore from '../../service/socket-store.js';
import UserSchema from '../../models/user';
import TripSchema from '../../models/trip';
import config from '../../../config/env';
import request from 'request';

/**
* updateLocation handler, handle location update of the rider or driver
* @param socket object
* @returns {*}
*/

function updateLocationHandler(socket) {
  /**
  * updateLocation event is fired by rider or driver whenever their location is changed. also it send location update to corresponding rider or driver if they are in any tripRequest or trip.
  * @param userObj - user whose location has to be updated
  * @returns emit an updateDriverLocation or updateRiderLocation event based on userType.
  */

  socket.on('updateLocation', (userObj) => {
    const userID = userObj._id;
    UserSchema.findOneAndUpdateAsync({ _id: userID }, { $set: { gpsLoc: [userObj.gpsLoc[1], userObj.gpsLoc[0]] } }, { new: true })
      .then((updatedUser) => {
        console.log(`[${updatedUser.email} location ${updatedUser.gpsLoc}]`);

        if (updatedUser.userType === 'driver') {
          TripSchema.findOneAsync({ $and: [{ driverId: userID }, { $or: [{ tripStatus: 'enRoute' }, { tripStatus: 'arriving' }, { tripStatus: 'arrived' }] }] })
            .then((tripObj) => {
              if (tripObj) {
                SocketStore.emitByUserId(tripObj.riderId, 'updateDriverLocation', [updatedUser.gpsLoc[1], updatedUser.gpsLoc[0]]);
                changeArrivalStatus(updatedUser, tripObj);
              }
            })
            .error((e) => {
              SocketStore.emitByUserId(userID, 'socketError', e);
            });
        }
      })
      .error((e) => {
        SocketStore.emitByUserId(userID, 'socketError', e);
      });
  });
}

function changeArrivalStatus(driverObj, tripObj) {
  const origin = `${driverObj.gpsLoc[1]},${driverObj.gpsLoc[0]}`;
  const destination = (tripObj.tripStatus === 'arriving') ? `${tripObj.srcLoc[0]},${tripObj.srcLoc[1]}` : `${tripObj.destLoc[0]},${tripObj.destLoc[1]}`;
  request.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${encodeURI(origin)}&destinations=${encodeURI(destination)}&key=${config.googleAPIkey}`, (err, resp, body) => {
    if (err) {
      const error = `Error in fetching google map api ${err ? err.message : ''} ${err}`;
      console.error(error);
      return;
    }

    body = JSON.parse(body);

    if (body.status === 'OK' && body.rows[0].elements[0].distance && body.rows[0].elements[0].duration) {
      const distance = Math.round(body.rows[0].elements[0].distance.value);
      const duration = Math.round(body.rows[0].elements[0].duration.value / 60);
      if (tripObj.arrivalTime !== duration) {
        tripObj.arrivalTime = duration;
        TripSchema.findOneAndUpdateAsync({ _id: tripObj._id }, { $set: tripObj }, { new: true })
          .then(() => {
            const newArrivalStatus = {
              remainingTime: duration,
              status: (distance <= config.arrivedDistance) ? 'arrived' : 'arriving'
            };
            SocketStore.emitByUserId(tripObj.riderId, 'arrivingStatusUpdated', newArrivalStatus);
            SocketStore.emitByUserId(tripObj.driverId, 'arrivingStatusUpdated', newArrivalStatus);
          });
      }
    }
  });
}

export default updateLocationHandler;
