import SocketStore from '../service/socket-store';
import requestTripHandler from './story/request-trip';
import startTripHandler from './story/start-trip';
import updateLocationHandler from './story/update-location';
import nearbyDriverHandler from './story/nearby-driver-handler';

const socketHandler = (socket) => {
  requestTripHandler(socket);
  startTripHandler(socket);
  updateLocationHandler(socket);
  nearbyDriverHandler(socket);
  socket.on('hello', () => {
    socket.emit('helloResponse', 'hello everyone');
  });

  socket.on('disconnect', () => {
    SocketStore.removeByUserId(socket.userId, socket.userType, socket);
  });
};

export default socketHandler;
