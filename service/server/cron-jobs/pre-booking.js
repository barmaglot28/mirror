import Trip from '../models/trip';
import { sendBookingReminder, sendAcceptedBookingReminder, } from '../socketHandler/story/request-trip';

export default (freq) => {
  setInterval(() => {
    const nowTime = new Date().getMinutes();

    const gtTime = new Date();
    gtTime.setMinutes(nowTime + 59);
    const lteTime = new Date();
    lteTime.setMinutes(nowTime + 60);

    Trip.find({
      tripStatus: 'booking',
      driverId: null,
      preBookedTime: { $gte: gtTime, $lte: lteTime }
    })
      .then((tripRequests) => {
        if (tripRequests.length > 0) {
          console.log(`${tripRequests.length} pre booked found`);
        }

        tripRequests.forEach((tripRequest) => {
          sendBookingReminder(tripRequest);
        });
      });

    const gtTimeRemind = new Date();
    gtTimeRemind.setMinutes(nowTime + 29);
    const lteTimeRemind = new Date();
    lteTimeRemind.setMinutes(nowTime + 30);
    Trip.find({
      tripStatus: 'booked',
      driverId: { $ne: null },
      preBookedTime: { $gt: gtTimeRemind,
                       $lte: lteTimeRemind }
    })
      .then(tripRequests => {
        tripRequests.forEach(tripRequest => sendAcceptedBookingReminder(tripRequest));
      });
  }, freq * 60 * 1000);

  setInterval(() => {
    const now = new Date();
    const deletedTimeFrom = new Date(now.setDate(now.getDate() - 7));

    Trip.find({
      tripStatus: 'deleted',
      deletedTime: { $lte: deletedTimeFrom }
    })
      .then((tripsForDelete) => {
        tripsForDelete.forEach(trip => {
          Trip.removeAsync({ _id: trip._id })
            .catch((e) => console.error(e));
        });
      });
    // one time per day
  }, 1000 * 60 * 60 * 24);
};
