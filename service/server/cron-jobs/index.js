import preBooking from './pre-booking';
import checkDriverStatus from './check-driverstatus';

export default () => {
  preBooking(1); // freq in minutes, offset in minutes
  checkDriverStatus(0.5);
};
