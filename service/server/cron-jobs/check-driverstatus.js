import UserSchema from '../models/user';
import moment from 'moment';
import tripStatus from '../constants/tripStatus';

export default (freq) => {
  setInterval(() => {
    const nowTime = new Date();
    const ltTime = moment(nowTime).subtract(1, 'm');

    UserSchema.updateAsync({ currTripState: tripStatus.request, statusTime: { $lte: ltTime } },
      { $set: { currTripId: null, currTripState: null, statusTime: null } },
      { new: true, multi: true })
      .then(result => {
        if (result.nModified > 0) {
          console.log(`check-driverstatus cron job, expired: ${result.nModified} ok: ${result.ok}`);
        }
      });
  }, freq * 60 * 1000);
};
