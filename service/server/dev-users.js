export default {
  testAdmin: {
    email: 'testAdmin@cabdo.de',
    password: '3ui3uti43uio3hjwwtert',
    userType: 'admin',
    fname: 'First',
    lname: 'Administrator',
    phoneNo: '9876543210'
  },
  users: [{
    email: 'abc@xyz.com',
    password: '123',
    userType: 'admin',
    fname: 'First',
    lname: 'Administrator',
    phoneNo: '9876543210'
  }, {
    email: 'abc@xyz2.com',
    password: '123',
    userType: 'rider',
    fname: 'First',
    lname: 'Rider',
    phoneNo: '9876543210'
  }, {
    email: 'abc@xyz3.com',
    password: '123',
    userType: 'rider',
    fname: 'Second',
    lname: 'Rider',
    phoneNo: '9876543210'
  }, {
    email: 'abc@xyz4.com',
    password: '123',
    userType: 'driver',
    fname: 'First',
    lname: 'Driver',
    phoneNo: '9876543210'
  }, {
    email: 'abc@xyz5.com',
    password: '123',
    userType: 'driver',
    fname: 'Second',
    lname: 'Driver',
    phoneNo: '9876543210'
  },
  {
    email: 'abc@xyz6.com',
    password: '123',
    userType: 'driver',
    fname: 'Third',
    lname: 'Driver',
    phoneNo: '9876543210'
  },
  {
    email: 'abc@xyz7.com',
    password: '123',
    userType: 'driver',
    fname: 'Fourth',
    lname: 'Driver',
    phoneNo: '9876543210'
  }],
  corporateUsers: [{
    email: 'hotel@corp.com',
    password: '123',
    userType: 'corporate',
    fname: 'Hotel',
    lname: 'Corp',
    phoneNo: '9876543210'
  }, {
    email: 'hospital@corp.com',
    password: '123',
    userType: 'corporate',
    fname: 'Hospital',
    lname: 'Corp',
    phoneNo: '9876543210'
  }]
};
