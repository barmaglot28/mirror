import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';
import io from 'socket.io-client';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import devUsers from '../dev-users';

chai.config.includeStack = true;

describe('## user Sync data api', () => {
  let rider1 = {
    email: 'abc321@xyz.com',
    password: '123',
    userType: 'rider',
    fname: 'abc321',
    lname: 'xyz321',
    phoneNo: '9876543221'
  };
  let riderJwtAccessToken = null;

  let driver1 = {
    email: 'xyz321@abc.com',
    password: '123',
    userType: 'driver',
    fname: 'xyz321',
    lname: 'abc321',
    phoneNo: '9876543222'
  };
  let driverJwtAccessToken = null;
  let tripRequestObject = null;

  let admin = devUsers.testAdmin;
  let jwtAccessToken = null;

  describe('# POST /api/auth/login', () => {
    it('should authenticate user and provide token', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(admin)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('jwtAccessToken', 'user', 'maxDistance', 'restoredWallet');
          expect(res.body.data.user.loginStatus).to.equal(true);
          admin = res.body.data.user;
          jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new rider', (done) => {
      request(app)
        .post('/api/users/register')
        .send(rider1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          rider1 = res.body.data.user;
          riderJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new driver', (done) => {
      request(app)
        .post('/api/users/register')
        .send(driver1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          driver1 = res.body.data.user;
          driverJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  const carcategoryData = {
    name: 'Category 3',
    pricePerKilometer: '1000',
    pricePerMinute: '30',
    baseFare: '20',
    minimumFare: '15',
    commissionToCabdo: '5',
    capacity: '3',
    isMetered: false,
  };

  let carcategory = {};

  describe('# POST /api/admin/carcategories', () => {
    it('should create a new car categories', (done) => {
      request(app)
        .post('/api/admin/carcategories')
        .send(carcategoryData)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          carcategory = res.body.data;
          done();
        });
    });
  });

  let car = {
    licensePlateNumber: 'DO-OO-333',
    make: '1980',
    model: 'Economy',
    canBeUsed: true,
  };

  describe('# POST /api/admin/car', () => {
    it('should create a new car', (done) => {
      car.carCategory = carcategory._id;
      request(app)
        .post('/api/admin/car')
        .send(car)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# POST /api/car/carId', () => {
    it('should lock a car', (done) => {
      request(app)
        .post(`/api/cars/${car._id}`)
        .send(car)
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# user not in any trip or tripRequest', () => {
    it('# rider should receive tripRequest and trip Object as null', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      riderSocket.disconnect();
      request(app)
        .get('/api/syncData')
        .set('Authorization', riderJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.message).to.eql('user is not in any trip or tripRequest');
          expect(res.body.data.trip).to.eql(null);
          done();
        });
    });
  });
  describe('# rider in a tripRequest state and socket gets disconnedted', () => {
    it('rider should get the curr Synced data', (done) => {
      let riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      const driverSocket = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken } });
      const payload = {
        rider: rider1,
        trip: {
          srcLoc: [72.8536, 19.0217],
          destLoc: [71, 81],
          pickUpAddress: 'geekyants(51,61)',
          destAddress: 'bommanahalli(71,81)',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket.emit('requestTrip', payload);
      driverSocket.on('requestDriver', (tripRequest) => {
        const driverResponse = 1;
        if (driverResponse) {
          // tripRequest object should have following property
          expect(tripRequest.tripStatus).to.eql('request');
          expect(tripRequest.driver).to.be.an('object');
          expect(tripRequest.driver.currTripId).to.eql(tripRequest._id);
          expect(tripRequest.driver.currTripState).to.eql('request');
          expect(tripRequest.rider).to.be.an('object');
          tripRequest.tripStatus = 'accept';
          tripRequest.tripRequestIssue = 'no Issue';
          driverSocket.emit('requestDriverResponse', tripRequest);
        }
      });
      driverSocket.on('tripUpdated', (data) => {
        if (data.tripStatus === 'arriving') {
          const index = 1;
          tripRequestAutomation(index, data, driverSocket);
        }
      });

      riderSocket.on('tripUpdated', (data) => {
        if (data.tripStatus === 'arriving') {
          // rider socket reconnect with the socket server and request a syncData api
          riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });

          setTimeout(() => {
            request(app)
              .get('/api/syncData')
              .set('Authorization', riderJwtAccessToken)
              .expect(httpStatus.OK)
              .then((res) => {
                expect(res.body.success).to.equal(true);
                expect(res.body.data.trip).to.be.an('object');
                done();
              });
          }, 200);
        }
      });

      riderSocket.on('tripUpdated', (tripRequestObj) => {
        tripRequestObject = tripRequestObj;
        if (tripRequestObj.tripStatus === 'arrived') {
          expect(tripRequestObj.tripStatus).to.be.eql('arrived');
          riderSocket.disconnect();
          driverSocket.disconnect();
        }
      });
    });
  });
  describe('# rider and driver is in trip and rider socket gets disconnedted', () => {
    it('# rider should get trip object in syncData api call', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      const driverSocket = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken } });
      // using the above tripRequestObject for start trip
      driverSocket.emit('startTrip', tripRequestObject, (tripObj) => {
        if (tripObj !== null) {
          // callback function receives trip Object with the following property
          expect(tripObj.rider).to.be.an('object');
          expect(tripObj.rider.currTripId).to.eql(tripObj._id);
          expect(tripObj.rider.currTripState).to.eql('enRoute');
          expect(tripObj.driver).to.be.an('object');
          expect(tripObj.driver.currTripId).to.eql(tripObj._id);
          expect(tripObj.driver.currTripState).to.eql('enRoute');
          setTimeout(() => {
            tripObj.tripStatus = 'endTrip';
            driverSocket.emit('endTrip', tripObj);
          }, 50);
        }
      });
      riderSocket.disconnect();
      // syncing rider data from the database
      setTimeout(() => {
        request(app)
          .get('/api/syncData')
          .set('Authorization', riderJwtAccessToken)
          .expect(httpStatus.OK)
          .then((res) => {
            expect(res.body.success).to.equal(true);
            expect(res.body.data.trip).to.be.an('object');
            done();
          });
      }, 50);
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the rider successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', riderJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the rider successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
});

function tripRequestAutomation(index, tripRequestObj, driverSocket) {
  index++;
  if (index === 2) {
    tripRequestObj.tripStatus = 'arriving';
  } else if (index === 3) {
    tripRequestObj.tripStatus = 'arrived';
  } else {
    return 0;
  }
  driverSocket.emit('tripUpdated', tripRequestObj);
  return tripRequestAutomation(index, tripRequestObj, driverSocket);
}
