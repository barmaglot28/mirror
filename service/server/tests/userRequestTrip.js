import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';
import io from 'socket.io-client';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import devUsers from '../dev-users';

chai.config.includeStack = true;

describe('## socket trip request ', () => {
  let rider1 = {
    email: 'abc1@xyz.com',
    password: '123',
    userType: 'rider',
    fname: 'abc1',
    lname: 'xyz1',
    phoneNo: '9876543215'
  };
  let riderJwtAccessToken = null;

  let driver1 = {
    email: 'xyz123@abc.com',
    password: '123',
    userType: 'driver',
    fname: 'xyz1',
    lname: 'abc1',
    phoneNo: '9876543216'
  };
  let driverJwtAccessToken = null;

  let admin = devUsers.testAdmin;
  let jwtAccessToken = null;

  describe('# POST /api/auth/login', () => {
    it('should authenticate user and provide token', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(admin)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('jwtAccessToken', 'user', 'maxDistance', 'restoredWallet');
          expect(res.body.data.user.loginStatus).to.equal(true);
          admin = res.body.data.user;
          jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new rider', (done) => {
      request(app)
        .post('/api/users/register')
        .send(rider1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          rider1 = res.body.data.user;
          riderJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new driver', (done) => {
      request(app)
        .post('/api/users/register')
        .send(driver1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          driver1 = res.body.data.user;
          driverJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  const carcategoryData = {
    name: 'CabdoBlack',
    pricePerKilometer: '1000',
    pricePerMinute: '30',
    baseFare: '20',
    minimumFare: '15',
    commissionToCabdo: '5',
    capacity: '3',
    isMetered: false,
  };

  let carcategory = {};

  describe('# POST /api/admin/carcategories', () => {
    it('should create a new car categories', (done) => {
      request(app)
        .post('/api/admin/carcategories')
        .send(carcategoryData)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          carcategory = res.body.data;
          done();
        });
    });
  });

  let car = {
    licensePlateNumber: 'DO-OO-123',
    make: '1980',
    model: 'Economy',
    canBeUsed: true,
  };

  describe('# POST /api/admin/car', () => {
    it('should create a new car', (done) => {
      car.carCategory = carcategory._id;
      request(app)
        .post('/api/admin/car')
        .send(car)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# POST /api/car/carId', () => {
    it('should lock a car', (done) => {
      request(app)
        .post(`/api/cars/${car._id}`)
        .send(car)
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# rider request for a trip with no driver available', () => {
    it('should receive noNearByDriver status', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      const payload = {
        rider: rider1,
        trip: {
          srcLoc: [15.123, 65.125],
          destLoc: [3, 4],
          pickUpAddress: 'geekyants',
          destAddress: 'bommanahalli',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket.emit('requestTrip', payload);
      riderSocket.on('tripUpdated', (data) => {
        expect(data.tripStatus).to.equal('noNearByDriver');
        riderSocket.disconnect();
        done();
      });
    });
  });
  describe('# rider request for a trip when driver available, no driver responds and automatically assign trip to the nearest driver', () => {
    it('rider should receive arriving status', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      const driverSocket = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken } });
      const payload = {
        rider: rider1,
        trip: {
          srcLoc: [72.82, 19.05],
          destLoc: [7, 8],
          pickUpAddress: 'geekyants',
          destAddress: 'bommanahalli',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket.emit('requestTrip', payload);
      riderSocket.on('tripUpdated', (tripRequestObj) => {
        expect(tripRequestObj.tripStatus).to.equal('arriving');
        tripRequestObj.tripStatus = 'endTrip';
        riderSocket.emit('tripUpdated', tripRequestObj);
        riderSocket.disconnect();
        driverSocket.disconnect();
        done();
      });
    });
  });
  describe('# rider request for a trip and driver accepts it but rider cancelled the tripRequest', () => {
    it('should receive cancelled status by driver', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      const driverSocket = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken } });
      const payload = {
        rider: rider1,
        trip: {
          srcLoc: [72.8536, 19.0217],
          destLoc: [11, 12],
          pickUpAddress: 'geekyants',
          destAddress: 'bommanahalli',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket.emit('requestTrip', payload);

      driverSocket.on('requestDriver', (tripRequest) => {
        const driverResponse = 1;
        if (driverResponse) {
          // tripRequest object should have following property
          expect(tripRequest.tripStatus).to.eql('request');
          expect(tripRequest.driver).to.be.an('object');
          expect(tripRequest.driver.currTripId).to.eql(tripRequest._id);
          expect(tripRequest.driver.currTripState).to.eql('request');
          expect(tripRequest.rider).to.be.an('object');
          tripRequest.tripStatus = 'accept';
          tripRequest.tripRequestIssue = 'no Issue';
        }
        driverSocket.emit('requestDriverResponse', tripRequest);
      });

      riderSocket.on('tripUpdated', (tripRequestObj) => {
        if (tripRequestObj.tripStatus === 'arriving') {
          expect(tripRequestObj.rider.currTripId).to.eql(tripRequestObj._id);
          expect(tripRequestObj.rider.currTripState).to.eql('arriving');
          // rider cancelling the trip request
          tripRequestObj.tripStatus = 'cancelled';
          tripRequestObj.tripIssue = 'driver taking too much time to arrive';
          riderSocket.emit('tripUpdated', tripRequestObj);
        }
      });
      driverSocket.on('tripUpdated', (data) => {
        if (data.tripStatus === 'cancelled') {
          expect(data.tripStatus).to.equal('cancelled');
          expect(data.driver).to.be.an('object');
          expect(data.driver.currTripId).to.eql(null);
          expect(data.driver.currTripState).to.eql(null);
          done();
          riderSocket.disconnect();
          driverSocket.disconnect();
        }
      });
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the rider successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', riderJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the rider successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
});
