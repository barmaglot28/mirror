import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';
import io from 'socket.io-client';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import devUsers from '../dev-users';

chai.config.includeStack = true;

describe('## socket start trip', () => {
  let rider1 = {
    email: 'abc2@xyz.com',
    password: '123',
    userType: 'rider',
    fname: 'abc2',
    lname: 'xyz2',
    phoneNo: '9876543217'
  };
  let riderJwtAccessToken = null;

  let rider2 = {
    email: 'asd2@xyz.com',
    password: '123',
    userType: 'rider',
    fname: 'asd2',
    lname: 'asd2',
    phoneNo: '9876543218'
  };
  let riderJwtAccessToken2 = null;

  let driver1 = {
    email: 'xyz12@abc.com',
    password: '123',
    userType: 'driver',
    fname: 'xyz2',
    lname: 'abc2',
    phoneNo: '9876543219'
  };
  let driverJwtAccessToken = null;

  let driver2 = {
    email: 'qwe123@abc.com',
    password: '123',
    userType: 'driver',
    fname: 'qwe2',
    lname: 'qwe2',
    phoneNo: '9876543220'
  };
  let driverJwtAccessToken2 = null;

  let admin = devUsers.testAdmin;
  let jwtAccessToken = null;

  describe('# POST /api/auth/login', () => {
    it('should authenticate user and provide token', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(admin)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('jwtAccessToken', 'user', 'maxDistance', 'restoredWallet');
          expect(res.body.data.user.loginStatus).to.equal(true);
          admin = res.body.data.user;
          jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        })
        .catch(err => console.error('# POST /api/auth/login failed', err));
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new rider', (done) => {
      request(app)
        .post('/api/users/register')
        .send(rider1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          rider1 = res.body.data.user;
          riderJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new rider', (done) => {
      request(app)
        .post('/api/users/register')
        .send(rider2)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          rider2 = res.body.data.user;
          riderJwtAccessToken2 = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new driver', (done) => {
      request(app)
        .post('/api/users/register')
        .send(driver1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          driver1 = res.body.data.user;
          driverJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new driver', (done) => {
      request(app)
        .post('/api/users/register')
        .send(driver2)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          driver2 = res.body.data.user;
          driverJwtAccessToken2 = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  const carcategoryData = {
    name: 'Category 2',
    pricePerKilometer: '1000',
    pricePerMinute: '30',
    baseFare: '20',
    minimumFare: '15',
    commissionToCabdo: '5',
    capacity: '3',
    isMetered: false,
  };

  let carcategory = {};

  describe('# POST /api/admin/carcategories', () => {
    it('should create a new car categories', (done) => {
      request(app)
        .post('/api/admin/carcategories')
        .send(carcategoryData)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          carcategory = res.body.data;
          done();
        });
    });
  });

  let car1 = {
    licensePlateNumber: 'DO-OO-111',
    make: '1980',
    model: 'Economy',
    canBeUsed: true,
  };

  describe('# POST /api/admin/car', () => {
    it('should create a new car', (done) => {
      car1.carCategory = carcategory._id;
      request(app)
        .post('/api/admin/car')
        .send(car1)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car1 = res.body.data;
          done();
        });
    });
  });

  let car2 = {
    licensePlateNumber: 'DO-OA-111',
    make: '1981',
    model: 'Economy',
    canBeUsed: true,
  };

  describe('# POST /api/admin/car', () => {
    it('should create a new car', (done) => {
      car2.carCategory = carcategory._id;
      request(app)
        .post('/api/admin/car')
        .send(car2)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car2 = res.body.data;
          done();
        });
    });
  });

  describe('# POST /api/car/carId', () => {
    it('should lock a car', (done) => {
      request(app)
        .post(`/api/cars/${car1._id}`)
        .send(car1)
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car1 = res.body.data;
          done();
        });
    });
  });

  describe('# POST /api/car/carId', () => {
    it('should lock a car', (done) => {
      request(app)
        .post(`/api/cars/${car2._id}`)
        .send(car2)
        .set('Authorization', driverJwtAccessToken2)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car2 = res.body.data;
          done();
        });
    });
  });

  describe('# rider request for trip and driver start, end trip ', () => {
    it('rider should receieve feedback status', (done) => {
      const riderSocket2 = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken2 } });
      const driverSocket2 = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken2 } });
      const payload = {
        rider: rider2,
        trip: {
          srcLoc: [72.8536, 19.0217],
          destLoc: [27, 28],
          pickUpAddress: 'geekyants(25, 26)',
          destAddress: 'bommanahalli(27, 28)',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket2.emit('requestTrip', payload);

      driverSocket2.on('requestDriver', (tripRequest) => {
        const driverResponse = 1;
        if (driverResponse) {
          // when driver accepts the tripRequest then server fires an event (tripUpdated) to rider
          expect(tripRequest.rider).to.be.an('object');
          expect(tripRequest.rider.currTripId).to.eql(tripRequest._id);
          expect(tripRequest.driver).to.be.an('object');
          expect(tripRequest.driver.currTripId).to.eql(tripRequest._id);

          tripRequest.tripStatus = 'accept';
          tripRequest.tripIssue = 'no Issue';
        }

        driverSocket2.emit('requestDriverResponse', tripRequest);

        // assuming driver has arrived at the pickUpAddress of the rider and start the trip
        tripRequest.tripStatus = 'arrived';

        driverSocket2.emit('startTrip', tripRequest, (tripObj) => {
          expect(tripObj.rider).to.be.an('object');
          expect(tripObj.rider.currTripId).to.eql(tripObj._id);
          expect(tripObj.rider.currTripState).to.eql('enRoute');
          expect(tripObj.driver).to.be.an('object');
          expect(tripObj.driver.currTripId).to.eql(tripObj._id);
          expect(tripObj.driver.currTripState).to.eql('enRoute');
          tripAutomation(tripObj, driverSocket2);
        });
      });

      driverSocket2.on('tripUpdated', (data) => {
        if (data.tripStatus === 'feedback' && data.riderRatingByDriver === 0) {
          expect(data.tripStatus).to.eql('feedback');
          expect(data.driver).to.be.an('object');
          data.tripStatus = 'endTrip';
          driverSocket2.emit('tripUpdated', data);
          driverSocket2.disconnect();
          riderSocket2.disconnect();
          done();
        }
      });
      riderSocket2.on('socketError', () => {
        riderSocket2.disconnect();
      });
      driverSocket2.on('socketError', () => {
        driverSocket2.disconnect();
      });
    });
  });

  describe('# rider request for a trip driver arrived at the pickUpAddress and starts the trip', () => {
    it('should receive arriving tripStatus by rider', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      const driverSocket2 = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken2 } });
      const payload = {
        rider: rider1,
        trip: {
          srcLoc: [72.8536, 19.0217],
          destLoc: [85, 104],
          pickUpAddress: 'geekyants(101, 102)',
          destAddress: 'bommanahalli(103, 104)',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket.emit('requestTrip', payload);

      driverSocket2.on('requestDriver', (tripRequest) => {
        const driverResponse = 1;
        if (driverResponse) {
          // when driver accepts the tripRequest then server fires an event (tripUpdated) to rider
          expect(tripRequest.rider).to.be.an('object');
          expect(tripRequest.rider.currTripId).to.eql(tripRequest._id);
          expect(tripRequest.rider.currTripState).to.eql('request');
          expect(tripRequest.driver).to.be.an('object');
          expect(tripRequest.driver.currTripId).to.eql(tripRequest._id);
          expect(tripRequest.driver.currTripState).to.eql('request');

          tripRequest.tripStatus = 'accept';
          tripRequest.tripIssue = 'noIssue';
        }
        driverSocket2.emit('requestDriverResponse', Object.assign({}, tripRequest));
      });

      riderSocket.on('tripUpdated', (data) => {
        if (data.tripStatus === 'arriving' && data.rider.currTripState === 'arriving') {
          expect(data.tripStatus).to.eql('arriving');
          expect(data.driver).to.be.an('object');
          expect(data.rider.currTripId).to.eql(data._id);
          expect(data.rider.currTripState).to.eql('arriving');
          done();
          driverSocket2.disconnect();
          riderSocket.disconnect();
        }
      });
      riderSocket.on('socketError', () => {
        riderSocket.disconnect();
      });
      driverSocket2.on('socketError', () => {
        driverSocket2.disconnect();
      });
    });
  });

  describe('# rider request for a trip and all driver (driver1) is on some trip ', () => {
    it('rider should receive noNearByDriver tripStatus', (done) => {
      const riderSocket2 = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken2 } });
      const driverSocket = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken } });
      const payload = {
        rider: rider2,
        trip: {
          srcLoc: [1, 2],
          destLoc: [12, 13],
          pickUpAddress: 'geekyants',
          destAddress: 'bommanahalli',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket2.emit('requestTrip', payload);
      riderSocket2.on('tripUpdated', (data) => {
        expect(data.tripStatus).to.eql('noNearByDriver');
        done();
        riderSocket2.disconnect();
        driverSocket.disconnect();
      });
      riderSocket2.on('socketError', () => {
        riderSocket2.disconnect();
      });
      driverSocket.on('socketError', () => {
        driverSocket.disconnect();
      });
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the rider successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', riderJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the rider successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', riderJwtAccessToken2)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the driver successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
  describe('# auth /api/auth/logout', () => {
    it('should logout the driver successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', driverJwtAccessToken2)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
});

function tripAutomation(tripObj, driverSocket) {
  if (tripObj !== null || tripObj !== undefined) {
    driverSocket.emit('meteredTrip', tripObj);
  }
}
