import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';
import io from 'socket.io-client';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import devUsers from '../dev-users';

chai.config.includeStack = true;

describe('## referral code ', () => {
  let rider1 = {
    email: 'abd1@xyz.com',
    password: '123',
    userType: 'rider',
    fname: 'abd1',
    lname: 'xyz1',
    phoneNo: '+499876543210'
  };
  let riderJwtAccessToken1 = null;

  let rider2 = {
    email: 'abd2@xyz.com',
    password: '123',
    userType: 'rider',
    fname: 'abd2',
    lname: 'xyz2',
    phoneNo: '+499876543211'
  };
  let riderJwtAccessToken2 = null;

  let driver1 = {
    email: 'xyz44@abc.com',
    password: '123',
    userType: 'driver',
    fname: 'xyz1',
    lname: 'abc1',
    phoneNo: '+499876543212'
  };
  let driverJwtAccessToken = null;

  let admin = devUsers.testAdmin;
  let jwtAccessToken = null;

  describe('# POST /api/auth/login', () => {
    it('should authenticate user and provide token', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(admin)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('jwtAccessToken', 'user', 'maxDistance', 'restoredWallet');
          expect(res.body.data.user.loginStatus).to.equal(true);
          admin = res.body.data.user;
          jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  const referralAmt = {
    amountToSenderWallet: 5,
    amountToRecipientWallet: 5
  };

  describe('# POST /api/admin/referralcode/amount', () => {
    it('should change referralAmount', (done) => {
      request(app)
        .post('/api/admin/referralcode/amount')
        .send(referralAmt)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new driver', (done) => {
      request(app)
        .post('/api/users/register')
        .send(driver1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          driverJwtAccessToken = res.body.data.jwtAccessToken;
          driver1 = res.body.data.user;
          done();
        });
    });
  });

  const carcategoryData1 = {
    name: 'CabdoBlack',
    pricePerKilometer: '1000',
    pricePerMinute: '30',
    baseFare: '20',
    minimumFare: '15',
    commissionToCabdo: '5',
    capacity: '3',
    isMetered: false,
  };

  let carcategory = {};

  describe('# POST /api/admin/carcategories', () => {
    it('should create a new car categories', (done) => {
      request(app)
        .post('/api/admin/carcategories')
        .send(carcategoryData1)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          carcategory = res.body.data;
          done();
        });
    });
  });

  let car = {
    licensePlateNumber: 'DO-AA-123',
    make: '1980',
    model: 'Economy',
    canBeUsed: true,
  };

  describe('# POST /api/admin/car', () => {
    it('should create a new car', (done) => {
      car.carCategory = carcategory._id;
      request(app)
        .post('/api/admin/car')
        .send(car)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# POST /api/car/carId', () => {
    it('should lock a car', (done) => {
      request(app)
        .post(`/api/cars/${car._id}`)
        .send(car)
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new rider and receive result of wallet and referral code creation', (done) => {
      request(app)
        .post('/api/users/register')
        .send(rider1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.referralCodeResult.success).to.equal(true);
          expect(res.body.referralCodeResult.message).to.equal('emptyCode');
          expect(res.body.referralCodeResult.referralCodeIsValid).to.equal(false);
          expect(res.body.walletResult.success).to.equal(true);
          expect(res.body.walletResult.bonusAdded).to.equal(referralAmt.amountToRecipientWallet);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          rider1 = res.body.data.user;
          riderJwtAccessToken1 = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/referralcodes/', () => {
    it('should get a referral code', (done) => {
      request(app)
        .get('/api/referralcodes')
        .set('Authorization', riderJwtAccessToken1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.be.an('object');
          expect(res.body.data.code).to.be.a('string');
          expect(res.body.data.userId).to.equal(rider1._id);
          rider2.referralCode = res.body.data.code;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create another rider with referral code and get bonus', (done) => {
      request(app)
        .post('/api/users/register')
        .send(rider2)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.referralCodeResult.success).to.equal(true);
          expect(res.body.referralCodeResult.referralCodeIsValid).to.equal(true);
          expect(res.body.walletResult.success).to.equal(true);
          expect(res.body.walletResult.bonusAdded).to.equal(referralAmt.amountToRecipientWallet);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          rider2 = res.body.data.user;
          riderJwtAccessToken2 = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  let walletBalance1;

  describe('# POST /api/payment/amount', () => {
    it('should get wallet balance of rider1', (done) => {
      request(app)
        .post('/api/payment/amount')
        .send(rider1)
        .set('Authorization', riderJwtAccessToken1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data.walletBalance).to.equal(referralAmt.amountToRecipientWallet);
          walletBalance1 = res.body.data.walletBalance;
          done();
        });
    });
  });

  describe('# rider request for a trip, driver accept and end the trip', () => {
    it('should receive feedback tripStatus by driver', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken2 } });
      const driverSocket = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken } });
      const payload = {
        rider: rider2,
        trip: {
          srcLoc: [72.8536, 19.0217],
          destLoc: [85, 104],
          pickUpAddress: 'geekyants(101, 102)',
          destAddress: 'bommanahalli(103, 104)',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      riderSocket.emit('requestTrip', payload);

      driverSocket.on('requestDriver', (tripRequest) => {
        const driverResponse = 1;
        if (driverResponse) {
          expect(tripRequest.rider).to.be.an('object');
          expect(tripRequest.rider.currTripId).to.eql(tripRequest._id);
          expect(tripRequest.rider.currTripState).to.eql('request');
          expect(tripRequest.driver).to.be.an('object');
          expect(tripRequest.driver.currTripId).to.eql(tripRequest._id);
          expect(tripRequest.driver.currTripState).to.eql('request');

          tripRequest.tripStatus = 'accept';
          tripRequest.tripIssue = 'no Issue';
        }
        driverSocket.emit('requestDriverResponse', tripRequest);

        driverSocket.on('tripUpdated', (data) => {
          if (data.tripStatus === 'arriving') {
            expect(data.tripStatus).to.eql('arriving');
            driverSocket.emit('meteredTrip', data);
          }
          if (data.tripStatus === 'feedback') {
            data.tripStatus = 'endTrip';
            driverSocket.emit('tripUpdated', data);
            setTimeout(() => {
              driverSocket.disconnect();
              riderSocket.disconnect();
              done();
            }, 1000);
          }
        });
      });
      riderSocket.on('socketError', () => {
        riderSocket.disconnect();
      });
      driverSocket.on('socketError', () => {
        driverSocket.disconnect();
      });
    });
  });

  describe('# POST /api/payment/amount', () => {
    it('should get wallet added by bonus balance of rider1', (done) => {
      request(app)
        .post('/api/payment/amount')
        .send(rider1)
        .set('Authorization', riderJwtAccessToken1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          const walletBalance2 = res.body.data.walletBalance;
          const diff = walletBalance2 - walletBalance1;
          expect(diff).to.equal(referralAmt.amountToSenderWallet);
          done();
        });
    });
  });

  describe('# POST /api/referralcodes/', () => {
    it('should get inviterCodeActive false of rider2', (done) => {
      request(app)
        .get('/api/referralcodes')
        .set('Authorization', riderJwtAccessToken2)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.be.an('object');
          expect(res.body.data.code).to.be.a('string');
          expect(res.body.data.inviterCodeActive).to.equal(false);
          rider2.referralCode = res.body.data.code;
          done();
        });
    });
  });

  describe('# auth /api/auth/logout', () => {
    it('should logout the rider1 successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', riderJwtAccessToken1)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });

  describe('# auth /api/auth/logout', () => {
    it('should logout the rider2 successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', riderJwtAccessToken2)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });

  describe('# auth /api/auth/logout', () => {
    it('should logout the driver successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
});
