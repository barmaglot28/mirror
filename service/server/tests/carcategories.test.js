import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';
import devUsers from '../dev-users';

chai.config.includeStack = true;

describe('## CAR CATEGORIES APIs', () => {
  let admin = {
    email: devUsers.testAdmin.email,
    password: devUsers.testAdmin.password,
    userType: devUsers.testAdmin.userType,
  };
  let jwtAccessToken = null;

  describe('# POST /api/auth/login', () => {
    it('should authenticate user and provide token', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(admin)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('jwtAccessToken', 'user', 'maxDistance', 'restoredWallet');
          expect(res.body.data.user.loginStatus).to.equal(true);
          admin = res.body.data.user;
          jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# GET /api/admin/carcategories', () => {
    it('should get the all car categories', (done) => {
      request(app)
        .get('/api/admin/carcategories')
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });

  let carcategories1 = {
    name: 'Category 1',
    pricePerKilometer: '1000',
    pricePerMinute: '30',
    baseFare: '20',
    minimumFare: '15',
    commissionToCabdo: '5',
    capacity: '3',
    isMetered: false,
  };

  const carcategories2 = {
    name: 'Category 2',
    isMetered: true,
  };

  let carcategories = {};

  describe('# POST /api/admin/carcategories', () => {
    it('should create a new car categories', (done) => {
      request(app)
        .post('/api/admin/carcategories')
        .send(carcategories1)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          carcategories = res.body.data;
          done();
        });
    });
  });

  describe('# Error handling POST /api/admin/carcategories', () => {
    it('should throw parameter validation error', (done) => {
      delete carcategories2.name;
      request(app)
        .post('/api/admin/carcategories')
        .send(carcategories2)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.BAD_REQUEST)
        .then(res => {
          expect(res.body.success).to.equal(false);
          done();
        });
    });
  });

  describe('# get /api/admin/carcategories/carcategoriesID', () => {
    it('should get the car categories details', (done) => {
      request(app)
        .get(`/api/admin/carcategoriesDetails/${carcategories._id}`)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data.name).to.equal(carcategories1.name);
          done();
        });
    });
  });

  describe('# Error handling get /api/admin/carcategories/carcategoriesID', () => {
    it('should get UNAUTHORIZED error as no token provided', (done) => {
      request(app)
        .get(`/api/admin/carcategoriesDetails/${carcategories._id}`)
        .expect(httpStatus.UNAUTHORIZED)
        .then(res => {
          expect(res.body.success).to.equal(false);
          done();
        });
    });
  });

  describe('# PUT /api/admin/carcategories/', () => {
    it('should update car categories details', (done) => {
      carcategories.name = 'shade';
      request(app)
        .put('/api/admin/carcategories/')
        .send(carcategories)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.be.an('object');
          carcategories1 = res.body.data;
          done();
        });
    });
  });
});
