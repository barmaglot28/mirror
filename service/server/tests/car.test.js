import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';
import devUsers from '../dev-users';

chai.config.includeStack = true;

describe('## CAR APIs', () => {
  let admin = devUsers.testAdmin;
  let jwtAccessToken = null;

  describe('# POST /api/auth/login', () => {
    it('should authenticate user and provide token', (done) => {
      request(app)
          .post('/api/auth/login')
          .send(admin)
          .expect(httpStatus.OK)
          .then((res) => {
            expect(res.body.success).to.equal(true);
            expect(res.body.data).to.have.all.keys('jwtAccessToken', 'user', 'maxDistance', 'restoredWallet');
            expect(res.body.data.user.loginStatus).to.equal(true);
            admin = res.body.data.user;
            jwtAccessToken = res.body.data.jwtAccessToken;
            done();
          });
    });
  });

  describe('# GET /api/admin/car', () => {
    it('should get the all car', (done) => {
      request(app)
          .get('/api/admin/car')
          .set('Authorization', jwtAccessToken)
          .expect(httpStatus.OK)
          .then((res) => {
            expect(res.body.success).to.equal(true);
            done();
          });
    });
  });

  let carCategory = {};

  const carCategories1 = {
    name: 'CategoryTest',
    pricePerKilometer: '1000',
    pricePerMinute: '30',
    baseFare: '20',
    minimumFare: '15',
    commissionToCabdo: '5',
    capacity: '3',
    isMetered: false,
  };

  describe('# POST /api/admin/carcategories', () => {
    it('should create a new car categories', (done) => {
      request(app)
          .post('/api/admin/carcategories')
          .send(carCategories1)
          .set('Authorization', jwtAccessToken)
          .expect(httpStatus.OK)
          .then(res => {
            expect(res.body.success).to.equal(true);
            carCategory = res.body.data;
            done();
          });
    });
  });

  let car1 = {
    licensePlateNumber: 'DO-AA-123',
    make: '1980',
    model: 'Economy',
    canBeUsed: false,
  };

  const car2 = {
    licensePlateNumber: 'DO-AA-456',
    make: '1999',
    model: 'VIP',
    canBeUsed: false,
  };

  let car = {};

  describe('# POST /api/admin/car', () => {
    it('should create a new car', (done) => {
      car1.carCategory = carCategory._id;
      request(app)
          .post('/api/admin/car')
          .send(car1)
          .set('Authorization', jwtAccessToken)
          .expect(httpStatus.OK)
          .then(res => {
            expect(res.body.success).to.equal(true);
            car = res.body.data;
            done();
          });
    });
  });

  describe('# Error handling POST /api/admin/car', () => {
    it('should throw parameter validation error', (done) => {
      delete car2.licensePlateNumber;
      request(app)
          .post('/api/admin/car')
          .send(car2)
          .set('Authorization', jwtAccessToken)
          .expect(httpStatus.BAD_REQUEST)
          .then(res => {
            expect(res.body.success).to.equal(false);
            done();
          });
    });
  });

  describe('# get /api/admin/car/carID', () => {
    it('should get the car details', (done) => {
      request(app)
          .get(`/api/admin/car/${car._id}`)
          .set('Authorization', jwtAccessToken)
          .expect(httpStatus.OK)
          .then(res => {
            expect(res.body.success).to.equal(true);
            expect(res.body.data.licensePlateNumber).to.equal(car1.licensePlateNumber);
            done();
          });
    });
  });

  describe('# Error handling get /api/admin/car/carID', () => {
    it('should get UNAUTHORIZED error as no token provided', (done) => {
      request(app)
          .get(`/api/admin/car/${car._id}`)
          .expect(httpStatus.UNAUTHORIZED)
          .then(res => {
            expect(res.body.success).to.equal(false);
            done();
          });
    });
  });

  describe('# PUT /api/admin/car/', () => {
    it('should update car details', (done) => {
      car.licensePlateNumber = 'DO-AA-789';
      request(app)
          .put('/api/admin/car/')
          .send(car)
          .set('Authorization', jwtAccessToken)
          .expect(httpStatus.OK)
          .then(res => {
            expect(res.body.success).to.equal(true);
            expect(res.body.data).to.be.an('object');
            car = res.body.data;
            done();
          });
    });
  });

  describe('# DELETE /api/admin/car/', () => {
    it('should update car details', (done) => {
      request(app)
          .delete('/api/admin/car/')
          .send(car)
          .set('Authorization', jwtAccessToken)
          .expect(httpStatus.OK)
          .then(res => {
            expect(res.body.success).to.equal(true);
            expect(res.body.data).to.be.an('object');
            car1 = res.body.data;
            done();
          });
    });
  });
});
