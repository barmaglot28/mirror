import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';
import io from 'socket.io-client';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import devUsers from '../dev-users';

chai.config.includeStack = true;

describe('## update location of the user(rider and driver)', () => {
  let rider1 = {
    email: 'abc3@xyz.com',
    password: '123',
    userType: 'rider',
    fname: 'abc3',
    lname: 'xyz3',
    phoneNo: '+499876543224'
  };
  let riderJwtAccessToken = null;

  let driver1 = {
    email: 'xyz3@abc.com',
    password: '123',
    userType: 'driver',
    fname: 'xyz3',
    lname: 'abc3',
    phoneNo: '+499876543225'
  };
  let driverJwtAccessToken = null;

  let admin = devUsers.testAdmin;
  let jwtAccessToken = null;

  describe('# POST /api/auth/login', () => {
    it('should authenticate user and provide token', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(admin)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('jwtAccessToken', 'user', 'maxDistance', 'restoredWallet');
          expect(res.body.data.user.loginStatus).to.equal(true);
          admin = res.body.data.user;
          jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new rider', (done) => {
      request(app)
        .post('/api/users/register')
        .send(rider1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken', 'maxDistance');
          rider1 = res.body.data.user;
          riderJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should create a new driver', (done) => {
      request(app)
        .post('/api/users/register')
        .send(driver1)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          driver1 = res.body.data.user;
          driverJwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  const carcategoryData = {
    name: 'Category 4',
    pricePerKilometer: '1000',
    pricePerMinute: '30',
    baseFare: '20',
    minimumFare: '15',
    commissionToCabdo: '5',
    capacity: '3',
    isMetered: false,
  };

  let carcategory = {};

  describe('# POST /api/admin/carcategories', () => {
    it('should create a new car categories', (done) => {
      request(app)
        .post('/api/admin/carcategories')
        .send(carcategoryData)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          carcategory = res.body.data;
          done();
        });
    });
  });

  let car = {
    licensePlateNumber: 'DO-OO-444',
    make: '1980',
    model: 'Economy',
    canBeUsed: true,
  };

  describe('# POST /api/admin/car', () => {
    it('should create a new car', (done) => {
      car.carCategory = carcategory._id;
      request(app)
        .post('/api/admin/car')
        .send(car)
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# POST /api/car/carId', () => {
    it('should lock a car', (done) => {
      request(app)
        .post(`/api/cars/${car._id}`)
        .send(car)
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.success).to.equal(true);
          car = res.body.data;
          done();
        });
    });
  });

  describe('# driver update location when in arriving tripRequest Status', () => {
    it('rider should receive driver updated location', (done) => {
      const riderSocket = io.connect('http://localhost:4123', { query: { token: riderJwtAccessToken } });
      const driverSocket = io.connect('http://localhost:4123', { query: { token: driverJwtAccessToken } });
      const payload = {
        rider: rider1,
        trip: {
          srcLoc: [72.8536, 19.0217],
          destLoc: [27, 28],
          pickUpAddress: 'geekyants',
          destAddress: 'bommanahalli',
          latitudeDelta: 0.123,
          longitudeDelta: 0.023
        },
        carCategory: carcategory
      };
      const loc = [12, 24];
      riderSocket.emit('requestTrip', payload);
      driverSocket.on('requestDriver', (tripRequest) => {
        const driverResponse = 1;
        if (driverResponse) {
          tripRequest.tripStatus = 'accept';
          tripRequest.tripRequestIssue = 'no Issue';
        }
        driverSocket.emit('requestDriverResponse', tripRequest);
      });
      driverSocket.on('tripUpdated', (data) => {
        if (data.tripStatus === 'arriving') {
          driver1.gpsLoc = loc;
          driverSocket.emit('updateLocation', driver1);
        }
      });
      riderSocket.on('updateDriverLocation', (driverGps) => {
        expect(driverGps).to.eql(loc);
        driverSocket.disconnect();
        riderSocket.disconnect();
        done();
      });
    });
  });

  describe('# auth /api/auth/logout', () => {
    it('should logout the rider successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', riderJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });

  describe('# auth /api/auth/logout', () => {
    it('should logout the driver successfully', (done) => {
      request(app)
        .get('/api/auth/logout')
        .set('Authorization', driverJwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          done();
        });
    });
  });
});
