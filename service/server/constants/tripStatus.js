export default {
  request: 'request',
  booking: 'booking',
  booked: 'booked',
  arriving: 'arriving',
  arrived: 'arrived',
  enRoute: 'enRoute',
  feedback: 'feedback',
  endTrip: 'endTrip',
  cancelled: 'cancelled',
  deleted: 'deleted'
};
