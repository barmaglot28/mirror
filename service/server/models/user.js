import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import bcrypt from 'bcrypt';
/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  fname: { type: String, default: null },
  lname: { type: String, default: null },
  email: { type: String, required: true },
  password: { type: String, required: true, select: false },
  dob: { type: Date, default: '1/1/1980' },
  fullAddress: { type: String, default: null },
  address: { type: String, default: 'Friedensplatz 1' },
  city: { type: String, default: 'Dortmund' },
  state: { type: String, default: 'NRW' },
  country: { type: String, default: 'Deutschland' },
  gpsLoc: {
    type: [Number],
    index: '2d',
  },
  latitudeDelta: { type: Number, default: 0.013 },
  longitudeDelta: { type: Number, default: 0.022 },
  userRating: { type: Number, default: 0 },
  phoneNo: { type: String, default: '1234567890' },
  currTripId: { type: String, default: null },
  currTripState: { type: String, default: null },
  statusTime: { type: Date, default: null },
  userType: { type: String, default: 'rider' },
  corporateUserId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
  loginStatus: { type: Boolean, default: false },
  availability: { type: Boolean, default: true },
  // extra fields which were are not defined in the userSchema diagram
  verified: { type: Boolean, default: false },
  verifyCode: { type: Number, default: 0 },
  jwtAccessToken: { type: String, default: null },
  carDetails: {
    make: { type: String, default: 'hatchback' },
    model: { type: String, default: 'maruti suzuki' },
    licensePlateNumber: { type: String, default: 'AB-CD-1234' },
    carCategory: { type: mongoose.Schema.Types.ObjectId, ref: 'CarCategory' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  },
  deviceId: { type: String, default: null },
  pushToken: { type: String, default: null },
  avatar: { type: mongoose.Schema.Types.ObjectId, ref: 'UserAvatar' },
  language: { type: String, default: 'de' },
  showPermanentBadge: { type: Boolean, default: true }
});
/**
 * converts the string value of the password to some hashed value
 * - pre-save hooks
 * - validations
 * - virtuals
 */
UserSchema.pre('save', function userSchemaPre(next) {
  const user = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, (hashErr, hash) => {
        if (hashErr) {
          return next(hashErr);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});
/**
 * comapare the stored hashed value of the password with the given value of the password
 * @param pw - password whose value has to be compare
 * @param cb - callback function
 */
UserSchema.methods.comparePassword = function comparePassword(pw, cb) {
  const that = this;
  bcrypt.compare(pw, that.password, (err, isMatch) => {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};
/**
 * Statics
 */
UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .execAsync().then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ skip = 0, limit = 20, corporateUserId, userType = null, value = null } = {}) {
    let query = {};
    if (value) {
      const regex = new RegExp(value, 'g');
      query = {
        userType,
        corporateUserId: null,
        $or: [{ fname: regex }, { lname: regex }]
      };
    } else if (userType) {
      query = { corporateUserId, userType };
    } else {
      query = {
        $or: [{ userType: 'rider' }, { userType: 'driver' }, { userType: 'corporate' }],
        corporateUserId
      };
    }

    return this.find(query)
      .sort({ _id: -1 })
      .select('-__v')
      .skip(skip)
      .limit(limit)
      .execAsync();
  }
};
/**
 * @typedef User
 */
UserSchema.index({ gpsLoc: '2d' });
export default mongoose.model('User', UserSchema);
