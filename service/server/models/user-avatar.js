import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserAvatarSchema = new Schema({
  base64: { type: Buffer, default: null, required: true },
  userId: { type: Schema.Types.ObjectId, ref: 'User', default: null },
});

export default mongoose.model('UserAvatar', UserAvatarSchema);
