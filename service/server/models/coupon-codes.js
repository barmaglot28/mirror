import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;

const CouponCodeSchema = new Schema({
  code: { type: String, default: 'asdertyd' },
  discount: { type: Number, default: 0 },
});

CouponCodeSchema.statics = {

  get(id) {
    return this.findById(id)
        .execAsync()
        .then((couponcodes) => {
          if (couponcodes) {
            return couponcodes;
          }
          const err = new APIError('No such couponcodes exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
  },

  list({ skip, limit, filter } = {}) {
    let searchObj = {};
    switch (filter) {
      default:
        searchObj = {};
    }
    return this.find(searchObj)
        .sort({ name: 1 })
        .select('-__v')
        .skip(skip)
        .limit(limit)
        .execAsync();
  },

};

export default mongoose.model('CouponCode', CouponCodeSchema);
