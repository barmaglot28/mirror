import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import UserSchema from './user';
import CarCategorySchema from './car-category';

const debug = require('debug')('Taxi-app-backend-web-dashboard: car model');

const Schema = mongoose.Schema;

const CarSchema = new Schema({
  licensePlateNumber: {
    type: String,
    default: null,
    unique: true,
    index: true,
    required: true
  },
  make: { type: String, default: null, required: true },
  model: { type: String, default: null, required: true },
  carCategory: { type: Schema.Types.ObjectId, ref: 'CarCategory', required: true },
  usedBy: { type: Schema.Types.ObjectId, ref: 'User' },
  canBeUsed: { type: Boolean, required: true },
  createdAt: { type: Date, default: Date.now, required: true },
  updatedAt: { type: Date, default: Date.now, required: true }
});

CarSchema.path('usedBy').validate((usedBy, respond) => {
  debug(`inside validator with usedBy value ->${usedBy}`);
  return UserSchema.findByIdAsync(usedBy)
      .then((usedByData) => {
        if (usedByData) {
          return respond(true);
        } else {
          debug(`usedBy validation failed ${usedByData}`);
          return respond(false);
        }
      });
}, 'Invalid usedBy');

CarSchema.path('carCategory').validate((carCategory, respond) => {
  debug(`inside validator with carCategory value ->${carCategory}`);
  return CarCategorySchema.findByIdAsync(carCategory)
      .then((carCategoryData) => {
        if (carCategoryData) {
          return respond(true);
        } else {
          debug(`carCategory validation failed ${carCategoryData}`);
          return respond(false);
        }
      });
}, 'Invalid carCategory');

CarSchema.statics = {
  /**
   * List cars in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of cars to be skipped.
   * @param {number} limit - Limit number of cars to be returned.
   * @param {object} filter - filter
   * @returns {Promise<Car[]>}
   */
  list({ skip, limit, filter } = {}) {
    let searchObj = {};
    switch (filter) {
      case 'free':
        searchObj = { usedBy: null, canBeUsed: true };
        break;
      default:
        searchObj = {};
    }
    return this.find(searchObj)
        .sort({ licensePlateNumber: 1 })
        .select('-__v')
        .skip(skip)
        .limit(limit)
        .populate('usedBy')
        .populate('carCategory')
        .execAsync();
  },

  get(carId) {
    return this.findById(carId)
        .populate('usedBy')
        .populate('carCategory')
        .execAsync()
        .then((carObj) => {
          if (carObj) {
            return carObj;
          }
          const err = new APIError('No such car exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
  },

  getCount(filter) {
    let searchObj = {};
    switch (filter) {
      default:
        searchObj = {};
    }
    return this.count(searchObj)
        .execAsync();
  },

};

export default mongoose.model('Car', CarSchema);
