import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const WalletSchema = new Schema({
  userEmail: { type: String, default: null },
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  userType: { type: String, default: 'rider' },
  stripeAccountId: { type: String, default: null },
  commissionToCabdo: { type: Number, default: 1.2 },
  walletBalance: { type: Number, default: 0 },  // in case of rider
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  paymentMethod: { type: String, default: 'cash' }, // variants: cash or card
});

WalletSchema.statics = {
  /**
   * List wallets in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of wallets to be skipped.
   * @param {number} limit - Limit number of wallets to be returned.
   * @returns {Promise<Wallet[]>}
   */
  list({ skip, limit, filter = null } = {}) {
    let searchObj = {};
    if (filter) {
      searchObj = filter;
      if (Array.isArray(filter)) {
        searchObj = { userId: { $in: filter } };
      }
    }
    return this.find(searchObj)
      .sort({ _id: -1 })
      .select('-__v')
      .skip(skip)
      .limit(limit)
      .populate('userId', 'fname lname')
      .execAsync();
  }
};

export default mongoose.model('Wallet', WalletSchema);
