import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const TransactionSchema = new Schema({
  userIdTo: { type: String, ref: 'User', default: 'owner' },
  userIdFrom: { type: String, ref: 'User', default: null },
  amount: { type: Number, default: null },
  tripId: { type: Schema.Types.ObjectId, ref: 'trip', default: null },
  walletIdFrom: { type: String, default: null, ref: 'Wallet' },
  walletIdTo: { type: String, default: 'owner', ref: 'Wallet' },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

export default mongoose.model('Transaction', TransactionSchema);
