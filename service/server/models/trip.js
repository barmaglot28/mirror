import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import UserSchema from './user';
import moment from 'moment';

const debug = require('debug')('Taxi-app-backend-web-dashboard: trip model');

const Schema = mongoose.Schema;
const TripSchema = new Schema({
  riderId: { type: Schema.Types.ObjectId, ref: 'User', default: null },
  driverId: { type: Schema.Types.ObjectId, ref: 'User' },
  srcLoc: {
    type: [Number],
    index: '2d'
  },
  destLoc: {
    type: [Number],
    index: '2d'
  },
  pickUpAddress: { type: String, default: null },
  destAddress: { type: String, default: null },
  latitudeDelta: { type: Number, default: 0.0123 },
  longitudeDelta: { type: Number, default: 0.0123 },
  paymentMode: { type: String, default: 'cash' },
  tripAmt: { type: Number, default: 0 },
  discount: { type: Number, default: 0 },
  fareDetails: { type: Object, default: null },
  bookingTime: { type: Date, default: Date.now },
  travelTime: { type: Number, default: 0 },
  taxiType: { type: String, default: 'Taxi' },
  isMetered: { type: Boolean, default: false },
  riderRatingByDriver: { type: Number, default: 0 },
  driverRatingByRider: { type: Number, default: 0 },
  seatBooked: { type: Number, default: 0 },
  preBookedTime: { type: Date, default: null },
  tripStatus: { type: String, default: 'onTrip' },
  tripIssue: { type: String, default: 'noIssue' },
  tripIssueComment: { type: String, default: 'null' },
  roadMapUrl: { type: String, default: null },
  corporateUserId: { type: Schema.Types.ObjectId, ref: 'User', default: null },
  endTime: { type: Date, default: null },
  tripNumber: { type: Number, default: 1000000 },
  showItemForRider: { type: Boolean, default: true },
  showItemForDriver: { type: Boolean, default: true },
  arrivalTime: { type: Number, default: null },
  riderComment: { type: String, default: null },
  switchedFromCabdo: { type: Boolean, default: false },
  deletedTime: { type: Date, default: null },
  distance: {
    text: String,
    value: Number
  },
  carDetails: {
    make: { type: String, default: 'hatchback' },
    model: { type: String, default: 'maruti suzuki' },
    licensePlateNumber: { type: String, default: 'AB-CD-1234' },
    carCategory: { type: mongoose.Schema.Types.ObjectId, ref: 'CarCategory' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  },
});


TripSchema.path('riderId').validate((riderId, respond) => {
  debug(`inside validator with riderId value ->${riderId}`);
  return UserSchema.findByIdAsync(riderId)
  .then((riderData) => {
    if (riderData) {
      return respond(true);
    } else {
      debug(`rider validation failed ${riderData}`);
      return respond(false);
    }
  });
}, 'Invalid Rider Id');

TripSchema.path('driverId').validate((driverId, respond) => {
  debug(`inside validator with driverId value ->${driverId}`);
  return UserSchema.findByIdAsync(driverId)
  .then((driverData) => {
    if (driverData) {
      return respond(true);
    } else {
      debug(`driver validation failed ${driverData}`);
      return respond(false);
    }
  });
}, 'Invalid DriverId');

function verifyFilter(filter) {
  let searchObj = null;
  switch (filter) {
    case 'Ongoing':
      searchObj = {};
      searchObj.tripStatus = 'onTrip';
      break;
    case 'Completed':
      searchObj = {};
      searchObj.tripStatus = 'endTrip';
      break;
    default: searchObj = {};
  }
  return searchObj;
}

// TripSchema.pre('save', function handler(next) {
//   this.constructor.findOne().sort({ tripNumber: -1 }).execAsync().then(trip => {
//     if (trip) {
//       this.tripNumber = trip.tripNumber + 1;
//     } else {
//       this.tripNumber = 1000000;
//     }
//     next();
//   });
// });

TripSchema.statics = {
  /**
  * List users in descending order of 'createdAt' timestamp.
  * @param {number} skip - Number of users to be skipped.
  * @param {number} limit - Limit number of users to be returned.
  * @returns {Promise<User[]>}
  */
  list({ skip, limit, filter, dateFilter = {} } = {}) {
    if ('bookingTime' in dateFilter && 'driverId' in dateFilter) {
      return this.find({ $or: [{ tripStatus: 'endTrip' }, { tripStatus: 'cancelled' }], driverId: mongoose.Types.ObjectId(dateFilter.driverId), bookingTime: { $gte: moment(dateFilter.bookingTime).startOf('day'), $lt: moment(dateFilter.bookingTime).add(1, 'days') } })
        .sort({ _id: -1 })
        .select('-__v')
        .populate('riderId driverId')
        .execAsync();
    } else if ('bookingTime' in dateFilter) {
      return this.find({ $or: [{ tripStatus: 'endTrip' }, { tripStatus: 'cancelled' }], bookingTime: { $gte: moment(dateFilter.bookingTime).startOf('day'), $lt: moment(dateFilter.bookingTime).add(1, 'days') } })
        .sort({ _id: -1 })
        .select('-__v')
        .populate('riderId driverId')
        .execAsync();
    } else {
      return this.find(verifyFilter(filter))
        .sort({ _id: -1 })
        .select('-__v')
        .skip(skip)
        .limit(limit)
        .populate('riderId driverId')
        .execAsync();
    }
  },

  get(tripId) {
    return this.findById(tripId)
      .populate('riderId driverId')
      .execAsync()
      .then((tripObj) => {
        if (tripObj) {
          return tripObj;
        }
        const err = new APIError('No such trip exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  getCount(filter) {
    return this.count(verifyFilter(filter))
      .execAsync();
  }
};

export default mongoose.model('trip', TripSchema);
