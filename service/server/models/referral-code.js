import Promise from 'bluebird';
import mongoose from 'mongoose';
import voucherCode from 'voucher-code-generator';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;

const ReferralCodeSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  code: { type: String, default: 'qwertyuase' },
  inviterId: { type: Schema.Types.ObjectId, ref: 'User' },
  codeFromInviter: { type: String, default: 'qwertyuase' },
  inviterCodeActive: { type: Boolean, default: null }
});

ReferralCodeSchema.statics = {
  get(id) {
    return this.findById(id)
      .execAsync()
      .then((referralcodes) => {
        if (referralcodes) {
          return referralcodes;
        }
        const err = new APIError('No such referralcodes exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },

  generate() {
    const charset = 'abcdefghijklmnopqrstuvwxyz123456789';
    const code = voucherCode.generate({ length: 5, charset })[0];
    return code;
  },

  list({ skip, limit, filter } = {}) {
    let searchObj = {};
    switch (filter) {
      default:
        searchObj = {};
    }
    return this.find(searchObj)
      .sort({ name: 1 })
      .select('-__v')
      .skip(skip)
      .limit(limit)
      .execAsync();
  },

};

export default mongoose.model('ReferralCode', ReferralCodeSchema);
