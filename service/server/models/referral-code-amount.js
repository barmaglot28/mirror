import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';


const Schema = mongoose.Schema;

const ReferralCodeAmountSchema = new Schema({
  amountToSenderWallet: { type: Number, default: 0 },
  amountToRecipientWallet: { type: Number, default: 0 }
});

ReferralCodeAmountSchema.statics = {

  get(id) {
    return this.findById(id)
        .execAsync()
        .then((referralcodes) => {
          if (referralcodes) {
            return referralcodes;
          }
          const err = new APIError('No such referralcodes exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
  },
};

export default mongoose.model('ReferralCodeAmount', ReferralCodeAmountSchema);
