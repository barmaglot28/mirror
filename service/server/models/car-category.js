import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

const Schema = mongoose.Schema;

const CarCategorySchema = new Schema({
  name: { type: String, default: null, required: true },
  isMetered: { type: Boolean, default: false, required: true },
  type: { type: String, default: 'unmetered', required: true },
  pricePerKilometer: { type: Number, default: null, required: false },
  pricePerMinute: { type: Number, default: null, required: false },
  baseFare: { type: Number, default: null, required: false },
  minimumFare: { type: Number, default: null, required: false },
  commissionToCabdo: { type: Number, default: null, required: false },
  baseFareDay: { type: Number, default: null, required: false },
  pricePerKilometerDay: { type: Number, default: null, required: false },
  priceFirstKilometerDay: { type: Number, default: null, required: false },
  baseFareNight: { type: Number, default: null, required: false },
  pricePerKilometerNight: { type: Number, default: null, required: false },
  priceFirstKilometerNight: { type: Number, default: null, required: false },
  capacity: { type: Number, default: null, required: false },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  default: { type: Boolean, default: null },
  extraFare: { type: Number, default: null }
});

CarCategorySchema.statics = {

  get(id) {
    return this.findById(id)
        .execAsync()
        .then((carcategories) => {
          if (carcategories) {
            return carcategories;
          }
          const err = new APIError('No such carcategories exists!', httpStatus.NOT_FOUND);
          return Promise.reject(err);
        });
  },

  list({ skip, limit, filter } = {}) {
    let searchObj = {};
    switch (filter) {
      default:
        searchObj = {};
    }
    return this.find(searchObj)
        .sort({ name: 1 })
        .select('-__v')
        .skip(skip)
        .limit(limit)
        .execAsync();
  },

};

export default mongoose.model('CarCategory', CarCategorySchema);
