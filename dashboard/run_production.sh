#!/bin/sh

docker run --rm -it \
	-e PORT=3000 \
	-e WEBSITE_HOSTNAME="http://172.17.0.3:3000" \
        -e NODE_ENV=production \
	eu.gcr.io/cabdo-web/dashboard:0.0.3
