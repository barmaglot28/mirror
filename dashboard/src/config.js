/* eslint-disable max-len */

export const port = process.env.PORT || 4000;
export const host = process.env.WEBSITE_HOSTNAME || `http://localhost:${port}`;

// default locale is the first one
export const locales = ['en-US', 'de-DE'];

export const GoogleMapsKey = {
  key: 'AIzaSyA-rEfh3RHxj4utD1nd8sfu8bXpv8cCOho' // not implemented, use maps.google.apiKey instead!
};

export const auth = {
  jwt: { secret: process.env.JWT_SECRET || 'React Starter Kit' },
};

export const maps = {
  google: {
    apiKey: process.env.GOOGLE_MAPS_API_KEY || 'AIzaSyA-rEfh3RHxj4utD1nd8sfu8bXpv8cCOho', // umbrella-ivan-zhukov account
    libraries: 'places'
  },
};
