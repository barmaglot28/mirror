import React from 'react';
// import Layout from '../../components/Layout';
import Login from './Login';

const title = 'Log In';

export default {

  path: '/',

  action() {
    return {
      title,
      component: <Login title={title} />,
    };
  },
};
