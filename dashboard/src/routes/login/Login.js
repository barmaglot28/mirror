import React, { Component } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { FormGroup, FormControl } from 'react-bootstrap';
import s from './Login.css';
import logo from '../../common/images/cabdo-icon.png';
import { loginUser } from '../../store/domain/auth/action';
// import loginAction from '../../store/domain/auth/action';
import History from '../../core/history';

class Login extends Component {

  static propTypes = {
    loginUser: React.PropTypes.func,
    user: React.PropTypes.any,
    invalidCredentials: React.PropTypes.bool,
    loggedInStatus: React.PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      userType: 'admin',
      email: '',
      password: '',
    };
    this.handleLogin = this.handleLogin.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassowrd = this.handlePassowrd.bind(this);
  }


  componentDidMount() {
    // [].slice.call(document.querySelectorAll('button.progress-login'))
    // .forEach((bttn) => {
    //   new ProgressButton(bttn, { // eslint-disable-line
    //     callback: (instance) => {
    //       let progress = 0;
    //       const interval = setInterval(() => {
    //         progress = Math.min(progress + (Math.random() * 0.1), 1);
    //         instance._setProgress(progress); // eslint-disable-line

    //         if (progress === 1) {
    //           instance._stop(1); // eslint-disable-line
    //           clearInterval(interval);
    //           setTimeout(() => {
    //             History.push('/dashboard/users');
    //           }, 500);
    //         }
    //       }, 200);
    //     },
    //   });
    // });
  }
  componentWillReceiveProps(nextProps) {
    localStorage.setItem('id_token', nextProps.user.jwtAccessToken);
    if (nextProps.user && nextProps.user.jwtAccessToken) {
      History.push('/dashboard');
    }
  }

  handleEmail(e) {
    this.setState({
      email: e.target.value,
    });
  }

  handlePassowrd(e) {
    this.setState({
      password: e.target.value,
    });
  }

  handleLogin(e) {
    e.preventDefault();
    this.props.loginUser(this.state);
    return false;
  }

  render() {
    return (
      <div className={`login-page animate ${s.loginPage}`}>
        <div className={`${s.loginForm}`}>
          <div className={s.userAvatar}>
            <img src={logo} alt="flat Avatar" className="user-avatar" />
          </div>
          <h1>Cabdo Dashboard</h1>
          {
            this.props.invalidCredentials
            ?
              <div className="alert alert-danger">
                Username or password wrong.
              </div>
            :
            null
          }
          <form role="form" onSubmit={this.handleLogin}>
            <div className={s.formContent}>
              <FormGroup>
                <FormControl id="email" type="text" placeholder="Email" className={s.inputStyle} onChange={this.handleEmail} value={this.state.email} />
              </FormGroup>
              <FormGroup>
                <FormControl id="password" type="password" placeholder="Password" className={s.inputStyle} onChange={this.handlePassowrd} value={this.state.password} />
              </FormGroup>
            </div>
            <button type="submit" className={`btn progress-login progress-button btn-block ${s.btn}`} data-style="fill" data-horizontal >Log in</button>
          </form>
          <div className={s.imprintLinkWrap}>
            <a className={s.imprintLink} href="https://www.cabdo.de/impressum">Imprint</a>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.domainReducer.auth.user,
    invalidCredentials: state.domainReducer.auth.invalidCredentials,
    loggedInStatus: state.domainReducer.auth.loggedInStatus,
  };
}

function bindActions(dispatch) {
  return {
    loginUser: userCredentials => dispatch(loginUser(userCredentials)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Login));
