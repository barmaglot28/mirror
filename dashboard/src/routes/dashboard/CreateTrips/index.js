import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/dashboard/createTrips',

  async action() {
    const CreateTrips = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CreateTrips').default);
      }, 'createTrips');
    });

    return {
      title: 'Create New Trip',
      component: <Layout><CreateTrips key="createTrips" /></Layout>,
    };
  },
};
