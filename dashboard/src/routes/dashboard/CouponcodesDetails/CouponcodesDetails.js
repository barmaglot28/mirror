import React, { Component } from 'react';
import { Pagination, Panel, Row, Col, FormGroup, ControlLabel, FormControl, InputGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CouponcodesDetails.css';
import { fetchCouponcodesDetails } from '../../../store/domain/couponcodesDetails/action';

class CouponcodesDetails extends Component {
  static propTypes = {
    fetchCouponcodesDetails: React.PropTypes.func,
    id: React.PropTypes.string,
    couponcodesDetails: React.PropTypes.object,
    loading: React.PropTypes.bool,
  };

  componentDidMount() {
    this.props.fetchCouponcodesDetails(this.props.id);
  }

  render() {
    return (
        <div className="animate">
          {
            !this.props.loading
                ?
                <Panel>
                  <div className={s.couponcodesDetails}>
                    <div className="panel panel-default">
                      <div className="panel-heading">Coupon Codes Details</div>
                      <div className="panel-body">
                        <Row>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Code
                              </ControlLabel>
                              <InputGroup>
                                {this.props.couponcodesDetails.code}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Col md={5} mdOffset={1}>
                          <FormGroup>
                            <ControlLabel>
                              Discount
                            </ControlLabel>
                            <InputGroup>
                              {this.props.couponcodesDetails.discount}
                            </InputGroup>
                          </FormGroup>
                        </Col>
                      </div>
                    </div>
                  </div>
                </Panel>
                :
                <div className="loading-wrap" style={{ minHeight: 500 }}>
                  <div className="loading">
                    <div id="spinner">
                      <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                        <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33"
                                r="30"/>
                      </svg>
                    </div>
                  </div>
                </div>
          }
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.domainReducer.couponcodesDetails.loading,
    couponcodesDetails: state.domainReducer.couponcodesDetails.currentCouponcodesDetails.data,
  };
}

function bindActions(dispatch) {
  return {
    fetchCouponcodesDetails: (id) => dispatch(fetchCouponcodesDetails(id)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(CouponcodesDetails));
