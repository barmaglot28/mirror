import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/couponcodes/:id',
  async action({ params }) {
    const CouponcodesDetails = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CouponcodesDetails').default);
      }, 'couponcodesDetails');
    });
    return {
      title: 'CouponcodesDetails',
      component: <Layout><CouponcodesDetails key="couponcodesDetails" id={params.id} /></Layout>,
    };
  },
};
