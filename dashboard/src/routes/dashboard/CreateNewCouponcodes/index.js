import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/dashboard/createCouponcodes',

  async action() {
    const CreateCouponcodes = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CreateNewCouponcodes').default);
      }, 'createCouponcodes');
    });
    return {
      title: 'Create New Coupon Codes',
      component: <Layout><CreateCouponcodes key="CreateNewCouponcodes" /></Layout>,
    };
  },
};
