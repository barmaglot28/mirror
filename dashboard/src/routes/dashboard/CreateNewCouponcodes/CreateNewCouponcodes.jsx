import React, { Component } from 'react';
import { Panel, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Checkbox, Radio } from 'react-bootstrap';
import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './CreateNewCouponcodes.css';
import CouponcodesAction from '../../../store/domain/couponcodes/action';
import Spinner from '../../../components/Spinner';
import History from '../../../core/history';


class CreateCouponcodes extends Component {
  static propTypes = {
    newCouponcodesObj: React.PropTypes.object,
    createCouponcodesErrorObj: React.PropTypes.object,
    failedCreateCouponcodesApi: React.PropTypes.bool,
    newCouponcodesLoading: React.PropTypes.bool,
    createCouponcodes: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      couponcodesObj: {
        code: 'NA',
        discount: 'NA',
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newCouponcodesObj && nextProps.newCouponcodesObj.data && !nextProps.newCouponcodesLoading) {
      if (!nextProps.failedCreateCouponcodesApi) {
        History.push('/dashboard/couponcodes');
      }
    }
  }

  handleCode(e) {
    const couponcodesObj = this.state.couponcodesObj;
    couponcodesObj.code = e.target.voucherCode.generate({count: 1, length: 8, charset: 'abcdefghijklmnopqrstuvwxyz'});
    this.setState({ couponcodesObj });
  }

  handleDiscount(e) {
    const couponcodesObj = this.state.couponcodesObj;
    couponcodesObj.discount = e.target.value;
    this.setState({ couponcodesObj });
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('inside submit form with event obj', e);
    const couponcodesObj = this.state.couponcodesObj;
    this.props.createCouponcodes(couponcodesObj);
  }


  render() {
    return (
        <div className="animate">

          <Row key="newCouponcodes">
            <Col md={10}>
              <Panel header="Create New Coupon codes" bsStyle="primary">
                {
                  this.props.failedCreateCouponcodesApi
                      ?
                      <div className="alert alert-danger">
                        {/* <strong>Api Error: Api server is not working properly.</strong> */}
                        <strong>{this.props.createCouponcodesErrorObj.message}</strong>
                      </div>
                      :
                      null

                }
                {
                  this.props.newCouponcodesLoading
                      ?
                      <Spinner componentsId="createNewCouponcodes"/>
                      :
                      <Form horizontal onSubmit={(e) => {
                        this.handleSubmit(e);
                      }} id="createCouponcodes">
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Discount
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-folder-close"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Discount" onChange={(e) => {
                                  this.handleDiscount(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <hr />
                        <div className="pull-right clearfix">
                          <Button className={`${s.formButton} btn-rounded`} type="reset"> Reset </Button>&nbsp;&nbsp;
                          <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} type="submit">
                            Generate </Button>
                        </div>
                      </Form>
                }
              </Panel>
            </Col>
          </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newCouponcodesLoading: state.domainReducer.couponcodes.newCouponcodesLoading,
    failedCreateCouponcodesApi: state.domainReducer.couponcodes.failedCreateCouponcodesApi,
    createCouponcodesErrorObj: state.domainReducer.couponcodes.createCouponcodesErrorObj,
    newCouponcodesObj: state.domainReducer.couponcodes.newCouponcodesObj,
  };
}

function bindActions(dispatch) {
  return {
    createCouponcodes: (couponcodesObj) => dispatch(CouponcodesAction.createNewCouponcodes(couponcodesObj)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(CreateCouponcodes));
