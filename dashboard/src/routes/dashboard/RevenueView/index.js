/**
 * Created by malinovskiynewpost@gmail.com Malynovs'kiy Volodymyr on 7/26/2017.
 */
import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/revenueview',
  async action() {
    const Users = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Users').default);
      }, 'users');
    });
    return {
      title: 'Revenue View',
      component: <Layout><Users key="users" /></Layout>
    };
  },
};
