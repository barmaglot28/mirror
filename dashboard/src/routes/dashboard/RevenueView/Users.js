/**
 * Created by malinovskiynewpost@gmail.com Malynovs'kiy Volodymyr on 7/26/2017.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Button } from 'react-bootstrap';
import DatePicker from 'react-bootstrap-date-picker';
import { BarChart, ResponsiveContainer, Pie, PieChart, Cell, Tooltip, Legend, XAxis, Bar } from 'recharts';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Users.css';
import tripAction from '../../../store/domain/trips/action';
import UserUpdateAction from '../../../store/domain/updateUser/action';
import History from '../../../core/history';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {  // eslint-disable-line
  const radius = innerRadius + ((outerRadius - innerRadius) * 0.5);
  const x = cx + (radius * Math.cos(-midAngle * RADIAN));
  const y = cy + (radius * Math.sin(-midAngle * RADIAN));

  return (
    <text
      x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? 'start' : 'end'}
      dominantBaseline="central"
      >
      {
        `${(percent * 100).toFixed(0)}%`
      }
    </text>
  );
};
class Users extends Component {

  static propTypes = {
    userList: React.PropTypes.any,
    isLoggedIn: React.PropTypes.bool,
    loading: React.PropTypes.bool,
    getAllTripsPerDay: React.PropTypes.func,
    getAllTripsPerDayRevenueData: React.PropTypes.func,
    meta: React.PropTypes.object,
    getTotalCommission: React.PropTypes.func,
    getTotalAmountFare: React.PropTypes.func,
    getTotalAmountCommission: React.PropTypes.func,
    getAllTrips: React.PropTypes.func,
    // chartLoading: React.PropTypes.bool,
    // chartData: React.PropTypes.object,
    getTotalFare: React.PropTypes.func,
    failedUserListApi: React.PropTypes.bool,
    revenueGraphLoading: React.PropTypes.bool,
    revenueGraphData: React.PropTypes.object,
  };

  static goToUserDetail(id) {
    History.push(`/dashboard/usertrips/${id}`);
  }

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      activePage: 1,
      value: new Date().toISOString()
    };
    this.setLoading = this.setLoading.bind(this);
    // this.handlePagination = this.handlePagination.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    // this.getChartData = this.getChartData.bind(this);
    this.getTotalFare = this.getTotalFare.bind(this);
    this.getTotalCommission = this.getTotalCommission.bind(this);
    this.getTotalAmountFare = this.getTotalAmountFare.bind(this);
    this.getAllTrips = this.getAllTrips.bind(this);
    this.getTotalAmountCommission = this.getTotalAmountCommission.bind(this);
    this.getRevenueData = this.getRevenueData.bind(this);
  }

  componentWillMount() {
    this.props.getAllTripsPerDay(new Date().toISOString().slice(0,10));
    this.props.getAllTripsPerDayRevenueData(this.state.value.slice(0,10));
  }

  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }

  handleChangeDate(date, formattedValue){
    this.setState({
      value: date,
      formattedValue: formattedValue
    });
    if(date != null && formattedValue != null){
      this.props.getAllTripsPerDay(date);
    }
    this.props.getAllTripsPerDayRevenueData(date);
  }

  // getChartData() {
  //   const data = [];
  //   if (this.props.chartData && this.props.chartData.success) {
  //     const chartArray = this.props.chartData.data;
  //     data.push({ name: 'rider', value: chartArray[0].rider });
  //     data.push({ name: 'driver', value: chartArray[0].driver });
  //   }
  //   return data;
  // }

  getTotalCommission(trips){
    let total = 0;
    if (trips[0].wallet){
      total += trips[0].wallet.commissionToCabdo * (this.getTotalFare(trips)) / 100;
    }

    return total;
  }

  getTotalFare(trips){
    let result = 0;
    trips.forEach((e) => {
      if (e.wallet){
        result += e.wallet.walletBalance;
      }
    });
    return result;
  }

  getAllTrips(allUsAndTrips){
    let result = 0;
    allUsAndTrips.forEach((e) => {
      result += e.trips.length;
    });
    return result;
  }

  getTotalAmountFare(allUsAndTrips){
    let result = 0;
    allUsAndTrips.forEach((e) => {
      result += this.getTotalFare(e.trips);
    });
    return result;
  }

  getTotalAmountCommission(allUsAndTrips){
    let result = 0;
    allUsAndTrips.forEach((e) => {
      result += this.getTotalCommission(e.trips);
    });
    return result;
  }

  getRevenueData() {
    let dayTime = [];
    if (!this.props.revenueGraphLoading && this.props.revenueGraphData) {
      dayTime = [
        {name: '00', value: 0 },
        {name: '01', value: 0 },
        {name: '02', value: 0 },
        {name: '03', value: 0 },
        {name: '04', value: 0 },
        {name: '05', value: 0 },
        {name: '06', value: 0 },
        {name: '07', value: 0 },
        {name: '08', value: 0 },
        {name: '09', value: 0 },
        {name: '10', value: 0 },
        {name: '11', value: 0 },
        {name: '12', value: 0 },
        {name: '13', value: 0 },
        {name: '14', value: 0 },
        {name: '15', value: 0 },
        {name: '16', value: 0 },
        {name: '17', value: 0 },
        {name: '18', value: 0 },
        {name: '19', value: 0 },
        {name: '20', value: 0 },
        {name: '21', value: 0 },
        {name: '22', value: 0 },
        {name: '23', value: 0 },
      ];
      const revenueData = this.props.revenueGraphData.data;
      if (revenueData.length !== 0) {
        revenueData.forEach((item, i) => {
          dayTime[item.hour -1].value = item.tripAmt;
        });
      }
    }
    return dayTime;
  }

  render() {
    return (
      <div className={`animate ${s.user}`}>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4>Taxi App Trips: Per day Uber Revenue statistics</h4>
          </div>
          <div className="panel-body">
            {
              this.props.revenueGraphLoading
                ?
                <div id="revenueGraphLoading" className="loading-wrap" style={{ minHeight: 500 }}>
                  <div className="loading">
                    <div id="spinner">
                      <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                        <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                      </svg>
                    </div>
                  </div>
                </div>
                :
                (
                  <ResponsiveContainer width="100%" height={142}>
                    <BarChart
                      data={this.getRevenueData()}
                      margin={{ top: 10, right: 10, left: 10, bottom: 0 }}
                    >
                      <XAxis dataKey="name" />
                      <Tooltip />
                      <Bar dataKey="value" fill="#DE6764" />
                    </BarChart>
                  </ResponsiveContainer>
                )
            }
          </div>
        </div>
        <div>
          {
            this.state.isLoading
              ?
              <div className="loading-wrap" style={{ minHeight: 500 }}>
                <div className="loading">
                  <div id="spinner">
                    <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                      <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                    </svg>
                  </div>
                </div>
              </div>
              :
              <div className="row">
                <div className="col-sm-12">
                  <div className="panel panel-body">
                    <label>Choose the day</label>
                    <div style={{ width: 40+'%' }}>
                      <div className="panel">
                        <DatePicker
                          dateFormat="YYYY/MM/DD"
                          value={this.state.value}
                          onChange={this.handleChangeDate} />
                      </div>
                    </div>
                    <div className="table-responsive">
                      <table className="table">
                        <tbody>
                        <tr>
                          <th>Name</th>
                          <th>Time online</th>
                          <th>Completed Trips</th>
                          <th>Cancelled trips</th>
                          <th>Total Fare</th>
                          <th>Total Commission</th>
                          <th>Today's trips</th>
                        </tr>
                        {
                          this.props.failedUserListApi
                            ?
                            <tr>
                              <td colSpan="10">
                                <div className="alert alert-danger">
                                  <strong>Api Error: Api server is not working properly.</strong>
                                </div>
                              </td>
                            </tr>
                            : null
                        }
                        {
                          this.props.userList !== null && this.props.userList.length
                            ?
                            this.props.userList.map((item, index) => (
                              <tr key={index}>
                                <td>{item.user.fname} {item.user.lname}</td>
                                <td>{'No such functionality'}</td>
                                <td>{item.trips.length}</td>
                                <td>{'No such functionality'}</td>
                                <td>{this.getTotalFare(item.trips)}</td>
                                <td>{this.getTotalCommission(item.trips)}</td>
                                <td>
                                  <button
                                    className="btn btn-warning"
                                    onClick={() => Users.goToUserDetail(item.user._id)}
                                    >
                                    <i className="glyphicon glyphicon-eye-open" />
                                  </button>
                                </td>
                              </tr>
                              )) : null
                        }
                        {
                          this.props.userList !== null && this.props.userList.length
                          ?
                           ( <tr>
                              <td>{'ALL'}</td>
                              <td>{'No such functionality'}</td>
                              <td>{this.getAllTrips(this.props.userList)}</td>
                              <td>{'No such functionality'}</td>
                              <td>{this.getTotalAmountFare(this.props.userList)}</td>
                              <td>{this.getTotalAmountCommission(this.props.userList)}</td>
                            </tr>)
                            : null
                        }
                        </tbody>
                      </table>
                      <hr />
                    </div>
                  </div>
                </div>
              </div>
          }
        </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    userList: state.domainReducer.trips.tripList,
    isLoggedIn: state.domainReducer.auth.isLoggedIn,
    loading: state.domainReducer.trips.loading,
    meta: state.domainReducer.trips.meta,
    // chartData: state.domainReducer.users.chartData,
    // chartLoading: state.domainReducer.users.chartLoading,
    failedUserListApi: state.domainReducer.trips.errorStatusCreateTripObj,
    revenueGraphLoading: state.domainReducer.trips.revenueGraphLoadingPerDay,
    revenueGraphData: state.domainReducer.trips.revenueGraphDataPerDay,
  };
}

function bindActions(dispatch) {
  return {
    getAllTripsPerDay: (filter) => dispatch(tripAction.getAllTripsPerDay(filter)),
    getAllTripsPerDayRevenueData: (date) => dispatch(tripAction.getAllTripsPerDayRevenueData(date)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Users));
