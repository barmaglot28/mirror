import React from 'react';
import Layout from '../../../../components/Layout/Layout';

export default {
  path: '/cancelled-pre-bookings',
  async action() {
    const PreBookings = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Cancelled').default);
      }, 'bookings');
    });
    return {
      title: 'CancelledPreBookings',
      component: <Layout><PreBookings key="cancelled-pre-bookings" /></Layout>,
    };
  },
};
