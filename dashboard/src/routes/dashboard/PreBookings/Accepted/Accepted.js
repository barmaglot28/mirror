import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Panel, Button, Modal } from 'react-bootstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import moment from 'moment';
import { BarChart, Bar, ResponsiveContainer, Tooltip, XAxis } from 'recharts';
import s from './Accepted.css';
import bookingsAction from '../../../../store/domain/bookings/action';

class AcceptedPreBookings extends Component {

  static propTypes = {
    fetchAcceptedBookings: React.PropTypes.func,
    unpinAcceptedBooking: React.PropTypes.func,
    loading: React.PropTypes.bool,
    acceptedBookingsList: React.PropTypes.array,
    meta: React.PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      modalShown: false,
      booking: null
    };
    this.handlePagination = this.handlePagination.bind(this);
    this.onToggleModal = this.onToggleModal.bind(this);
    this.onSubmitUnpinAcceptedPreBooking = this.onSubmitUnpinAcceptedPreBooking.bind(this);
  }

  componentWillMount() {
    this.props.fetchAcceptedBookings(this.state.activePage);
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchAvailableBookings(eventkey);
  }

  onUnpinAcceptedPreBooking(item) {
    this.setState({
      modalShown: true,
      booking: item
    });
  }

  onToggleModal() {
    this.setState({
      modalShown: !this.state.modalShown
    });
  }

  onSubmitUnpinAcceptedPreBooking() {
    this.props.unpinAcceptedBooking(this.state.booking);
    this.props.fetchAcceptedBookings(this.state.activePage);
    this.setState({
      modalShown: false
    });
  }

  render() {
    const driverName = this.state.booking ? this.state.booking.driver.fname + ' ' + this.state.booking.driver.lname : '';
    return (
      <div className={`animate ${s.trips}`}>
        <Panel  style={{ 'background-color': '#f5f5f5' }}>
          <h4>Accepted pre-bookings</h4>
        </Panel>
        {
          this.props.loading
            ?
            <div id="table" className="loading-wrap" style={{ minHeight: 500 }} key="spinnerdiv">
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
            :
            <div key="dddiv">
              <div className="row">
                <div className="col-sm-12">
                  <div className="panel panel-body">
                    {
                      !this.props.acceptedBookingsList || this.props.acceptedBookingsList.length === 0
                        ?
                        <div> No trip Accepted </div>
                        :
                        <div className="table-responsive">
                          <table className="table">
                            <tbody>
                            <tr>
                              <th>S.No</th>
                              <th>Trip No</th>
                              <th>RiderName</th>
                              <th>RiderPhoneNo</th>
                              <th>DriverName</th>
                              <th>Date</th>
                              <th>Pick-up Address</th>
                              <th>Destination Address</th>
                              <th>Fare</th>
                              <th>Trip status</th>
                              <td>Action</td>
                            </tr>
                            {
                              this.props.acceptedBookingsList.map((item, index) => (
                                <tr key={index}>
                                  <td>
                                    {
                                      (
                                        (this.state.activePage - 1) * this.props.meta.limit
                                      ) + (index + 1)
                                    }
                                  </td>
                                  <td>{`#${item.tripNumber}`}</td>
                                  <td>{item.hasOwnProperty('rider') ? item.rider.fname + ' ' + item.rider.lname : '------'}</td>
                                  <td>{item.hasOwnProperty('rider') && item.rider.phoneNo !== '9876543210' ? item.rider.phoneNo : '------'}</td>
                                  <td>{item.hasOwnProperty('driver') ? item.driver.fname + ' ' + item.driver.lname : 'driver deleted'}</td>
                                  <td>{moment(item.preBookedTime).format('Do MMM [at] HH:mm')}</td>
                                  <td className="text-capitalize">{item.pickUpAddress}</td>
                                  <td className="text-capitalize">{item.destAddress}</td>
                                  <td>{item.tripAmt}</td>
                                  <td>{item.tripStatus}</td>
                                  <td style={{ minWidth: 50 }}>
                                    <button className="btn btn-info" onClick={() => { this.onUnpinAcceptedPreBooking(item); }}>
                                      <i className="glyphicon glyphicon-export" />
                                    </button>
                                  </td>
                                </tr>),
                              )
                            }
                            </tbody>
                          </table>
                          <Pagination
                            bsSize="medium"
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            maxButtons={5}
                            items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                            activePage={this.state.activePage}
                            onSelect={this.handlePagination}
                          />
                        </div>
                    }
                  </div>
                </div>
              </div>
            </div>
        }
        <Modal show={this.state.modalShown} onHide={this.onToggleModal}>
          <Modal.Header closeButton>
            <Modal.Title>Unpin accept bookings</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Are you sure you want to unpin driver - {driverName} and move accepted booking to available bookings?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.onSubmitUnpinAcceptedPreBooking}>Yes, move prebooking</Button>
            <Button onClick={this.onToggleModal}>No, close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    loading: state.domainReducer.bookings.loading,
    acceptedBookingsList: state.domainReducer.bookings.acceptedBookingsList,
    meta: state.domainReducer.bookings.meta,
  };
}

function bindActions(dispatch) {
  return {
    fetchAcceptedBookings: (pageNo) => dispatch(bookingsAction.fetchAcceptedBookings(pageNo)),
    unpinAcceptedBooking: (booking) => dispatch(bookingsAction.unpinAcceptedBooking(booking)),
  };
}
export default connect(mapStateToProps, bindActions)(withStyles(s)(AcceptedPreBookings));
