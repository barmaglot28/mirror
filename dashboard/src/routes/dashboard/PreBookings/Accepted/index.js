import React from 'react';
import Layout from '../../../../components/Layout';

export default {
  path: '/accepted-pre-bookings',
  async action() {
  // async action({ store }) {
  //   const authObj = store.getState().domainReducer.auth;
  //   if (authObj && !authObj.loggedInStatus) {
  //     console.log('store ', store.getState());
  //     return { redirect: '/' };
  //   }
    const PreBookings = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Accepted').default);
      }, 'bookings');
    });
    return {
      title: 'AcceptedPreBookings',
      component: <Layout><PreBookings key="accepted-pre-bookings" /></Layout>,
    };
  },
};
