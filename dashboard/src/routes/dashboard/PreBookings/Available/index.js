import React from 'react';
import Layout from '../../../../components/Layout/Layout';

export default {
  path: '/available-pre-bookings',
  async action() {
  // async action({ store }) {
  //   const authObj = store.getState().domainReducer.auth;
  //   if (authObj && !authObj.loggedInStatus) {
  //     console.log('store ', store.getState());
  //     return { redirect: '/' };
  //   }
    const PreBookings = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Available').default);
      }, 'bookings');
    });
    return {
      title: 'AvailablePreBookings',
      component: <Layout><PreBookings key="available-pre-bookings" /></Layout>,
    };
  },
};
