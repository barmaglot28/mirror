import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Panel } from 'react-bootstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import moment from 'moment';
import { BarChart, Bar, ResponsiveContainer, Tooltip, XAxis } from 'recharts';
import s from './Available.css';
import bookingsAction from '../../../../store/domain/bookings/action';

class AvailablePreBookings extends Component {

  static propTypes = {
    fetchAvailableBookings: React.PropTypes.func,
    removePreBooking: React.PropTypes.func,
    removePreBookingSuccess: React.PropTypes.object,
    loading: React.PropTypes.bool,
    availableBookingsList: React.PropTypes.array,
    meta: React.PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
    };
    this.handlePagination = this.handlePagination.bind(this);
  }
  componentWillMount() {
    this.props.fetchAvailableBookings(this.state.activePage);
  }

  onDeletePreBooking(item) {
    this.props.removePreBooking(item);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.removePreBookingSuccess) {
      this.props.fetchAvailableBookings(this.state.activePage);
    }
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchAvailableBookings(eventkey);
  }

  render() {
    return (
      <div className={'animate'}>
        <Panel  style={{ 'background-color': '#f5f5f5' }}>
          <h4>Available pre-bookings</h4>
        </Panel>
        {
          this.props.loading
            ?
            <div id="table" className="loading-wrap" style={{ minHeight: 500 }} key="spinnerdiv">
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
            :
            <div key="dddiv">
              <div className="row">
                <div className="col-sm-12">
                  <div className="panel panel-body">
                    {
                      !this.props.availableBookingsList || this.props.availableBookingsList.length === 0
                        ?
                        <div> No trip Available </div>
                        :
                        <div className="table-responsive">
                          <table className="table">
                            <tbody>
                            <tr>
                              <th>S.No</th>
                              <th>Trip No</th>
                              <th>RiderName</th>
                              <th>RiderPhoneNo</th>
                              <th>Date</th>
                              <th>Pick-up Address</th>
                              <th>Destination Address</th>
                              <th>Fare</th>
                              <th>Trip status</th>
                              <td>Action</td>
                            </tr>
                            {
                              this.props.availableBookingsList.map((item, index) => (
                                <tr key={index}>
                                  <td>
                                    {
                                      (
                                        (this.state.activePage - 1) * this.props.meta.limit
                                      ) + (index + 1)
                                    }
                                  </td>
                                  <td>{`#${item.tripNumber}`}</td>
                                  <td>{item.hasOwnProperty('rider') ? item.rider.fname + ' ' + item.rider.lname : '------'}</td>
                                  <td>{item.hasOwnProperty('rider') && item.rider.phoneNo !== '9876543210' ? item.rider.phoneNo : '------'}</td>
                                  <td>{moment(item.preBookedTime).local().format('Do MMM [at] HH:mm')}</td>
                                  <td className="text-capitalize">{item.pickUpAddress}</td>
                                  <td className="text-capitalize">{item.destAddress}</td>
                                  <td>{item.tripAmt}</td>
                                  <td>{item.tripStatus}</td>
                                  <td style={{ minWidth: 100 }}>
                                    <button className="btn btn-danger" onClick={() => { this.onDeletePreBooking(item); }}>
                                      <i className="glyphicon glyphicon-remove" />
                                    </button>
                                  </td>
                                </tr>),
                              )
                            }
                            </tbody>
                          </table>
                          <Pagination
                            bsSize="medium"
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            maxButtons={5}
                            items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                            activePage={this.state.activePage}
                            onSelect={this.handlePagination}
                          />
                        </div>
                    }
                  </div>
                </div>
              </div>
            </div>
        }
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    loading: state.domainReducer.bookings.loading,
    availableBookingsList: state.domainReducer.bookings.availableBookingsList,
    removePreBookingSuccess: state.domainReducer.bookings.removePreBookingSuccess,
    meta: state.domainReducer.bookings.meta,
  };
}

function bindActions(dispatch) {
  return {
    fetchAvailableBookings: (pageNo, filter) => dispatch(bookingsAction.fetchAvailableBookings(pageNo)),
    removePreBooking: (item) => dispatch(bookingsAction.removePreBooking(item))
  };
}
export default connect(mapStateToProps, bindActions)(withStyles(s)(AvailablePreBookings));
