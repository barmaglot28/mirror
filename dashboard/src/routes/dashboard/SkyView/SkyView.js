import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, ButtonToolbar, ToggleButtonGroup, ToggleButton, Row } from 'react-bootstrap';
import { InputGroup } from 'react-bootstrap/lib/InputGroup';
import UserAction from '../../../store/domain/users/action';
import SkyViewAction from '../../../store/domain/skyView/action';
import GoogleMap from 'google-map-react';
import { GoogleMapsKey } from '../../../config';
import carDefault from '../../../common/images/car2.png';
import carOptional from '../../../common/images/car3.png';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SkyView.css';

let DriverPositionComponent = ({driver}) => {
  let colorPin = 'green';
  if (driver.currTripId !== null) colorPin = 'red';
  return <div className={'driver'}>
    <img src={require(`../../../common/images/marker-${colorPin}.png`)} style={{width: '18px', height: '30px', resizeMode: 'contain', marginTop: '-35px', marginLeft: '-9px', position: 'absolute'}} />
  </div>
};

class SkyView extends Component {

  static propTypes = {
    userList: React.PropTypes.any,
    isLoggedIn: React.PropTypes.bool,
    loading: React.PropTypes.bool,
    fetchCarCategories: React.PropTypes.func,
    fetchDrivers: React.PropTypes.func,
    failedUserListApi: React.PropTypes.bool,
    carCategories: React.PropTypes.array,
    defaultCategory: React.PropTypes.object,
  };

  static defaultProps = {
    center: {lat: 51.5166667, lng: 7.45},
    zoom: 11,
    googleKey: GoogleMapsKey
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      carCategory: null,
      drivers: []
    };
    this.setLoading = this.setLoading.bind(this);
  }

  componentWillMount() {
    this.props.fetchCarCategories();
    this.props.fetchDrivers();
  }

  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
    if (nextProps.defaultCategory && !this.state.carCategory) {
      this.setState({
        carCategory: nextProps.defaultCategory,
        drivers: nextProps.defaultCategory.drivers
      });
    }
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }

  handleTaxiType(value) {
    let category = this.props.carCategories.find((arr, i) => {
      return arr._id === value;
    });
    this.setState({ carCategory: category, drivers: category.drivers });
    return null;
  }
  render() {
    return (
      <div className={`animate`}>
        <div>
          {
            this.state.isLoading
              ?
              <div className="loading-wrap" style={{ minHeight: 500 }}>
                <div className="loading">
                  <div id="spinner">
                    <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                      <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33"
                              r="30"/>
                    </svg>
                  </div>
                </div>
              </div>
              :
              <div style={{width: 80+'vw', height: 50+'vw'}} >
                { this.props.carCategories ?
                  <Row>

                      <ButtonToolbar>
                        <ToggleButtonGroup type="radio" name="options" value={ this.state.carCategory ? this.state.carCategory._id : null }
                                            onChange={(value) => { this.handleTaxiType(value); }}>
                          { this.props.carCategories.map((item, index) => (
                            <ToggleButton key={index} value={item._id}  className={`test ${s.taxiWrapper}`}>
                              <p>{item.name}</p>
                              <img src={ item.default ? carDefault : carOptional }/>
                            </ToggleButton>
                          )) }
                        </ToggleButtonGroup>
                      </ButtonToolbar>
                  </Row>
                  :
                  <span>no cars</span>
                }
                <br/><br/>
                <GoogleMap
                  bootstrapURLKeys={this.props.googleKey}
                  defaultCenter={this.props.center}
                  defaultZoom={this.props.zoom}
                  >
                  { this.state.drivers
                    ?
                    this.state.drivers.map((item, index) => (
                      <DriverPositionComponent
                        key={index}
                        lat={item.gpsLoc[1]}
                        lng={item.gpsLoc[0]}
                        driver={item}
                        />
                      ))
                     :  null
                  }
                </GoogleMap>
              </div>
          }
          {
            this.props.failedUserListApi
              ?
              <tr>
                <td colSpan="10">
                  <div className="alert alert-danger">
                    <strong>Api Error: Api server is not working properly.</strong>
                  </div>
                </td>
              </tr>
              : null
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userList: state.domainReducer.users.userList,
    isLoggedIn: state.domainReducer.auth.isLoggedIn,
    loading: state.domainReducer.users.loading,
    meta: state.domainReducer.users.meta,
    failedUserListApi: state.domainReducer.users.failedUserListApi,
    carCategories: state.domainReducer.skyView.carCategories,
    defaultCategory: state.domainReducer.skyView.defaultCategory,
  };
}

function bindActions(dispatch) {
  return {
    fetchDrivers: () => dispatch(UserAction.fetchDrivers()),
    fetchCarCategories: () => (dispatch(SkyViewAction.fetchCarCategories()))
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(SkyView));
