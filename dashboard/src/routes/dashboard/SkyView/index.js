import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/godsview',
  async action({ params }) {
    const UserDetail = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./SkyView').default);
      }, 'skyView');
    });
    return {
      title: 'Gods View',
      component: <Layout><UserDetail key="users" id={params.id} /></Layout>,
    };
  },
};
