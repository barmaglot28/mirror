import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/user/:id',
  async action({ params }) {
    const UserDetail = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./UserDetail').default);
      }, 'userDetail');
    });
    return {
      title: 'UserDetail',
      component: <Layout><UserDetail key="userDetail" id={params.id} /></Layout>,
    };
  },
};
