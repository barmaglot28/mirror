import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Button, Col, Row, FormGroup, ControlLabel, Panel } from 'react-bootstrap';
import DatePicker from 'react-bootstrap-date-picker';
import Moment from 'moment';
import withStyles from 'isomorphic-style-loader/lib/withStyles';


import s from './UserDetail.css';
import { fetchUserDetails, fetchTripDetails, exportCompleted } from '../../../store/domain/userDetails/action';
import TripList from './tripList';

class Users extends Component {
  static propTypes = {
    currentUser: React.PropTypes.object,
    fetchUserDetails: React.PropTypes.func,
    fetchTripDetails: React.PropTypes.func,
    id: React.PropTypes.string,
    userDetails: React.PropTypes.object,
    userTrips: React.PropTypes.array,
    meta: React.PropTypes.object,
    tripLoading: React.PropTypes.bool,
    exportCompleted: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      startRange: null,
      endRange: null
    };
    this.handlePagination = this.handlePagination.bind(this);
    this.handleChangeStartRange = this.handleChangeStartRange.bind(this);
    this.handleChangeEndRange = this.handleChangeEndRange.bind(this);
  }

  componentDidMount() {
    this.props.fetchUserDetails(this.props.id);
    this.props.fetchTripDetails(this.props.id);
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchTripDetails(this.props.id, eventkey);
  }

  handleChangeStartRange(startRange) {
    this.setState({ startRange });
  }

  handleChangeEndRange(endRange) {
    this.setState({ endRange });
  }

  handleExportCompleted() {
    this.props.exportCompleted(this.props.id, {
      startRange: this.state.startRange ? Moment(this.state.startRange).startOf('day').toISOString() : null,
      endRange: this.state.endRange ? Moment(this.state.endRange).endOf('day').toISOString() : null
    });
  }

  render() {
    return (
      <div className={`animate ${s.user} ${s.userDetails}`}>
        <div className="container-fluid">
          <div className="panel panel-default">
            <div className="panel-body">
              <div className="media no-margin">
                <div className="media-left">
                  <a href>
                    <img
                      width="72px"
                      className="media-object"
                      src="https://pbs.twimg.com/profile_images/685550474880143360/HjrqdoE4.png"
                      alt=""
                    />
                  </a>
                </div>
                <div className="media-body">
                  <div className="row">
                    <div className="col-lg-3 col-md-6">
                      <blockquote>
                        <p>
                          { this.props.userDetails ? this.props.userDetails.fname : 'NA' }
                          { this.props.userDetails ? this.props.userDetails.lname : 'NA' }
                        </p>
                        <small><cite title="Source Title">
                          { this.props.userDetails ? this.props.userDetails.state : 'NA' } <i className="glyphicon glyphicon-map-marker" /></cite>
                        </small>
                      </blockquote>
                    </div>
                    <div className="col-lg-3 col-md-6">
                      <p><i className="glyphicon glyphicon-envelope" /> { this.props.userDetails ? this.props.userDetails.email : 'NA'} </p>
                      <p><i className="glyphicon glyphicon-phone-alt" /> { this.props.userDetails ? this.props.userDetails.phoneNo : 'NA'} </p>
                      <p><i className="glyphicon glyphicon-calendar" /> {new Date(Date.parse(this.props.userDetails ? this.props.userDetails.dob : 'NA')).toDateString()}</p>
                    </div>
                  </div>
                </div>
              </div>
              <Panel header='Exporting'>
                <Row>
                  <Col md={4}>
                    <FormGroup>
                      <ControlLabel>From date</ControlLabel>
                      <DatePicker
                        dateFormat="DD/MM/YYYY"
                        value={this.state.startRange}
                        onChange={this.handleChangeStartRange} />
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup>
                      <ControlLabel>To date</ControlLabel>
                      <DatePicker
                        dateFormat="DD/MM/YYYY"
                        value={this.state.endRange}
                        onChange={this.handleChangeEndRange} />
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup>
                      <ControlLabel className="visible-md visible-lg" >&nbsp;</ControlLabel>
                      <Button type="button" bsStyle="primary" style={{ display: 'block' }}
                        onClick={() => { this.handleExportCompleted(); }}
                      >Export completed trips</Button>
                    </FormGroup>
                  </Col>
                </Row>
              </Panel>
              <h4><b>Last Trip Details:</b></h4>
              {
                this.props.tripLoading
                ?
                  <div className="loading-wrap" style={{ minHeight: 500 }}>
                    <div className="loading">
                      <div id="spinner">
                        <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                          <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                        </svg>
                      </div>
                    </div>
                  </div>
                :
                (
                  this.props.userTrips.length >= 1
                  ?
                    <div>
                      {
                      this.props.userTrips.map(
                        (trips, index) => <TripList key={index} trip={trips} />,
                      )
                    }
                      <Pagination
                        bsSize="medium"
                        prev
                        next
                        first
                        last
                        ellipsis
                        boundaryLinks
                        maxButtons={5}
                        items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                        activePage={this.state.activePage}
                        onSelect={this.handlePagination}
                      />
                    </div>
                  : <div> No Trips Details Avaliable </div>
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.domainReducer.currentUser.loading,
    tripLoading: state.domainReducer.currentUser.tripLoading,
    userDetails: state.domainReducer.currentUser.currentUserDetails.data,
    userTrips: state.domainReducer.currentUser.currentUserTrips.data,
    meta: state.domainReducer.currentUser.currentUserTrips.meta,
  };
}

function bindActions(dispatch) {
  return {
    fetchUserDetails: (id) => dispatch(fetchUserDetails(id)),
    fetchTripDetails: (id, pageNo) => dispatch(fetchTripDetails(id, pageNo)),
    exportCompleted: (id, ranges) => dispatch(exportCompleted(id, ranges))
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Users));
