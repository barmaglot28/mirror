import Rider from '../Users/Rider';
import Driver from '../Users/Driver';
import Corporate from '../Users/Corporate';
import UserDetails from '../UserDetails';
import Wallets from '../Wallets/index';
import Cars from '../Cars';
import CarDetails from '../CarDetails';
import CreateNewCar from '../CreateNewCar';
import Trips from '../Trips';
import AvailablePreBookings from '../PreBookings/Available/index';
import AcceptedPreBookings from '../PreBookings/Accepted/index';
import CancelledPreBookings from '../PreBookings/Cancelled/index';
import TripDetails from '../TripDetails';
import Carcategories from '../Carcategories';
import CarcategoriesDetails from '../CarcategoriesDetails';
import Referralcodes from '../Referralcodes';
import RevenueViews from '../RevenueView';
import RevenueViewDetails from '../RevenueViewDetail';
import SkyView from '../SkyView';
import Couponcodes from '../couponCodes';
import CouponcodesDetails from '../CouponcodesDetails';




export default {
  path: '/dashboard',
  children: [
    Rider,
    Driver,
    Corporate,
    UserDetails,
    Wallets,
    Cars,
    CarDetails,
    CreateNewCar,
    Trips,
    AvailablePreBookings,
    AcceptedPreBookings,
    CancelledPreBookings,
    TripDetails,
    Carcategories,
    CarcategoriesDetails,
    RevenueViews,
    RevenueViewDetails,
    SkyView,
    Couponcodes,
    CouponcodesDetails,
    Referralcodes
  ],
  async action({ next, store }) {
    const route = await next();
    const authObj = store.getState().domainReducer.auth;
    if (authObj && !authObj.loggedInStatus) {
      console.log('inside protectedRoutes');
      return { redirect: '/' };
    }
    return route;
  },
};
