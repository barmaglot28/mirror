import moment from 'moment';
import React from 'react';
import logo from '../../../common/images/logo.jpg';
import s from './UserDetail.css';
import Utils from '../../../helpers/utils';

function getDate(day) {
  if (day) {
    return moment(day, 'Do MMM [at] HH:mm:ss').date();
  }
  return 'NA';
}
function getTotalTime(duration) {
  if (duration) {
    return `${moment.duration(duration).hours()}:${moment.duration(duration).minutes()}:${moment.duration(duration).seconds()}`;
  }
  return 'NA';
}

class TripList extends React.Component {
  static propTypes = {
    trip: React.PropTypes.object,
    tripId: React.PropTypes.object,
    getDay: React.PropTypes.func,
  };


  constructor(props) {
    super(props);

    this.getDay = this.getDay.bind(this);
  }

  getDay(day) {
    if (day) {
      return moment(day, 'YYYY-MM-DD HH:mm:ss').format('dddd');
    }
    return 'NA';
  }

  render() {
    return (
      this.props.trip
      ?
        <div className={`row ${s.tripBody}`}>
          <div className="col-sm-3 text-center">
            <div className="media">
              <div className="media-left">
                <img src={logo} alt="flat Avatar" className="user-avatar" height="50px" />
              </div>
              <div className="media-body">
                <span className="media-heading">{this.props.trip.pickUpAddress} <span className="glyphicon glyphicon-arrow-right" /> {this.props.trip.destAddress}</span>
              </div>
            </div>
          </div>
          <div className="col-sm-1">
            <div className={s.calendar}>
              <div>zzzzzzzzzzzz</div>
              <div>{getDate(this.props.trip.requestTime)}</div>
              <div>{moment(this.props.trip.requestTime).format('MMM')}</div>
            </div>
          </div>
          <div className={`col-sm-2 text-center ${s.startTime}`}>
            {this.props.trip.bookingTime && this.props.trip.pickUpAddress ? <div><p className={s.location}>{this.props.trip.pickUpAddress}</p><p className="time">{moment(this.props.trip.bookingTime).format('hh:mm:ss')}</p></div> : <div className="mt-22"><b>Not Available</b></div> }
          </div>
          <div className={`col-sm-2 text-center ${s.totalTime}`}>
            {this.props.trip.travelTime ? <div><p className="glyphicon glyphicon-time" /><p>{getTotalTime(this.props.trip.travelTime)}</p></div> : <div className="mt-22"><b>Not Available</b></div> }
          </div>
          <div className={`col-sm-2 text-center ${s.endTime}`}>
            {this.props.trip.endTime && this.props.trip.destAddress ? <div><p className={s.location}>{this.props.trip.destAddress}</p><p className="time">{Utils.getDateTime(this.props.trip.endTime)}</p></div> : <div className="mt-22"><b>Not Available</b></div> }
          </div>
          <div className={`col-sm-2 text-center ${s.endTime}`}>
            <p className={s.location}>Status</p>
            <p>
              {
                this.props.trip._id
                ?
                  <label htmlFor="null" className="label label-success text-capitalize">
                    {
                      this.props.trip.tripStatus
                    }
                  </label>
                :
                  <label htmlFor="null" className="label label-danger">
                    {
                    this.props.trip.tripRequestStatus
                    }
                  </label>
              }
            </p>
          </div>
        </div>
      : null
    );
  }
}

export default TripList;
