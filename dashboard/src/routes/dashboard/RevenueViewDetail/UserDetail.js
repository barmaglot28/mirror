import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination } from 'react-bootstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserDetail.css';
import { fetchUserDetails, fetchTripDetailsPerDay } from '../../../store/domain/userDetails/action';
import TripList from './tripList';

class Users extends Component {
  static propTypes = {
    currentUser: React.PropTypes.object,
    fetchUserDetails: React.PropTypes.func,
    fetchTripDetailsPerDay: React.PropTypes.func,
    id: React.PropTypes.string,
    userDetails: React.PropTypes.object,
    userTrips: React.PropTypes.array,
    meta: React.PropTypes.object,
    tripLoading: React.PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
    };
    this.handlePagination = this.handlePagination.bind(this);
  }

  componentDidMount() {
    this.props.fetchUserDetails(this.props.id);
    this.props.fetchTripDetailsPerDay(this.props.id, new Date().toISOString().slice(0, 10));
  }


  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchTripDetailsPerDay(this.props.id, new Date().toISOString().slice(0, 10));
  }

  render() {
    return (
      <div className={`animate ${s.user} ${s.userDetails}`}>
        <div className="container-fluid">
          <div className="panel panel-default">
            <div className="panel-body">
              <div className="media no-margin">
                <div className="media-left">
                  <a href>
                    <img
                      width="72px"
                      className="media-object"
                      src="https://pbs.twimg.com/profile_images/685550474880143360/HjrqdoE4.png"
                      alt=""
                    />
                  </a>
                </div>
                <div className="media-body">
                  <div className="row">
                    <div className="col-lg-3 col-md-6">
                      <blockquote>
                        <p>
                          { this.props.userDetails ? this.props.userDetails.fname : 'NA' }
                          { this.props.userDetails ? this.props.userDetails.lname : 'NA' }
                        </p>
                        <small><cite title="Source Title">
                          { this.props.userDetails ? this.props.userDetails.state : 'NA' } <i className="glyphicon glyphicon-map-marker" /></cite>
                        </small>
                      </blockquote>
                    </div>
                    <div className="col-lg-3 col-md-6">
                      <p><i className="glyphicon glyphicon-envelope" /> { this.props.userDetails ? this.props.userDetails.email : 'NA'} </p>
                      <p><i className="glyphicon glyphicon-phone-alt" /> { this.props.userDetails ? this.props.userDetails.phoneNo : 'NA'} </p>
                      <p><i className="glyphicon glyphicon-calendar" /> {new Date(Date.parse(this.props.userDetails ? this.props.userDetails.dob : 'NA')).toDateString()}</p>
                    </div>
                  </div>
                </div>
              </div>
              <h4><b>Today Trip Details:</b></h4>
              {
                this.props.tripLoading
                ?
                  <div className="loading-wrap" style={{ minHeight: 500 }}>
                    <div className="loading">
                      <div id="spinner">
                        <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                          <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                        </svg>
                      </div>
                    </div>
                  </div>
                :
                (
                  this.props.userTrips != undefined && this.props.userTrips.length
                  ?
                    <div>
                      {
                      this.props.userTrips.map(
                        (trips, index) => <TripList key={index} trip={trips} />,
                      )
                    }
                    </div>
                  : <div> No Trips Details Avaliable </div>
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.domainReducer.currentUser.loading,
    tripLoading: state.domainReducer.currentUser.tripLoading,
    userDetails: state.domainReducer.currentUser.currentUserDetails.data,
    userTrips: state.domainReducer.currentUser.currentUserTrips.data,
    meta: state.domainReducer.currentUser.currentUserTrips.meta,
  };
}

function bindActions(dispatch) {
  return {
    fetchUserDetails: (id) => dispatch(fetchUserDetails(id)),
    fetchTripDetailsPerDay: (id, time) => dispatch(fetchTripDetailsPerDay(id, time)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Users));
