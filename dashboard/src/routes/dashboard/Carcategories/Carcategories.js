import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Button } from 'react-bootstrap';
import { ResponsiveContainer, Pie, PieChart, Cell, Tooltip, Legend } from 'recharts';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Carcategories.css';
import CarcategoriesAction from '../../../store/domain/carcategories/action';
import CarcategoriesUpdateAction from '../../../store/domain/updateCarcategories/action';
import History from '../../../core/history';
import UpdateCarcategoriesDetails from './updateCarcategories';

class Carcategories extends Component {

  static propTypes = {
    carcategoriesList: React.PropTypes.any,
    isLoggedIn: React.PropTypes.bool,
    loading: React.PropTypes.bool,
    fetchCarcategories: React.PropTypes.func,
    meta: React.PropTypes.object,
    chartLoading: React.PropTypes.bool,
    chartData: React.PropTypes.object,
    removeCarcategoriesErrorObj: React.PropTypes.object,
    removeCarcategoriesSuccess: React.PropTypes.object,
    failedCarcategoriesListApi: React.PropTypes.bool,
    failedCarcategoriesChartApi: React.PropTypes.bool,
    openCarcategoriesDetailModal: React.PropTypes.func,
    removeCarcategories: React.PropTypes.func,
  };

  static goToCarcategoriesDetail(id) {
    History.push(`/dashboard/carcategories/${id}`);
  }

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      activePage: 1,
    };
    this.setLoading = this.setLoading.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
    this.UpdateCarcategoriesData = this.UpdateCarcategoriesData.bind(this);
    this.DeleteCarcategoriesData = this.DeleteCarcategoriesData.bind(this);
  }

  componentWillMount() {
    this.props.fetchCarcategories(this.state.activePage);
  }

  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
    if(nextProps.removeCarcategoriesSuccess.success) {
      this.setState({ isLoading: false });
      this.setState({ loading: false });
      this.props.fetchCarcategories(this.state.activePage);
    }
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }

  UpdateCarcategoriesData(carcategoriesObj) {
    this.props.openCarcategoriesDetailModal(carcategoriesObj);
  }

  DeleteCarcategoriesData(carcategoriesObj) {
    this.props.removeCarcategories(carcategoriesObj);
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchCarcategories(eventkey);
  }

  render() {
    return (
      <div className={`animate ${s.carcategories}`}>
        <div>
          {
            this.props.failedCarcategoriesListApi
              ?
              <div className="alert alert-danger">
                {/* <strong>Api Error: Api server is not working properly.</strong> */}
                <strong>{this.props.removeCarcategoriesErrorObj.message}</strong>
              </div>
              :
              null

          }
          {
            this.props.removeCarcategoriesSuccess.success
              ?
              <div className="alert alert-danger">
                {/* <strong>Api Error: Api server is not working properly.</strong> */}
                <strong>{this.props.removeCarcategoriesSuccess.message}</strong>
              </div>
              :
              null

          }
          {
          this.state.isLoading
          ?
            <div className="loading-wrap" style={{ minHeight: 500 }}>
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
          :
            <div className="row">
              <div className="col-sm-12">
                <div className="panel panel-body">
                  <div className="table-responsive">
                    <table className="table">
                      <tbody>
                        <tr>
                          <th>S.No</th>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Type</th>
                          <th>Price Per Kilometer</th>
                          <th>Price Per Minute</th>
                          <th>Base Fare</th>
                          <th>Minimum Fare</th>
                          <th>Commission To Cabdo</th>
                          <th>Capacity</th>
                          <th style={{ minWidth: 140 }}>Action</th>
                        </tr>
                        {
                          this.props.failedCarcategoriesListApi
                        ?
                          <tr>
                              <td colSpan="10">
                                  <div className="alert alert-danger">
                                    <strong>Api Error: Api server is not working properly.</strong>
                                </div>
                              </td>
                          </tr>
                        : null
                        }
                        {
                          this.props.carcategoriesList.data
                        ?
                          this.props.carcategoriesList.data.map((item, index) => (
                            <tr key={index}>
                              <td>
                              {
                                (
                                    (this.state.activePage - 1) * this.props.meta.limit
                                ) + (index + 1)
                              }
                              </td>
                              <td>{item._id}</td>
                              <td>{item.name}</td>
                              <td>{item.type}</td>
                              <td>{item.pricePerKilometer}</td>
                              <td>{item.pricePerMinute}</td>
                              <td>{item.baseFare}</td>
                              <td>{item.minimumFare}</td>
                              <td>{item.commissionToCabdo}</td>
                              <td>{item.capacity}</td>
                              <td style={{ minWidth: 140 }}>
                                <button
                                    className="btn btn-warning"
                                    onClick={() => Carcategories.goToCarcategoriesDetail(item._id)}
                                >
                                  <i className="glyphicon glyphicon-eye-open" />
                                </button>
                                <button className="btn btn-info" onClick={() => { this.UpdateCarcategoriesData(item); }}>
                                  <i className="glyphicon glyphicon-edit" />
                                </button>
                                <button className="btn btn-danger" onClick={() => { this.DeleteCarcategoriesData(item); }}>
                                  <i className="glyphicon glyphicon-remove" />
                                </button>
                              </td>
                            </tr>)) : null
                          }
                      </tbody>
                    </table>
                    <UpdateCarcategoriesDetails />
                    <Pagination
                      bsSize="medium"
                      prev
                      next
                      first
                      last
                      ellipsis
                      boundaryLinks
                      maxButtons={5}
                      items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                      activePage={this.state.activePage}
                      onSelect={this.handlePagination}
                    />
                    <hr />
                    <div className="clearfix">
                      <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} onClick={() => { window.location.href = "/dashboard/createCarcategories"; }}> Create </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        }
        </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    carcategoriesList: state.domainReducer.carcategories.carcategoriesList,
    isLoggedIn: state.domainReducer.auth.isLoggedIn,
    loading: state.domainReducer.carcategories.loading,
    meta: state.domainReducer.carcategories.meta,
    failedCarcategoriesListApi: state.domainReducer.carcategories.failedCarcategoriesListApi,
    removeCarcategoriesErrorObj: state.domainReducer.carcategories.removeCarcategoriesErrorObj,
    removeCarcategoriesSuccess: state.domainReducer.carcategories.removeCarcategoriesSuccess,
  };
}

function bindActions(dispatch) {
    return {
        fetchCarcategories: (pageNo) => dispatch(CarcategoriesAction.fetchCarcategories(pageNo)),
        openCarcategoriesDetailModal: (carcategoriesObj) => dispatch(CarcategoriesUpdateAction.openCarcategoriesDetailModal(carcategoriesObj)),
        removeCarcategories: (carcategoriesObj) => dispatch(CarcategoriesAction.removeCarcategories(carcategoriesObj)),
    };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Carcategories));
