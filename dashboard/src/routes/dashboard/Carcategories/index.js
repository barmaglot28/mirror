import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/carcategories',
  async action() {
    const Carcategories = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Carcategories').default);
      }, 'carcategories');
    });
    return {
      title: 'Carcategories',
      component: <Layout><Carcategories key="carcategories" /></Layout>,
    };
  },
};