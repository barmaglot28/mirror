import React, { Component } from 'react';
import { Row, Col, Form, FormControl, FormGroup, ControlLabel, Button, Checkbox, Radio } from 'react-bootstrap';
import Modal, { Header as ModalHeader, Title as ModalTitle, Body as ModalBody } from 'react-bootstrap/lib/Modal';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './Carcategories.css';
import carcategoriesUpdateAction from '../../../store/domain/updateCarcategories/action';

class UpdateCarcategories extends Component {
  static propTypes = {
    showCarcategoriesupdateModal: React.PropTypes.bool,
    closeCarcategoriesDetailModal: React.PropTypes.func,
    updateCarcategoriesObject: React.PropTypes.func,
    carcategoriesObjectToUpdate: React.PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.state = {
      carcategoriesObj: {
        name: 'Na',
        isMetered: 'Na',
      },
    };
    this.hideCarcategoriesupdateModal = this.hideCarcategoriesupdateModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const carcategoriesObjReceived = Object.assign({}, nextProps.carcategoriesObjectToUpdate);
    if (carcategoriesObjReceived._id) { // eslint-disable-line
      this.setState({ carcategoriesObj: carcategoriesObjReceived });
    }
  }

  hideCarcategoriesupdateModal() {
    this.props.closeCarcategoriesDetailModal();
  }

  handleName(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.name = e.target.value;
    this.setState({ carcategoriesObj });
  }
  handlePricePerKilometer(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.pricePerKilometer = e.target.value;
    this.setState({ carcategoriesObj });
  }
  handlePricePerMinute(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.pricePerMinute = e.target.value;
    this.setState({ carcategoriesObj });
  }
  handleBaseFare(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.baseFare = e.target.value;
    this.setState({ carcategoriesObj });
  }
  handleMinimumFare(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.minimumFare = e.target.value;
    this.setState({ carcategoriesObj });
  }
  handleCommissionToCabdo(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.commissionToCabdo = e.target.value;
    this.setState({ carcategoriesObj });
  }
  handleCapacity(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.capacity = e.target.value;
    this.setState({ carcategoriesObj });
  }
  handleIsMetered(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.isMetered = e.target.checked;
    var element = document.getElementsByClassName('is-metered');
    for(var iel = 0; iel < element.length; iel++) {
      if(element[iel].children[1].disabled){
        element[iel].children[1].disabled = false;
      } else {
        element[iel].children[1].value = "";
        carcategoriesObj.pricePerKilometer = " ";
        carcategoriesObj.pricePerMinute = " ";
        carcategoriesObj.baseFare = " ";
        carcategoriesObj.minimumFare = " ";
        element[iel].children[1].disabled = true;
      }
    }
    this.setState({ carcategoriesObj });
  }
  handleType(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.type = e.target.value;
    console.log(e.target.value)
    this.setState({ carcategoriesObj });
  }
  handleSubmit(e) {
    e.preventDefault();
    // console.log('inside handleSubmit');
    // const tripObj = checkNullValue(this.state.tripObj);
    const carcategoriesObj = this.state.carcategoriesObj;
    this.props.updateCarcategoriesObject(carcategoriesObj);
  }

  render() {
    return (
      <Modal show={this.props.showCarcategoriesupdateModal} id="updateTrip" className={s.updateCarcategoriesDetail} onHide={() => { this.hideCarcategoriesupdateModal(); }}>
        <ModalHeader closeButton>
          <ModalTitle>
            Update Carcategories Details
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col>
              <Form horizontal onSubmit={(e) => { this.handleSubmit(e); }}>
                <Row>
                  <Col md={5}>
                    <ControlLabel>
                      Name
                    </ControlLabel>
                    <FormControl type="text" placeholder="Name" value={this.state.carcategoriesObj.name} onChange={(e) => { this.handleName(e); }} />
                  </Col>
                  <Col md={5} mdOffset={1} className="is-metered">
                    <ControlLabel>
                      Price Per Kilometer
                    </ControlLabel>
                    <FormControl type="text" placeholder="Price Per Kilometer" disabled={this.state.carcategoriesObj.isMetered} value={this.state.carcategoriesObj.pricePerKilometer} onChange={(e) => { this.handlePricePerKilometer(e); }} />
                  </Col>
                </Row>
                <Row>
                  <Col md={5} className="is-metered">
                    <ControlLabel>
                      Price Per Minute
                    </ControlLabel>
                    <FormControl type="text" placeholder="Price Per Minute" disabled={this.state.carcategoriesObj.isMetered} value={this.state.carcategoriesObj.pricePerMinute} onChange={(e) => { this.handlePricePerMinute(e); }} />
                  </Col>
                  <Col md={5} mdOffset={1} className="is-metered">
                    <ControlLabel>
                      Base Fare
                    </ControlLabel>
                    <FormControl type="text" placeholder="Base Fare" disabled={this.state.carcategoriesObj.isMetered} value={this.state.carcategoriesObj.baseFare} onChange={(e) => { this.handleBaseFare(e); }} />
                  </Col>
                </Row>
                <Row>
                  <Col md={5} className="is-metered">
                    <ControlLabel>
                      Minimum Fare
                    </ControlLabel>
                    <FormControl type="text" placeholder="Minimum Fare" disabled={this.state.carcategoriesObj.isMetered} value={this.state.carcategoriesObj.minimumFare} onChange={(e) => { this.handleMinimumFare(e); }} />
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <ControlLabel>
                      Commission To Cabdo
                    </ControlLabel>
                    <FormControl type="text" placeholder="Commission To Cabdo" value={this.state.carcategoriesObj.commissionToCabdo} onChange={(e) => { this.handleCommissionToCabdo(e); }} />
                  </Col>
                </Row>
                <Row>
                  <Col md={5}>
                    <ControlLabel>
                      Capacity
                    </ControlLabel>
                    <FormControl type="text" placeholder="Capacity" value={this.state.carcategoriesObj.capacity} onChange={(e) => { this.handleCapacity(e); }} />
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <ControlLabel>
                      Type
                    </ControlLabel>
                      <FormGroup>
                      <Radio name="radioGroup" value="classic" inline onChange={(e) => this.handleType(e)}>
                       Classic
                      </Radio>
                      {' '}
                      <Radio name="radioGroup" value="luxurious" inline onChange={(e) => this.handleType(e)}>
                       Luxurious
                      </Radio>
                      {' '}
                      <Radio name="radioGroup" value="metered" inline onChange={(e) => this.handleType(e)}>
                       Metered taxi
                      </Radio>
                    </FormGroup>

                  </Col>
                </Row>
                <hr />
                <div className="pull-right clearfix">
                  <Button className="btn-rounded" onClick={() => { this.hideCarcategoriesupdateModal(); }}> Cancel </Button>&nbsp;&nbsp;
                  <Button bsStyle="primary" className="btn-rounded" type="submit"> Save </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    );
  }
}


function mapStateToProps(state) {
  return {
    showCarcategoriesupdateModal: state.domainReducer.updateCarcategories.showCarcategoriesupdateModal,
    carcategoriesObjectToUpdate: state.domainReducer.updateCarcategories.carcategoriesObjectToUpdate,
  };
}
function bindActions(dispatch) {
  return {
    openCarcategoriesDetailModal: () => dispatch(carcategoriesUpdateAction.openCarcategoriesDetailModal()),
    closeCarcategoriesDetailModal: () => dispatch(carcategoriesUpdateAction.closeCarcategoriesDetailModal()),
    updateCarcategoriesObject: (carcategoriesObj) => dispatch(carcategoriesUpdateAction.updateCarcategoriesObject(carcategoriesObj)),
  };
}
export default connect(mapStateToProps, bindActions)(withStyles(s)(UpdateCarcategories));
