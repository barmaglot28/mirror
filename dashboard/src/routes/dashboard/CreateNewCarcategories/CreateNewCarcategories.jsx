import React, { Component } from 'react';
import { Panel, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Checkbox, Radio } from 'react-bootstrap';
import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './CreateNewCarcategories.css';
import CarcategoriesAction from '../../../store/domain/carcategories/action';
import Spinner from '../../../components/Spinner';
import History from '../../../core/history';


class CreateCarcategories extends Component {
  static propTypes = {
    newCarcategoriesObj: React.PropTypes.object,
    createCarcategoriesErrorObj: React.PropTypes.object,
    failedCreateCarcategoriesApi: React.PropTypes.bool,
    newCarcategoriesLoading: React.PropTypes.bool,
    createCarcategories: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      carcategoriesObj: {
        name: 'NA',
        type: undefined,
        isMetered: false,
        pricePerKilometer: 'NA',
        pricePerMinute: 'NA',
        baseFare: 'NA',
        minimumFare: 'NA',
        commissionToCabdo: 'NA',
        capacity: 'NA',
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newCarcategoriesObj && nextProps.newCarcategoriesObj.data && !nextProps.newCarcategoriesLoading) {
      if (!nextProps.failedCreateCarcategoriesApi) {
        History.push('/dashboard/carcategories');
      }
    }
  }

  handleName(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.name = e.target.value;
    this.setState({ carcategoriesObj });
  }

  handlePricePerKilometer(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.pricePerKilometer = e.target.value;
    this.setState({ carcategoriesObj });
  }

  handlePricePerMinute(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.pricePerMinute = e.target.value;
    this.setState({ carcategoriesObj });
  }

  handleBaseFare(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.baseFare = e.target.value;
    this.setState({ carcategoriesObj });
  }

  handleMinimumFare(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.minimumFare = e.target.value;
    this.setState({ carcategoriesObj });
  }

  handleCommissionToCabdo(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.commissionToCabdo = e.target.value;
    this.setState({ carcategoriesObj });
  }

  handleCapacity(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.capacity = e.target.value;
    this.setState({ carcategoriesObj });
  }

  handleType(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.type = e.target.value;
    console.log(e.target.value)
    this.setState({ carcategoriesObj });
  }

  handleIsMetered(e) {
    const carcategoriesObj = this.state.carcategoriesObj;
    carcategoriesObj.isMetered = !carcategoriesObj.isMetered;
    this.setState({ carcategoriesObj });
    var element = document.getElementsByClassName('is-metered');
    for (var iel = 0; iel < element.length; iel++) {
      if ((' ' + element[iel].className + ' ').indexOf('hidden') > -1) {
        element[iel].className = element[iel].className.replace('hidden', '');
      } else {
        element[iel].className += " hidden";
      }
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('inside submit form with event obj', e);
    const carcategoriesObj = this.state.carcategoriesObj;
    this.props.createCarcategories(carcategoriesObj);
  }

  handleReset(e) {
    var element = document.getElementsByClassName('is-metered');
    for (var iel = 0; iel < element.length; iel++) {
      if ((' ' + element[iel].className + ' ').indexOf('hidden') > -1) {
        element[iel].className = element[iel].className.replace('hidden', '');
      }
    }
  }

  render() {
    return (
        <div className="animate">

          <Row key="newCarcategories">
            <Col md={10}>
              <Panel header="Create New Car Categories" bsStyle="primary">
                {
                  this.props.failedCreateCarcategoriesApi
                      ?
                      <div className="alert alert-danger">
                        {/* <strong>Api Error: Api server is not working properly.</strong> */}
                        <strong>{this.props.createCarcategoriesErrorObj.message}</strong>
                      </div>
                      :
                      null

                }
                {
                  this.props.newCarcategoriesLoading
                      ?
                      <Spinner componentsId="createNewCarcategories"/>
                      :
                      <Form horizontal onReset={(e) => {
                        this.handleReset(e);
                      }} onSubmit={(e) => {
                        this.handleSubmit(e);
                      }} id="createCarcategories">
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Name
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-folder-close"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Name" onChange={(e) => {
                                  this.handleName(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1} className="is-metered">
                            <FormGroup>
                              <ControlLabel>
                                Price Per Kilometer
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-eur"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Price Per Kilometer" onChange={(e) => {
                                  this.handlePricePerKilometer(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} className="is-metered">
                            <FormGroup>
                              <ControlLabel>
                                Price Per Minute
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-eur"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Price Per Minute" onChange={(e) => {
                                  this.handlePricePerMinute(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1} className="is-metered">
                            <FormGroup>
                              <ControlLabel>
                                Base Fare
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-eur"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Base Fare" onChange={(e) => {
                                  this.handleBaseFare(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} className="is-metered">
                            <FormGroup>
                              <ControlLabel>
                                Minimum Fare
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-eur"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Minimum Fare" onChange={(e) => {
                                  this.handleMinimumFare(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Commission To Cabdo
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-info-sign"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Commission To Cabdo" onChange={(e) => {
                                  this.handleCommissionToCabdo(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Capacity
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-info-sign"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Capacity" onChange={(e) => {
                                  this.handleCapacity(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <Radio name="radioGroup" value="classic" inline onChange={(e) => this.handleType(e)}>
                               Classic
                              </Radio>
                              {' '}
                              <Radio name="radioGroup" value="luxurious" inline onChange={(e) => this.handleType(e)}>
                               Luxurious
                              </Radio>
                              {' '}
                              <Radio name="radioGroup" value="metered" inline onChange={(e) => this.handleType(e)}>
                               Metered taxi
                              </Radio>
                           </FormGroup>
                          </Col>
                        </Row>
                        <hr />
                        <div className="pull-right clearfix">
                          <Button className={`${s.formButton} btn-rounded`} type="reset"> Reset </Button>&nbsp;&nbsp;
                          <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} type="submit">
                            Create </Button>
                        </div>
                      </Form>
                }
              </Panel>
            </Col>
          </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newCarcategoriesLoading: state.domainReducer.carcategories.newCarcategoriesLoading,
    failedCreateCarcategoriesApi: state.domainReducer.carcategories.failedCreateCarcategoriesApi,
    createCarcategoriesErrorObj: state.domainReducer.carcategories.createCarcategoriesErrorObj,
    newCarcategoriesObj: state.domainReducer.carcategories.newCarcategoriesObj,
  };
}

function bindActions(dispatch) {
  return {
    createCarcategories: (carcategoriesObj) => dispatch(CarcategoriesAction.createNewCarcategories(carcategoriesObj)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(CreateCarcategories));
