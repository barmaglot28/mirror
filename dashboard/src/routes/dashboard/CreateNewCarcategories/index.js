import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/dashboard/createCarcategories',

  async action() {
    const CreateCarcategories = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CreateNewCarcategories').default);
      }, 'createCarcategories');
    });

    return {
      title: 'Create New Carcategories',
      component: <Layout><CreateCarcategories key="createNewCarcategories" /></Layout>,
    };
  },
};