import React from 'react';
import Layout from '../../../../components/Layout';

export default {
  path: '/driver',
  async action() {
    const Driver = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Driver').default);
      }, 'driver');
    });
    return {
      title: 'Driver',
      component: <Layout><Driver key="driver" /></Layout>,
    };
  },
};
