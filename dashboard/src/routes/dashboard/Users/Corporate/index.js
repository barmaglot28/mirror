import React from 'react';
import Layout from '../../../../components/Layout';

export default {
  path: '/corporate',
  async action() {
    const Corporate = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Corporate').default);
      }, 'corporate');
    });
    return {
      title: 'Corporate',
      component: <Layout><Corporate key="corporate" /></Layout>,
    };
  },
};
