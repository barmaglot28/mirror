import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Button, Modal, FormControl } from 'react-bootstrap';
import { ResponsiveContainer, Pie, PieChart, Cell, Tooltip, Legend } from 'recharts';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Corporate.css';
import UserAction from '../../../../store/domain/users/action';
import UserUpdateAction from '../../../../store/domain/updateUser/action';
import History from '../../../../core/history';
import UpdateUserDetails from '../updateUser';
import { CORPORATE_TYPE } from '../../../../store/constants';

class Corporate extends Component {

  static propTypes = {
    loading: React.PropTypes.bool,
    meta: React.PropTypes.object,
    userList: React.PropTypes.array,
    failedUserListApi: React.PropTypes.bool,
    errorMsg: React.PropTypes.string,

    // fetchChartData: React.PropTypes.func,

    // chartLoading: React.PropTypes.bool,
    // chartData: React.PropTypes.object,

    // failedUserChartApi: React.PropTypes.bool,
    openUserDetailModal: React.PropTypes.func,
    removeUser: React.PropTypes.func,
    removeUserErrorObj: React.PropTypes.object,
    removeUserSuccess: React.PropTypes.object,

    fetchCorporate: React.PropTypes.func,
    submitSmsToAll: React.PropTypes.func,
    submitEmailToAll: React.PropTypes.func,

    sendingSms: React.PropTypes.bool,
    sendSmsSuccess: React.PropTypes.bool,
    sendingEmail: React.PropTypes.bool,
    sendEmailSuccess: React.PropTypes.bool,

    changeFilterText: React.PropTypes.func,
    filterUsers: React.PropTypes.func,

    filteredText: React.PropTypes.string,
  };

  static goToUserDetail(id) {
    History.push(`/dashboard/user/${id}`);
  }

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      activePage: 1,

      showModalSendSms: false,
      showModalSendEmail: false,
      messageText: '',
      needToSubmit: true
    };
    this.setLoading = this.setLoading.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
    // this.getChartData = this.getChartData.bind(this);
    this.UpdateUserData = this.UpdateUserData.bind(this);
    this.onToggleModalSms = this.onToggleModalSms.bind(this);
    this.onSubmitSmsToAll = this.onSubmitSmsToAll.bind(this);

    this.onToggleModalEmail = this.onToggleModalEmail.bind(this);
    this.onSubmitEmailToAll = this.onSubmitEmailToAll.bind(this);

    this.onFilterUsers = this.onFilterUsers.bind(this);
    this.callback = this.callback.bind(this);
  }

  componentWillMount() {
    this.props.fetchCorporate(this.state.activePage);
    // this.props.fetchChartData();
  }

  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
    if(nextProps.removeUserSuccess.success) {
      this.setState({ isLoading: false });
      this.setState({ loading: false });
      this.props.fetchCorporate(this.state.activePage);
    }
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }

  // getChartData() {
  //   const data = [];
  //   if (this.props.chartData && this.props.chartData.success) {
  //     const chartArray = this.props.chartData.data;
  //     data.push({ name: 'rider', value: chartArray[0].rider });
  //     data.push({ name: 'driver', value: chartArray[0].driver });
  //   }
  //   return data;
  // }

  UpdateUserData(userObj) {
    this.props.openUserDetailModal(userObj);
  }

  onDeleteUserData(userObj) {
    this.props.removeUser(userObj);
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    if (this.props.filteredText) {
      this.props.filterUsers(eventkey);
    } else {
      this.props.fetchCorporate(eventkey);
    }
  }

  onToggleModalSms() {
    this.setState({
      showModalSendSms: !this.state.showModalSendSms,
      messageText: ''
    })
  }

  onSubmitSmsToAll() {
    if (this.state.needToSubmit) {
      this.props.submitSmsToAll(this.state.messageText);
      this.setState({
        needToSubmit: false
      })
    } else {
      this.onToggleModalSms();
    }
  }

  onToggleModalEmail() {
    console.log('this.state.needToSubmit', this.state.needToSubmit);
    this.setState({
      showModalSendEmail: !this.state.showModalSendEmail,
      messageText: '',
      needToSubmit: this.state.showModalSendEmail ? this.state.needToSubmit : true
    });
  }

  onSubmitEmailToAll() {
    if (this.state.needToSubmit) {
      this.props.submitEmailToAll(this.state.messageText);
      this.setState({
        needToSubmit: false
      });
    } else {
      this.onToggleModalEmail();
    }
  }

  onFilterUsers(e) {
    this.props.changeFilterText(e.target.value);
    clearTimeout(this.timerId);
    this.timerId = setTimeout(this.callback, 2000);
  }

  callback() {
    this.setState({
      activePage: 1
    });
    clearTimeout(this.timerId);
    this.props.filterUsers(this.state.activePage);
  }

  render() {
    const showSystemMessage = !this.state.needToSubmit;
    let systemTextBody = '';
    if (showSystemMessage) {
      if (this.props.sendingSms) {
        systemTextBody = 'processing...';
      }
      if (this.props.sendSmsSuccess) {
        systemTextBody = 'Your message sent successful!';
      }
      if (!this.props.sendSmsSuccess) {
        systemTextBody = 'something went wrong. Try again later...';
      }
      if (this.state.showModalSendEmail) {
        systemTextBody = this.props.sendEmailSuccess ? 'Your message was sent successfully!' : 'Something went wrong. Try again later...';
      }
    }

    const buttonText = this.state.needToSubmit ? `Send ${this.state.showModalSendEmail ? 'email' : 'sms'}` : 'Ok';
    return (
      <div className={`animate ${s.user}`}>
        <div>
          {
            this.props.failedUserListApi
              ?
              <div className="alert alert-danger">
                {/* <strong>Api Error: Api server is not working properly.</strong> */}
                <strong>{this.props.removeUserErrorObj.message}</strong>
              </div>
              :
              null

          }
          {
            this.props.removeUserSuccess.success
              ?
              <div className="alert alert-danger">
                {/* <strong>Api Error: Api server is not working properly.</strong> */}
                <strong>{this.props.removeUserSuccess.message}</strong>
              </div>
              :
              null
          }
          {
          this.state.isLoading
          ?
            <div className="loading-wrap" style={{ minHeight: 500 }}>
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
          :
            <div className="row">
              <div className="col-sm-12">
                <div className="panel panel-body">
                  <div className="table-responsive">
                    <FormControl
                      type="text"
                      value={this.props.filteredText}
                      step={0.1}
                      placeholder="Enter driver name"
                      onChange={this.onFilterUsers}
                    />
                    <table className="table">
                      <tbody>
                        <tr>
                          <th>S.No</th>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>phoneNo</th>
                          <th>State</th>
                          <th>Usertype</th>
                          <th>User Rating</th>
                          <th>Current Trip Id</th>
                          <th>Current Trip State</th>
                          <th>Action</th>
                        </tr>
                        {
                          this.props.failedUserListApi
                          ?
                            <tr>
                              <td colSpan="10">
                                <div className="alert alert-danger">
                                  <strong>Api Error: Api server is not working properly.</strong>
                                </div>
                              </td>
                            </tr>
                          : null
                        }
                        {
                          this.props.userList.data
                          ?
                          this.props.userList.data.map((item, index) => (
                            <tr key={index}>
                              <td>
                                {
                                  (
                                    (this.state.activePage - 1) * this.props.meta.limit
                                  ) + (index + 1)
                                }
                              </td>
                              <td>{item._id}</td>
                              <td>{`${item.fname} ${item.lname}`}</td>
                              <td>{item.email}</td>
                              <td>{item.phoneNo}</td>
                              <td>{item.state}</td>
                              <td>{item.userType}</td>
                              <td>{item.userRating}</td>
                              <td>{item.currTripId}</td>
                              <td>{item.currTripState}</td>
                              <td style={{ minWidth: 100 }}>
                                <button
                                  className="btn btn-warning"
                                  onClick={() => Corporate.goToUserDetail(item._id)}
                                >
                                  <i className="glyphicon glyphicon-eye-open" />
                                </button>
                                <button className="btn btn-info" onClick={() => { this.UpdateUserData(item); }}>
                                  <i className="glyphicon glyphicon glyphicon-edit" />
                                </button>
                                <button className="btn btn-danger" onClick={() => { this.onDeleteUserData(item); }}>
                                  <i className="glyphicon glyphicon-remove" />
                                </button>
                              </td>
                            </tr>)) : null
                        }
                      </tbody>
                    </table>
                    <UpdateUserDetails />
                    <Pagination
                      bsSize="medium"
                      prev
                      next
                      first
                      last
                      ellipsis
                      boundaryLinks
                      maxButtons={5}
                      items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                      activePage={this.state.activePage}
                      onSelect={this.handlePagination}
                    />
                    <hr />
                    <div className="clearfix">
                      <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} onClick={() => { window.location.href = "/dashboard/createUser"; }}> Create </Button>
                      <Button bsStyle="primary" style={{ float: 'right' }} className={`${s.formButton} btn-rounded`} onClick={ this.onToggleModalSms }> Send sms to all users </Button>
                      <Button bsStyle="primary" style={{ float: 'right', marginRight: 10 }} className={`${s.formButton} btn-rounded`} onClick={ this.onToggleModalEmail }> Send email to all users </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        }
        </div>

        <Modal show={this.state.showModalSendSms || this.state.showModalSendEmail}
               onHide={ this.state.showModalSendEmail ? this.onToggleModalEmail : this.onToggleModalSms }>
          <Modal.Header closeButton>
            <Modal.Title>{ this.state.showModalSendEmail ? 'Email' : 'Sms' } to all users</Modal.Title>
          </Modal.Header>
          { showSystemMessage ?
            <Modal.Body>
              <p>{systemTextBody}</p>
            </Modal.Body>
            :
            <Modal.Body>
              <p>Enter {this.state.showModalSendEmail ? 'email' : 'sms'} that will be shown for all users</p>
              <FormControl
                value={this.state.messageText}
                placeholder="Enter message"
                onChange={(e) => { this.setState({ messageText: e.target.value }) }}
              />
            </Modal.Body>
          }
          <Modal.Footer>
            <Button onClick={this.state.showModalSendEmail ? this.onSubmitEmailToAll : this.onSubmitSmsToAll}
                    disabled={!this.state.messageText || this.props.sendingSms || this.props.sendingEmail}>{buttonText}</Button>
            <Button onClick={ this.state.showModalSendEmail ? this.onToggleModalEmail : this.onToggleModalSms }
                    disabled={this.props.sendingSms || this.props.sendingEmail}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    loading: state.domainReducer.users.loading,
    meta: state.domainReducer.users.meta,
    userList: state.domainReducer.users.userList,
    failedUserListApi: state.domainReducer.users.failedUserListApi,
    errorMsg: state.domainReducer.users.errorMsg,

    // chartData: state.domainReducer.users.chartData,
    // chartLoading: state.domainReducer.users.chartLoading,
    // failedUserChartApi: state.domainReducer.users.failedUserChartApi,

    removeUserErrorObj: state.domainReducer.users.removeUserErrorObj,
    removeUserSuccess: state.domainReducer.users.removeUserSuccess,

    sendingSms: state.domainReducer.users.sendingSms,
    sendSmsSuccess: state.domainReducer.users.sendSmsSuccess,
    sendingEmail: state.domainReducer.users.sendingEmail,
    sendEmailSuccess: state.domainReducer.users.sendEmailSuccess,

    filteredText: state.domainReducer.users.filteredText,
  };
}

function bindActions(dispatch) {
  return {
    fetchCorporate: (pageNo) => dispatch(UserAction.fetchCorporate(pageNo)),
    // fetchChartData: () => dispatch(UserAction.fetchChartData()),
    openUserDetailModal: (userObj) => dispatch(UserUpdateAction.openUserDetailModal(userObj)),
    removeUser: (userObj) => dispatch(UserAction.removeUser(userObj)),
    submitSmsToAll: (messageText) => dispatch(UserAction.submitSmsToAll(messageText)),
    submitEmailToAll: (messageText) => dispatch(UserAction.submitEmailToAll(messageText)),

    changeFilterText: (text) => dispatch(UserAction.changeFilterText(text)),
    filterUsers: (pageNo) => dispatch(UserAction.filterUsers(pageNo, CORPORATE_TYPE)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Corporate));
