import React, { Component } from 'react';
import { Row, Col, Form, FormControl, ControlLabel, Button } from 'react-bootstrap';
import Modal, { Header as ModalHeader, Title as ModalTitle, Body as ModalBody } from 'react-bootstrap/lib/Modal';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './Corporate/Corporate.css';
import userUpdateAction from '../../../store/domain/updateUser/action';

class UpdateUser extends Component {
  static propTypes = {
    showUserupdateModal: React.PropTypes.bool,
    closeUserDetailModal: React.PropTypes.func,
    updateUserObject: React.PropTypes.func,
    userObjectToUpdate: React.PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      userObj: {
        fname: '',
        lname: '',
        address: 'NA',
        city: 'NA',
        state: 'NA',
        country: 'NA',
        phoneNo: '',
        // gpsLoc: null,
        // fullAddress: ''
      },
    };
    this.hideUserupdateModal = this.hideUserupdateModal.bind(this);
  }

  clearAddress(userObj) {
    userObj.address = userObj.city = userObj.state = userObj.country = 'NA';
    userObj.gpsLoc = null;
    userObj.fullAddress = '';
  }
  componentWillReceiveProps(nextProps) {
    const userObjReceived = Object.assign({}, nextProps.userObjectToUpdate);
    if (userObjReceived._id) { // eslint-disable-line
      this.setState({ userObj: userObjReceived });
    }
  }

  hideUserupdateModal() {
    this.props.closeUserDetailModal();
  }

  handleFirstName(e) {
    const userObj = this.state.userObj;
    userObj.fname = e.target.value;
    this.setState({ userObj });
  }
  handleLastName(e) {
    const userObj = this.state.userObj;
    userObj.lname = e.target.value;
    this.setState({ userObj });
  }
  handleAddress(e) {
    const userObj = this.state.userObj;
    userObj.address = e.target.value;
    this.setState({ userObj });
  }
  handleCity(e) {
    const userObj = this.state.userObj;
    userObj.city = e.target.value;
    this.setState({ userObj });
  }
  handleState(e) {
    const userObj = this.state.userObj;
    userObj.state = e.target.value;
    this.setState({ userObj });
  }
  handleCountry(e) {
    const userObj = this.state.userObj;
    userObj.country = e.target.value;
    this.setState({ userObj });
  }
  handlePhoneNumber(e) {
    const userObj = this.state.userObj;
    userObj.phoneNo = e.target.value;
    this.setState({ userObj });
  }
  handleSubmit(e) {
    e.preventDefault();
    // console.log('inside handleSubmit');
    // const tripObj = checkNullValue(this.state.tripObj);
    const userObj = this.state.userObj;
    this.props.updateUserObject(userObj);
  }
  showModalCallback() {
    const autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('corporateLoc'),
      { types: ['geocode'] }
    );
    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      const userObj = this.state.userObj;

      console.log(place);

      this.clearAddress(userObj);
      const street = [];
      place.address_components.forEach((component) => {
        if (component.types.includes('country')) {
          userObj.country = component.long_name;
        } else if (component.types.includes('administrative_area_level_1')) {
          userObj.state = component.long_name;
        } else if (component.types.includes('locality')) {
          userObj.city = component.short_name;
        } else if (component.types.includes('route')) {
          street[0] = component.short_name;
        } else if (component.types.includes('street_number')) {
          street[1] = component.short_name;
        }
      });
      userObj.address = street.join(' ');
      userObj.gpsLoc = [place.geometry.location.lng(), place.geometry.location.lat()];
      userObj.fullAddress = place.formatted_address;

      this.setState({ userObj });
    });
  }
  render() {
    return (
      <Modal show={this.props.showUserupdateModal} id="updateTrip" className={s.updateUserDetail}
        onHide={() => { this.hideUserupdateModal(); }}
        onShow={() => { this.showModalCallback(); }}>
        <ModalHeader closeButton>
          <ModalTitle>
            Update User Details
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col>
              <Form horizontal onSubmit={(e) => { this.handleSubmit(e); }}>
                <Row>
                  <Col md={5}>
                    <ControlLabel>
                      First Name
                    </ControlLabel>
                    <FormControl type="text" placeholder="First Name" value={this.state.userObj.fname} onChange={(e) => { this.handleFirstName(e); }} />
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <ControlLabel>
                      Last Name
                    </ControlLabel>
                    <FormControl type="text" placeholder="Last Name" value={this.state.userObj.lname} onChange={(e) => { this.handleLastName(e); }} />
                  </Col>
                </Row>

                <Row>
                  <Col md={5}>
                    <ControlLabel>
                      Address
                    </ControlLabel>
                    <FormControl type="text" placeholder="Address" id="corporateLoc" value={this.state.userObj.address} onChange={(e) => { this.handleAddress(e); }} />
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <ControlLabel>
                      City
                    </ControlLabel>
                    <FormControl type="text" placeholder="City" value={this.state.userObj.city} onChange={(e) => { this.handleCity(e); }} />
                  </Col>
                </Row>

                <Row>
                  <Col md={5}>
                    <ControlLabel>
                      State
                    </ControlLabel>
                    <FormControl type="text" placeholder="State" value={this.state.userObj.state} onChange={(e) => { this.handleState(e); }} />
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <ControlLabel>
                      Country
                    </ControlLabel>
                    <FormControl type="text" placeholder="Country" value={this.state.userObj.country} onChange={(e) => { this.handleCountry(e); }} />
                  </Col>
                </Row>

                <Row>
                  <Col md={5}>
                    <ControlLabel>
                      Phone Number
                    </ControlLabel>
                    <FormControl type="text" placeholder="Phone No" value={this.state.userObj.phoneNo} onChange={(e) => { this.handlePhoneNumber(e); }} />
                  </Col>
                </Row>
                <hr />
                <div className="pull-right clearfix">
                  <Button className="btn-rounded" onClick={() => { this.hideUserupdateModal(); }}> Cancel </Button>&nbsp;&nbsp;
                  <Button bsStyle="primary" className="btn-rounded" type="submit"> Save </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    );
  }
}


function mapStateToProps(state) {
  return {
    showUserupdateModal: state.domainReducer.updateUser.showUserupdateModal,
    userObjectToUpdate: state.domainReducer.updateUser.userObjectToUpdate,
  };
}
function bindActions(dispatch) {
  return {
    openUserDetailModal: () => dispatch(userUpdateAction.openUserDetailModal()),
    closeUserDetailModal: () => dispatch(userUpdateAction.closeUserDetailModal()),
    updateUserObject: (userObj) => dispatch(userUpdateAction.updateUserObject(userObj)),
  };
}
export default connect(mapStateToProps, bindActions)(withStyles(s)(UpdateUser));
