import React from 'react';
import Layout from '../../../../components/Layout/Layout';

export default {
  path: '/rider',
  async action() {
    const Rider = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Rider').default);
      }, 'rider');
    });
    return {
      title: 'Rider',
      component: <Layout><Rider key="rider" /></Layout>,
    };
  },
};
