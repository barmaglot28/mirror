import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { ResponsiveContainer, Pie, PieChart, Cell, Tooltip, Legend } from 'recharts';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Dashboard.css';
import StatisticAction from '../../../store/domain/statistic/action';
import Link from '../../../components/Link/Link';
import _ from 'lodash';

const data = [{name: 'Group A', value: 500}, {name: 'Group B', value: 300},
  {name: 'Group C', value: 300}];
const COLORS = ['#00c0ef', '#00a65a', '#f39c12'];

const RADIAN = Math.PI / 180;

class Dashboard extends Component {

  static propTypes = {
    isNeedToSelect: React.PropTypes.bool,
    tripForSelect: React.PropTypes.number,

    generalStatistic: React.PropTypes.object,
    todayRideStatistic: React.PropTypes.object,
    totalCompletedRidesStatistic: React.PropTypes.object,
    driversStatistic: React.PropTypes.object,
    fetchGeneralStatistic: React.PropTypes.func,
    fetchTodayRideStatistic: React.PropTypes.func,
    fetchCompletedRideStatistic: React.PropTypes.func,
    fetchDriversStatistic: React.PropTypes.func,
    fetchRatingCommentsStatistic: React.PropTypes.func,
    fetchCancelledCommentsStatistic: React.PropTypes.func,
    fetchDriverWorklistStatistic: React.PropTypes.func,
    fetchTripsStatistic: React.PropTypes.func,
    fetchVehicleStatistic: React.PropTypes.func,
    tripSelected: React.PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      activePage: 1,
    };
  }

  componentDidMount() {

    this.props.fetchGeneralStatistic();
    this.props.fetchTodayRideStatistic();
    this.props.fetchCompletedRideStatistic();
    this.props.fetchDriversStatistic();
    this.props.fetchRatingCommentsStatistic();
    this.props.fetchCancelledCommentsStatistic();
    this.props.fetchDriverWorklistStatistic();
    this.props.fetchTripsStatistic();
    this.props.fetchVehicleStatistic();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isNeedToSelect && nextProps.tripForSelect !== null) {
      this.selectTripByColor(nextProps.tripForSelect);
      this.props.tripSelected();
    }
  }

  selectTripByColor(tripIndex) {
    let element = document.getElementById(`trip-${tripIndex}`);
    element.style.backgroundColor = "#97CE68";
    let timer = setTimeout(() => {
      element.style.backgroundColor = "transparent";
      clearTimeout(timer);
    }, 1000);
  }

  render() {
    // trips less than one hour first else sort by bookingTime
    const nowPlusHour = new Date(new Date().setHours(new Date().getHours() + 1));
    let less1hArray = [];
    let otherArray = [];
    for (let i = 0; i < this.props.tripsStatistic.length; i++) {
      let item = this.props.tripsStatistic[i];
      if (item.preBookedTime && new Date(item.preBookedTime) < nowPlusHour && new Date(item.preBookedTime) > new Date()) {
        less1hArray.push(item);
      } else {
        otherArray.push(item);
      }
    }
    less1hArray.sort((a, b) => new Date(a.preBookedTime) - new Date(b.preBookedTime));
    otherArray.sort((a, b) => new Date(b.bookingTime) - new Date(a.bookingTime));
    const allTrips = less1hArray.concat(otherArray);

    // sort offline first DO-Y and online DO-Y
    let allDrivers = _.sortBy(this.props.driverWorklistStatistic, (item) => (!item.carDetails.licensePlateNumber.startsWith('DO-Y')), (item) => (item.availability === false));
    return (
      <div className={`animate ${s.dashboardWrap}`}>
        {
          this.state.isLoading ?
            <div className="loading-wrap" style={{minHeight: 500}}>
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33"
                            r="30"/>
                  </svg>
                </div>
              </div>
            </div>
            :
            <div>
              <div className="row">
                <div className="col-sm-12">
                  <h1>Admin Dashboard</h1>
                </div>
              </div>
              <div className="row">
                <div className="col-sm-6">
                  <div className="panel panel-primary bg-gray-light">
                    <div className="panel-heading">
                      <div className="panel-title-box">
                        <i className="fa fa-bar-chart"/> Site Statistics
                      </div>
                    </div>
                    <div className="row padding1520">
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-aqua">
                          <span className={s.infoBoxIcon}><i className="fa fa-users"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Riders</span>
                            <span className="info-box-number">{this.props.generalStatistic.riders}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-green">
                          <span className={s.infoBoxIcon}><i className="fa fa-money"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Total Earnings</span>
                            <span className="info-box-number">{this.props.generalStatistic.totalEarnings} €</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-yellow">
                          <span className={s.infoBoxIcon}><i className="fa fa-check-circle-o"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Accepted rides</span>
                            <span className="info-box-number">{this.props.generalStatistic.acceptedRides}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-red">
                          <span className={s.infoBoxIcon}><i className="fa fa-times-circle-o"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Not Accepted rides</span>
                            <span className="info-box-number">{this.props.generalStatistic.notAcceptedRides}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-6">
                  <div className="panel panel-primary bg-gray-light">
                    <div className="panel-heading">
                      <div className="panel-title-box">
                        <i className="fa fa-area-chart"/> Today Ride Statistics
                      </div>
                    </div>
                    <div className="row padding1520">
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-aqua">
                          <span className={s.infoBoxIcon}><i className="fa fa-cubes"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Total ride requests</span>
                            <span className="info-box-number">{this.props.todayRideStatistic.total}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-yellow">
                          <span className={s.infoBoxIcon}><i className="fa fa-clone"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">On going rides</span>
                            <span className="info-box-number">{this.props.todayRideStatistic.ongoing}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-green">
                          <span className={s.infoBoxIcon}><i className="fa fa-check"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Completed rides</span>
                            <span className="info-box-number">{this.props.todayRideStatistic.completed}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-red">
                          <span className={s.infoBoxIcon}><i className="fa fa-times-circle-o"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Cancelled rides</span>
                            <span className="info-box-number">{this.props.todayRideStatistic.cancelled}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-yellow">
                          <span className={s.infoBoxIcon}><i className="fa fa-check-circle-o"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Accepted rides</span>
                            <span className="info-box-number">{this.props.todayRideStatistic.accepted}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                      <div className="col-lg-6"><a href="">
                        <div className="info-box bg-green">
                          <span className={s.infoBoxIcon}><i className="fa fa-money"/></span>
                          <div className="info-box-content">
                            <span className="info-box-text">Revenue</span>
                            <span className="info-box-number">{this.props.todayRideStatistic.revenue}</span>
                          </div>
                        </div>
                      </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
              <div className="col-sm-6">
                <div className="panel panel-primary bg-gray-light">
                  <div className="panel-heading">
                    <div className="panel-title-box">
                      <i className="fa fa-car"/> Total Completed Rides
                    </div>
                  </div>
                  <div className="row padding1520">
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-aqua">
                        <span className={s.infoBoxIcon}><i className="fa fa-calendar-o"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Today</span>
                          <span className="info-box-number">{this.props.totalCompletedRidesStatistic.today}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-yellow">
                        <span className={s.infoBoxIcon}><i className="fa fa-calendar"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">This months</span>
                          <span className="info-box-number">{this.props.totalCompletedRidesStatistic.month}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-yellow">
                        <span className={s.infoBoxIcon}><i className="fa fa-calendar-o"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text"><b>CabdoBlack:</b><br/>Today</span>
                          <span className="info-box-number">{this.props.totalCompletedRidesStatistic.blackToday}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-aqua">
                        <span className={s.infoBoxIcon}><i className="fa fa-calendar"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text"><b>CabdoBlack:</b><br/>This months</span>
                          <span className="info-box-number">{this.props.totalCompletedRidesStatistic.blackMonth}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-sm-6">
                <div className="panel panel-primary bg-gray-light">
                  <div className="panel-heading">
                    <div className="panel-title-box">
                      <i className="fa fa-user-secret"/> Drivers
                    </div>
                  </div>
                  <div className="row padding1520">
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-aqua">
                        <span className={s.infoBoxIcon}><i className="fa fa-id-card-o"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Total Registered</span>
                          <span className="info-box-number">{this.props.driversStatistic.registered}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-green">
                        <span className={s.infoBoxIcon}><i className="fa fa-toggle-on"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Online</span>
                          <span className="info-box-number">{this.props.driversStatistic.online}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-yellow">
                        <span className={s.infoBoxIcon}><i className="fa fa-car"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">On trip</span>
                          <span className="info-box-number">{this.props.driversStatistic.onTrip}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div className="col-lg-6"><a href="">
                      <div className="info-box bg-red">
                        <span className={s.infoBoxIcon}><i className="fa fa-toggle-off"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Offline</span>
                          <span className="info-box-number">{this.props.driversStatistic.offline}</span>
                        </div>
                      </div>
                    </a>
                    </div>
                  </div>
                </div>
              </div>
              </div>

              <div className="row">
              <div className="col-sm-6">
                <div className="panel panel-primary bg-gray-light">
                  <div className="panel-heading">
                    <div className="panel-title-box">
                      <i className="fa fa-cubes"/> Requested rides
                    </div>
                  </div>
                  {this.props.vehicleStatistic.requested.length !== 0 ?
                    <div className="row padding1520">
                      <div className={s.pieWrap}>
                        <PieChart width={200} height={200}>
                          <Pie
                            data={this.props.vehicleStatistic.requested}
                            cx={100}
                            cy={100}
                            innerRadius={55}
                            outerRadius={80}
                            fill="#8884d8"
                            paddingAngle={5}
                          >
                            {
                              data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]}/>)
                            }
                          </Pie>
                        </PieChart>
                        <div className={s.legendWrap}>
                          {this.props.vehicleStatistic.requested.map((entry, index) =>
                            <p className={s.legendItem} key={index}>
                              <span style={{
                                width: 20,
                                height: 10,
                                marginRight: 15,
                                backgroundColor: COLORS[index % COLORS.length]
                              }}/> {entry.name} - <b>&nbsp;{entry.value}</b>
                            </p>)}
                        </div>
                      </div>
                    </div>
                    :
                    <p style={{margin: 15, textAlign: 'center'}}>There are no requested trips statistic</p>
                  }
                </div>
              </div>
                <div className="col-sm-6">
                  <div className="panel panel-primary bg-gray-light">
                    <div className="panel-heading">
                      <div className="panel-title-box">
                        <i className="fa fa-check-circle-o"/> Accepted Rides
                      </div>
                    </div>
                    {this.props.vehicleStatistic.accepted.length !== 0 ?
                      <div className="row padding1520">
                        <div className={s.pieWrap}>
                          <PieChart width={200} height={200}>
                            <Pie
                              data={this.props.vehicleStatistic.accepted}
                              cx={100}
                              cy={100}
                              innerRadius={55}
                              outerRadius={80}
                              fill="#8884d8"
                              paddingAngle={5}
                            >
                              {
                                this.props.vehicleStatistic.accepted.map((entry, index) => <Cell
                                  key={index}
                                  fill={COLORS[index % COLORS.length]}/>)
                              }
                            </Pie>
                          </PieChart>
                          <div className={s.legendWrap}>
                            {this.props.vehicleStatistic.accepted.map((entry, index) =>
                              <p className={s.legendItem} key={index}>
                              <span style={{
                                width: 20,
                                height: 10,
                                marginRight: 15,
                                backgroundColor: COLORS[index % COLORS.length]
                              }}/> {entry.name} - <b>&nbsp;{entry.value}</b>
                              </p>)}
                          </div>
                        </div>
                      </div>
                      :
                      <p style={{margin: 15, textAlign: 'center'}}>There are no accepted trips statistic</p>
                    }
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-sm-6">
                  <div className="panel panel-primary bg-gray-light">
                    <div className="panel-heading">
                      <div className="panel-title-box">
                        <i className="fa fa-check"/> Completed Rides
                      </div>
                    </div>
                    {this.props.vehicleStatistic.completed.length !== 0 ?
                      <div className="row padding1520">
                        <div className={s.pieWrap}>
                          <PieChart width={200} height={200}>
                            <Pie
                              data={this.props.vehicleStatistic.completed}
                              cx={100}
                              cy={100}
                              innerRadius={55}
                              outerRadius={80}
                              fill="#8884d8"
                              paddingAngle={5}
                            >
                              {
                                this.props.vehicleStatistic.completed.map((entry, index) => <Cell
                                  fill={COLORS[index % COLORS.length]}/>)
                              }
                            </Pie>
                          </PieChart>
                          <div className={s.legendWrap}>
                            {this.props.vehicleStatistic.completed.map((entry, index) =>
                              <p key={`legend-${index}`} className={s.legendItem}>
                              <span key={`span-${index}`}
                                style={{
                                width: 20,
                                height: 10,
                                marginRight: 15,
                                backgroundColor: COLORS[index % COLORS.length]
                              }}/> {entry.name} - <b>&nbsp;{entry.value}</b>
                              </p>)}
                          </div>
                        </div>
                      </div>
                      :
                      <p style={{margin: 15, textAlign: 'center'}}>There are no completed trips statistic</p>
                    }
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="panel panel-primary bg-gray-light">
                    <div className="panel-heading">
                      <div className="panel-title-box">
                        <i className="fa fa-money"/> Revenue
                      </div>
                    </div>
                    {this.props.vehicleStatistic.revenue.length !== 0 ?
                      <div className="row padding1520">
                        <div className={s.pieWrap}>
                          <PieChart width={200} height={200}>
                            <Pie
                              data={this.props.vehicleStatistic.revenue}
                              cx={100}
                              cy={100}
                              innerRadius={55}
                              outerRadius={80}
                              fill="#8884d8"
                              paddingAngle={5}
                            >
                              {
                                this.props.vehicleStatistic.revenue.map((entry, index) => <Cell
                                  fill={COLORS[index % COLORS.length]}/>)
                              }
                            </Pie>
                          </PieChart>
                          <div className={s.legendWrap}>
                            {this.props.vehicleStatistic.revenue.map((entry, index) =>
                              <p className={s.legendItem}>
                              <span style={{
                                width: 20,
                                height: 10,
                                marginRight: 15,
                                backgroundColor: COLORS[index % COLORS.length]
                              }}/> {entry.name} - <b>&nbsp;{entry.value}</b>
                              </p>)}
                          </div>
                        </div>
                      </div>
                      :
                      <p style={{margin: 15, textAlign: 'center'}}>There are no revenue statistic</p>
                    }
                  </div>
                </div>
              </div>

              <div className="row">
              <div className="col-sm-6">
                <div className="panel panel-primary bg-gray-light">
                  <div className="panel-heading">
                    <div className="panel-title-box">
                      <i className="fa fa-star"/> Latest ratings and comments
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12">
                      { this.props.ratingCommentsStatistic.length > 0 ?
                        this.props.ratingCommentsStatistic.map(item =>
                        <div className="chat-item" key={item._id}>
                          <div className="media">
                            <div className="media-left">
                              <a href="#">
                                <img className="media-object img-circle" src={require('../../../common/images/flat-avatar.png')} />
                              </a>
                            </div>
                            <div className="media-body">
                              <h4 className="media-heading">
                                <span style={{marginRight: 25, fontSize: 12}}>
                                  {item.riderId && `${item.riderId.fname} ${item.riderId.lname}`} <span style={{fontWeight: '100'}}>(rider)</span>: <i className="fa fa-star-o"/> { item.riderRatingByDriver }
                                </span>
                                <span style={{fontSize: 12}}>
                                  {item.driverId && `${item.driverId.fname} ${item.driverId.lname}`} <span style={{fontWeight: '100'}}>(driver)</span>: <i className="fa fa-star-o"/> { item.driverRatingByRider }
                                </span>
                                <small className="pull-right text-muted label label-info">
                                  <i className="icon-time" /> { moment(item.endTime).fromNow() }
                                </small>
                              </h4>
                              { item.riderComment ? item.riderComment : '' }
                            </div>
                          </div>
                        </div>
                        )
                        :
                        <p style={{margin: 15, textAlign: 'center'}}>There are no Ratings</p>
                      }
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-sm-6">
                <div className="panel panel-primary bg-gray-light">
                  <div className="panel-heading">
                    <div className="panel-title-box">
                      <i className="fa fa-car"/> Reason of canceled trips
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12">
                      {this.props.cancelledCommentsStatistic.length > 0 ?
                        this.props.cancelledCommentsStatistic.map((item, index) =>
                          <div className="chat-item" key={index}>
                            <div className="media">
                              <div className="media-left">
                                <a href="#">
                                  <img className="media-object img-circle"
                                       src={require('../../../common/images/flat-avatar.png')}/>
                                </a>
                              </div>
                              <div className="media-body">
                                <h4 className="media-heading">#{item.tripNumber}
                                  <small className="pull-right text-muted label label-danger">
                                    <i className="icon-time"/> {moment(item.bookingTime).fromNow()}
                                  </small>
                                </h4>
                                <p>
                                  {item.riderId &&
                                  <span style={{marginRight: 25}}>
                                        Rider - {item.riderId.fname} {item.riderId.lname}
                                      </span>
                                  }
                                  {item.driverId &&
                                  <span>
                                        Driver - {item.driverId.fname} {item.driverId.lname}
                                      </span>
                                  }
                                </p>
                                <b>{item.tripIssue}</b>
                                <br/>
                                {item.tripIssueComment}
                              </div>
                            </div>
                          </div>
                        )
                        :
                        <p style={{margin: 15, textAlign: 'center'}}>There are no cancelled comments</p>
                      }
                    </div>
                  </div>
                </div>
              </div>
              </div>



              <div className="row">
                <div className="col-lg-12">
                  <div className="panel panel-primary bg-gray-light">
                    <div className="panel-heading">
                      <div className="panel-title-box">
                        <i className="fa fa-user-secret"/> Trips
                        <span className="pull-right">
                          <Link to="/dashboard/trips">View all <i className="fa fa-arrow-circle-right" />
                          </Link>
                        </span>
                      </div>
                    </div>
                    {allTrips.length > 0 ?
                      <div className="row padding1520">
                        <div className="col-lg-12">
                          <div className="table-responsive">
                            <table className="table">
                              <thead>
                              <tr>
                                <th>S.No</th>
                                <th>PickUp Address</th>
                                <th>Destination Address</th>
                                <th>Rider</th>
                                <th>Rider phone</th>
                                <th>Driver</th>
                                <th>Date & Time</th>
                                <th>Fare</th>
                                <th>Vehicle Category</th>
                                <th>Trip Status</th>
                                <th>Pre-booking</th>
                              </tr>
                              </thead>
                              <tbody>
                              {allTrips.map((item, index) =>
                                <tr key={index} id={`trip-${index}`} className={'animated'}>
                                  <td>{this.props.tripsStatistic.length - index}</td>
                                  <td style={{maxWidth: 250}}>{item.pickUpAddress.match(/^[^,]*,[^,]*/)}</td>
                                  <td style={{maxWidth: 250}}>{item.destAddress ? item.destAddress.match(/^[^,]*,[^,]*/) : ''}</td>
                                  <td>{item.riderId ? `${item.riderId.fname} ${item.riderId.lname}` : '-----'}</td>
                                  <td>{item.riderId ? item.riderId.phoneNo : '-----'}</td>
                                  <td>{item.driverId ? `${item.driverId.fname} ${item.driverId.lname}` : '-----'}</td>
                                  <td>{moment(item.bookingTime).format('Do MMM [at] HH:mm')}</td>
                                  <td>{`€ ${item.tripAmt ? item.tripAmt.toFixed(2) : '0.00'}`}</td>
                                  <td>{item.taxiType}</td>
                                  <td>{item.tripStatus}</td>
                                  <td className='text-center'>
                                    {item.preBookedTime ? moment(item.preBookedTime).format('Do MMM [at] HH:mm') : '-----'}
                                  </td>
                                </tr>
                              )}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      :
                      <p style={{margin: 15, textAlign: 'center'}}>There are no trips</p>
                    }
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-12">
                  <div className="panel panel-primary bg-gray-light">
                    <div className="panel-heading">
                      <div className="panel-title-box">
                        <span><i className="fa fa-user-secret"/> Driver Work List</span>
                      </div>
                    </div>
                    {allDrivers.length > 0 ?
                      <div className="row padding1520">
                        <div className="col-lg-12">
                          <div className="table-responsive">
                            <table className="table">
                              <thead>
                              <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Trips</th>
                                <th>Current Revenue</th>
                                <th>KM</th>
                                <th className="text-center">Online / Offline</th>
                                <th className="text-center">On trip / Free</th>
                                <th>Last Trip</th>
                                <th>Car</th>
                              </tr>
                              </thead>
                              <tbody>
                              {allDrivers.map((item, index) =>
                                <tr key={index}>
                                  <td>{index + 1}</td>
                                  <td>{item.fname} {item.lname}</td>
                                  <td>{item.phoneNo}</td>
                                  <td>{item.completedTrips || 0}</td>
                                  <td>{`€ ${item.totalRevenue ? item.totalRevenue.toFixed(2) : '0.00'}`}</td>
                                  <td>-----</td>
                                  <td className="text-center">
                                    {item.availability ?
                                      <span className={s.greenText}>Online</span>
                                      :
                                      <span className={s.redText}>Offline</span>
                                    }
                                  </td>
                                  <td className="text-center">{item.currTripId ? 'On trip' : 'Free'}</td>
                                  <td>{item.lastTime ? moment(item.lastTime).format('Do MMM [at] HH:mm') : '-----'}</td>
                                  <td>{item.carDetails.licensePlateNumber !== 'AB-CD-1234' ? (item.carDetails.licensePlateNumber + ', ' + item.carDetails.model) : '-----'}</td>
                                </tr>
                              )}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      :
                      <p style={{margin: 15, textAlign: 'center'}}>There are no drivers worklist</p>
                    }
                  </div>
                </div>
              </div>
            </div>
        }
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    isLoggedIn: state.domainReducer.auth.isLoggedIn,
    loading: state.domainReducer.statistic.loading,
    generalStatistic: state.domainReducer.statistic.general,
    todayRideStatistic: state.domainReducer.statistic.todayRide,
    totalCompletedRidesStatistic: state.domainReducer.statistic.totalCompletedRides,
    driversStatistic: state.domainReducer.statistic.drivers,
    ratingCommentsStatistic: state.domainReducer.statistic.ratingComments,
    cancelledCommentsStatistic: state.domainReducer.statistic.cancelledComments,
    driverWorklistStatistic: state.domainReducer.statistic.driverWorklist,
    tripsStatistic: state.domainReducer.statistic.tripsList,
    vehicleStatistic: state.domainReducer.statistic.vehicle,
    isNeedToSelect: state.domainReducer.statistic.isNeedToSelect,
    tripForSelect: state.domainReducer.statistic.tripForSelect
  };
}

function bindActions(dispatch) {
  return {
    fetchGeneralStatistic: () => dispatch(StatisticAction.fetchGeneralStatistic()),
    fetchTodayRideStatistic: () => dispatch(StatisticAction.fetchTodayRideStatistic()),
    fetchCompletedRideStatistic: () => dispatch(StatisticAction.fetchCompletedRideStatistic()),
    fetchDriversStatistic: () => dispatch(StatisticAction.fetchDriversStatistic()),
    fetchRatingCommentsStatistic: () => dispatch(StatisticAction.fetchRatingCommentsStatistic()),
    fetchCancelledCommentsStatistic: () => dispatch(StatisticAction.fetchCancelledCommentsStatistic()),
    fetchDriverWorklistStatistic: () => dispatch(StatisticAction.fetchDriverWorklistStatistic()),
    fetchTripsStatistic: () => dispatch(StatisticAction.fetchTripsStatistic()),
    fetchVehicleStatistic: () => dispatch(StatisticAction.fetchVehicleStatistic()),

    tripSelected: () => dispatch(StatisticAction.tripSelected())
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Dashboard));
