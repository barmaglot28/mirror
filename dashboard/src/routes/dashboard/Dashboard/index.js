import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/dashboard',
  async action() {
    const Dashboard = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Dashboard').default);
      }, 'dashboard');
    });
    return {
      title: 'Dashboard',
      component: <Layout><Dashboard key="dashboard" /></Layout>,
    };
  },
};
