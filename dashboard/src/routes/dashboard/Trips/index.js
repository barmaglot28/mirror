import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/trips',
  async action() {
  // async action({ store }) {
  //   const authObj = store.getState().domainReducer.auth;
  //   if (authObj && !authObj.loggedInStatus) {
  //     console.log('store ', store.getState());
  //     return { redirect: '/' };
  //   }
    const Trips = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Trips').default);
      }, 'trips');
    });
    return {
      title: 'Trips',
      component: <Layout><Trips key="trips" /></Layout>,
    };
  },
};
