import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination } from 'react-bootstrap';
import DatePicker from 'react-bootstrap-date-picker';
import { BarChart, ResponsiveContainer, Pie, PieChart, Cell, Tooltip, Legend, XAxis, YAxis, Bar } from 'recharts';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Trips.css';
import History from '../../../core/history';
import tripAction from '../../../store/domain/trips/action';
import UpdateTripDetails from './updateTrip';
import tripUpdateAction from '../../../store/domain/updateTrip/action';
import apiConfig from '../../../services/apiConfig';

class Trips extends Component {
  static propTypes = {
    tripList: React.PropTypes.array,
    tripFilter: React.PropTypes.string,
    isLoggedIn: React.PropTypes.bool,
    loading: React.PropTypes.bool,
    pageNo: React.PropTypes.any,
    getAllTripsPerDay: React.PropTypes.func,
    getAllTripsPerDayRevenueData: React.PropTypes.func,
    getAllTripsPerDayData: React.PropTypes.func,
    meta: React.PropTypes.object,
    getTotalCommission: React.PropTypes.func,
    getTotalAmountFare: React.PropTypes.func,
    getTotalAmountCommission: React.PropTypes.func,
    getAllTrips: React.PropTypes.func,
    // chartLoading: React.PropTypes.bool,
    chartData: React.PropTypes.object,
    getTotalFare: React.PropTypes.func,
    failedUserListApi: React.PropTypes.bool,
    // failedUserChartApi: React.PropTypes.bool,
    revenueGraphLoading: React.PropTypes.bool,
    revenueGraphData: React.PropTypes.object,
    tripsGraphLoading: React.PropTypes.bool,
    tripsGraphData: React.PropTypes.object,
    openModal: React.PropTypes.func,
    showModal: React.PropTypes.bool,
  };

  static goToTripDetail(id) {
    History.push(`/dashboard/trip/${id}`);
  }

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      activePage: 1,
      value: new Date().toISOString(),
      statistic: {
        revenue: {
          cash: 0,
          card: 0
        },
        trips: {
          black: 0,
          taxi: 0,
          taxiXL: 0
        },
        rides: {
          black: 0,
          taxi: 0,
          taxiXL: 0
        }
      }
    };
    this.setLoading = this.setLoading.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    // this.getChartData = this.getChartData.bind(this);
    this.getTotalFare = this.getTotalFare.bind(this);
    this.getTotalCommission = this.getTotalCommission.bind(this);
    this.getTotalAmountFare = this.getTotalAmountFare.bind(this);
    this.getAllTrips = this.getAllTrips.bind(this);
    this.getTotalAmountCommission = this.getTotalAmountCommission.bind(this);
    this.getRevenueData = this.getRevenueData.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
    this.handleTripFilter = this.handleTripFilter.bind(this);
  }

  componentWillMount() {
    this.props.getAllTripsPerDay(new Date().toISOString().slice(0,10));
    this.props.getAllTripsPerDayRevenueData(this.state.value.slice(0,10));
    this.props.getAllTripsPerDayData(this.state.value.slice(0,10));
  }

  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }

  handleChangeDate(date, formattedValue){
    this.setState({
      value: date,
      formattedValue: formattedValue
    });
    if(date != null && formattedValue != null){
      this.props.getAllTripsPerDay(date);
    }
    this.props.getAllTripsPerDayRevenueData(date);
    this.props.getAllTripsPerDayData(date);
  }

  // getChartData() {
  //   const data = [];
  //   if (this.props.chartData && this.props.chartData.success) {
  //     const chartArray = this.props.chartData.data;
  //     data.push({ name: 'rider', value: chartArray[0].rider });
  //     data.push({ name: 'driver', value: chartArray[0].driver });
  //   }
  //   return data;
  // }

  getTotalCommission(trips){
    let total = 0;
    if (trips[0].wallet){
      total += trips[0].wallet.commissionToCabdo * (this.getTotalFare(trips)) / 100;
    }

    return total;
  }

  getTotalFare(trips){
    let result = 0;
    trips.forEach((e) => {
      if (e.wallet){
        result += e.wallet.walletBalance;
      }
    });
    return result;
  }

  getAllTrips(allUsAndTrips){
    let result = 0;
    allUsAndTrips.forEach((e) => {
      result += e.trips.length;
    });
    return result;
  }

  getTotalAmountFare(allUsAndTrips){
    let result = 0;
    allUsAndTrips.forEach((e) => {
      result += this.getTotalFare(e.trips);
    });
    return result;
  }

  getTotalAmountCommission(allUsAndTrips){
    let result = 0;
    allUsAndTrips.forEach((e) => {
      result += this.getTotalCommission(e.trips);
    });
    return result;
  }

  getRevenueData() {
    let dayTime = [];
    let newStatistic = this.state.statistic;
    if (!this.props.revenueGraphLoading && this.props.revenueGraphData) {
      dayTime = [
        {name: '00', cash: null, card: null },
        {name: '01', cash: null, card: null },
        {name: '02', cash: null, card: null },
        {name: '03', cash: null, card: null },
        {name: '04', cash: null, card: null },
        {name: '05', cash: null, card: null },
        {name: '06', cash: null, card: null },
        {name: '07', cash: null, card: null },
        {name: '08', cash: null, card: null },
        {name: '09', cash: null, card: null },
        {name: '10', cash: null, card: null },
        {name: '11', cash: null, card: null },
        {name: '12', cash: null, card: null },
        {name: '13', cash: null, card: null },
        {name: '14', cash: null, card: null },
        {name: '15', cash: null, card: null },
        {name: '16', cash: null, card: null },
        {name: '17', cash: null, card: null },
        {name: '18', cash: null, card: null },
        {name: '19', cash: null, card: null },
        {name: '20', cash: null, card: null },
        {name: '21', cash: null, card: null },
        {name: '22', cash: null, card: null },
        {name: '23', cash: null, card: null },
      ];
      const revenueData = this.props.revenueGraphData.data;
      newStatistic.revenue.cash = 0;
      newStatistic.revenue.card = 0;
      let hourUTC;
      if (revenueData.length !== 0) {
        revenueData.forEach((item, i) => {
          hourUTC = new Date(item.date).getHours();
          if (item.paymentMode === 'cash') {
            dayTime[hourUTC].cash = item.tripAmt;
            newStatistic.revenue.cash += item.tripAmt;
          }
          if (item.paymentMode === 'card') {
            dayTime[hourUTC].card = item.tripAmt;
            newStatistic.revenue.card += item.tripAmt;
          }
        });
        // this.setState({statistic: newStatistic});
      }
    }
    return dayTime;
  }

  getTripsData() {
    let newStatistic = this.state.statistic;
    let dayTime = [];
    if (!this.props.tripsGraphLoading && this.props.tripsGraphData) {
      dayTime = [
        {name: '00', black: null, taxi: null, taxiXL: null },
        {name: '01', black: null, taxi: null, taxiXL: null },
        {name: '02', black: null, taxi: null, taxiXL: null },
        {name: '03', black: null, taxi: null, taxiXL: null },
        {name: '04', black: null, taxi: null, taxiXL: null },
        {name: '05', black: null, taxi: null, taxiXL: null },
        {name: '06', black: null, taxi: null, taxiXL: null },
        {name: '07', black: null, taxi: null, taxiXL: null },
        {name: '08', black: null, taxi: null, taxiXL: null },
        {name: '09', black: null, taxi: null, taxiXL: null },
        {name: '10', black: null, taxi: null, taxiXL: null },
        {name: '11', black: null, taxi: null, taxiXL: null },
        {name: '12', black: null, taxi: null, taxiXL: null },
        {name: '13', black: null, taxi: null, taxiXL: null },
        {name: '14', black: null, taxi: null, taxiXL: null },
        {name: '15', black: null, taxi: null, taxiXL: null },
        {name: '16', black: null, taxi: null, taxiXL: null },
        {name: '17', black: null, taxi: null, taxiXL: null },
        {name: '18', black: null, taxi: null, taxiXL: null },
        {name: '19', black: null, taxi: null, taxiXL: null },
        {name: '20', black: null, taxi: null, taxiXL: null },
        {name: '21', black: null, taxi: null, taxiXL: null },
        {name: '22', black: null, taxi: null, taxiXL: null },
        {name: '23', black: null, taxi: null, taxiXL: null },
      ];
      const tripsData = this.props.tripsGraphData.data;
      newStatistic.trips.black = 0;
      newStatistic.trips.taxi = 0;
      newStatistic.trips.taxiXL = 0;
      newStatistic.rides.black = 0;
      newStatistic.rides.taxi = 0;
      newStatistic.rides.taxiXL = 0;
      let hourUTC;
      if (tripsData.length !== 0) {
        tripsData.forEach((item, i) => {
          hourUTC = new Date(item.date).getHours();
          if (item.taxiType === 'CabdoBlack') {
            newStatistic.trips.black += item.count;
            if (item.tripStatus === 'endTrip') newStatistic.rides.black += item.count;
            dayTime[hourUTC].black = item.count;
          }
          if (item.taxiType === 'Taxi') {
            newStatistic.trips.taxi += item.count;
            if (item.tripStatus === 'endTrip') newStatistic.rides.taxi += item.count;
            dayTime[hourUTC].taxi = item.count;
          }
          if (item.taxiType === 'TaxiXL') {
            newStatistic.trips.taxiXL += item.count;
            if (item.tripStatus === 'endTrip') newStatistic.rides.taxiXL += item.count;
            dayTime[hourUTC].taxiXL = item.count;
          }
        });
        // this.setState({statistic: newStatistic});
      }
    }
    return dayTime;
  }

  handleTripFilter(filter) {
    if (filter !== this.state.activeFilter) {
      this.setState({ activeFilter: filter, activePage: 1 });
      this.props.fetchTrips(1, filter);
    }
  }
  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchTrips(eventkey, this.props.tripFilter);
  }
  updateTripDetails(tripObj) {
    // console.log('update trip object', tripObj);
    this.props.openModal(tripObj);
  }
  render() {
    return (
      <div className={`animate ${s.trips}`}>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4>Trips: Per day statistics</h4>
          </div>
          <div className="panel-body">
            {
              this.props.tripsGraphLoading
                ?
                <div id="tripsGraphLoading" className="loading-wrap" style={{ minHeight: 500 }}>
                  <div className="loading">
                    <div id="spinner">
                      <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                        <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                      </svg>
                    </div>
                  </div>
                </div>
                :
                (
                  <ResponsiveContainer width="100%" height={155} style={{padding: 25}}>
                    <BarChart
                      barGap={2}
                      data={this.getTripsData()}
                      margin={{ top: 25, right: 10, left: 10, bottom: 0 }}
                    >
                      <XAxis dataKey="name" />
                      <YAxis />
                      <Tooltip />
                      <Bar stackId="type" dataKey="black" fill="#DE6764" label />
                      <Bar stackId="type" dataKey="taxi" fill="#f0ad4e" />
                      <Bar stackId="type" dataKey="taxiXL" fill="#5bc0de" />
                    </BarChart>
                  </ResponsiveContainer>
                )
            }
          </div>
        </div>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4>Revenue: Per day statistics</h4>
          </div>
          <div className="panel-body">
            {
              this.props.revenueGraphLoading
                ?
                <div id="revenueGraphLoading" className="loading-wrap" style={{ minHeight: 500 }}>
                  <div className="loading">
                    <div id="spinner">
                      <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                        <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                      </svg>
                    </div>
                  </div>
                </div>
                :
                (
                  <ResponsiveContainer width="100%" height={200}>
                    <BarChart
                      barGap={2}
                      data={this.getRevenueData()}
                      margin={{ top: 10, right: 10, left: 10, bottom: 0 }}
                    >
                      <XAxis dataKey="name" />
                      <YAxis />
                      <Tooltip />
                      <Bar stackId="method" dataKey="cash" fill="#DE6764" label />
                      <Bar stackId="method" dataKey="card" fill="#f0ad4e" />
                    </BarChart>
                  </ResponsiveContainer>
                )
            }
          </div>
        </div>

        <div className="panel panel-default">
          <div className="panel-heading">
            <h4>Total: Per day statistics</h4>
          </div>
          <div className="panel-body">
            {
              this.props.revenueGraphLoading
                ?
                <div id="revenueGraphLoading" className="loading-wrap" style={{ minHeight: 500 }}>
                  <div className="loading">
                    <div id="spinner">
                      <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                        <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                      </svg>
                    </div>
                  </div>
                </div>
                :
                (
                  <div>
                    <div className="col-md-12">
                      <h4>Total Revenue</h4>
                    </div>
                    <div className="col-sm-6">
                      <div className="info-box bg-green">
                        <span className="infoBoxIcon"><i className="fa fa-money"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Cash</span>
                          <span className="info-box-number">{this.state.statistic.revenue.cash.toFixed(2)} €</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="info-box bg-green">
                        <span className="infoBoxIcon"><i className="fa fa-money"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Card</span>
                          <span className="info-box-number">{this.state.statistic.revenue.card.toFixed(2)} €</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <h4>Total Number of Trips</h4>
                    </div>
                    <div className="col-sm-4">
                      <div className="info-box bg-yellow">
                        <span className="infoBoxIcon"><i className="fa fa-car"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">CabdoBlack</span>
                          <span className="info-box-number">{this.state.statistic.trips.black}</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4">
                      <div className="info-box bg-yellow">
                        <span className="infoBoxIcon"><i className="fa fa-car"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Taxi</span>
                          <span className="info-box-number">{this.state.statistic.trips.taxi}</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4">
                      <div className="info-box bg-yellow">
                        <span className="infoBoxIcon"><i className="fa fa-car"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">TaxiXL</span>
                          <span className="info-box-number">{this.state.statistic.trips.taxiXL}</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <h4>Total Completed Rides</h4>
                    </div>
                    <div className="col-sm-4">
                      <div className="info-box bg-aqua">
                        <span className="infoBoxIcon"><i className="fa fa-car"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">CabdoBlack</span>
                          <span className="info-box-number">{this.state.statistic.rides.black}</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4">
                      <div className="info-box bg-aqua">
                        <span className="infoBoxIcon"><i className="fa fa-car"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">Taxi</span>
                          <span className="info-box-number">{this.state.statistic.rides.taxi}</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-4">
                      <div className="info-box bg-aqua">
                        <span className="infoBoxIcon"><i className="fa fa-car"/></span>
                        <div className="info-box-content">
                          <span className="info-box-text">TaxiXL</span>
                          <span className="info-box-number">{this.state.statistic.rides.taxiXL}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                )
            }
          </div>
        </div>

        <div>
          {
            this.state.isLoading
              ?
              <div className="loading-wrap" style={{ minHeight: 500 }}>
                <div className="loading">
                  <div id="spinner">
                    <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                      <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                    </svg>
                  </div>
                </div>
              </div>
              :
              <div className="row">
                <div className="col-sm-12">
                  <div className="panel panel-body">
                    <div className="row">
                      <div className="col-sm-6">
                        <label>Choose the day</label>
                        <div className="panel">
                          <DatePicker
                            dateFormat="YYYY/MM/DD"
                            value={this.state.value}
                            onChange={this.handleChangeDate} />
                        </div>
                      </div>
                      <div className="col-sm-6 text-right">
                        <div className="btn-group">
                          <a className={'btn btn-primary'} target={'blank'} href={`${apiConfig.serverUrl}/api/admin/trip/exportToExcel/${new Date().toISOString().slice(0,10)}`}>Export to Excel</a>
                        </div>
                      </div>
                    </div>
                   {/* <div className="row" style={{ marginBottom: 15 }}>
                      <div className="col-sm-12 text-center">
                        <div className="btn-group">
                          <button type="button" onClick={() => this.handleTripFilter('All')} className="btn btn-primary">All</button>
                          <button type="button" onClick={() => this.handleTripFilter('Ongoing')} className="btn btn-primary">Ongoing</button>
                          <button type="button" onClick={() => this.handleTripFilter('Completed')} className="btn btn-primary">Completed</button>
                        </div>
                      </div>
                    </div> */}
                    {
                      this.props.fetchTripFailed || this.props.tripList === null
                      ?
                        <div> No trip Available </div>
                      :
                        <div className="table-responsive">
                          <table className="table" style={{fontSize: 12}}>
                            <tbody>
                              <tr>
                                <th>S.No</th>
                                <th>ID</th>
                                <th>RideId</th>
                                <td>DriverId</td>
                                <td>Pick-up Address</td>
                                <td>Destination Address</td>
                                <td>Fare</td>
                                <td>Payment</td>
                                <td>Travel Time</td>
                                <td>Riders Rating</td>
                                <td>Drivers Rating</td>
                                <td>Trip Issue</td>
                                <td>Trip Status</td>
                                <td>Action</td>
                              </tr>
                              {
                                this.props.tripList.data.map((item, index) => (
                                  <tr key={index}>
                                    <td>
                                      {
                                        (
                                          (this.state.activePage - 1) /* * this.props.meta.limit*/
                                        ) + (index + 1)
                                      }
                                    </td>
                                    <td>{`#${item.tripNumber}`}</td>
                                    <td>{item.rider ? `${item.rider.fname} ${item.rider.lname}` : '------'}</td>
                                    <td>{item.driver ? `${item.driver.fname} ${item.driver.lname}` : '------'}</td>
                                    <td className="text-capitalize">{item.pickUpAddress}</td>
                                    <td className="text-capitalize">{item.destAddress}</td>
                                    <td>{`€ ${item.tripAmt ? item.tripAmt.toFixed(2) : 0.00}`}</td>
                                    <td>{item.paymentMode}</td>
                                    <td>{item.travelTime}</td>
                                    <td>{item.riderRatingByDriver}</td>
                                    <td>{item.driverRatingByRider}</td>
                                    <td className="text-capitalize">{item.tripIssue}</td>
                                    <td className="text-capitalize">{item.tripStatus}</td>
                                    <td style={{ minWidth: 100 }}>
                                      <button className="btn btn-warning" onClick={() => Trips.goToTripDetail(item._id)}>
                                        <i className="glyphicon glyphicon-eye-open" />
                                      </button>
                                      <button className="btn btn-info" onClick={() => this.updateTripDetails(item)}>
                                        <i className="glyphicon glyphicon glyphicon-edit" />
                                      </button>
                                    </td>
                                  </tr>),
                                )
                              }
                            </tbody>
                          </table>
                          {/* {
                            this.props.showModal && <UpdateTripDetails />
                          } */}
                          <UpdateTripDetails />
                          <Pagination
                            bsSize="medium"
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            maxButtons={5}
                            items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                            activePage={this.state.activePage}
                            onSelect={this.handlePagination}
                          />
                        </div>
                    }
                  </div>
                </div>
              </div>
          }
        </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    tripList: state.domainReducer.trips.tripList,
    loading: state.domainReducer.trips.loading,
    meta: state.domainReducer.trips.meta,
    // chartData: state.domainReducer.users.chartData,
    // chartLoading: state.domainReducer.users.chartLoading,
    failedUserListApi: state.domainReducer.trips.errorStatusCreateTripObj,
    // failedUserChartApi: state.domainReducer.users.failedUserChartApi,
    revenueGraphLoading: state.domainReducer.trips.revenueGraphLoadingPerDay,
    revenueGraphData: state.domainReducer.trips.revenueGraphDataPerDay,
    tripsGraphLoading: state.domainReducer.trips.tripsGraphLoadingPerDay,
    tripsGraphData: state.domainReducer.trips.tripsGraphDataPerDay,
    tripFilter: state.domainReducer.trips.tripFilter,
    showModal: state.domainReducer.updateTrip.showModal,
  };
}

function bindActions(dispatch) {
  return {
    getAllTripsPerDay: (filter) => dispatch(tripAction.getAllTripsPerDay(filter)),
    getAllTripsPerDayRevenueData: (date) => dispatch(tripAction.getAllTripsPerDayRevenueData(date)),
    getAllTripsPerDayData: (date) => dispatch(tripAction.getAllTripsPerDayData(date)),
    openModal: (updateTripObj) => dispatch(tripUpdateAction.openModal(updateTripObj)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Trips));
