import React, { Component } from 'react';
import { Row, Col, Form, FormControl, ControlLabel, Button } from 'react-bootstrap';
import Modal, { Header as ModalHeader, Title as ModalTitle, Body as ModalBody } from 'react-bootstrap/lib/Modal';
// import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './Trips.css';
import tripUpdateAction from '../../../store/domain/updateTrip/action';


// function checkNullValue(obj) {
//   const newObj = obj;
//   // newObj.forEach((member) => {
//   //   console.log('member', member);
//   // });
//   return newObj;
// }

class UpdateTrip extends Component {

  static propTypes = {
    showModal: React.PropTypes.bool,
    updateTripObjectLoading: React.PropTypes.bool,
    errorOnTripObjectUpdate: React.PropTypes.bool,
    closeModal: React.PropTypes.func,
    updateTripObject: React.PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      tripObj: {
        riderId: 'NA',
        driverId: 'NA',
        pickUpAddress: 'NA',
        destAddress: 'NA',
        paymentMode: 'NA',
        tripAmt: 0,
        taxiType: 'NA',
        riderRatingByDriver: 0,
        driverRatingByRider: 0,
        seatBooked: 0,
        tripStatus: 'NA',
        tripIssue: 'NA',
      },
    };
    // this.close = this.close.bind(this);
    // this.open = this.open.bind(this);
  }
  // close() {
  //   this.setState({ showModal: false });
  // }
  // open() {
  //   this.setState({ showModal: true });
  // }
  componentWillReceiveProps(nextProps) {
    // const tripObj123 = nextProps.tripObjectToUpdate;
    const tripObj123 = Object.assign({}, nextProps.tripObjectToUpdate);
    if (tripObj123._id) { // eslint-disable-line
      this.setState({ tripObj: tripObj123 });
    }
  }
  handleRiderId(e) {
    const tripObj = this.state.tripObj;
    tripObj.riderId = e.target.value;
    this.setState({ tripObj });
  }
  handleDriverId(e) {
    const tripObj = this.state.tripObj;
    tripObj.driverId = e.target.value;
    this.setState({ tripObj });
  }
  handlePickUpAddress(e) {
    const tripObj = this.state.tripObj;
    tripObj.pickUpAddress = e.target.value;
    this.setState({ tripObj });
  }
  handleDestAddress(e) {
    const tripObj = this.state.tripObj;
    tripObj.destAddress = e.target.value;
    this.setState({ tripObj });
  }
  handleTaxiType(e) {
    const tripObj = this.state.tripObj;
    tripObj.taxiType = e.target.value;
    this.setState({ tripObj });
  }
  handlePaymentMode(e) {
    const tripObj = this.state.tripObj;
    tripObj.paymentMode = e.target.value;
    this.setState({ tripObj });
  }
  handleRiderRating(e) {
    const tripObj = this.state.tripObj;
    tripObj.riderRatingByDriver = e.target.value;
    this.setState({ tripObj });
  }
  handleDriverRating(e) {
    const tripObj = this.state.tripObj;
    tripObj.driverRatingByRider = e.target.value;
    this.setState({ tripObj });
  }
  handleTripStatus(e) {
    const tripObj = this.state.tripObj;
    tripObj.tripStatus = e.target.value;
    this.setState({ tripObj });
  }
  handleTripIssue(e) {
    const tripObj = this.state.tripObj;
    tripObj.tripIssue = e.target.value;
    this.setState({ tripObj });
  }
  handleTripAmount(e) {
    const tripObj = this.state.tripObj;
    tripObj.tripAmt = e.target.value;
    this.setState({ tripObj });
  }
  handleSeatBooked(e) {
    const tripObj = this.state.tripObj;
    tripObj.seatBooked = e.target.value;
    this.setState({ tripObj });
  }

  hideModal() {
    this.props.closeModal();
  }
  handleSubmit(e) {
    e.preventDefault();
    // console.log('inside handleSubmit');
    // const tripObj = checkNullValue(this.state.tripObj);
    const tripObj = this.state.tripObj;
    this.props.updateTripObject(tripObj);
  }

  render() {
    return (
      <Modal show={this.props.showModal} id="updateTrip" className={s.updateTrip} onHide={() => { this.hideModal(); }}>
        <ModalHeader closeButton>
          <ModalTitle>
            Update Trips
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          {
            this.props.errorOnTripObjectUpdate
            ?
              <div className="alert alert-danger">
                <strong>Api Error: Api server is not working properly.</strong>
              </div>
            : null
          }
          {
            this.props.updateTripObjectLoading
            ?
              <div id="tripObjectLoading" className="loading-wrap" style={{ minHeight: 500 }}>
                <div className="loading">
                  <div id="spinner">
                    <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                      <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                    </svg>
                  </div>
                </div>
              </div>
            :
              <Row>
                <Col>
                  <Form horizontal onSubmit={(e) => { this.handleSubmit(e); }}>
                    <Row>
                      <Col md={5}>
                        <ControlLabel>
                        RiderId
                      </ControlLabel>
                        <FormControl type="text" placeholder="Rider Id" value={this.state.tripObj.riderId ? this.state.tripObj.riderId : 'NA'} onChange={(e) => { this.handleRiderId(e); }} />
                      </Col>
                      <Col md={5} mdOffset={1}>
                        <ControlLabel>
                        DriverId
                      </ControlLabel>
                        <FormControl type="text" placeholder="Driver Id" value={this.state.tripObj.driverId ? this.state.tripObj.driverId : 'NA'} onChange={(e) => { this.handleDriverId(e); }} />
                      </Col>
                    </Row>

                    <Row>
                      <Col md={5}>
                        <ControlLabel>
                        PickUp Address
                      </ControlLabel>
                        <FormControl componentClass="textarea" placeholder="PickUp Address" rows={3} value={this.state.tripObj.pickUpAddress ? this.state.tripObj.pickUpAddress : 'NA'} onChange={(e) => { this.handlePickUpAddress(e); }} />
                      </Col>
                      <Col md={5} mdOffset={1}>
                        <ControlLabel>
                        Destination Address
                      </ControlLabel>
                        <FormControl componentClass="textarea" placeholder="Destination Address" rows={3} value={this.state.tripObj.destAddress ? this.state.tripObj.destAddress : 'NA'} onChange={(e) => { this.handleDestAddress(e); }} />
                      </Col>
                    </Row>

                    <Row>
                      <Col md={5}>
                        <ControlLabel>
                        Payment Mode
                      </ControlLabel>
                        <FormControl componentClass="select" placeholder="Payment Mode" value={this.state.tripObj.paymentMode ? this.state.tripObj.paymentMode : 'NA'} onChange={(e) => { this.handlePaymentMode(e); }}>
                          <option value="cash">Cash</option>
                          <option value="paytm">Paytm</option>
                        </FormControl>
                      </Col>
                      <Col md={5} mdOffset={1}>
                        <ControlLabel>
                        Taxi Type
                      </ControlLabel>
                        <FormControl componentClass="select" placeholder="Taxi Type" value={this.state.tripObj.taxiType ? this.state.tripObj.taxiType : 'NA'} onChange={(e) => { this.handleTaxiType(e); }}>
                          <option value="TaxiXL">Taxi XL</option>
                          <option value="taxiMini">Taxi Mini</option>
                          <option value="taxiPrime">Taxi Prime</option>
                          <option value="taxiMacro">Taxi Macro</option>
                        </FormControl>
                      </Col>
                    </Row>

                    <Row>
                      <Col md={5}>
                        <ControlLabel>
                        Rider Rating
                      </ControlLabel>
                        <FormControl type="text" placeholder="Rider Rating" value={this.state.tripObj.riderRatingByDriver ? this.state.tripObj.riderRatingByDriver : 0} onChange={(e) => { this.handleRiderRating(e); }} />
                      </Col>
                      <Col md={5} mdOffset={1}>
                        <ControlLabel>
                        Driver Rating
                      </ControlLabel>
                        <FormControl type="text" placeholder="Driver Rating" value={this.state.tripObj.driverRatingByRider ? this.state.tripObj.driverRatingByRider : 0} onChange={(e) => { this.handleDriverRating(e); }} />
                      </Col>
                    </Row>

                    <Row>
                      <Col md={5}>
                        <ControlLabel>
                        Trip Status
                      </ControlLabel>
                        <FormControl componentClass="select" placeholder="Trip Status" value={this.state.tripObj.tripStatus ? this.state.tripObj.tripStatus : 'NA'} onChange={(e) => { this.handleTripStatus(e); }}>
                          <option value="onTrip">On Trip</option>
                          <option value="endTrip">End Trip</option>
                        </FormControl>
                      </Col>
                      <Col md={5} mdOffset={1}>
                        <ControlLabel>
                        Trip Issue
                      </ControlLabel>
                        <FormControl type="text" placeholder="Trip Issue" value={this.state.tripObj.tripIssue ? this.state.tripObj.tripIssue : 'NA'} onChange={(e) => { this.handleTripIssue(e); }} />
                      </Col>
                    </Row>

                    <Row>
                      <Col md={5}>
                        <ControlLabel>
                        Trip Amount
                      </ControlLabel>
                        <FormControl type="text" placeholder="Trip Amount" value={this.state.tripObj.tripAmt ? this.state.tripObj.tripAmt.toFixed(2) : 0} onChange={(e) => { this.handleTripAmount(e); }} />
                      </Col>
                      <Col md={5} mdOffset={1}>
                        <ControlLabel>
                        Total Seats Booked
                      </ControlLabel>
                        <FormControl componentClass="select" placeholder="Seats Booked" value={this.state.tripObj.seatBooked ? this.state.tripObj.seatBooked : 0} onChange={(e) => { this.handleSeatBooked(e); }}>
                          <option value="0">0</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                        </FormControl>
                      </Col>
                    </Row>
                    <hr />
                    <div className="pull-right clearfix">
                      <Button className="btn-rounded" onClick={() => { this.hideModal(); }}> Cancel </Button>&nbsp;&nbsp;
                      <Button bsStyle="primary" className="btn-rounded" type="submit"> Save </Button>
                    </div>
                  </Form>
                </Col>
              </Row>
          }
        </ModalBody>
        {/* <ModalFooter>
          <Button className="btn-rounded" onClick={() => { this.hideModal(); }}> Cancel </Button>
          <Button bsStyle="primary" className="btn-rounded" type="submit"> Save </Button>
        </ModalFooter> */}
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    showModal: state.domainReducer.updateTrip.showModal,
    tripObjectToUpdate: state.domainReducer.updateTrip.tripObjectToUpdate,
    updateTripObjectLoading: state.domainReducer.updateTrip.updateTripObjectLoading,
    errorOnTripObjectUpdate: state.domainReducer.updateTrip.errorOnTripObjectUpdate,
  };
}

function bindActions(dispatch) {
  return {
    openModal: () => dispatch(tripUpdateAction.openModal()),
    closeModal: () => dispatch(tripUpdateAction.closeModal()),
    updateTripObject: (tripObj) => dispatch(tripUpdateAction.updateTripObject(tripObj)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(UpdateTrip));
