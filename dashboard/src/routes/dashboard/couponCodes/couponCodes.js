import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Button } from 'react-bootstrap';
import { ResponsiveContainer, Pie, PieChart, Cell, Tooltip, Legend } from 'recharts';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './couponCodes.css';
import CouponcodesAction from '../../../store/domain/couponcodes/action';
import CouponcodesUpdateAction from '../../../store/domain/updateCouponcodes/action';
import History from '../../../core/history';
import UpdateCouponcodesDetails from './updateCouponCodes';
import voucherCode from 'voucher-code-generator';


class Couponcodes extends Component {
  static propTypes = {
    couponcodesList: React.PropTypes.any,
    isLoggedIn: React.PropTypes.bool,
    loading: React.PropTypes.bool,
    fetchCouponcodes: React.PropTypes.func,
    meta: React.PropTypes.object,
    chartLoading: React.PropTypes.bool,
    chartData: React.PropTypes.object,
    removeCouponcodesErrorObj: React.PropTypes.object,
    removeCouponcodesSuccess: React.PropTypes.object,
    failedCouponcodesListApi: React.PropTypes.bool,
    failedCouponcodesChartApi: React.PropTypes.bool,
    openCouponcodesDetailModal: React.PropTypes.func,
    removeCouponcodes: React.PropTypes.func,
  };

  static goToCouponcodesDetail(id) {
    History.push(`/dashboard/couponcodes/${id}`);
  }

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      activePage: 1,
    };
    this.setLoading = this.setLoading.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
    this.UpdateCouponcodesData = this.UpdateCouponcodesData.bind(this);
    this.DeleteCouponcodesData = this.DeleteCouponcodesData.bind(this);
  }

  componentWillMount() {
    this.props.fetchCouponcodes(this.state.activePage);
  }

  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
    if(nextProps.removeCouponcodesSuccess.success) {
      this.setState({ isLoading: false });
      this.setState({ loading: false });
      this.props.fetchCouponcodes(this.state.activePage);
    }
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }

  UpdateCouponcodesData(couponcodesObj) {
    this.props.openCouponcodesDetailModal(couponcodesObj);
  }

  DeleteCouponcodesData(couponcodesObj) {
    this.props.removeCouponcodes(couponcodesObj);
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchCouponcodes(eventkey);
  }

  generateCode () {
    const charset = 'abcdefghijklmnopqrstuvwxyz1234567890';
    const code = voucherCode.generate({ length: 8, charset })[0];
    return code;
  }


  render() {
    return (
      <div className={`animate ${s.couponcodes}`}>
        <div>
          {
            this.props.failedCouponcodesListApi
              ?
              <div className="alert alert-danger">
                {/* <strong>Api Error: Api server is not working properly.</strong> */}
                <strong>{this.props.removeCouponcodesErrorObj.message}</strong>
              </div>
              :
              null
          }
          {
            this.props.removeCouponcodesSuccess.success
              ?
              <div className="alert alert-danger">
                {/* <strong>Api Error: Api server is not working properly.</strong> */}
                <strong>{this.props.removeCouponcodesSuccess.message}</strong>
              </div>
              :
              null

          }
          {
          this.state.isLoading
          ?
            <div className="loading-wrap" style={{ minHeight: 500 }}>
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
          :
            <div className="row">
              <div className="col-sm-12">
                <div className="panel panel-body">
                  <div className="table-responsive">
                    <table className="table">
                      <tbody>
                        <tr>
                          <th>S.No</th>
                          <th>ID</th>
                          <th>Code</th>
                          <th>Discount</th>
                          <th style={{ minWidth: 140 }}>Action</th>
                        </tr>
                        {
                          this.props.failedCouponcodesListApi
                        ?
                          <tr>
                              <td colSpan="10">
                                  <div className="alert alert-danger">
                                    <strong>Api Error: Api server is not working properly.</strong>
                                </div>
                              </td>
                          </tr>
                        : null
                        }
                        {
                          this.props.couponcodesList.data
                        ?
                          this.props.couponcodesList.data.map((item, index) => (
                            <tr key={index}>
                              <td>
                              {
                                (
                                    (this.state.activePage - 1) * this.props.meta.limit
                                ) + (index + 1)
                              }
                              </td>
                              <td>{item._id}</td>
                              <td>{ this.generateCode()}</td>
                              <td>{item.discount}</td>
                              <td style={{ minWidth: 140 }}>
                                <button className="btn btn-info" onClick={() => { this.UpdateCouponcodesData(item); }}>
                                  <i className="glyphicon glyphicon-edit" />
                                </button>
                                <button className="btn btn-danger" onClick={() => { this.DeleteCouponcodesData(item); }}>
                                  <i className="glyphicon glyphicon-remove" />
                                </button>
                              </td>
                            </tr>)) : null
                          }
                      </tbody>
                    </table>
                    <UpdateCouponcodesDetails />
                    <Pagination
                      bsSize="medium"
                      prev
                      next
                      first
                      last
                      ellipsis
                      boundaryLinks
                      maxButtons={5}
                      items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                      activePage={this.state.activePage}
                      onSelect={this.handlePagination}
                    />
                    <hr />
                    <div className="clearfix">
                      <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} onClick={() => { window.location.href = "/dashboard/createCouponcodes"; }}>Generate</Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        }
        </div>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    couponcodesList: state.domainReducer.couponcodes.couponcodesList,
    isLoggedIn: state.domainReducer.auth.isLoggedIn,
    loading: state.domainReducer.couponcodes.loading,
    meta: state.domainReducer.couponcodes.meta,
    failedCouponcodesListApi: state.domainReducer.couponcodes.failedCouponcodesListApi,
    removeCouponcodesErrorObj: state.domainReducer.couponcodes.removeCouponcodesErrorObj,
    removeCouponcodesSuccess: state.domainReducer.couponcodes.removeCouponcodesSuccess,
  };
}

function bindActions(dispatch) {
    return {
        fetchCouponcodes: (pageNo) => dispatch(CouponcodesAction.fetchCouponcodes(pageNo)),
        openCouponcodesDetailModal: (couponcodesObj) => dispatch(CouponcodesUpdateAction.openCouponcodesDetailModal(couponcodesObj)),
        removeCouponcodes: (couponcodesObj) => dispatch(CouponcodesAction.removeCouponcodes(couponcodesObj)),
    };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Couponcodes));
