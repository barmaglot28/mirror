import React, { Component } from 'react';
import { Row, Col, Form, FormControl, FormGroup, ControlLabel, Button, Checkbox, Radio } from 'react-bootstrap';
import Modal, { Header as ModalHeader, Title as ModalTitle, Body as ModalBody } from 'react-bootstrap/lib/Modal';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './couponCodes.css';
import couponcodesUpdateAction from '../../../store/domain/updateCouponcodes/action';

class UpdateCouponcodes extends Component {
  static propTypes = {
    showCouponcodesupdateModal: React.PropTypes.bool,
    closeCouponcodesDetailModal: React.PropTypes.func,
    updateCouponcodesObject: React.PropTypes.func,
    couponcodesObjectToUpdate: React.PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.state = {
      couponcodesObj: {
        name: 'Discount code',
        countMax: 1,
      },
    };
    this.hideCouponcodesupdateModal = this.hideCouponcodesupdateModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const couponcodesObjReceived = Object.assign({}, nextProps.couponcodesObjectToUpdate);
    if (couponcodesObjReceived._id) { // eslint-disable-line
      this.setState({ couponcodesObj: couponcodesObjReceived });
    }
  }

  hideCouponcodesupdateModal() {
    this.props.closeCouponcodesDetailModal();
  }

  handleDiscount(e) {
    const couponcodesObj = this.state.couponcodesObj;
    couponcodesObj.discount = e.target.value;
    this.setState({ couponcodesObj });
  }

  handleSubmit(e) {
    e.preventDefault();
    // console.log('inside handleSubmit');
    // const tripObj = checkNullValue(this.state.tripObj);
    const couponcodesObj = this.state.couponcodesObj;
    this.props.updateCouponcodesObject(couponcodesObj);
  }

  render() {
    return (
      <Modal show={this.props.showCouponcodesupdateModal} id="updateCouponcode" className={s.updateCouponcodesDetail} onHide={() => { this.hideCouponcodesupdateModal(); }}>
        <ModalHeader closeButton>
          <ModalTitle>
            Update Discount Details
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col>
              <Form horizontal onSubmit={(e) => { this.handleSubmit(e); }}>
                <Row>
                  <Col md={5} mdOffset={1} >
                    <ControlLabel>
                      Discount
                    </ControlLabel>
                    <FormControl type="text" placeholder="Name"  value={this.state.couponcodesObj.discount} onChange={(e) => { this.handleDiscount(e); }} />
                  </Col>
                </Row>
                <hr />
                <div className="pull-right clearfix">
                  <Button className="btn-rounded" onClick={() => { this.hideCouponcodesupdateModal(); }}> Cancel </Button>&nbsp;&nbsp;
                  <Button bsStyle="primary" className="btn-rounded" type="submit"> Save </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    );
  }
}


function mapStateToProps(state) {
  return {
    showCouponcodesupdateModal: state.domainReducer.updateCouponcodes.showCouponcodesupdateModal,
    couponcodesObjectToUpdate: state.domainReducer.updateCouponcodes.couponcodesObjectToUpdate,
  };
}
function bindActions(dispatch) {
  return {
    openCouponcodesDetailModal: () => dispatch(couponcodesUpdateAction.openCouponcodesDetailModal()),
    closeCouponcodesDetailModal: () => dispatch(couponcodesUpdateAction.closeCouponcodesDetailModal()),
    updateCouponcodesObject: (couponcodesObj) => dispatch(couponcodesUpdateAction.updateCouponcodesObject(couponcodesObj)),
  };
}
export default connect(mapStateToProps, bindActions)(withStyles(s)(UpdateCouponcodes));
