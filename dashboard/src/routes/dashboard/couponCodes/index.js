import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/couponcodes',
  async action() {
    const Couponcodes = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./couponCodes').default);
      }, 'couponcodes');
    });
    return {
      title: 'Couponcodes',
      component: <Layout><Couponcodes key="couponcodes" /></Layout>,
    };
  },
};
