import React, { Component } from 'react';
import { Pagination, Panel, Row, Col, FormGroup, ControlLabel, FormControl, InputGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CarcategoriesDetails.css';
import { fetchCarcategoriesDetails } from '../../../store/domain/carcategoriesDetails/action';

class CarcategoriesDetails extends Component {
  static propTypes = {
    fetchCarcategoriesDetails: React.PropTypes.func,
    id: React.PropTypes.string,
    carcategoriesDetails: React.PropTypes.object,
    loading: React.PropTypes.bool,
  };

  componentDidMount() {
    this.props.fetchCarcategoriesDetails(this.props.id);
  }

  render() {
    return (
        <div className="animate">
          {
            !this.props.loading
                ?
                <Panel>
                  <div className={s.carcategoriesDetails}>
                    <div className="panel panel-default">
                      <div className="panel-heading">Car Categories Details</div>
                      <div className="panel-body">
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Name
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.name}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Price Per Kilometer
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.pricePerKilometer}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Price Per Minute
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.pricePerMinute}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Base Fare
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.baseFare}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Minimum Fare
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.minimumFare}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Commission To Cabdo
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.commissionToCabdo}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Capacity
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.capacity}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Type
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carcategoriesDetails.type}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </div>
                </Panel>
                :
                <div className="loading-wrap" style={{ minHeight: 500 }}>
                  <div className="loading">
                    <div id="spinner">
                      <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                        <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33"
                                r="30"/>
                      </svg>
                    </div>
                  </div>
                </div>
          }
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.domainReducer.carcategoriesDetails.loading,
    carcategoriesDetails: state.domainReducer.carcategoriesDetails.currentCarcategoriesDetails.data,
  };
}

function bindActions(dispatch) {
  return {
    fetchCarcategoriesDetails: (id) => dispatch(fetchCarcategoriesDetails(id)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(CarcategoriesDetails));
