import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/carcategories/:id',
  async action({ params }) {
    const CarcategoriesDetails = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CarcategoriesDetails').default);
      }, 'carcategoriesDetails');
    });
    return {
      title: 'CarcategoriesDetails',
      component: <Layout><CarcategoriesDetails key="carcategoriesDetails" id={params.id} /></Layout>,
    };
  },
};
