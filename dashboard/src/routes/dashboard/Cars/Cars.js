import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Button } from 'react-bootstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Cars.css';
import CarAction from '../../../store/domain/cars/action';
import CarUpdateAction from '../../../store/domain/updateCar/action';
import History from '../../../core/history';
import UpdateCarDetails from './updateCar';

class Cars extends Component {

  static propTypes = {
    carList: React.PropTypes.any,
    isLoggedIn: React.PropTypes.bool,
    loading: React.PropTypes.bool,
    fetchCars: React.PropTypes.func,
    meta: React.PropTypes.object,
    failedCarListApi: React.PropTypes.bool,
    openCarDetailModal: React.PropTypes.func,
    removeCar: React.PropTypes.func,
  };

  static goToCarDetail(id) {
    History.push(`/dashboard/car/${id}`);
  }

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      activePage: 1,
      showCarUpdateModal: false,
      deleteCar: false,
    };
    this.setLoading = this.setLoading.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
    this.UpdateCarData = this.UpdateCarData.bind(this);
  }

  componentWillMount() {
    this.props.fetchCars(this.state.activePage);
  }

  componentWillReceiveProps(nextProps) {
    this.setLoading(nextProps.loading);
    if (!nextProps.showCarUpdateModal && this.state.showCarUpdateModal) {
      this.setState({ showCarUpdateModal: nextProps.showCarUpdateModal });
      this.props.fetchCars(this.state.activePage);
    }
    if (nextProps.removeCarSuccess && nextProps.removeCarSuccess.success && this.state.deleteCar) {
      this.setState({ deleteCar: false });
      this.props.fetchCars(this.state.activePage);
    }
    this.setState({ showCarUpdateModal: nextProps.showCarUpdateModal });
  }

  setLoading(loading) {
    this.setState({ isLoading: loading });
  }

  UpdateCarData(carObj) {
    this.props.openCarDetailModal(carObj);
  }

  DeleteCarData(carObj) {
    this.setState({ deleteCar: true });
    this.props.removeCar(carObj);
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    this.props.fetchCars(eventkey);
  }

  render() {
    return (
        <div className={`animate ${s.car}`}>
          {
            this.state.isLoading ?
              <div className="loading-wrap" style={{ minHeight: 500 }}>
                <div className="loading">
                  <div id="spinner">
                    <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                      <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33"
                              r="30"/>
                    </svg>
                  </div>
                </div>
              </div>
              :
              <div className="row">
                <div className="col-sm-12">
                  <div className="panel panel-body">
                    <div className="table-responsive">
                      <table className="table">
                        <tbody>
                        <tr>
                          <th>S.No</th>
                          <th>License Plate Number</th>
                          <th>Make</th>
                          <th>Model</th>
                          <th>Category</th>
                          <th>Used By</th>
                          <th>Can Be Used</th>
                          <th>Action</th>
                        </tr>
                        {
                          this.props.failedCarListApi
                              ?
                              <tr>
                                <td colSpan="10">
                                  <div className="alert alert-danger">
                                    <strong>Api Error: Api server is not working properly.</strong>
                                  </div>
                                </td>
                              </tr>
                              : null
                        }
                        {
                          this.props.carList.data
                              ?
                              this.props.carList.data.map((item, index) => (
                                  <tr key={index}>
                                    <td>
                                      {
                                        (
                                            (this.state.activePage - 1) * this.props.meta.limit
                                        ) + (index + 1)
                                      }
                                    </td>
                                    <td>{item.licensePlateNumber}</td>
                                    <td>{item.make}</td>
                                    <td>{item.model}</td>
                                    <td>{item.carCategory ? item.carCategory.name : ''}</td>
                                    <td>{item.usedBy ? `${item.usedBy.fname} ${item.usedBy.lname}` : 'Free'}</td>
                                    <td>{item.canBeUsed ? 'Yes' : 'No'}</td>
                                    <td style={{ minWidth: 100 }}>
                                      <button
                                          className="btn btn-warning"
                                          onClick={() => Cars.goToCarDetail(item._id)}
                                      >
                                        <i className="glyphicon glyphicon-eye-open"/>
                                      </button>
                                      <button className="btn btn-info" onClick={() => {
                                        this.UpdateCarData(item);
                                      }}>
                                        <i className="glyphicon glyphicon-edit"/>
                                      </button>
                                      <button className="btn btn-danger" onClick={() => {
                                        this.DeleteCarData(item);
                                      }}>
                                        <i className="glyphicon glyphicon-remove"/>
                                      </button>
                                    </td>
                                  </tr>)) : null
                        }
                        </tbody>
                      </table>
                      <UpdateCarDetails />
                      <Pagination
                          bsSize="medium"
                          prev
                          next
                          first
                          last
                          ellipsis
                          boundaryLinks
                          maxButtons={5}
                          items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                          activePage={this.state.activePage}
                          onSelect={this.handlePagination}
                      />
                      <hr />
                      <div className="clearfix">
                        <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} onClick={() => {
                          window.location.href = "/dashboard/createCar";
                        }}> Create </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          }
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.domainReducer.auth.isLoggedIn,
    carList: state.domainReducer.cars.carList,
    showCarUpdateModal: state.domainReducer.updateCar.showCarUpdateModal,
    loading: state.domainReducer.cars.loading,
    meta: state.domainReducer.cars.meta,
    failedCarListApi: state.domainReducer.cars.failedCarListApi,
    removeCarErrorObj: state.domainReducer.cars.removeCarErrorObj,
    removeCarSuccess: state.domainReducer.cars.removeCarSuccess,
  };
}

function bindActions(dispatch) {
  return {
    fetchCars: (pageNo) => dispatch(CarAction.fetchCars(pageNo)),
    openCarDetailModal: (carObj) => dispatch(CarUpdateAction.openCarDetailModal(carObj)),
    removeCar: (carObj) => dispatch(CarAction.removeCar(carObj)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Cars));
