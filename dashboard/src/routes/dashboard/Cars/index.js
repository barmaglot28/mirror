import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/cars',
  async action() {
    const Cars = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Cars').default);
      }, 'cars');
    });
    return {
      title: 'Cars',
      component: <Layout><Cars key="cars" /></Layout>,
    };
  },
};
