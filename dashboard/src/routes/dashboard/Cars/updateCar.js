import React, { Component } from 'react';
import { Panel, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Checkbox } from 'react-bootstrap';
import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import Modal, { Header as ModalHeader, Title as ModalTitle, Body as ModalBody } from 'react-bootstrap/lib/Modal';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './Cars.css';
import carUpdateAction from '../../../store/domain/updateCar/action';
import CarcategoriesAction from '../../../store/domain/carcategories/action';

class UpdateCar extends Component {
  static propTypes = {
    showCarUpdateModal: React.PropTypes.bool,
    closeCarDetailModal: React.PropTypes.func,
    updateCarObject: React.PropTypes.func,
    carObjectToUpdate: React.PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      carObj: {
        licensePlateNumber: '',
        make: '',
        model: '',
        usedBy: undefined,
        carCategory: undefined,
        canBeUsed: false,
      },
      carCategories: [],
    };
    this.hideCarupdateModal = this.hideCarupdateModal.bind(this);
  }

  componentDidMount() {
    this.props.fetchCarcategories(1, 1000);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.carcategoriesList && nextProps.carcategoriesList.success) {
      this.setState({
        carCategories: nextProps.carcategoriesList.data,
        // carCategory: this.state.carCategory ? this.state.carCategory._id : nextProps.carcategoriesList.data[0]
      })
    }
    const carObjReceived = Object.assign({}, nextProps.carObjectToUpdate);
    if (carObjReceived._id) { // eslint-disable-line
      this.setState({ carObj: carObjReceived });
    }
  }

  hideCarupdateModal() {
    this.props.closeCarDetailModal();
  }

  handleLicensePlateNumber(e) {
    const carObj = this.state.carObj;
    carObj.licensePlateNumber = e.target.value;
    this.setState({ carObj });
  }

  handleMake(e) {
    const carObj = this.state.carObj;
    carObj.make = e.target.value;
    this.setState({ carObj });
  }

  handleModel(e) {
    const carObj = this.state.carObj;
    carObj.model = e.target.value;
    this.setState({ carObj });
  }

  handleCategory(e) {
    const carObj = this.state.carObj;
    carObj.carCategory = e.target.value;
    this.setState({ carObj });
  }

  handleCanBeUsed() {
    const carObj = this.state.carObj;
    carObj.canBeUsed = !carObj.canBeUsed;
    this.setState({ carObj });
  }

  handleSubmit(e) {
    e.preventDefault();
    let carObj = this.state.carObj;
    // set carCategory if default
    if (this.state.carObj.carCategory && this.state.carObj.carCategory._id)
      carObj.carCategory = this.state.carCategories[0]._id;
    // set carCategory if not defined
    if (!this.state.carObj.carCategory)
      carObj.carCategory = this.state.carCategories[0]._id;
    // set usedBy if defined
    if (this.state.carObj.usedBy && this.state.carObj.usedBy._id)
      carObj.usedBy = this.state.carObj.usedBy._id;
    this.props.updateCarObject(carObj);
  }

  render() {

    return (
        <Modal show={this.props.showCarUpdateModal} id="updateCar" className={s.updateCarDetail} onHide={() => {
          this.hideCarupdateModal();
        }}>
          <ModalHeader closeButton>
            <ModalTitle>
              Update Car Details
            </ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col>
                <Form horizontal onSubmit={(e) => {
                  this.handleSubmit(e);
                }}>
                  <Row>
                    <Col md={5}>
                      <FormGroup>
                        <ControlLabel>
                          License Plate Number
                        </ControlLabel>
                        <InputGroup>
                          <InputGroupAddon>
                            <i className="glyphicon glyphicon-car"/>
                          </InputGroupAddon>
                          <FormControl type="text" placeholder="DO-AA-1234"
				value={this.state.carObj.licensePlateNumber}
                                onChange={(e) => {
                            this.handleLicensePlateNumber(e);
                          }}/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col md={5} mdOffset={1}>
                      <FormGroup>
                        <ControlLabel>
                          Make
                        </ControlLabel>
                        <InputGroup>
                          <InputGroupAddon>
                            <i className="glyphicon glyphicon-car"/>
                          </InputGroupAddon>
                          <FormControl type="text" placeholder="Make" value={this.state.carObj.make} onChange={(e) => {
                            this.handleMake(e);
                          }}/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={5}>
                      <FormGroup>
                        <ControlLabel>
                          Model
                        </ControlLabel>
                        <InputGroup>
                          <InputGroupAddon>
                            <i className="glyphicon glyphicon-car"/>
                          </InputGroupAddon>
                          <FormControl type="text" placeholder="Model" value={this.state.carObj.model}
                                       onChange={(e) => {
                                         this.handleModel(e);
                                       }}/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col md={5} mdOffset={1}>
                      <FormGroup>
                        <ControlLabel>
                          Car Category
                        </ControlLabel>
                        <InputGroup>
                          <InputGroupAddon>
                            <i className="glyphicon glyphicon-car"/>
                          </InputGroupAddon>
                          <FormControl componentClass="select" placeholder="Car Category"
                                       value={this.state.carObj.carCategory ? this.state.carObj.carCategory._id : this.state.carCategories[0]}
                                       onChange={(e) => {
                                         this.handleCategory(e);
                                       }}>
                            {
                              this.state.carCategories.map((item, index) =>
                                  <option key={index} value={item._id}>{item.name}</option>
                              )
                            }
                          </FormControl>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={5}>
                      <FormGroup>
                        <ControlLabel>
                          Used By
                        </ControlLabel>
                        <InputGroup>
                          <InputGroupAddon>
                            <i className="glyphicon glyphicon-car"/>
                          </InputGroupAddon>
                          <FormControl type="text" placeholder="Used By"
                                       value={this.state.carObj.usedBy ? `${this.state.carObj.usedBy.fname} ${this.state.carObj.usedBy.lname}` : ''}
                                       disabled/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                    <Col md={5} mdOffset={1}>
                      <FormGroup>
                        <ControlLabel>
                          Can Be Used
                        </ControlLabel>
                        <InputGroup>
                          <Checkbox checked={this.state.carObj.canBeUsed} onChange={() => {
                            this.handleCanBeUsed();
                          }}/>
                        </InputGroup>
                      </FormGroup>
                    </Col>
                  </Row>

                  <hr/>
                  <div className="pull-right clearfix">
                    <Button className="btn-rounded" onClick={() => {
                      this.hideCarupdateModal();
                    }}> Cancel </Button>&nbsp;&nbsp;
                    <Button bsStyle="primary" className="btn-rounded" type="submit"> Save </Button>
                  </div>
                </Form>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
    );
  }
}


function mapStateToProps(state) {
  return {
    showCarUpdateModal: state.domainReducer.updateCar.showCarUpdateModal,
    carObjectToUpdate: state.domainReducer.updateCar.carObjectToUpdate,
    carcategoriesList: state.domainReducer.carcategories.carcategoriesList,
  };
}

function bindActions(dispatch) {
  return {
    openCarDetailModal: () => dispatch(carUpdateAction.openCarDetailModal()),
    closeCarDetailModal: () => dispatch(carUpdateAction.closeCarDetailModal()),
    updateCarObject: (carObj) => dispatch(carUpdateAction.updateCarObject(carObj)),
    fetchCarcategories: (pageNo, limit) => dispatch(CarcategoriesAction.fetchCarcategories(pageNo, limit)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(UpdateCar));
