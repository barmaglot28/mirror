import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/trip/:id',
  async action({ params }) {
    const TripDetail = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./TripDetail').default);
      }, 'tripDetail');
    });
    return {
      title: 'TripDetail',
      component: <Layout><TripDetail key="tripDetail" id={params.id} /></Layout>,
    };
  },
};
