import React, { Component } from 'react';
import { Panel } from 'react-bootstrap';
import { connect } from 'react-redux';
import moment from 'moment';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TripDetail.css';
import logo from '../../../common/images/logo.jpg';
import tripDetailsAction from '../../../store/domain/tripDetails/action';
import Utils from '../../../helpers/utils';

function getDay(day) {
  if (day) {
    return moment(day, 'YYYY-MM-DD HH:mm:ss').format('dddd');
  }
  return 'NA';
}
function getDate(day) {
  if (day) {
    return moment(day, 'YYYY-MM-DD HH:mm:ss').date();
  }
  return 'NA';
}
function getTotalTime(duration) {
  if (duration) {
    return `${moment.duration(duration).hours()}:${moment.duration(duration).minutes()}:${moment.duration(duration).seconds()}`;
  }
  return 'NA';
}

class TripDetails extends Component {
  static propTypes = {
    fetchTripDetails: React.PropTypes.func,
    id: React.PropTypes.string,
    tripDetails: React.PropTypes.object,
    loading: React.PropTypes.bool,
  };
  // constructor(props) {
  //   super(props);
    // this.getDay = this.getDay.bind(this);
    // this.getDate = this.getDate.bind(this);
    // this.getTotalTime = this.getTotalTime.bind(this);
  // }
  componentDidMount() {
    this.props.fetchTripDetails(this.props.id);
  }

  render() {
    return (
      <div className="animate">
        {
          !this.props.loading
          ?
            <Panel>
              <div className={s.tripDetails}>
                <div className="row">
                  <div className="col-md-12">
                    <div className={`media ${s.tripHeader}`}>
                      <div className="media-left">
                        <a href>
                          <img src={logo} alt="flat Avatar" className="user-avatar" height="80px" />
                        </a>
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">
                          {
                            this.props.tripDetails.pickUpAddress
                            ?
                            this.props.tripDetails.pickUpAddress
                            : 'Location A'
                          }
                          <span className="glyphicon glyphicon-arrow-right" />
                          {
                            this.props.tripDetails.destAddress
                            ?
                            this.props.tripDetails.destAddress
                            : 'Location B'
                          }
                        </h4>
                        <p>Booking reference:&nbsp;
                          {
                            this.props.tripDetails._id
                            ?
                            this.props.tripDetails._id
                            : 'NA'
                          }  |  Booked on: {moment(this.props.tripDetails.bookingTime).format('Do MMM [at] HH:mm')}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">Trip Details</div>
                  <div className="panel-body">
                    <div className="media">
                      <div className="media-left">
                        <a href>
                          <img src={logo} alt="flat Avatar" className="user-avatar" height="40px" />
                        </a>
                      </div>
                      <div className="media-body">
                        <h4 className="media-heading">
                          {this.props.tripDetails.pickUpAddress}
                          <span className="glyphicon glyphicon-arrow-right" />
                          {this.props.tripDetails.destAddress}
                        </h4>
                      </div>
                    </div>
                    <div className={`row ${s.tripBody}`}>
                      <div className="col-sm-2">
                        <div className={s.calendar}>
                          <div>{getDay(this.props.tripDetails.bookingTime)}</div>
                          <div>{getDate(this.props.tripDetails.bookingTime)}</div>
                          <div>{moment(this.props.tripDetails.bookingTime).format('MMM')}</div>
                        </div>
                      </div>
                      <div className={`col-sm-3 text-center ${s.startTime}`}>
                        {this.props.tripDetails.pickUpAddress
                          ?
                            <div>
                              <p className={s.location}>{this.props.tripDetails.pickUpAddress}</p>
                              <p className="time">{moment(this.props.tripDetails.bookingTime).format('Do MMM [at] HH:mm:ss')}</p>
                            </div>
                          :
                            <div className="mt-22"><b>Not Available</b></div>
                          }
                      </div>
                      <div className={`col-sm-3 text-center ${s.totalTime}`}>
                        {
                          this.props.tripDetails.travelTime
                          ?
                            <div>
                              <p className="glyphicon glyphicon-time" />
                              <p>{getTotalTime(this.props.tripDetails.travelTime)}</p>
                            </div>
                          :
                            <div className="mt-22"><b>Not Available</b></div>
                          }
                      </div>
                      <div className={`col-sm-3 text-center ${s.endTime}`}>
                        {
                          this.props.tripDetails.destAddress
                          ?
                            <div>
                              <p className={s.location}>{this.props.tripDetails.destAddress}</p>
                              <p className="time">{Utils.getDateTime(this.props.tripDetails.endTime)}</p>
                            </div>
                          :
                            <div className="mt-22">
                              <b>Not Available</b>
                            </div>
                          }
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="panel panel-default">
                      <div className="panel-heading">Rider Details</div>
                      <div className={`panel-body ${s.driverDetails}`}>
                        <p>Name:
                          &nbsp;
                          {
                            this.props.tripDetails.rider
                            ?
                            this.props.tripDetails.rider.fname
                            : 'NA'
                          }
                          {
                            this.props.tripDetails.rider
                             ?
                             this.props.tripDetails.rider.lname
                             : 'NA'
                           }
                        </p>
                        <p>Email: {this.props.tripDetails.rider ? this.props.tripDetails.rider.email : 'NA'}</p>
                        <p>Rating: {this.props.tripDetails.rider ? this.props.tripDetails.rider.userRating : 'NA'}</p>
                        <p>Price: {this.props.tripDetails ? `${Number(this.props.tripDetails.tripAmt).toFixed(2)} €` : 'NA'}</p>
                        <p>Trip Status: {this.props.tripDetails.tripStatus ? this.props.tripDetails.tripStatus : 'NA'}</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="panel panel-default">
                      <div className="panel-heading">Driver Details</div>
                      <div className={`panel-body ${s.driverDetails}`}>
                        <p>Name:
                          &nbsp;
                          {
                            this.props.tripDetails.driver
                            ?
                            this.props.tripDetails.driver.fname
                            : 'NA'
                          }
                          {
                            this.props.tripDetails.driver
                            ?
                            this.props.tripDetails.driver.lname
                            : 'NA'
                          }
                        </p>
                        <p>Phone No: {this.props.tripDetails.driver ? this.props.tripDetails.driver.phoneNo : 'NA'}</p>
                        <p>Rating: {this.props.tripDetails.driver ? this.props.tripDetails.driver.userRating : 'NA'}</p>
                        <p>State: {this.props.tripDetails.driver ? this.props.tripDetails.driver.state : 'NA'}</p>
                        <p>Country: {this.props.tripDetails.driver ? this.props.tripDetails.driver.country : 'NA'}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">Fair Details</div>
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-lg-8">
                        <table className={`table no-margin ${s.tripTable}`}>
                          <tbody>
                            <tr>
                              <td className="text-left col-md-4 col-xs-6">Adult x 1</td>
                              <td className="text-right col-md-4 col-xs-6"><strong>{this.props.tripDetails.tripAmt !== undefined ? `${Number(this.props.tripDetails.tripAmt).toFixed(2)} €` : 'NA'}</strong></td>
                            </tr>
                          </tbody>
                        </table>
                        <hr />
                        <table className={`table no-margin ${s.tripTable}`}>
                          <tbody>
                            <tr>
                              <td className="text-left col-md-4 col-xs-6">(-) Discount</td>
                              <td className="text-right col-md-4 col-xs-6">- <strong>0 €</strong></td>
                            </tr>
                          </tbody>
                        </table>
                        <hr />
                        <table className={`table ${s.tripTable}`} style={{ marginBottom: -5 }}>
                          <tbody>
                            <tr>
                              <td className="text-left col-md-4 col-xs-6">Total</td>
                              <td className="text-right col-md-4 col-xs-6"><strong>{this.props.tripDetails.tripAmt !== undefined ? `${Number(this.props.tripDetails.tripAmt).toFixed(2)} €` : 'NA'}</strong></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="alert alert-success">
                  <h4 className="no-margin">Rider: &nbsp;
                    <span className="text-capitalize">
                      {this.props.tripDetails.rider ? this.props.tripDetails.rider.fname : 'NA'} &nbsp;
                    </span>
                    {
                      this.props.tripDetails.rider ? this.props.tripDetails.rider.lname : 'NA'
                    } paid &nbsp;
                    {
                      this.props.tripDetails.tripAmt
                      ?
                      `${Number(this.props.tripDetails.tripAmt).toFixed(2)} €`
                      : 'NA'
                    } to Driver: &nbsp;
                    <span className="text-capitalize">
                      {
                        this.props.tripDetails.driver
                        ? this.props.tripDetails.driver.fname
                        : 'NA'
                      } &nbsp;</span>
                    {this.props.tripDetails.driver ? this.props.tripDetails.driver.lname : 'NA'}
                  </h4>
                </div>
              </div>
            </Panel>
          :
            <div className="loading-wrap" style={{ minHeight: 500 }}>
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.domainReducer.tripDetails.tripLoading,
    tripDetails: state.domainReducer.tripDetails.tripData,
  };
}

function bindActions(dispatch) {
  return {
    fetchTripDetails: (id) => dispatch(tripDetailsAction.fetchTripDetails(id)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(TripDetails));
