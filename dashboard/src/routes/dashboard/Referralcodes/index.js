import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/referralcodes',
  async action() {
    const Referralcodes = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Referralcodes').default);
      }, 'referralcodes');
    });
    return {
      title: 'Referralcodes',
      component: <Layout><Referralcodes key="referralcodes" /></Layout>,
    };
  },
};
