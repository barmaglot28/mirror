import React, { Component } from 'react';
import { Panel, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Checkbox, Radio } from 'react-bootstrap';
import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './Referralcodes.css';
import ReferralcodesAction from '../../../store/domain/referralcodes/action';
import Spinner from '../../../components/Spinner';
import History from '../../../core/history';


class Referralcodes extends Component {
  static propTypes = {
    fetchReferralcode: React.PropTypes.func,
    newReferralcodesObj: React.PropTypes.object,
    createReferralcodesErrorObj: React.PropTypes.object,
    failedCreateReferralcodesApi: React.PropTypes.bool,
    newReferralcodesLoading: React.PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      amountToSenderWallet: this.props.amountToSenderWallet,
      amountToRecipientWallet: this.props.amountToRecipientWallet,
    };
    this.handleAmountToSender = this.handleAmountToSender.bind(this);
    this.handleAmountToRecipient = this.handleAmountToRecipient.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    const { amountToSenderWallet, amountToRecipientWallet } = nextProps.referralAmount.referralAmount;
    if (this.state.amountToSenderWallet !== amountToSenderWallet && amountToSenderWallet) {
      this.setState({ amountToRecipientWallet, amountToSenderWallet })
    }
  }

  componentDidMount() {
    this.props.fetchReferralAmt();
  }

  handleAmountToSender(e) {
    const amountToSenderWallet = e.target.value;
    this.setState({ amountToSenderWallet });
  }

  handleAmountToRecipient(e) {
    const amountToRecipientWallet = e.target.value;
    this.setState({ amountToRecipientWallet });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { amountToSenderWallet, amountToRecipientWallet } = this.state;
    if (amountToRecipientWallet && amountToSenderWallet)
      this.props.saveReferralAmt({ amountToSenderWallet, amountToRecipientWallet });
  }

  render() {
    const { fetchSuccess, saveSuccess, saveErrMsg, loading } = this.props.referralAmount;
    return (
        <div className="animate">
          <Row key="newReferralcodes">
            <Col md={10}>
              <Panel header="Referral Codes" bsStyle="primary">
                {!(saveSuccess && fetchSuccess) ?
                  <div className="alert alert-danger">
                    <strong>Api Error: Api server is not working properly.</strong>
                  </div> : null
                }
                {loading ?
                  <Spinner componentsId="createNewReferralcodes"/>
                  :
                  <Form horizontal onSubmit={(e) => {
                    this.handleSubmit(e);
                  }} id="createReferralcodes">
                    <Row>
                      <Col md={5}>
                        <FormGroup>
                          <ControlLabel>
                            Amount To Sender
                          </ControlLabel>
                          <InputGroup>
                            <InputGroupAddon>
                              <i className="glyphicon glyphicon-eur"/>
                            </InputGroupAddon>
                            <FormControl type="number" placeholder="amount to sender" value={this.state.amountToSenderWallet} onChange={(e) => {
                              this.handleAmountToSender(e);
                            }}/>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col md={5} mdOffset={1}>
                        <FormGroup>
                          <ControlLabel>
                            Amount To Recipient
                          </ControlLabel>
                          <InputGroup>
                            <InputGroupAddon>
                              <i className="glyphicon glyphicon-eur"/>
                            </InputGroupAddon>
                            <FormControl type="number" placeholder="amount to recipient" value={this.state.amountToRecipientWallet} onChange={(e) => {
                              this.handleAmountToRecipient(e);
                            }}/>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                    </Row>
                    <hr />
                    <div className="pull-right clearfix">
                      <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} type="submit">
                        Save</Button>
                    </div>
                  </Form>
                }
              </Panel>
            </Col>
          </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    referralAmount: state.domainReducer.referralcodes
  };
}

function bindActions(dispatch) {
  return {
    saveReferralAmt: (referralcodesObj) => dispatch(ReferralcodesAction.saveReferralAmt(referralcodesObj)),
    fetchReferralAmt: (id) => dispatch(ReferralcodesAction.fetchReferralAmt(id)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(Referralcodes));
