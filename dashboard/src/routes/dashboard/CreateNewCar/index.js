import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/dashboard/createCar',

  async action() {
    const CreateTrips = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CreateNewCar').default);
      }, 'createTrips');
    });

    return {
      title: 'Create New Car',
      component: <Layout><CreateTrips key="createNewCar" /></Layout>,
    };
  },
};
