import React, { Component } from 'react';
import { Panel, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Checkbox } from 'react-bootstrap';
import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import s from './CreateNewCar.css';
import CarAction from '../../../store/domain/cars/action';
import CarcategoriesAction from '../../../store/domain/carcategories/action';
import Spinner from '../../../components/Spinner';
import History from '../../../core/history';


let categoryArray = [
  { name: 'select' },
];

class CreateNewCar extends Component {
  static propTypes = {
    newCarObj: React.PropTypes.object,
    createCarErrorObj: React.PropTypes.object,
    failedCreateCarApi: React.PropTypes.bool,
    newCarLoading: React.PropTypes.bool,
    createNewCar: React.PropTypes.func,
    fetchCarcategories: React.PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      carObj: {
        licensePlateNumber: '',
        make: '',
        model: '',
        carCategory: '',
        canBeUsed: false,
      },
      carCategories: categoryArray,
    };
  }

  componentDidMount() {
    this.props.fetchCarcategories(1, 1000);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.carcategoriesList && nextProps.carcategoriesList.success) {
      const carCategories = categoryArray.concat(nextProps.carcategoriesList.data);
      this.setState({ carCategories })
    }
    if (nextProps.newCarObj && nextProps.newCarObj.data && !nextProps.newCarLoading) {
      if (!nextProps.failedCreateCarApi) {
        History.push('/dashboard/cars');
      }
    }
  }

  handleLicensePlateNumber(e) {
    const carObj = this.state.carObj;
    carObj.licensePlateNumber = e.target.value;
    this.setState({ carObj });
  }

  handleMake(e) {
    const carObj = this.state.carObj;
    carObj.make = e.target.value;
    this.setState({ carObj });
  }

  handleModel(e) {
    const carObj = this.state.carObj;
    carObj.model = e.target.value;
    this.setState({ carObj });
  }

  handleCategory(e) {
    const carObj = this.state.carObj;
    if (this.state.carCategories[e.target.value]._id)
      carObj.carCategory = this.state.carCategories[e.target.value]._id;
    else
      carObj.carCategory = undefined;
    this.setState({ carObj });
  }

  handleCanBeUsed() {
    const carObj = this.state.carObj;
    carObj.canBeUsed = !carObj.canBeUsed;
    this.setState({ carObj });
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('inside submit form with event obj', e);
    const carObj = this.state.carObj;
    this.props.createNewCar(carObj);
  }

  render() {
    return (
        <div className="animate">

          <Row key="newCar">
            <Col md={10}>
              <Panel header="Create New Car" bsStyle="primary">
                {
                  this.props.failedCreateCarApi
                      ?
                      <div className="alert alert-danger">
                        <strong>{this.props.createCarErrorObj.message}</strong>
                      </div>
                      :
                      null
                }
                {
                  this.props.newCarLoading
                      ?
                      <Spinner componentsId="createNewCar"/>
                      :
                      <Form horizontal onSubmit={(e) => {
                        this.handleSubmit(e);
                      }} id="createNewCar">
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                License Plate Number
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-car"/>
                                </InputGroupAddon>
                                <FormControl type="text"
                                    value={this.state.carObj.licensePlateNumber}
                                    placeholder="DO-AA-1234"
                                     onChange={(e) => {
                                  this.handleLicensePlateNumber(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Make
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-car"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Make" onChange={(e) => {
                                  this.handleMake(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Model
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-car"/>
                                </InputGroupAddon>
                                <FormControl type="text" placeholder="Model" onChange={(e) => {
                                  this.handleModel(e);
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Car Category
                              </ControlLabel>
                              <InputGroup>
                                <InputGroupAddon>
                                  <i className="glyphicon glyphicon-car"/>
                                </InputGroupAddon>
                                <FormControl componentClass="select" placeholder="Car Category" onChange={(e) => {
                                  this.handleCategory(e);
                                }}>
                                  {
                                    this.state.carCategories.map((item, index) =>
                                        <option key={index} value={index}>{item.name}</option>)
                                  }
                                </FormControl>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Can Be Used
                              </ControlLabel>
                              <InputGroup>
                                <Checkbox onChange={() => {
                                  this.handleCanBeUsed();
                                }}/>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>

                        <hr/>
                        <div className="pull-right clearfix">
                          <Button className={`${s.formButton} btn-rounded`} type="reset"> Reset </Button>&nbsp;&nbsp;
                          <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} type="submit">
                            Create </Button>
                        </div>
                      </Form>
                }
              </Panel>
            </Col>
          </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newCarLoading: state.domainReducer.cars.newCarLoading,
    failedCreateCarApi: state.domainReducer.cars.failedCreateCarApi,
    createCarErrorObj: state.domainReducer.cars.createCarErrorObj,
    newCarObj: state.domainReducer.cars.newCarObj,
    carcategoriesList: state.domainReducer.carcategories.carcategoriesList,
  };
}

function bindActions(dispatch) {
  return {
    createNewCar: (carObj) => dispatch(CarAction.createNewCar(carObj)),
    fetchCarcategories: (pageNo, limit) => dispatch(CarcategoriesAction.fetchCarcategories(pageNo, limit)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(CreateNewCar));
