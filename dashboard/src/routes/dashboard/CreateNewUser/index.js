import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/dashboard/createUser',

  async action() {
    const CreateTrips = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CreateNewUser').default);
      }, 'createTrips');
    });

    return {
      title: 'Create New User',
      component: <Layout><CreateTrips key="createNewUser" /></Layout>,
    };
  },
};
