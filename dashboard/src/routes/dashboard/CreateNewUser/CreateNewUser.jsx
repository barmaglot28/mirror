import React, { Component } from 'react';
import { Panel, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import InputGroup, { Addon as InputGroupAddon } from 'react-bootstrap/lib/InputGroup';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import DatePicker from 'react-bootstrap-date-picker';
import { connect } from 'react-redux';
import s from './CreateNewUser.css';
import UserAction from '../../../store/domain/users/action';
import Spinner from '../../../components/Spinner';

class CreateUser extends Component {
  static propTypes = {
    newUserObj: React.PropTypes.object,
    createUserErrorObj: React.PropTypes.object,
    failedCreateUserApi: React.PropTypes.bool,
    newUserLoading: React.PropTypes.bool,
    createUser: React.PropTypes.func,
  }
  constructor(props) {
    super(props);
    this.state = {
      userObj: {
        fname: 'NA',
        lname: 'NA',
        email: 'NA',
        password: 'NA',
        confirmPassword: 'NA',
        userType: 'NA',
        phoneNo: '',
        address: 'NA',
        city: 'NA',
        state: 'NA',
        country: 'NA',
        dob: 'NA',
        gpsLoc: null, // hidden
        fullAddress: '' // hidden
      },
    };
    this.handleBirthdate = this.handleBirthdate.bind(this);
  }
  componentDidMount() {
    const autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('corporateLoc'),
      { types: ['geocode'] }
    );
    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      const userObj = this.state.userObj;
      console.log(place);

      this.clearAddress(userObj);
      const street = [];
      place.address_components.forEach((component) => {
        if (component.types.includes('country')) {
          userObj.country = component.long_name;
        } else if (component.types.includes('administrative_area_level_1')) {
          userObj.state = component.long_name;
        } else if (component.types.includes('locality')) {
          userObj.city = component.short_name;
        } else if (component.types.includes('route')) {
          street[0] = component.short_name;
        } else if (component.types.includes('street_number')) {
          street[1] = component.short_name;
        }
      });
      userObj.address = street.join(' ');
      userObj.gpsLoc = [place.geometry.location.lng(), place.geometry.location.lat()];
      userObj.fullAddress = place.formatted_address;

      this.setState({ userObj });
    });
  }
  clearAddress(userObj) {
    userObj.address = userObj.city = userObj.state = userObj.country = 'NA';
    userObj.gpsLoc = null;
    userObj.fullAddress = '';
  }
  handleFirstName(e) {
    const userObj = this.state.userObj;
    userObj.fname = e.target.value;
    this.setState({ userObj });
  }
  handleLastName(e) {
    const userObj = this.state.userObj;
    userObj.lname = e.target.value;
    this.setState({ userObj });
  }
  handleEmail(e) {
    const userObj = this.state.userObj;
    userObj.email = e.target.value;
    this.setState({ userObj });
  }
  handlePassword(e) {
    const userObj = this.state.userObj;
    userObj.password = e.target.value;
    this.setState({ userObj });
  }
  handleConfirmPassword(e) {
    const userObj = this.state.userObj;
    userObj.confirmPassword = e.target.value;
    this.setState({ userObj });
  }
  handleUserType(e) {
    const userObj = this.state.userObj;
    userObj.userType = e.target.value;
    this.setState({ userObj });
  }
  handlePhoneNumber(e) {
    const userObj = this.state.userObj;
    userObj.phoneNo = e.target.value;
    this.setState({ userObj });
  }
  handleAddress(e) {
    const userObj = this.state.userObj;
    userObj.address = e.target.value;
    this.setState({ userObj });
  }
  handleCity(e) {
    const userObj = this.state.userObj;
    userObj.city = e.target.value;
    this.setState({ userObj });
  }
  handleState(e) {
    const userObj = this.state.userObj;
    userObj.state = e.target.value;
    this.setState({ userObj });
  }
  handleCountry(e) {
    const userObj = this.state.userObj;
    userObj.country = e.target.value;
    this.setState({ userObj });
  }
  handleBirthdate(value) {
    const userObj = this.state.userObj;
    console.log('value', value);
    userObj.dob = value;
    this.setState({ userObj });
  }
  handleSubmit(e) {
    e.preventDefault();
    const userObj = this.state.userObj;
    this.props.createUser(userObj);
  }
  handleReset() {
    const userObj = this.state.userObj;
    this.clearAddress(userObj);
    this.setState({ userObj });
  }
  render() {
    return (
      <div className="animate">

        <Row key="newUser">
          <Col md={10}>
            <Panel header="Create New User" bsStyle="primary">
              <div className="alert alert-danger" style={{ display: this.props.failedCreateUserApi ? 'block' : 'none' }}>
                {/* <strong>Api Error: Api server is not working properly.</strong> */}
                <strong>{this.props.createUserErrorObj.message}</strong>
              </div>
              {
                this.props.newUserLoading ?
                  <Spinner componentsId="createNewUser" /> :
                  null
              }
              <Form horizontal id="createUser"
                style={{ display: this.props.newUserLoading ? 'none' : 'block' }}
                onReset={() => { this.handleReset(); }}
                onSubmit={(e) => { this.handleSubmit(e); }}>
                <Row>
                  <Col md={5}>
                    <FormGroup>
                      <ControlLabel>
                        First Name
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-user" />
                        </InputGroupAddon>
                        <FormControl type="text" placeholder="First Name" onChange={(e) => { this.handleFirstName(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <FormGroup>
                      <ControlLabel>
                        Last Name
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-user" />
                        </InputGroupAddon>
                        <FormControl type="text" placeholder="Last Name" onChange={(e) => { this.handleLastName(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md={5}>
                    <FormGroup>
                      <ControlLabel>
                        Email
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-envelope" />
                        </InputGroupAddon>
                        <FormControl type="text" placeholder="Email" onChange={(e) => { this.handleEmail(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md={5}>
                    <FormGroup>
                      <ControlLabel>
                        Password
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-lock" />
                        </InputGroupAddon>
                        <FormControl type="password" placeholder="Password" onChange={(e) => { this.handlePassword(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <FormGroup>
                      <ControlLabel>
                        Confirm Password
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-lock" />
                        </InputGroupAddon>
                        <FormControl type="password" placeholder="Password" onChange={(e) => { this.handleConfirmPassword(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col md={5}>
                    <FormGroup>
                      <ControlLabel>
                        User Type
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-user" />
                        </InputGroupAddon>
                        <FormControl componentClass="select" placeholder="User Type" onChange={(e) => { this.handleUserType(e); }}>
                          <option value="select"> Select </option>
                          <option value="rider"> Rider </option>
                          <option value="driver"> Driver </option>
                          <option value="corporate"> Corporate </option>
                        </FormControl>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <FormGroup>
                      <ControlLabel>
                        Phone Number
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-earphone" />
                        </InputGroupAddon>
                        <FormControl type="number" placeholder="Phone No" onChange={(e) => { this.handlePhoneNumber(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col md={5}>
                    <FormGroup>
                      <ControlLabel>
                        Address
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-home" />
                        </InputGroupAddon>
                        <FormControl type="text" value={this.state.userObj.address} placeholder="Address" id="corporateLoc" onChange={(e) => { this.handleAddress(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <FormGroup>
                      <ControlLabel>
                        City
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-home" />
                        </InputGroupAddon>
                        <FormControl type="text" value={this.state.userObj.city} placeholder="City" onChange={(e) => { this.handleCity(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col md={5}>
                    <FormGroup>
                      <ControlLabel>
                        State
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-home" />
                        </InputGroupAddon>
                        <FormControl type="text" value={this.state.userObj.state} placeholder="State" onChange={(e) => { this.handleState(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                  <Col md={5} mdOffset={1}>
                    <FormGroup>
                      <ControlLabel>
                        Country
                      </ControlLabel>
                      <InputGroup>
                        <InputGroupAddon>
                          <i className="glyphicon glyphicon-home" />
                        </InputGroupAddon>
                        <FormControl type="text" value={this.state.userObj.country} placeholder="Country" onChange={(e) => { this.handleCountry(e); }} />
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md={5}>
                    <FormGroup>
                      <ControlLabel>
                        BirthDate
                      </ControlLabel>
                      <DatePicker placeholder="DD/MM/YYYY" showClearButton={false} onChange={(value) => { this.handleBirthdate(value); }} />
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <div className="pull-right clearfix">
                  <Button className={`${s.formButton} btn-rounded`} type="reset" onClick={() => { this.handleReset(); }}> Reset </Button>&nbsp;&nbsp;
                  <Button bsStyle="primary" className={`${s.formButton} btn-rounded`} type="submit"> Create </Button>
                </div>
              </Form>
            </Panel>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    newUserLoading: state.domainReducer.users.newUserLoading,
    failedCreateUserApi: state.domainReducer.users.failedCreateUserApi,
    createUserErrorObj: state.domainReducer.users.createUserErrorObj,
    newUserObj: state.domainReducer.users.newUserObj,
  };
}

function bindActions(dispatch) {
  return {
    createUser: (userObj) => dispatch(UserAction.createNewUser(userObj)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(CreateUser));
