import React, { Component } from 'react';
import { Panel, Row, Col, FormGroup, ControlLabel, FormControl, InputGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CarDetail.css';
import logo from '../../../common/images/logo.jpg';
import { fetchCarDetails } from '../../../store/domain/carDetails/action';

class CarDetails extends Component {
  static propTypes = {
    fetchCarDetails: React.PropTypes.func,
    id: React.PropTypes.string,
    carDetails: React.PropTypes.object,
    loading: React.PropTypes.bool,
  };

  componentDidMount() {
    this.props.fetchCarDetails(this.props.id);
  }

  render() {

    return (
        <div className="animate">
          {
            !this.props.loading
                ?
                <Panel>
                  <div className={s.carDetails}>
                    <div className="row">
                      <div className="col-md-12">
                        <div className={`media ${s.carHeader}`}>
                          <div className="media-left">
                            <a href>
                              <img src={logo} alt="flat Avatar" className="user-avatar" height="80px"/>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="panel panel-default">
                      <div className="panel-heading">Car Details</div>
                      <div className="panel-body">
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                License Plate Number
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carDetails.licensePlateNumber}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Make
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carDetails.make}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Model
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carDetails.model}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Car Category
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carDetails.carCategory && this.props.carDetails.carCategory.name}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col md={5}>
                            <FormGroup>
                              <ControlLabel>
                                Used By
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carDetails.usedBy && this.props.carDetails.usedBy.fname ? `${this.props.carDetails.usedBy.fname} ${this.props.carDetails.usedBy.lname}` : ''}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                          <Col md={5} mdOffset={1}>
                            <FormGroup>
                              <ControlLabel>
                                Can Be Used
                              </ControlLabel>
                              <InputGroup>
                                {this.props.carDetails.canBeUsed ? 'Yes' : 'No'}
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        </Row>
                      </div>
                    </div>
                  </div>
                </Panel>
                :
                <div className="loading-wrap" style={{ minHeight: 500 }}>
                  <div className="loading">
                    <div id="spinner">
                      <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                        <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33"
                                r="30"/>
                      </svg>
                    </div>
                  </div>
                </div>
          }
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.domainReducer.currentCar.loading,
    carDetails: state.domainReducer.currentCar.currentCarDetails.data,
  };
}

function bindActions(dispatch) {
  return {
    fetchCarDetails: (id) => dispatch(fetchCarDetails(id)),
  };
}

export default connect(mapStateToProps, bindActions)(withStyles(s)(CarDetails));
