import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/car/:id',
  async action({ params }) {
    const CarDetail = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./CarDetail').default);
      }, 'carDetail');
    });
    return {
      title: 'CarDetail',
      component: <Layout><CarDetail key="carDetail" id={params.id} /></Layout>,
    };
  },
};
