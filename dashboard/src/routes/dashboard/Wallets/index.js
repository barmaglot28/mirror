import React from 'react';
import Layout from '../../../components/Layout';

export default {
  path: '/wallets',
  async action() {
    const Wallets = await new Promise((resolve) => {
      require.ensure([], (require) => {
        resolve(require('./Wallets').default);
      }, 'wallets');
    });
    return {
      title: 'Wallets',
      component: <Layout><Wallets key="wallets" /></Layout>,
    };
  },
};
