import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Pagination, Panel, Button, Modal, FormControl } from 'react-bootstrap';
import moment from 'moment';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Wallets.css';
import walletsAction from '../../../store/domain/wallets/action';

class Wallets extends Component {

  static propTypes = {
    fetchWalletList: React.PropTypes.func,
    addWalletBalance: React.PropTypes.func,
    changeFilterText: React.PropTypes.func,
    filterRiders: React.PropTypes.func,

    loading: React.PropTypes.bool,
    meta: React.PropTypes.object,
    errorMsg: React.PropTypes.string,

    walletList: React.PropTypes.array,
    filteredText: React.PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      showModal: false,
      walletIndex: null,
      value: null,
    };
    this.handlePagination = this.handlePagination.bind(this);
    this.addBalance = this.addBalance.bind(this);
    this.onFilterRiders = this.onFilterRiders.bind(this);
    this.callback = this.callback.bind(this);
  }

  componentWillMount() {
    this.props.fetchWalletList(this.state.activePage);
  }

  handlePagination(eventkey) {
    this.setState({ activePage: eventkey });
    if (this.props.filteredText) {
      this.props.filterRiders(eventkey);
    } else {
      this.props.fetchWalletList(eventkey);
    }
  }

  addBalance() {
    this.props.addWalletBalance(this.state.wallet._id, this.state.value);
    this.setState({
      showModal: false,
      wallet: null,
      value: null
    });
  }

  onFilterRiders(e) {
    this.props.changeFilterText(e.target.value);
    clearTimeout(this.timerId);
    this.timerId = setTimeout(this.callback, 2000);
  }

  callback() {
    this.setState({
      activePage: 1
    });
    clearTimeout(this.timerId);
    this.props.filterRiders(this.state.activePage);
  }

  render() {
    if (this.props.errorMsg) {
      return (<h4>
        {this.props.errorMsg}
      </h4>);
    }
    return (
      <div className={`animate ${s.wallets}`}>
        <Panel  style={{ 'background-color': '#f5f5f5' }}>
          <h4>Wallets</h4>
        </Panel>
        {
          this.props.loading
            ?
            <div id="table" className="loading-wrap" style={{ minHeight: 500 }} key="spinnerdiv">
              <div className="loading">
                <div id="spinner">
                  <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66">
                    <circle className="path" fill="none" strokeWidth="4" strokeLinecap="round" cx="33" cy="33" r="30" />
                  </svg>
                </div>
              </div>
            </div>
            :
            <div key="dddiv">
              <div className="row">
                <div className="col-sm-12">
                  <div className="panel panel-body">
                    <FormControl
                      type="text"
                      value={this.props.filteredText}
                      step={0.1}
                      placeholder="Enter rider name"
                      onChange={this.onFilterRiders}
                    />
                    {
                      !this.props.walletList || this.props.walletList.length === 0
                        ?
                        <div> No wallets </div>
                        :
                        <div className="table-responsive">
                          <table className="table wallets-table">
                            <tbody>
                            <tr>
                              <th>S.No</th>
                              <th>User name</th>
                              <th>User type</th>
                              <th>Payment method</th>
                              <th>Updated at</th>
                              <th>Created at</th>
                              <th>Wallet balance</th>
                              <th>Commission to Cabdo</th>
                              <th>Strip account id</th>
                              <th>User e-mail</th>
                            </tr>
                            {
                              this.props.walletList.map((item, index) => (
                                <tr key={index}>
                                  <td>
                                    {
                                      (
                                        (this.state.activePage - 1) * this.props.meta.limit
                                      ) + (index + 1)
                                    }
                                  </td>
                                  <td>{`${item.userId.fname} ${item.userId.lname}`}</td>
                                  <td>{item.userType}</td>
                                  <td className="center">{item.paymentMethod}</td>
                                  <td>{moment(item.updatedAt).format('Do MMM [at] HH:mm YYYY')}</td>
                                  <td>{moment(item.createdAt).format('Do MMM [at] HH:mm YYYY')}</td>
                                  <td className="center">
                                    <span>{item.walletBalance}</span>
                                    <Button className="ballance" onClick={() => { this.setState({ showModal: true, wallet: item }) }}>
                                      <i className="glyphicon glyphicon glyphicon-plus" />
                                    </Button>
                                  </td>
                                  <td className="center">{item.commissionToCabdo}</td>
                                  <td>{item.stripeAccountId ? item.stripeAccountId : '-----'}</td>
                                  <td>{item.userEmail}</td>
                                </tr>),
                              )
                            }
                            </tbody>
                          </table>
                          <Pagination
                            bsSize="medium"
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            maxButtons={5}
                            items={this.props.meta ? this.props.meta.totalNoOfPages : 1}
                            activePage={this.state.activePage}
                            onSelect={this.handlePagination}
                          />
                        </div>
                    }
                  </div>
                </div>
              </div>
            </div>
        }
        <Modal show={this.state.showModal} onHide={() => { this.setState({ showModal: false, userEmail: null }) }}>
          <Modal.Header closeButton>
            <Modal.Title>Add to ballance</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Enter sum if you want to add balance to user - {this.state.wallet && this.state.wallet.userEmail}</p>
            <FormControl
              type="number"
              value={this.state.value}
              step={0.1}
              placeholder="Enter summ"
              onChange={(e) => { this.setState({ value: e.target.value }) }}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.addBalance} disabled={!this.state.value}>Add</Button>
            <Button onClick={() => { this.setState({ showModal: false, userEmail: null }) }}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    loading: state.domainReducer.wallets.loading,
    meta: state.domainReducer.wallets.meta,
    errorMsg: state.domainReducer.wallets.errorMsg,

    walletList: state.domainReducer.wallets.walletList,
    filteredText: state.domainReducer.wallets.filteredText,
  };
}

function bindActions(dispatch) {
  return {
    fetchWalletList: (pageNo) => dispatch(walletsAction.fetchWalletList(pageNo)),
    addWalletBalance: (id, value) => dispatch(walletsAction.addWalletBalance(id, value)),
    changeFilterText: (value) => dispatch(walletsAction.changeFilterText(value)),
    filterRiders: (pageNo) => dispatch(walletsAction.filterRiders(pageNo)),
  };
}
export default connect(mapStateToProps, bindActions)(withStyles(s)(Wallets));
