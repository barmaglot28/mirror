/* eslint-disable global-require */

// The top-level (parent) route
import createTrips from './dashboard/CreateTrips';
import createUser from './dashboard/CreateNewUser';
import createCar from './dashboard/CreateNewCar';
import createCarcategories from './dashboard/CreateNewCarcategories';
// import createReferralcodes from './dashboard/CreateNewReferralcodes';
import createCouponcodes from './dashboard/CreateNewCouponcodes';
import Dashboard from './dashboard/Dashboard';

export default {

  path: '/',

  // Keep in mind, routes are evaluated in order
  children: [
    require('./login').default,
    require('./dashboard/protectedRoutes').default,
    createTrips,
    createUser,
    createCar,
    createCarcategories,
    Dashboard,
  // createReferralcodes,
    createCouponcodes,
    // Wildcard routes, e.g. { path: '*', ... } (must go last)
    require('./notFound').default,
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next();
    route.title = `${route.title || 'Untitled Page'}`;
    route.description = route.description || '';
    return route;
  },

};
