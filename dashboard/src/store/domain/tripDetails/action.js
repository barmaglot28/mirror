import {
  FETCHING_TRIP_DETAILS,
  FETCH_TRIP_DETAIL_SUCCESS,
  FETCH_TRIP_DETAIL_FAILED,
} from './actionType';
import TripDetailService from '../../../services/tripDetails';

// for user deatils
function fetchingTripDetails() {
  return {
    type: FETCHING_TRIP_DETAILS,
  };
}

function fetchTripDetailsSuccess(data) {
  return {
    type: FETCH_TRIP_DETAIL_SUCCESS,
    data,
  };
}

function fetchTripDetailsFailed(data) {
  return {
    type: FETCH_TRIP_DETAIL_FAILED,
    data,
  };
}

function fetchTripDetails(id) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingTripDetails());
    TripDetailService.getTripDetail(token, id)
    // .then((response) => {
    //   if (response.status === 401) {
    //     return dispatch({ type: 'FLUSH_DATA' });
    //   }
    //   return response.json();
    // })
    .then((response) => {
      if (response.success) {
        dispatch(fetchTripDetailsSuccess(response.data));
      }
      // else {
      //   dispatch(fetchTripDetailsFailed(response));
      // }
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchTripDetailsFailed(e));
    });
  };
}


export default { fetchTripDetails };
