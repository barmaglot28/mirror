import {
  OPEN_CAR_DETAILS_MODAL,
  CLOSE_CAR_DETAILS_MODAL,
  UPDATING_CAR_DETAILS_OBJECT,
  UPDATING_CAR_DETAILS_OBJECT_SUCCESS,
  UPDATING_CAR_DETAILS_OBJECT_FAILED,
} from './actionType';


import {
  UPDATE_CAR_LIST_OBJECT,
} from '../cars/actionType';

import carService from '../../../services/cars';

function openCarDetailModal(carObj) {
  return {
    type: OPEN_CAR_DETAILS_MODAL,
    modalStatus: true,
    carObj,
  };
}
function closeCarDetailModal() {
  return {
    type: CLOSE_CAR_DETAILS_MODAL,
    modalStatus: false,
  };
}

function updatingCarObject() {
  return {
    type: UPDATING_CAR_DETAILS_OBJECT,
  };
}

function updatingCarObjectSuccess() {
  return {
    type: UPDATING_CAR_DETAILS_OBJECT_SUCCESS,
  };
}
function updatingCarObjectFailed() {
  return {
    type: UPDATING_CAR_DETAILS_OBJECT_FAILED,
  };
}

function updateCarList(carListObject) {
  return {
    type: UPDATE_CAR_LIST_OBJECT,
    carList: carListObject,
  };
}

function updateCarListArray(updatedCarObj) {
  const newCarObj = updatedCarObj.data;
  // console.log('inside updateCarListArray with data', newCarObj);
  return (dispatch, getState) => {
    const newCarListObj = getState().domainReducer.cars.carList;
    // console.log('inside updateCarListArray with data', newCarListObj);
    const carList = newCarListObj.data;
    for (let i = 0; i < carList.length; i += 1) {
      if (carList[i]._id === newCarObj._id) { // eslint-disable-line
        carList[i] = newCarObj;
        break;
      }
    }
    newCarListObj.data = carList;
    dispatch(updateCarList(newCarListObj));
  };
}

function updateCarObject(carObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(updatingCarObject());
    carService.updateCarDetails(token, carObj)
    .then((response) => {
      dispatch(updatingCarObjectSuccess());
      dispatch(updateCarListArray(response));
      dispatch(closeCarDetailModal());
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(updatingCarObjectFailed());
    });
  };
}


export default { openCarDetailModal, closeCarDetailModal, updateCarObject };
