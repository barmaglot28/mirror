import {
  OPEN_CAR_DETAILS_MODAL,
  CLOSE_CAR_DETAILS_MODAL,
  UPDATING_CAR_DETAILS_OBJECT,
  UPDATING_CAR_DETAILS_OBJECT_SUCCESS,
  UPDATING_CAR_DETAILS_OBJECT_FAILED,
} from './actionType';

const initialState = {
  showCarUpdateModal: false,
  carObjectToUpdate: {},
  errorOnCarObjectUpdate: false,
  updateCarObjectLoading: false,
};


const updateTrip = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_CAR_DETAILS_MODAL:
      return {
        ...state,
        showCarUpdateModal: action.modalStatus,
        carObjectToUpdate: action.carObj,
      };
    case CLOSE_CAR_DETAILS_MODAL:
      return { ...state, showCarUpdateModal: action.modalStatus, carObjectToUpdate: {} };
    case UPDATING_CAR_DETAILS_OBJECT:
      return { ...state, updateCarObjectLoading: true };
    case UPDATING_CAR_DETAILS_OBJECT_SUCCESS:
      return { ...state, updateCarObjectLoading: false };
    case UPDATING_CAR_DETAILS_OBJECT_FAILED:
      return { ...state, updateCarObjectLoading: false, errorOnCarObjectUpdate: true };
    default:
      return { ...state };
  }
};

export default updateTrip;
