import {
  GET_TRIPS_PER_DAY,
  GET_TRIPS_SUCCESS,
  GET_TRIPS_FAILED,
  FETCH_REVENUE_GRAPH_DATA_PER_DAY,
  FETCH_REVENUE_GRAPH_DATA_PER_DAY_SUCCESS,
  FETCH_REVENUE_GRAPH_DATA_PER_DAY_FAILED,
  FETCH_TRIPS_GRAPH_DATA_PER_DAY,
  FETCH_TRIPS_GRAPH_DATA_PER_DAY_SUCCESS,
  FETCH_TRIPS_GRAPH_DATA_PER_DAY_FAILED,
  FETCHING_TRIPS,
  FETCH_TRIPS_SUCCESS,
  FETCH_TRIPS_FAILED,
  FETCH_REVENUE_GRAPH_DATA,
  FETCH_REVENUE_GRAPH_DATA_SUCCESS,
  FETCH_REVENUE_GRAPH_DATA_FAILED,
  UPDATE_TRIP_LIST_OBJECT,
  CREATING_NEW_TRIP,
  CREATING_NEW_TRIP_SUCCESS,
  CREATING_NEW_TRIP_FAILED,
} from './actionType';

const initialState = {
  loading: true,
  tripList: {
    data: [],
  },
  pageNo: 1,
  revenueGraphLoadingPerDay: true,
  revenueGraphDataPerDay: null,
  revenueGraphLoading: true,
  revenueGraphData: null,
  meta: null,
  tripFilter: 'All',
  requestTripLoading: false,
  errorStatusCreateTripObj: false,
  newTripObj: {},
  errorTripObj: {},
  tripsGraphLoadingPerDay: true,
  tripsGraphDataPerDay: null,
};

function prepareTripsSuccessData(result){
  if ('data' in result.data && result.data.data.length) {
    return result.data;
  } else {
    return null;
  }
}

const trips = (state = initialState, action) => {
  switch (action.type) {
    case GET_TRIPS_PER_DAY:
      return { ...state, loading: true, time: action.date };
    case GET_TRIPS_SUCCESS:
      return {
        ...state,
        tripList: prepareTripsSuccessData(action),
        loading: false,
        meta: action.data.meta,
      };
    case GET_TRIPS_FAILED:
      return { ...state, errorStatusCreateTripObj: true, loading: false };
    case FETCH_REVENUE_GRAPH_DATA_PER_DAY:
      return { ...state, revenueGraphLoadingPerDay: true };
    case FETCH_REVENUE_GRAPH_DATA_PER_DAY_SUCCESS:
      return {
        ...state,
        revenueGraphLoadingPerDay: false,
        revenueGraphDataPerDay: action.data,
      };
    case FETCH_REVENUE_GRAPH_DATA_PER_DAY_FAILED:
      return { ...state, revenueGraphLoadingPerDay: false };
    case FETCH_TRIPS_GRAPH_DATA_PER_DAY:
      return { ...state, tripsGraphLoadingPerDay: true };
    case FETCH_TRIPS_GRAPH_DATA_PER_DAY_SUCCESS:
      return {
        ...state,
        tripsGraphLoadingPerDay: false,
        tripsGraphDataPerDay: action.data,
      };
    case FETCH_TRIPS_GRAPH_DATA_PER_DAY_FAILED:
      return { ...state, tripsGraphLoadingPerDay: false };
    case FETCHING_TRIPS:
      return { ...state, loading: true, tripFilter: action.filter };
    case FETCH_TRIPS_SUCCESS:
      return {
        ...state,
        tripList: action.data,
        loading: false,
        meta: action.data.meta,
      };
    case FETCH_TRIPS_FAILED:
      return { ...state, loading: false, errorStatusCreateTripObj: true };
    case FETCH_REVENUE_GRAPH_DATA:
      return { ...state, revenueGraphLoading: true };
    case FETCH_REVENUE_GRAPH_DATA_SUCCESS:
      return {
        ...state,
        revenueGraphLoading: false,
        revenueGraphData: action.data,
      };
    case FETCH_REVENUE_GRAPH_DATA_FAILED:
      return { ...state, revenueGraphLoading: false };
    case CREATING_NEW_TRIP:
      return { ...state, requestTripLoading: true };
    case CREATING_NEW_TRIP_SUCCESS:
      return { ...state, requestTripLoading: false, newTripObj: action.newTripObj };
    case CREATING_NEW_TRIP_FAILED:
      return {
        ...state,
        requestTripLoading: false,
        errorTripObj: action.errorTripObj,
        errorStatusCreateTripObj: true,
      };
    case UPDATE_TRIP_LIST_OBJECT: {
      const tripList = Object.assign({}, action.tripList);
      return { ...state, tripList };
    }
    default:
      return state;
  }
};

export default trips;
