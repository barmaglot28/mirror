import {
  GET_TRIPS_SUCCESS,
  GET_TRIPS_FAILED,
  GET_TRIPS_PER_DAY,
  FETCH_REVENUE_GRAPH_DATA_PER_DAY,
  FETCH_REVENUE_GRAPH_DATA_PER_DAY_SUCCESS,
  FETCH_REVENUE_GRAPH_DATA_PER_DAY_FAILED,
  FETCH_TRIPS_GRAPH_DATA_PER_DAY,
  FETCH_TRIPS_GRAPH_DATA_PER_DAY_SUCCESS,
  FETCH_TRIPS_GRAPH_DATA_PER_DAY_FAILED,
  FETCHING_TRIPS,
  FETCH_TRIPS_SUCCESS,
  FETCH_TRIPS_FAILED,
  FETCH_REVENUE_GRAPH_DATA,
  FETCH_REVENUE_GRAPH_DATA_SUCCESS,
  FETCH_REVENUE_GRAPH_DATA_FAILED,
  CREATING_NEW_TRIP,
  CREATING_NEW_TRIP_SUCCESS,
  CREATING_NEW_TRIP_FAILED,
} from './actionType';

import TripService from '../../../services/trip';

function getTripsPerDay(date){
  return {
    type: GET_TRIPS_PER_DAY,
    date
  }
}

function getTripsPerDaySuccess(data){
  return {
    type: GET_TRIPS_SUCCESS,
    data
  }
}

function getTripsPerDayFailed(data) {
  return {
    type: GET_TRIPS_FAILED,
    data,
  };
}

function fetchAllTripsPerDayRevenue(date){
  return {
    type: FETCH_REVENUE_GRAPH_DATA_PER_DAY,
  }
}

function fetchAllTripsPerDayRevenueSuccess(data){
  return {
    type: FETCH_REVENUE_GRAPH_DATA_PER_DAY_SUCCESS,
    data
  }
}

function fetchAllTripsPerDayRevenueFailed(data) {
  return {
    type: FETCH_REVENUE_GRAPH_DATA_PER_DAY_FAILED,
    data,
  };
}

function fetchAllTripsPerDay(date){
  return {
    type: FETCH_TRIPS_GRAPH_DATA_PER_DAY,
  }
}

function fetchAllTripsPerDaySuccess(data){
  return {
    type: FETCH_TRIPS_GRAPH_DATA_PER_DAY_SUCCESS,
    data
  }
}

function fetchAllTripsPerDayFailed(data) {
  return {
    type: FETCH_TRIPS_GRAPH_DATA_PER_DAY_FAILED,
    data,
  };
}

function fetchingTrip(filter) {
  return {
    type: FETCHING_TRIPS,
    filter,
  };
}

function fetchTripSuccess(data) {
  return {
    type: FETCH_TRIPS_SUCCESS,
    data,
  };
}

function fetchTripFailed(data) {
  return {
    type: FETCH_TRIPS_FAILED,
    data,
  };
}

function fetchTripRevenue() {
  return {
    type: FETCH_REVENUE_GRAPH_DATA,
  };
}

function fetchTripRevenueSuccess(data) {
  return {
    type: FETCH_REVENUE_GRAPH_DATA_SUCCESS,
    data,
  };
}

function fetchTripRevenueFailed(data) {
  return {
    type: FETCH_REVENUE_GRAPH_DATA_FAILED,
    data,
  };
}

function requestTripCreate() {
  return {
    type: CREATING_NEW_TRIP,
  };
}

function requestTripCreateSuccess(data) {
  return {
    type: CREATING_NEW_TRIP_SUCCESS,
    newTripObj: data,
  };
}
function requestTripCreateFailed(data) {
  return {
    type: CREATING_NEW_TRIP_FAILED,
    errorTripObj: data,
  };
}

function getAllTripsPerDay(date){
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(getTripsPerDay(date));
    TripService.getAllTripsPerDay(token, date)
      .then((response) => {
      console.log('getAllTripsPerDay', response);
        if (response.success) {
          dispatch(getTripsPerDaySuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(getTripsPerDayFailed(e));
      });
  };
}

function getAllTripsPerDayRevenueData(date) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchAllTripsPerDayRevenue());
    TripService.getAllTripsPerDayRevenueDetails(token, date)
      .then((response) => {
        if (response.success) {
          dispatch(fetchAllTripsPerDayRevenueSuccess(response));
        }
         else {
          dispatch(fetchAllTripsPerDayRevenueFailed(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchAllTripsPerDayRevenueFailed(e));
      });
  };
}

function getAllTripsPerDayData(date) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchAllTripsPerDay());
    TripService.getAllTripsPerDayDetails(token, date)
      .then((response) => {
        if (response.success) {
          dispatch(fetchAllTripsPerDaySuccess(response));
        }
        else {
          dispatch(fetchAllTripsPerDayFailed(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchAllTripsPerDayFailed(e));
      });
  };
}


function fetchTrips(pageNo, filter) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingTrip(filter));
    TripService.getAllTripDetails(token, pageNo, filter)
    // .then((response) => {
    //   if (response.status === 401) {
    //     return dispatch({ type: 'FLUSH_DATA' });
    //   }
    //   return response.json();
    // })
    .then((response) => {
      if (response.success) {
        dispatch(fetchTripSuccess(response));
      }
      // else {
      //   dispatch(fetchTripFailed(response));
      // }
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchTripFailed(e));
    });
  };
}

function fetctTripRevenueData(year = new Date().getFullYear()) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchTripRevenue());
    TripService.getTripRevenueDetails(token, year)
    // .then((response) => {
    //   if (response.status === 401) {
    //     return dispatch({ type: 'FLUSH_DATA' });
    //   }
    //   return response.json();
    // })
    .then((response) => {
      if (response.success) {
        dispatch(fetchTripRevenueSuccess(response));
      }
      //  else {
      //   dispatch(fetchTripRevenueFailed(response));
      // }
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchTripRevenueFailed(e));
    });
  };
}


function createNewTrip(tripObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(requestTripCreate());
    TripService.createTrip(token, tripObj)
    .then((response) => {
      dispatch(requestTripCreateSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(requestTripCreateFailed(e));
    });
  };
}

export default { getAllTripsPerDay, getAllTripsPerDayRevenueData, getAllTripsPerDayData, fetchTrips, fetctTripRevenueData, createNewTrip };
