import {
  FETCHING_WALLET_LIST,
  FETCH_WALLET_LIST_SUCCESS,
  FETCH_WALLET_LIST_FAILED,

  ADD_BALANCE_SUCCESS,
  ADD_BALANCE_FAILED,

  CHANGE_FILTER_TEXT,

  FILTERED_RIDER_LIST_PROCESS,
  FILTERED_RIDER_LIST_PROCESS_SUCCESS,
  FILTERED_RIDER_LIST_PROCESS_FAILED
} from './actionType';
import WalletsService from '../../../services/wallets';

function fetchingWalletList() {
  return {
    type: FETCHING_WALLET_LIST,
  };
}

function fetchWalletListSuccess(data) {
  return {
    type: FETCH_WALLET_LIST_SUCCESS,
    data,
  };
}

function fetchWalletListFailed(data) {
  return {
    type: FETCH_WALLET_LIST_FAILED,
    data,
  };
}

function addWalletBalanceSuccess(id, value) {
  return {
    type: ADD_BALANCE_SUCCESS,
    id,
    value
  };
}

function addWalletBalanceFailed(data) {
  return {
    type: ADD_BALANCE_FAILED,
    data,
  };
}

function changeFilterTextSuccess(value) {
  return {
    type: CHANGE_FILTER_TEXT,
    value,
  };
}

function filteredRiderListProcess() {
  return {
    type: FILTERED_RIDER_LIST_PROCESS,
  };
}

function filteredRiderListProcessSuccess(data) {
  return {
    type: FILTERED_RIDER_LIST_PROCESS_SUCCESS,
    data,
  };
}

function filteredRiderListProcessFailed(data) {
  return {
    type: FILTERED_RIDER_LIST_PROCESS_FAILED,
    data,
  };
}

function fetchWalletList(pageNo) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingWalletList());
    WalletsService.getAllWallets(token, pageNo)
      .then((response) => {
        dispatch(fetchWalletListSuccess(response));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchWalletListFailed(e));
      });
  };
}

function addWalletBalance(id, value) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    WalletsService.addWalletBalance(token, id, value)
      .then((response) => {
        if (response.success) {
          dispatch(addWalletBalanceSuccess(id, value));
        } else {
          dispatch(addWalletBalanceFailed(response.message));
        }

      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(addWalletBalanceFailed(e));
      });
  };
}

function changeFilterText(value) {
  return (dispatch, getState) => {
      dispatch(changeFilterTextSuccess(value));
  };
}

function filterRiders(pageNo) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    const filteredText = getState().domainReducer.wallets.filteredText;
    dispatch(filteredRiderListProcess());
    WalletsService.filterRiderList(token, filteredText, pageNo)
      .then((response) => {
        if (response.success) {
          dispatch(filteredRiderListProcessSuccess(response));
        } else {
          dispatch(filteredRiderListProcessFailed(response.message));
        }

      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(filteredRiderListProcessFailed(e));
      });
  };
}

export default { fetchWalletList, addWalletBalance, changeFilterText, filterRiders };
