import {
  FETCHING_WALLET_LIST,
  FETCH_WALLET_LIST_SUCCESS,
  FETCH_WALLET_LIST_FAILED,

  ADD_BALANCE_SUCCESS,
  ADD_BALANCE_FAILED,

  CHANGE_FILTER_TEXT,

  FILTERED_RIDER_LIST_PROCESS,
  FILTERED_RIDER_LIST_PROCESS_SUCCESS,
  FILTERED_RIDER_LIST_PROCESS_FAILED
} from './actionType';

const initialState = {
  loading: false,
  meta: null,
  errorMsg: '',

  walletList: [],
  filteredText: ''
};

const wallets = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_WALLET_LIST:
      return { ...state, loading: true, errorMsg: false };

    case FETCH_WALLET_LIST_SUCCESS:
      return {
        ...state,
        walletList: action.data.data,
        loading: false,
        meta: action.data.meta
      };

    case FETCH_WALLET_LIST_FAILED:
      return { ...state, loading: false, errorMsg: action.data.message };

    case ADD_BALANCE_SUCCESS:
      let walletList = state.walletList.slice();
      let index = walletList.map(item => item._id).indexOf(action.id);
      walletList[index].walletBalance += Number(action.value);
      walletList[index].updatedAt = Date.now();
      return {
        ...state,
        walletList: walletList,
      };

    case ADD_BALANCE_FAILED:
      return { ...state, errorMsg: action.data.message };

    case CHANGE_FILTER_TEXT:
      return { ...state, filteredText: action.value };

    case FILTERED_RIDER_LIST_PROCESS:
      return { ...state, loading: true, errorMsg: false };

    case FILTERED_RIDER_LIST_PROCESS_SUCCESS:
      return {
        ...state,
        walletList: action.data.data,
        loading: false,
        meta: action.data.meta
      };

    case FILTERED_RIDER_LIST_PROCESS_FAILED:
      return { ...state, loading: false, errorMsg: action.data.message };

    default:
      return state;
  }
};

export default wallets;
