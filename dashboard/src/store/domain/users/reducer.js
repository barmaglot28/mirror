import {
  FETCHING_CORPORATE,
  FETCH_CORPORATE_SUCCESS,
  FETCH_CORPORATE_FAILED,
  FETCHING_DRIVER,
  FETCH_DRIVER_SUCCESS,
  FETCH_DRIVER_FAILED,
  FETCHING_RIDER,
  FETCH_RIDER_SUCCESS,
  FETCH_RIDER_FAILED,


  FETCHING_DRIVERS,
  FETCH_DRIVERS_SUCCESS,
  FETCH_DRIVERS_FAILED,
  // FETCH_USER_STATS_CHARTS_SUCCESS,
  // FETCH_USER_STATS_CHARTS_FAILED,
  // FETCHING_USER_STATS_CHARTS,
  CREATE_NEW_USER_REQ,
  CREATE_NEW_USER_REQ_SUCCESS,
  CREATE_NEW_USER_REQ_FAILED,
  REMOVE_USER_REQ,
  REMOVE_USER_REQ_SUCCESS,
  REMOVE_USER_REQ_FAILED,
  UPDATE_USER_LIST_OBJECT,


  SEND_SMS_TO_ALL,
  SEND_SMS_TO_ALL_SUCCESS,
  SEND_SMS_TO_ALL_FAILED,

  SEND_EMAIL_TO_ALL,
  SEND_EMAIL_TO_ALL_SUCCESS,
  SEND_EMAIL_TO_ALL_FAILED,

  CHANGE_FILTER_TEXT,

  FILTER_USERS_PROCESS,
  FILTER_USERS_SUCCESS,
  FILTER_USERS_FAILED,
} from './actionType';

const initialState = {
  loading: false,
  meta: null,
  failedUserListApi: false,
  errorMsg: '',

  userList: [],
  // failedUserChartApi: false,

  // chartData: {},
  // chartLoading: true,
  newUserLoading: false,
  failedCreateUserApi: false,
  createUserErrorObj: {},
  removeUserErrorObj: {},
  removeUserSuccess: {},
  newUserObj: {},

  sendingSms: false,
  sendSmsSuccess: false,

  sendEmail: false,
  sendEmailSuccess: false,

  filteredText: ''
};

const users = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_DRIVERS:
    case FILTER_USERS_PROCESS:
    case FETCHING_CORPORATE:
    case FETCHING_DRIVER:
    case FETCHING_RIDER:
    case REMOVE_USER_REQ:
      return { ...state, loading: true, failedUserListApi: false, removeUserSuccess: {}, errorMsg: '' };
    case FETCH_DRIVERS_SUCCESS:
      return {
        ...state,
        userList: action.data,
        loading: false,
        meta: action.data.meta,
      };
    case FETCH_DRIVERS_FAILED:
      return { ...state, loading: false, failedUserListApi: true };

    case FETCH_CORPORATE_SUCCESS:
      return {
        ...state,
        loading: false,
        meta: action.data.meta,
        userList: action.data,
      };

    case FETCH_CORPORATE_FAILED:
      return { ...state, loading: false, failedUserListApi: true, errorMsg: action.error };

    case FETCH_DRIVER_SUCCESS:
      return {
        ...state,
        loading: false,
        meta: action.data.meta,
        userList: action.data,
      };

    case FETCH_DRIVER_FAILED:
      return { ...state, loading: false, failedUserListApi: true, errorMsg: action.error };


    case FETCH_RIDER_SUCCESS:
      return {
        ...state,
        loading: false,
        meta: action.data.meta,
        userList: action.data,
      };

    case FETCH_RIDER_FAILED:
      return { ...state, loading: false, failedUserListApi: true, errorMsg: action.error };
    // case FETCHING_USER_STATS_CHARTS:
    //   return { ...state, chartLoading: true, failedUserChartApi: false, removeUserSuccess: {} };
    // case FETCH_USER_STATS_CHARTS_SUCCESS:
    //   return { ...state, chartLoading: false, chartData: action.data, failedUserChartApi: false };
    // case FETCH_USER_STATS_CHARTS_FAILED:
    //   return { ...state, chartLoading: false, failedUserChartApi: true };
    case CREATE_NEW_USER_REQ:
      return { ...state, newUserLoading: true, failedCreateUserApi: false, createUserErrorObj: {}, removeUserSuccess: {} };
    case CREATE_NEW_USER_REQ_SUCCESS:
      return {
        ...state,
        newUserLoading: false,
        failedCreateUserApi: false,
        newUserObj: action.newUser,
        createUserErrorObj: {},
      };
    case CREATE_NEW_USER_REQ_FAILED:
      return {
        ...state,
        newUserLoading: false,
        failedCreateUserApi: true,
        createUserErrorObj: action.createUserErrorObj,
      };

    case REMOVE_USER_REQ_SUCCESS:
      return {
        ...state,
        loading: false,
        failedUserListApi: false,
        removeUserSuccess: action.removeUserSuccess
      };
    case REMOVE_USER_REQ_FAILED:
      return {
        ...state,
        loading: false,
        failedUserListApi: true,
        removeUserErrorObj: action.removeUserErrorObj
      };
    case UPDATE_USER_LIST_OBJECT: {
      const userList = Object.assign({}, action.userList);
      return { ...state, userList };
    }

    case SEND_SMS_TO_ALL:
      return { ...state, sendingSms: true };

    case SEND_SMS_TO_ALL_SUCCESS:
      return { ...state, sendSmsSuccess: true, sendingSms: false };

    case SEND_SMS_TO_ALL_FAILED:
      return { ...state, sendSmsSuccess: false, sendingSms: false };

    case SEND_EMAIL_TO_ALL:
      return { ...state, sendingEmail: true };
    case SEND_EMAIL_TO_ALL_SUCCESS:
      return { ...state, sendEmailSuccess: true, sendingEmail: false };
    case SEND_EMAIL_TO_ALL_FAILED:
      return { ...state, sendEmailSuccess: false, sendingEmail: false };

    case CHANGE_FILTER_TEXT:
      return { ...state, filteredText: action.value };


    case FILTER_USERS_SUCCESS:
      return { ...state,
        loading: false,
        meta: action.data.meta,
        userList: action.data };

    case FILTER_USERS_FAILED:
      return { ...state, loading: false, failedUserListApi: true, errorMsg: action.error.message };
    default:
      return state;
  }
};

export default users;
