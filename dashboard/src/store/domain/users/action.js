import { CORPORATE_TYPE, RIDER_TYPE, DRIVER_TYPE} from '../../constants/index';
import {
  FETCHING_CORPORATE,
  FETCH_CORPORATE_SUCCESS,
  FETCH_CORPORATE_FAILED,
  FETCHING_DRIVER,
  FETCH_DRIVER_SUCCESS,
  FETCH_DRIVER_FAILED,
  FETCHING_RIDER,
  FETCH_RIDER_SUCCESS,
  FETCH_RIDER_FAILED,
  FETCHING_DRIVERS,
  FETCH_DRIVERS_SUCCESS,
  FETCH_DRIVERS_FAILED,
  // FETCHING_USER_STATS_CHARTS,
  // FETCH_USER_STATS_CHARTS_SUCCESS,
  // FETCH_USER_STATS_CHARTS_FAILED,
  CREATE_NEW_USER_REQ,
  CREATE_NEW_USER_REQ_SUCCESS,
  CREATE_NEW_USER_REQ_FAILED,
  REMOVE_USER_REQ,
  REMOVE_USER_REQ_SUCCESS,
  REMOVE_USER_REQ_FAILED,

  SEND_SMS_TO_ALL,
  SEND_SMS_TO_ALL_SUCCESS,
  SEND_SMS_TO_ALL_FAILED,

  SEND_EMAIL_TO_ALL,
  SEND_EMAIL_TO_ALL_SUCCESS,
  SEND_EMAIL_TO_ALL_FAILED,

  CHANGE_FILTER_TEXT,

  FILTER_USERS_PROCESS,
  FILTER_USERS_SUCCESS,
  FILTER_USERS_FAILED,

} from './actionType';
import UsersService from '../../../services/users';


// function errorHandler() {
//   console.log('error handling');
//   return {
//     type: 'FLUSH_DATA',
//   };
// }

function fetchingDrivers() {
  return {
    type: FETCHING_DRIVERS,
  };
}

function fetchDriversSuccess(data) {
  return {
    type: FETCH_DRIVERS_SUCCESS,
    data,
  };
}

function fetchDriversFailed(data) {
  return {
    type: FETCH_DRIVERS_FAILED,
    data,
  };
}

function fetchingCorporate() {
  return {
    type: FETCHING_CORPORATE,
  };
}

function fetchCorporateSuccess(data) {
  return {
    type: FETCH_CORPORATE_SUCCESS,
    data,
  };
}

function fetchCorporateFailed(error) {
  return {
    type: FETCH_CORPORATE_FAILED,
    error,
  };
}

function fetchingDriver() {
  return {
    type: FETCHING_DRIVER,
  };
}

function fetchDriverSuccess(data) {
  return {
    type: FETCH_DRIVER_SUCCESS,
    data,
  };
}

function fetchDriverFailed(error) {
  return {
    type: FETCH_DRIVER_FAILED,
    error,
  };
}

function fetchingRider() {
  return {
    type: FETCHING_RIDER,
  };
}

function fetchRiderSuccess(data) {
  return {
    type: FETCH_RIDER_SUCCESS,
    data,
  };
}

function fetchRiderFailed(error) {
  return {
    type: FETCH_RIDER_FAILED,
    error,
  };
}

// function fetchingUsersStatusCharts() {
//   return {
//     type: FETCHING_USER_STATS_CHARTS,
//   };
// }
//
// function fetchUsersStatusChartsSuccess(data) {
//   return {
//     type: FETCH_USER_STATS_CHARTS_SUCCESS,
//     data,
//   };
// }
//
// function fetchUsersStatusChartsFailed(data) {
//   return {
//     type: FETCH_USER_STATS_CHARTS_FAILED,
//     data,
//   };
// }

function RequestCreateNewUser() {
  return {
    type: CREATE_NEW_USER_REQ,
  };
}

function RequestCreateNewUserSuccess(data) {
  return {
    type: CREATE_NEW_USER_REQ_SUCCESS,
    newUser: data,
  };
}

function RequestCreateNewUserFailed(data) {
  return {
    type: CREATE_NEW_USER_REQ_FAILED,
    createUserErrorObj: data,
  };
}

function RequestRemoveUser() {
  return {
    type: REMOVE_USER_REQ,
  };
}

function RequestRemoveUserSuccess(data) {
  return {
    type: REMOVE_USER_REQ_SUCCESS,
    removeUserSuccess: data,
  };
}

function RequestRemoveUserFailed(data) {
  return {
    type: REMOVE_USER_REQ_FAILED,
    removeUserErrorObj: data,
  };
}

function sendSmsToAll() {
  return {
    type: SEND_SMS_TO_ALL,
  };
}

function sendSmsToAllSuccess() {
  return {
    type: SEND_SMS_TO_ALL_SUCCESS
  };
}

function sendSmsToAllFailed(data) {
  return {
    type: SEND_SMS_TO_ALL_FAILED,
    removeUserErrorObj: data,
  };
}

function sendEmailToAll() {
  return {
    type: SEND_EMAIL_TO_ALL,
  };
}

function sendEmailToAllSuccess() {
  return {
    type: SEND_EMAIL_TO_ALL_SUCCESS,
  };
}

function sendEmailToAllFailed(data) {
  return {
    type: SEND_EMAIL_TO_ALL_FAILED,
    removeUserErrorObj: data,
  };
}


function changeFilterTextSuccess(value) {
  return {
    type: CHANGE_FILTER_TEXT,
    value,
  };
}

function filterUsersProcess() {
  return {
    type: FILTER_USERS_PROCESS,
  };
}

function filterUsersSuccess(data) {
  return {
    type: FILTER_USERS_SUCCESS,
    data
  };
}

function filterUsersFailed(data) {
  return {
    type: FILTER_USERS_FAILED,
    error: data,
  };
}

function fetchDrivers() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingDrivers());
    UsersService.getAllDrivers(token)
      .then((response) => {
        dispatch(fetchDriversSuccess(response));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchDriversFailed(e));
      });
  };
}

function fetchCorporate(pageNo) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCorporate());
    UsersService.getAllUsers(token, pageNo, CORPORATE_TYPE)
    .then((response) => {
      dispatch(fetchCorporateSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchCorporateFailed(e));
    });
  };
}

function fetchDriver(pageNo) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingDriver());
    UsersService.getAllUsers(token, pageNo, DRIVER_TYPE)
    .then((response) => {
      dispatch(fetchDriverSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchDriverFailed(e));
    });
  };
}

function fetchRider(pageNo) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingRider());
    UsersService.getAllUsers(token, pageNo, RIDER_TYPE)
    .then((response) => {
      dispatch(fetchRiderSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchRiderFailed(e));
    });
  };
}

// function fetchChartData() {
//   return (dispatch, getState) => {
//     const token = getState().domainReducer.auth.user.jwtAccessToken;
//     dispatch(fetchingUsersStatusCharts());
//     UsersService.getUserChartStats(token)
//     .then((response) => {
//       dispatch(fetchUsersStatusChartsSuccess(response));
//     })
//     .catch((e) => {
//       if (e.message === 'Unauthorized') {
//         dispatch({ type: 'FLUSH_DATA' });
//       }
//       dispatch(fetchUsersStatusChartsFailed(e));
//     });
//   };
// }

function createNewUser(userObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestCreateNewUser());
    UsersService.createUser(token, userObj)
    .then((response) => {
      dispatch(RequestCreateNewUserSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(RequestCreateNewUserFailed(e));
    });
  };
}

function removeUser(userObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestRemoveUser());
    UsersService.removeUser(token, userObj)
      .then((response) => {
        if(response.success) {
          dispatch(RequestRemoveUserSuccess(response));
        } else {
          dispatch(RequestRemoveUserFailed(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(RequestRemoveUserFailed(e));
      });
  };
}

function submitSmsToAll(messageText) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(sendSmsToAll());
    UsersService.sendSmsToAll(token, messageText)
      .then((response) => {
        if(response.success) {
          dispatch(sendSmsToAllSuccess(response));
        } else {
          dispatch(sendSmsToAllFailed(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(sendSmsToAllFailed(e));
      });
  };
}

function submitEmailToAll(messageText) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(sendEmailToAll());
    UsersService.sendEmailToAll(token, messageText)
      .then(response => {
        console.log('submitEmailToAll::response::', response);
        if(response.success) {
          dispatch(sendEmailToAllSuccess(response));
        } else {
          dispatch(sendEmailToAllFailed(response));
        }
      })
      .catch(e => {
        if(e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(sendEmailToAllFailed(e));
      });
  };
}


function changeFilterText(value) {
  return (dispatch, getState) => {
    dispatch(changeFilterTextSuccess(value));
  };
}

function filterUsers(pageNo, userType) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    const filteredText = getState().domainReducer.users.filteredText;
    dispatch(filterUsersProcess());
    UsersService.filterUsers(token, filteredText, userType, pageNo)
      .then((response) => {
        if(response.success) {
          dispatch(filterUsersSuccess(response));
        } else {
          dispatch(filterUsersFailed(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(filterUsersFailed(e));
      });
  };
}

export default { fetchCorporate, fetchDriver, fetchRider, createNewUser, fetchDrivers, removeUser, submitSmsToAll, submitEmailToAll, changeFilterText, filterUsers };
