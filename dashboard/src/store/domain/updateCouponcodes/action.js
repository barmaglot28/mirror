import {
  OPEN_COUPONCODES_DETAILS_MODAL,
  CLOSE_COUPONCODES_DETAILS_MODAL,
  UPDATING_COUPONCODES_DETAILS_OBJECT,
  UPDATING_COUPONCODES_DETAILS_OBJECT_SUCCESS,
  UPDATING_COUPONCODES_DETAILS_OBJECT_FAILED,
} from './actionType';


import {
  UPDATE_COUPONCODES_LIST_OBJECT,
} from '../couponcodes/actionType';

import couponcodesService from '../../../services/couponcodes';

function openCouponcodesDetailModal(couponcodesObj) {
  return {
    type: OPEN_COUPONCODES_DETAILS_MODAL,
    modalStatus: true,
    couponcodesObj,
  };
}
function closeCouponcodesDetailModal() {
  return {
    type: CLOSE_COUPONCODES_DETAILS_MODAL,
    modalStatus: false,
  };
}

function updatingCouponcodesObject() {
  return {
    type: UPDATING_COUPONCODES_DETAILS_OBJECT,
  };
}

function updatingCouponcodesObjectSuccess() {
  return {
    type: UPDATING_COUPONCODES_DETAILS_OBJECT_SUCCESS
  };
}
function updatingCouponcodesObjectFailed() {
  return {
    type: UPDATING_COUPONCODES_DETAILS_OBJECT_FAILED,
  };
}

function updateCouponcodesList(couponcodesListObject) {
  return {
    type: UPDATE_COUPONCODES_LIST_OBJECT,
    couponcodesList: couponcodesListObject,
  };
}

function updateCouponcodesListArray(updatedCouponcodesObj) {
  const newCouponcodesObj = updatedCouponcodesObj.data;
  // console.log('inside updateUserListArray with data', newUserObj);
  return (dispatch, getState) => {
    const newCouponcodesListObj = getState().domainReducer.couponcodes.couponcodesList;
    // console.log('inside updateUserListArray with data', newUserListObj);
    const couponcodesList = newCouponcodesListObj.data;
    for (let i = 0; i < couponcodesList.length; i += 1) {
      if (couponcodesList[i]._id == newCouponcodesObj._id) { // eslint-disable-line
        couponcodesList[i] = newCouponcodesObj;
        break;
      }
    }
    newCouponcodesListObj.data = couponcodesList;
    dispatch(updateCouponcodesList(newCouponcodesListObj));
  };
}

function updateCouponcodesObject(couponcodesObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(updatingCouponcodesObject());
    couponcodesService.updateCouponcodesDetails(token, couponcodesObj)
    .then((response) => {
      dispatch(updatingCouponcodesObjectSuccess());
      dispatch(updateCouponcodesListArray(response));
      dispatch(closeCouponcodesDetailModal());
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(updatingCouponcodesObjectFailed());
    });
  };
}


export default { openCouponcodesDetailModal, closeCouponcodesDetailModal, updateCouponcodesObject };
