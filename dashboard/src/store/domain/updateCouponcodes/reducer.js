import {
  OPEN_COUPONCODES_DETAILS_MODAL,
  CLOSE_COUPONCODES_DETAILS_MODAL,
  UPDATING_COUPONCODES_DETAILS_OBJECT,
  UPDATING_COUPONCODES_DETAILS_OBJECT_SUCCESS,
  UPDATING_COUPONCODES_DETAILS_OBJECT_FAILED,
} from './actionType';

const initialState = {
  showCouponcodesupdateModal: false,
  couponcodesObjectToUpdate: {},
  errorOnCouponcodesObjectUpdate: false,
  updateCouponcodesObjectLoading: false,
};


const updateCouponcodes = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_COUPONCODES_DETAILS_MODAL:
      return {
        ...state,
        showCouponcodesupdateModal: action.modalStatus,
        couponcodesObjectToUpdate: action.couponcodesObj,
      };
    case CLOSE_COUPONCODES_DETAILS_MODAL:
      return { ...state, showCouponcodesupdateModal: action.modalStatus, couponcodesObjectToUpdate: {} };
    case UPDATING_COUPONCODES_DETAILS_OBJECT:
      return { ...state, updateCouponcodesObjectLoading: true };
    case UPDATING_COUPONCODES_DETAILS_OBJECT_SUCCESS:
      return { ...state, updateCouponcodesObjectLoading: false };
    case UPDATING_COUPONCODES_DETAILS_OBJECT_FAILED:
      return { ...state, updateCouponcodesObjectLoading: false, errorOnCouponcodesObjectUpdate: true };
    default: return { ...state };
  }
};

export default updateCouponcodes;
