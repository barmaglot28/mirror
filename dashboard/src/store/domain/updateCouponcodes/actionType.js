export const OPEN_COUPONCODES_DETAILS_MODAL = 'OPEN_COUPONCODES_DETAILS_MODAL';
export const CLOSE_COUPONCODES_DETAILS_MODAL = 'CLOSE_COUPONCODES_DETAILS_MODAL';


export const UPDATING_COUPONCODES_DETAILS_OBJECT = 'UPDATING_COUPONCODES_DETAILS_OBJECT';
export const UPDATING_COUPONCODES_DETAILS_OBJECT_SUCCESS = 'UPDATING_COUPONCODES_DETAILS_OBJECT_SUCCESS';
export const UPDATING_COUPONCODES_DETAILS_OBJECT_FAILED = 'UPDATING_COUPONCODES_DETAILS_OBJECT_FAILED';
