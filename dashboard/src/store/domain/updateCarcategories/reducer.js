import {
  OPEN_CARCATEGORIES_DETAILS_MODAL,
  CLOSE_CARCATEGORIES_DETAILS_MODAL,
  UPDATING_CARCATEGORIES_DETAILS_OBJECT,
  UPDATING_CARCATEGORIES_DETAILS_OBJECT_SUCCESS,
  UPDATING_CARCATEGORIES_DETAILS_OBJECT_FAILED,
} from './actionType';

const initialState = {
  showCarcategoriesupdateModal: false,
  carcategoriesObjectToUpdate: {},
  errorOnCarcategoriesObjectUpdate: false,
  updateCarcategoriesObjectLoading: false,
};


const updateCarcategories = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_CARCATEGORIES_DETAILS_MODAL:
      return {
        ...state,
        showCarcategoriesupdateModal: action.modalStatus,
        carcategoriesObjectToUpdate: action.carcategoriesObj,
      };
    case CLOSE_CARCATEGORIES_DETAILS_MODAL:
      return { ...state, showCarcategoriesupdateModal: action.modalStatus, carcategoriesObjectToUpdate: {} };
    case UPDATING_CARCATEGORIES_DETAILS_OBJECT:
      return { ...state, updateCarcategoriesObjectLoading: true };
    case UPDATING_CARCATEGORIES_DETAILS_OBJECT_SUCCESS:
      return { ...state, updateCarcategoriesObjectLoading: false };
    case UPDATING_CARCATEGORIES_DETAILS_OBJECT_FAILED:
      return { ...state, updateCarcategoriesObjectLoading: false, errorOnCarcategoriesObjectUpdate: true };
    default: return { ...state };
  }
};

export default updateCarcategories;
