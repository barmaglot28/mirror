import {
  OPEN_CARCATEGORIES_DETAILS_MODAL,
  CLOSE_CARCATEGORIES_DETAILS_MODAL,
  UPDATING_CARCATEGORIES_DETAILS_OBJECT,
  UPDATING_UCARCATEGORIES_DETAILS_OBJECT_SUCCESS,
  UPDATING_CARCATEGORIES_DETAILS_OBJECT_FAILED,
} from './actionType';


import {
  UPDATE_CARCATEGORIES_LIST_OBJECT,
} from '../carcategories/actionType';

import carcategoriesService from '../../../services/carcategories';

function openCarcategoriesDetailModal(carcategoriesObj) {
  return {
    type: OPEN_CARCATEGORIES_DETAILS_MODAL,
    modalStatus: true,
    carcategoriesObj,
  };
}
function closeCarcategoriesDetailModal() {
  return {
    type: CLOSE_CARCATEGORIES_DETAILS_MODAL,
    modalStatus: false,
  };
}

function updatingCarcategoriesObject() {
  return {
    type: UPDATING_CARCATEGORIES_DETAILS_OBJECT,
  };
}

function updatingCarcategoriesObjectSuccess() {
  return {
    type: UPDATING_CARCATEGORIES_DETAILS_OBJECT_SUCCESS,
  };
}
function updatingCarcategoriesObjectFailed() {
  return {
    type: UPDATING_CARCATEGORIES_DETAILS_OBJECT_FAILED,
  };
}

function updateCarcategoriesList(carcategoriesListObject) {
  return {
    type: UPDATE_CARCATEGORIES_LIST_OBJECT,
    carcategoriesList: carcategoriesListObject,
  };
}

function updateCarcategoriesListArray(updatedCarcategoriesObj) {
  const newCarcategoriesObj = updatedCarcategoriesObj.data;
  // console.log('inside updateUserListArray with data', newUserObj);
  return (dispatch, getState) => {
    const newCarcategoriesListObj = getState().domainReducer.carcategories.carcategoriesList;
    // console.log('inside updateUserListArray with data', newUserListObj);
    const carcategoriesList = newCarcategoriesListObj.data;
    for (let i = 0; i < carcategoriesList.length; i += 1) {
      if (carcategoriesList[i]._id == newCarcategoriesObj._id) { // eslint-disable-line
        carcategoriesList[i] = newCarcategoriesObj;
        break;
      }
    }
    newCarcategoriesListObj.data = carcategoriesList;
    dispatch(updateCarcategoriesList(newCarcategoriesListObj));
  };
}

function updateCarcategoriesObject(carcategoriesObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(updatingCarcategoriesObject());
    carcategoriesService.updateCarcategoriesDetails(token, carcategoriesObj)
    .then((response) => {
      dispatch(updatingCarcategoriesObjectSuccess());
      dispatch(updateCarcategoriesListArray(response));
      dispatch(closeCarcategoriesDetailModal());
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(updatingCarcategoriesObjectFailed());
    });
  };
}


export default { openCarcategoriesDetailModal, closeCarcategoriesDetailModal, updateCarcategoriesObject };
