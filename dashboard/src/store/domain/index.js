import { combineReducers } from 'redux';
import auth from './auth/reducer';
import users from './users/reducer';
import wallets from './wallets/reducer';
import skyView from './skyView/reducer';
import currentUser from './userDetails/reducer';
import cars from './cars/reducer';
import currentCar from './carDetails/reducer';
import trips from './trips/reducer';
import tripDetails from './tripDetails/reducer';
import bookings from './bookings/reducer';
import updateTrip from './updateTrip/reducer';
import updateUser from './updateUser/reducer';
import updateCar from './updateCar/reducer';
import carcategories from './carcategories/reducer';
import updateCarcategories from './updateCarcategories/reducer';
import carcategoriesDetails from './carcategoriesDetails/reducer';
import referralcodes from './referralcodes/reducer';
// import updateReferralcodes from './updateReferralcodes/reducer';
// import referralcodesDetails from './referralcodesDetails/reducer';
import couponcodes from './couponcodes/reducer';
import updateCouponcodes from './updateCouponcodes/reducer';
import couponcodesDetails from './couponcodesDetails/reducer';
import statistic from './statistic/reducer';

export default combineReducers({
  auth,
  users,
  updateUser,
  wallets,
  currentUser,
  trips,
  tripDetails,
  bookings,
  updateTrip,
  cars,
  currentCar,
  updateCar,
  carcategories,
  updateCarcategories,
  carcategoriesDetails,
  referralcodes,
  skyView,
  // updateReferralcodes,
  // referralcodesDetails,
  couponcodes,
  updateCouponcodes,
  couponcodesDetails,
  statistic,
});
