import {
  FETCHING_COUPONCODES,
  FETCH_COUPONCODES_SUCCESS,
  FETCH_COUPONCODES_FAILED,
  FETCH_COUPONCODES_STATS_CHARTS_SUCCESS,
  FETCH_COUPONCODES_STATS_CHARTS_FAILED,
  FETCHING_COUPONCODES_STATS_CHARTS,
  CREATE_NEW_COUPONCODES_REQ,
  CREATE_NEW_COUPONCODES_REQ_SUCCESS,
  CREATE_NEW_COUPONCODES_REQ_FAILED,
  UPDATE_COUPONCODES_LIST_OBJECT,
  REMOVE_COUPONCODES_REQ,
  REMOVE_COUPONCODES_REQ_SUCCESS,
  REMOVE_COUPONCODES_REQ_FAILED,
} from './actionType';

const initialState = {
  loading: false,
  failedCouponcodesListApi: false,
  failedCouponcodesChartApi: false,
  couponcodesList: {},
  meta: null,
  chartData: {},
  chartLoading: true,
  newCouponcodesLoading: false,
  failedCreateCouponcodesApi: false,
  createCouponcodesErrorObj: {},
  removeCouponcodesErrorObj: {},
  removeCouponcodesSuccess: {},
  newCouponcodesObj: {},
};

const couponcodes = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_COUPONCODES:
      return { ...state, loading: true, failedCouponcodesListApi: false };
    case FETCH_COUPONCODES_SUCCESS:
      return {
        ...state,
        couponcodesList: action.data,
        loading: false,
        failedCouponcodesListApi: false,
        meta: action.data.meta,
        removeCouponcodesErrorObj: {},
        removeCouponcodesSuccess: {},
      };
    case FETCH_COUPONCODES_FAILED:
      return {
        ...state,
        loading: false,
        failedCouponcodesListApi: true,
        removeCouponcodesErrorObj: {},
        removeCouponcodesSuccess: {},
      };
    case FETCHING_COUPONCODES_STATS_CHARTS:
      return { ...state, chartLoading: true, failedCouponcodesChartApi: false };
    case FETCH_COUPONCODES_STATS_CHARTS_SUCCESS:
      return { ...state, chartLoading: false, chartData: action.data, failedCouponcodesChartApi: false };
    case FETCH_COUPONCODES_STATS_CHARTS_FAILED:
      return { ...state, chartLoading: false, failedCouponcodesChartApi: true };
    case CREATE_NEW_COUPONCODES_REQ:
      return { ...state, newCouponcodesLoading: true, failedCreateCouponcodesApi: false, createCouponcodesErrorObj: {} };
    case CREATE_NEW_COUPONCODES_REQ_SUCCESS:
      return {
        ...state,
        newCouponcodesLoading: false,
        failedCreateCouponcodesApi: false,
        newCouponcodesObj: action.newCouponcodes,
        createCouponcodesErrorObj: {},
        removeCouponcodesErrorObj: {},
        removeCouponcodesSuccess: {},
      };
    case CREATE_NEW_COUPONCODES_REQ_FAILED:
      return {
        ...state,
        newCouponcodesLoading: false,
        failedCreateCouponcodesApi: true,
        createCouponcodesErrorObj: action.createCouponcodesErrorObj,
        removeCouponcodesErrorObj: {},
        removeCouponcodesSuccess: {},
      };
    case UPDATE_COUPONCODES_LIST_OBJECT: {
      const couponcodesList = Object.assign({}, action.couponcodesList);
      return { ...state, couponcodesList };
    }
    case REMOVE_COUPONCODES_REQ:
      return { ...state, loading: true, failedCouponcodesListApi: false };
    case REMOVE_COUPONCODES_REQ_SUCCESS:
      return {
        ...state,
        loading: false,
        failedCouponcodesListApi: false,
        removeCouponcodesSuccess: action.removeCouponcodesSuccess
      };
    case REMOVE_COUPONCODES_REQ_FAILED:
      return {
        ...state,
        loading: false,
        failedCouponcodesListApi: true,
        removeCouponcodesErrorObj: action.removeCouponcodesErrorObj
      };
    default:
      return state;
  }
};

export default couponcodes;
