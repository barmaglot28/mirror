import {
  FETCHING_COUPONCODES,
  FETCH_COUPONCODES_SUCCESS,
  FETCH_COUPONCODES_FAILED,
  CREATE_NEW_COUPONCODES_REQ,
  CREATE_NEW_COUPONCODES_REQ_SUCCESS,
  CREATE_NEW_COUPONCODES_REQ_FAILED,
  REMOVE_COUPONCODES_REQ,
  REMOVE_COUPONCODES_REQ_SUCCESS,
  REMOVE_COUPONCODES_REQ_FAILED,
} from './actionType';
import CouponcodesService from '../../../services/couponcodes';

function RequestRemoveCouponcodes() {
  return {
    type: REMOVE_COUPONCODES_REQ,
  };
}

function RequestRemoveCouponcodesSuccess(data) {
  return {
    type: REMOVE_COUPONCODES_REQ_SUCCESS,
    removeCouponcodesSuccess: data,
  };
}

function RequestRemoveCouponcodesFailed(data) {
  return {
    type: REMOVE_COUPONCODES_REQ_FAILED,
    removeCouponcodesErrorObj: data,
  };
}

function fetchingCouponcodes() {
  return {
    type: FETCHING_COUPONCODES,
  };
}

function fetchCouponcodesSuccess(data) {
  return {
    type: FETCH_COUPONCODES_SUCCESS,
    data,
    removeCouponcodesSuccess: {},
    removeCouponcodesErrorObj: {},
  };
}

function fetchCouponcodesFailed(data) {
  return {
    type: FETCH_COUPONCODES_FAILED,
    data,
    removeCouponcodesSuccess: {},
    removeCouponcodesErrorObj: {},
  };
}

function RequestCreateNewCouponcodes() {
  return {
    type: CREATE_NEW_COUPONCODES_REQ,
  };
}

function RequestCreateNewCouponcodesSuccess(data) {
  return {
    type: CREATE_NEW_COUPONCODES_REQ_SUCCESS,
    newCouponcodes: data,
  };
}

function RequestCreateNewCouponcodesFailed(data) {
  return {
    type: CREATE_NEW_COUPONCODES_REQ_FAILED,
    createCouponcodesErrorObj: data,
  };
}

function fetchCouponcodes(pageNo, limit) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCouponcodes());
    CouponcodesService.getAllCouponcodes(token, pageNo, limit)
    .then((response) => {
      dispatch(fetchCouponcodesSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchCouponcodesFailed(e));
    });
  };
}

function createNewCouponcodes(couponcodesObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestCreateNewCouponcodes());
    CouponcodesService.createCouponcodes(token, couponcodesObj)
    .then((response) => {
      dispatch(RequestCreateNewCouponcodesSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(RequestCreateNewCouponcodesFailed(e));
    });
  };
}

function removeCouponcodes(couponcodesObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestRemoveCouponcodes());
    CouponcodesService.removeCouponcodesService(token, couponcodesObj)
      .then((response) => {
        if(response.success) {
          dispatch(RequestRemoveCouponcodesSuccess(response));
        } else {
          dispatch(RequestRemoveCouponcodesFailed(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(RequestRemoveCouponcodesFailed(e));
      });
  };
}

export default { fetchCouponcodes, createNewCouponcodes, removeCouponcodes };
