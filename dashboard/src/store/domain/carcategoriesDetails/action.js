import {
  FETCHING_CARCATEGORIES_DETAILS,
  FETCH_CARCATEGORIES_DETAIL_SUCCESS,
  FETCH_CARCATEGORIES_DETAIL_FAILED,
} from './actionType';
import CarcategoriesDetailService from '../../../services/carcategoriesDetails';

// for user deatils
function fetchingCarcategoriesDetails() {
  return {
    type: FETCHING_CARCATEGORIES_DETAILS,
  };
}

function fetchCarcategoriesDetailsSuccess(data) {
  return {
    type: FETCH_CARCATEGORIES_DETAIL_SUCCESS,
    data,
  };
}

function fetchCarcategoriesDetailsFailed(data) {
  return {
    type: FETCH_CARCATEGORIES_DETAIL_FAILED,
    data,
  };
}

export function fetchCarcategoriesDetails(id) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCarcategoriesDetails());
    CarcategoriesDetailService.getCarcategoriesDetails(token, id)
      .then((response) => {
        if (response.success) {
          dispatch(fetchCarcategoriesDetailsSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchCarcategoriesDetailsFailed(e));
      });
  };
}