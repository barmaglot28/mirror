import {
  FETCHING_CARCATEGORIES_DETAILS,
  FETCH_CARCATEGORIES_DETAIL_SUCCESS,
  FETCH_CARCATEGORIES_DETAIL_FAILED,
} from './actionType';

const initialState = {
  loading: true,
  currentCarcategoriesDetails: {},
};

const carcategoriesDetails = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_CARCATEGORIES_DETAILS:
      return { ...state, loading: true };
    case FETCH_CARCATEGORIES_DETAIL_SUCCESS:
      return { ...state, currentCarcategoriesDetails: action.data, loading: false };
    case FETCH_CARCATEGORIES_DETAIL_FAILED:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default carcategoriesDetails;
