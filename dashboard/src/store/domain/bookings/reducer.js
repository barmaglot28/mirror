import {
  FETCHING_AVAILABLE_BOOKINGS,
  FETCH_AVAILABLE_BOOKINGS_SUCCESS,
  FETCH_AVAILABLE_BOOKINGS_FAILED,

  FETCHING_ACCEPTED_BOOKINGS,
  FETCH_ACCEPTED_BOOKINGS_SUCCESS,
  FETCH_ACCEPTED_BOOKINGS_FAILED,

  REMOVE_PREBOOKING_REQ,
  REMOVE_PREBOOKING_REQ_SUCCESS,
  REMOVE_PREBOOKING_REQ_FAILED,

  FETCHING_CANCELLED_BOOKINGS,
  FETCH_CANCELLED_BOOKINGS_SUCCESS,
  FETCH_CANCELLED_BOOKINGS_FAILED
} from './actionType';

const initialState = {
  loading: false,
  availableBookingsList: [],
  acceptedBookingsList: [],
  cancelledBookingsList: [],
  removePreBookingSuccess: false,
  meta: null,
  error: null
};

const bookings = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_AVAILABLE_BOOKINGS:
    case FETCHING_ACCEPTED_BOOKINGS:
    case FETCHING_CANCELLED_BOOKINGS:
    case REMOVE_PREBOOKING_REQ:
      return { ...state, loading: true, error: null, removePreBookingSuccess: initialState.removePreBookingSuccess };

    case FETCH_AVAILABLE_BOOKINGS_SUCCESS:
      return {
        ...state,
        availableBookingsList: action.data.data,
        loading: false,
        meta: action.data.meta,
      };

    case FETCH_AVAILABLE_BOOKINGS_FAILED:
    case FETCH_ACCEPTED_BOOKINGS_FAILED:
    case REMOVE_PREBOOKING_REQ_FAILED:
    case FETCH_CANCELLED_BOOKINGS_FAILED:
      return { ...state, loading: false, error: action.data };

    case FETCH_ACCEPTED_BOOKINGS_SUCCESS:
      return {
        ...state,
        acceptedBookingsList: action.data.data,
        loading: false,
        meta: action.data.meta,
      };

    case REMOVE_PREBOOKING_REQ_SUCCESS:
      return {
        ...state,
        loading: false,
        removePreBookingSuccess: true
      };

    case FETCH_CANCELLED_BOOKINGS_SUCCESS:
      return {
        ...state,
        cancelledBookingsList: action.data.data,
        loading: false,
        meta: action.data.meta,
      };

    default:
      return state;
  }
};

export default bookings;
