import {
  FETCHING_AVAILABLE_BOOKINGS,
  FETCH_AVAILABLE_BOOKINGS_SUCCESS,
  FETCH_AVAILABLE_BOOKINGS_FAILED,

  FETCHING_ACCEPTED_BOOKINGS,
  FETCH_ACCEPTED_BOOKINGS_SUCCESS,
  FETCH_ACCEPTED_BOOKINGS_FAILED,

  REMOVE_PREBOOKING_REQ,
  REMOVE_PREBOOKING_REQ_SUCCESS,
  REMOVE_PREBOOKING_REQ_FAILED,

  FETCHING_CANCELLED_BOOKINGS,
  FETCH_CANCELLED_BOOKINGS_SUCCESS,
  FETCH_CANCELLED_BOOKINGS_FAILED
} from './actionType';
import BookingsService from '../../../services/bookings';
import { unpinAcceptedPreBooking, cancelPreBooking } from '../../socket/socket';


function fetchingAvailableBookings() {
  return {
    type: FETCHING_AVAILABLE_BOOKINGS,
  };
}

function fetchAvailableBookingsSuccess(data) {
  return {
    type: FETCH_AVAILABLE_BOOKINGS_SUCCESS,
    data,
  };
}

function fetchAvailableBookingsFailed(data) {
  return {
    type: FETCH_AVAILABLE_BOOKINGS_FAILED,
    data,
  };
}

function fetchingAcceptedBookings() {
  return {
    type: FETCHING_ACCEPTED_BOOKINGS,
  };
}

function fetchAcceptedBookingsSuccess(data) {
  return {
    type: FETCH_ACCEPTED_BOOKINGS_SUCCESS,
    data,
  };
}

function fetchAcceptedBookingsFailed(data) {
  return {
    type: FETCH_ACCEPTED_BOOKINGS_FAILED,
    data,
  };
}

function requestRemovePreBooking() {
  return {
    type: REMOVE_PREBOOKING_REQ,
  };
}

function requestRemovePreBookingSuccess() {
  return {
    type: REMOVE_PREBOOKING_REQ_SUCCESS
  };
}

function requestRemovePreBookingFailed(data) {
  return {
    type: REMOVE_PREBOOKING_REQ_FAILED,
    removePreBookingErrorObj: data,
  };
}

function fetchingCancelledBookings() {
  return {
    type: FETCHING_CANCELLED_BOOKINGS,
  };
}

function fetchCancelledBookingsSuccess(data) {
  return {
    type: FETCH_CANCELLED_BOOKINGS_SUCCESS,
    data,
  };
}

function fetchCancelledBookingsFailed(data) {
  return {
    type: FETCH_CANCELLED_BOOKINGS_FAILED,
    data,
  };
}

function fetchAvailableBookings(pageNo) {
  return (dispatch, getState) => {

    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingAvailableBookings());
    BookingsService.getAllAvailableBookings(token, pageNo)
      .then((response) => {
        dispatch(fetchAvailableBookingsSuccess(response));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchAvailableBookingsFailed(e));
      });
  };
}

function fetchAcceptedBookings(pageNo) {
  return (dispatch, getState) => {

    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingAcceptedBookings());
    BookingsService.getAllAcceptedBookings(token, pageNo)
      .then((response) => {
        dispatch(fetchAcceptedBookingsSuccess(response));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchAcceptedBookingsFailed(e));
      });
  };
}

function unpinAcceptedBooking(booking) {
  return (dispatch, getState) => {
    unpinAcceptedPreBooking(booking);
  }
}

function removePreBooking(booking) {
  return (dispatch, getState) => {
      Promise.resolve()
      .then(() => {
        dispatch(requestRemovePreBooking);
        cancelPreBooking(booking);
      })
      .then(() => {
        dispatch(requestRemovePreBookingSuccess());
      })
      .catch((error) => {
        if (error.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(requestRemovePreBookingFailed(error));
      })
  };
}

function fetchCancelledBookings(pageNo) {
  return (dispatch, getState) => {

    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCancelledBookings());
    BookingsService.getAllCancelledBookings(token, pageNo)
      .then((response) => {
        dispatch(fetchCancelledBookingsSuccess(response));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchCancelledBookingsFailed(e));
      });
  };
}

export default { fetchAvailableBookings, fetchAcceptedBookings, unpinAcceptedBooking, removePreBooking, fetchCancelledBookings };
