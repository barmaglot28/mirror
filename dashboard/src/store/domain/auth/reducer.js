import {
  CLEAR_USER_DATA,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  SET_LOGGED_IN_STATUS,
  SOCKET_DISCONNECTED
  // FLUSH_DATA,
} from './actionType';


const initialState = {
  user: {},
  invalidCredentials: false,
  loggedInStatus: false,
  socketDisconnected: false
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    // case FLUSH_DATA:
    //   return { ...initialState };
    case CLEAR_USER_DATA:
      return { ...initialState, loggedInStatus: false };
    case LOGIN_USER_SUCCESS:
      return { ...state, user: action.data, invalidCredentials: false, loggedInStatus: true };
    case LOGIN_USER_FAILED:
      return { ...state, user: action.data, invalidCredentials: true, loggedInStatus: false };
    case SET_LOGGED_IN_STATUS: {
      // state.user.jwtAccessToken = action.jwtAccessToken;
      // user.jwtAccessToken = action.jwtAccessToken;
      const newState = Object.assign({}, state);
      newState.user.jwtAccessToken = action.jwtAccessToken;
      return {
        ...newState,
        loggedInStatus: action.loggedInStatus,
        invalidCredentials: false,
      };
    }
    case SOCKET_DISCONNECTED:
      return { ...state, socketDisconnected: action.payload };
    default:
      return state;
  }
};

export default auth;
