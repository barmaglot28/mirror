import {
  FETCHING_CAR,
  FETCH_CARS_SUCCESS,
  FETCH_CARS_FAILED,
  CREATE_NEW_CAR_REQ,
  CREATE_NEW_CAR_REQ_SUCCESS,
  CREATE_NEW_CAR_REQ_FAILED,
  UPDATE_CAR_LIST_OBJECT,
  REMOVE_CAR_REQ,
  REMOVE_CAR_REQ_FAILED,
  REMOVE_CAR_REQ_SUCCESS
} from './actionType';

const initialState = {
  loading: false,
  failedCarListApi: false,
  carList: {},
  meta: null,
  newCarLoading: false,
  failedCreateCarApi: false,
  createCarErrorObj: {},
  newCarObj: {},
  removeCarErrorObj: {},
  removeCarSuccess: {},
};

const cars = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_CAR:
      return { ...state, loading: true, failedCarListApi: false };
    case FETCH_CARS_SUCCESS:
      return {
        ...state,
        carList: action.data,
        loading: false,
        failedCarListApi: false,
        meta: action.data.meta,
      };
    case FETCH_CARS_FAILED:
      return { ...state, loading: false, failedCarListApi: true }
    case CREATE_NEW_CAR_REQ:
      return { ...state, newCarLoading: true, failedCreateCarApi: false, createCarErrorObj: {} };
    case CREATE_NEW_CAR_REQ_SUCCESS:
      return {
        ...state,
        newCarLoading: false,
        failedCreateCarApi: false,
        newCarObj: action.newCar,
        createCarErrorObj: {},
      };
    case CREATE_NEW_CAR_REQ_FAILED:
      return {
        ...state,
        newCarLoading: false,
        failedCreateCarApi: true,
        createCarErrorObj: action.createCarErrorObj,
      };
    case UPDATE_CAR_LIST_OBJECT: {
      const carList = Object.assign({}, action.carList);
      return { ...state, carList };
    }
    case REMOVE_CAR_REQ:
      return { ...state, loading: true, failedCarListApi: false };
    case REMOVE_CAR_REQ_SUCCESS:
      return {
        ...state,
        loading: false,
        failedCarListApi: false,
        removeCarSuccess: action.removeCarSuccess
      };
    case REMOVE_CAR_REQ_FAILED:
      return {
        ...state,
        loading: false,
        failedCarListApi: true,
        removeCarErrorObj: action.removeCarErrorObj
      };
    default:
      return state;
  }
};

export default cars;
