import {
  FETCHING_CAR,
  FETCH_CARS_SUCCESS,
  FETCH_CARS_FAILED,
  CREATE_NEW_CAR_REQ,
  CREATE_NEW_CAR_REQ_SUCCESS,
  CREATE_NEW_CAR_REQ_FAILED,
  REMOVE_CAR_REQ,
  REMOVE_CAR_REQ_FAILED,
  REMOVE_CAR_REQ_SUCCESS
} from './actionType';
import CarsService from '../../../services/cars';


function fetchingCars() {
  return {
    type: FETCHING_CAR,
  };
}

function fetchCarsSuccess(data) {
  return {
    type: FETCH_CARS_SUCCESS,
    data,
  };
}

function fetchCarsFailed(data) {
  return {
    type: FETCH_CARS_FAILED,
    data,
  };
}

function RequestCreateNewCar() {
  return {
    type: CREATE_NEW_CAR_REQ,
  };
}

function RequestCreateNewCarSuccess(data) {
  return {
    type: CREATE_NEW_CAR_REQ_SUCCESS,
    newCar: data,
  };
}

function RequestCreateNewCarFailed(data) {
  return {
    type: CREATE_NEW_CAR_REQ_FAILED,
    createCarErrorObj: data,
  };
}

function RequestRemoveCar() {
  return {
    type: REMOVE_CAR_REQ,
  };
}

function RequestRemoveCarSuccess(data) {
  return {
    type: REMOVE_CAR_REQ_SUCCESS,
    removeCarSuccess: data,
  };
}

function RequestRemoveCarFailed(data) {
  return {
    type: REMOVE_CAR_REQ_FAILED,
    removeCarErrorObj: data,
  };
}


function fetchCars(pageNo) {
  return (dispatch, getState) => {

    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCars());
    CarsService.getAllCars(token, pageNo)
        .then((response) => {
          dispatch(fetchCarsSuccess(response));
        })
        .catch((e) => {
          if (e.message === 'Unauthorized') {
            dispatch({ type: 'FLUSH_DATA' });
          }
          dispatch(fetchCarsFailed(e));
        });
  };
}

function createNewCar(carObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestCreateNewCar());
    CarsService.createNewCar(token, carObj)
        .then((response) => {
          dispatch(RequestCreateNewCarSuccess(response));
        })
        .catch((e) => {
          if (e.message === 'Unauthorized') {
            dispatch({ type: 'FLUSH_DATA' });
          }
          dispatch(RequestCreateNewCarFailed(e));
        });
  };
}

function removeCar(carObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestRemoveCar());
    CarsService.removeCar(token, carObj)
        .then((response) => {
          if (response.success) {
            dispatch(RequestRemoveCarSuccess(response));
          } else {
            dispatch(RequestRemoveCarFailed(response));
          }
        })
        .catch((e) => {
          if (e.message === 'Unauthorized') {
            dispatch({ type: 'FLUSH_DATA' });
          }
          dispatch(RequestRemoveCarFailed(e));
        });
  };
}


export default { fetchCars, createNewCar, removeCar };
