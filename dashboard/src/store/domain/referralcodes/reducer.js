import {
  FETCH_REFERRAL_AMOUNT_REQ,
  FETCH_REFERRAL_AMOUNT_SUCCESS,
  FETCH_REFERRAL_AMOUNT_FAILED,
  SAVE_REFERRAL_AMOUNT_REQ,
  SAVE_REFERRAL_AMOUNT_REQ_SUCCESS,
  SAVE_REFERRAL_AMOUNT_REQ_FAILED
} from './actionType';

const initialState = {
  loading: false,
  referralAmount: {},
  fetchSuccess: true,
  saveSuccess: true,
  saveErrMsg: ''
};

const referralcodes = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REFERRAL_AMOUNT_REQ:
      return {
        ...state,
        loading: true,
        fetchSuccess: true,
      };

    case FETCH_REFERRAL_AMOUNT_SUCCESS:
        return {
        ...state,
        referralAmount: action.data,
        loading: false,
        fetchSuccess: true,
      };

    case FETCH_REFERRAL_AMOUNT_FAILED:
      return {
        ...state,
        referralAmount: {},
        loading: false,
        fetchSuccess: false
      };

    case SAVE_REFERRAL_AMOUNT_REQ:
      return { ...state,
        loading: true,
        saveSuccess: true,
      };

    case SAVE_REFERRAL_AMOUNT_REQ_SUCCESS:
      return {
        ...state,
       loading: false,
       referralAmount: action.data,
       saveSuccess: true,
      };

    case SAVE_REFERRAL_AMOUNT_REQ_FAILED:
      return {
        ...state,
       loading: false,
       saveSuccess: false,
       saveErrMsg: action.data.message
      };

    default:
      return state;
  }
};

export default referralcodes;
