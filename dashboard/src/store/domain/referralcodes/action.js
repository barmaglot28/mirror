import {
  FETCH_REFERRAL_AMOUNT_REQ,
  FETCH_REFERRAL_AMOUNT_SUCCESS,
  FETCH_REFERRAL_AMOUNT_FAILED,
  SAVE_REFERRAL_AMOUNT_REQ,
  SAVE_REFERRAL_AMOUNT_REQ_SUCCESS,
  SAVE_REFERRAL_AMOUNT_REQ_FAILED
} from './actionType';
import ReferralcodesService from '../../../services/referralcodes';

function fetchReferralAmtStart() {
  return {
    type: FETCH_REFERRAL_AMOUNT_REQ,
  };
}

function fetchReferralAmtSuccess(data) {
  return {
    type: FETCH_REFERRAL_AMOUNT_SUCCESS,
    data,
  };
}

function fetchReferralAmtFailed() {
  return {
    type: FETCH_REFERRAL_AMOUNT_FAILED,
  };
}

function requestSaveReferralAmt() {
  return {
    type: SAVE_REFERRAL_AMOUNT_REQ,
  };
}

function requestSaveReferralAmtSuccess(data) {
  return {
    type: SAVE_REFERRAL_AMOUNT_REQ_SUCCESS,
    data,
  };
}

function requestSaveReferralAmtFailed(data) {
  return {
    type: SAVE_REFERRAL_AMOUNT_REQ_FAILED,
    data
  };
}

export function fetchReferralAmt() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchReferralAmtStart());
    ReferralcodesService.fetchReferralAmt(token)
      .then((response) => {
        const amount = response.data || {};
        dispatch(fetchReferralAmtSuccess(amount));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchReferralAmtFailed());
      });
  };
}


function saveReferralAmt(referralcodesObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(requestSaveReferralAmt());
    ReferralcodesService.saveReferralAmt(token, referralcodesObj)
    .then((response) => {
      const amount = response.data || {};
      dispatch(requestSaveReferralAmtSuccess(amount));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(requestSaveReferralAmtFailed(e));
    });
  };
}

export default { fetchReferralAmt, saveReferralAmt };
