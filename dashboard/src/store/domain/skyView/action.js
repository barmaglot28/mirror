
import {
  FETCHING_CAR_CATEGORIES,
  FETCHING_CAR_CATEGORIES_SUCCESS,
  FETCHING_CAR_CATEGORIES_FAILED,
} from './actionType';
import SkyViewService from '../../../services/skyView';


function fetchingCarCategories() {
  return {
    type: FETCHING_CAR_CATEGORIES,
  };
}

function fetchCategoriesSuccess(data, defaultCategory) {
  return {
    type: FETCHING_CAR_CATEGORIES_SUCCESS,
    data,
    defaultCategory
  };
}

function fetchCategoriesFailed() {
  return {
    type: FETCHING_CAR_CATEGORIES_FAILED,
  };
}

function fetchCarCategories() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCarCategories());
    SkyViewService.getAllCategories(token)
      .then((response) => {
        const categories = response.data;
        categories.sort((a, b) => a.name > b.name);
        dispatch(fetchCategoriesSuccess(categories, response.defaultCategory));
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchCategoriesFailed(e));
      });
  };
}

export default { fetchCarCategories };
