import {
  FETCHING_CAR_CATEGORIES,
  FETCHING_CAR_CATEGORIES_SUCCESS,
  FETCHING_CAR_CATEGORIES_FAILED,
} from './actionType';

const initialState = {
  loading: false,
  carCategories: [],
  defaultCategory: {},
};

const skyView = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_CAR_CATEGORIES:
      return { ...state, loading: true };

    case FETCHING_CAR_CATEGORIES_SUCCESS:
      return { ...state, loading: false, carCategories: action.data, defaultCategory: action.defaultCategory };

    case FETCHING_CAR_CATEGORIES_FAILED:
      return { ...state, loading: false, carCategories: [], defaultCategory: null };

    default:
      return state;
  }
};

export default skyView;
