import {
  FETCHING_STATISTIC_DETAILS,
  FETCH_STATISTIC_SUCCESS, FETCH_STATISTIC_FAILED,
  FETCH_GENERAL_STATISTIC_SUCCESS, FETCH_GENERAL_STATISTIC_FAILED,
  FETCH_TODAY_RIDE_STATISTIC_SUCCESS, FETCH_TODAY_RIDE_STATISTIC_FAILED,
  FETCH_COMPLETED_RIDE_STATISTIC_SUCCESS, FETCH_COMPLETED_RIDE_STATISTIC_FAILED,
  FETCH_DRIVERS_STATISTIC_SUCCESS, FETCH_DRIVERS_STATISTIC_FAILED,
  FETCH_RATING_STATISTIC_SUCCESS, FETCH_RATING_STATISTIC_FAILED,
  FETCH_CANCELLED_STATISTIC_SUCCESS, FETCH_CANCELLED_STATISTIC_FAILED,
  FETCH_DRIVER_WORKLIST_STATISTIC_SUCCESS, FETCH_DRIVER_WORKLIST_STATISTIC_FAILED,
  FETCH_TRIPS_STATISTIC_SUCCESS, FETCH_TRIPS_STATISTIC_FAILED,
  FETCH_VEHICLE_STATISTIC_SUCCESS, FETCH_VEHICLE_STATISTIC_FAILED,

  TRIP_SELECTED,

  SOCKET_TRIP_UPDATED,
  SOCKET_TRIP_CREATED,
  SOCKET_TRIP_DELETED,
  SOCKET_DRIVER_UPDATED
} from './actionType';
import StatisticService from '../../../services/statistic';

function fetchingStatistic() {
  return {
    type: FETCHING_STATISTIC_DETAILS,
  };
}

function fetchStatisticSuccess(data) {
  return {
    type: FETCH_STATISTIC_SUCCESS,
    data,
  };
}

function fetchStatisticFailed(data) {
  return {
    type: FETCH_STATISTIC_FAILED,
    data,
  };
}

function fetchGeneralStatisticSuccess(data) {
  return {
    type: FETCH_GENERAL_STATISTIC_SUCCESS,
    data,
  };
}

function fetchGeneralStatisticFailed(data) {
  return {
    type: FETCH_GENERAL_STATISTIC_FAILED,
    data,
  };
}

function fetchTodayRideStatisticSuccess(data) {
  return {
    type: FETCH_TODAY_RIDE_STATISTIC_SUCCESS,
    data,
  };
}

function fetchTodayRideStatisticFailed(data) {
  return {
    type: FETCH_TODAY_RIDE_STATISTIC_FAILED,
    data,
  };
}

function fetchCompletedRideStatisticSuccess(data) {
  return {
    type: FETCH_COMPLETED_RIDE_STATISTIC_SUCCESS,
    data,
  };
}

function fetchCompletedRideStatisticFailed(data) {
  return {
    type: FETCH_COMPLETED_RIDE_STATISTIC_FAILED,
    data,
  };
}

function fetchDriversStatisticSuccess(data) {
  return {
    type: FETCH_DRIVERS_STATISTIC_SUCCESS,
    data,
  };
}

function fetchDriversStatisticFailed(data) {
  return {
    type: FETCH_DRIVERS_STATISTIC_FAILED,
    data,
  };
}

function fetchRatingCommentsStatisticSuccess(data) {
  return {
    type: FETCH_RATING_STATISTIC_SUCCESS,
    data,
  };
}

function fetchRatingCommentsStatisticFailed(data) {
  return {
    type: FETCH_RATING_STATISTIC_FAILED,
    data,
  };
}

function fetchCancelledCommentsStatisticSuccess(data) {
  return {
    type: FETCH_CANCELLED_STATISTIC_SUCCESS,
    data,
  };
}

function fetchCancelledCommentsStatisticFailed(data) {
  return {
    type: FETCH_CANCELLED_STATISTIC_FAILED,
    data,
  };
}

function fetchDriverWorklistStatisticSuccess(data) {
  return {
    type: FETCH_DRIVER_WORKLIST_STATISTIC_SUCCESS,
    data,
  };
}

function fetchDriverWorklistStatisticFailed(data) {
  return {
    type: FETCH_DRIVER_WORKLIST_STATISTIC_FAILED,
    data,
  };
}

function fetchTripsStatisticSuccess(data) {
  return {
    type: FETCH_TRIPS_STATISTIC_SUCCESS,
    data,
  };
}

function fetchTripsStatisticFailed(data) {
  return {
    type: FETCH_TRIPS_STATISTIC_FAILED,
    data,
  };
}

function fetchVehicleStatisticSuccess(data) {
  return {
    type: FETCH_VEHICLE_STATISTIC_SUCCESS,
    data,
  };
}

function fetchVehicleStatisticFailed(data) {
  return {
    type: FETCH_VEHICLE_STATISTIC_FAILED,
    data,
  };
}

export function fetchGeneralStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getGeneralStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchGeneralStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchGeneralStatisticFailed(e));
      });
  };
}

export function fetchTodayRideStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getTodayRideStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchTodayRideStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchTodayRideStatisticFailed(e));
      });
  };
}

export function fetchCompletedRideStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getCompletedRideStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchCompletedRideStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchCompletedRideStatisticFailed(e));
      });
  };
}

export function fetchDriversStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getDriversStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchDriversStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchDriversStatisticFailed(e));
      });
  };
}

export function fetchRatingCommentsStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getRatingCommentsStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchRatingCommentsStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchRatingCommentsStatisticFailed(e));
      });
  };
}

export function fetchCancelledCommentsStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getCancelledCommentsStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchCancelledCommentsStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchCancelledCommentsStatisticFailed(e));
      });
  };
}

export function fetchDriverWorklistStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getDriverWorklistStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchDriverWorklistStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchDriverWorklistStatisticFailed(e));
      });
  };
}

export function fetchTripsStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getTripsStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchTripsStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchTripsStatisticFailed(e));
      });
  };
}

export function fetchVehicleStatistic() {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingStatistic());
    StatisticService.getVehicleStatistic(token)
      .then((response) => {
        if (response.success) {
          dispatch(fetchVehicleStatisticSuccess(response.data));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({type: 'FLUSH_DATA'});
        }
        dispatch(fetchVehicleStatisticFailed(e));
      });
  };
}


export function tripSelected() {
  return {
    type: TRIP_SELECTED
  };
}

export function tripUpdated(data) {
  return {
    type: SOCKET_TRIP_UPDATED,
    data,
  };
}

export function tripCreated(data) {
  return {
    type: SOCKET_TRIP_CREATED,
    data,
  };
}

export function tripDeleted(tripId) {
  return {
    type: SOCKET_TRIP_DELETED,
    tripId,
  };
}

export function driverUpdated(driver) {
  return {
    type: SOCKET_DRIVER_UPDATED,
    driver,
  };
}

export default { fetchGeneralStatistic, fetchTodayRideStatistic, fetchCompletedRideStatistic, fetchDriversStatistic,
  fetchRatingCommentsStatistic, fetchCancelledCommentsStatistic, fetchDriverWorklistStatistic, fetchTripsStatistic,
  fetchVehicleStatistic, tripSelected };
