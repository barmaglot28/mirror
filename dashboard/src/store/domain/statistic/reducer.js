import {
  FETCHING_STATISTIC_DETAILS,
  FETCH_STATISTIC_SUCCESS,
  FETCH_STATISTIC_FAILED,
  FETCH_GENERAL_STATISTIC_SUCCESS, FETCH_GENERAL_STATISTIC_FAILED,
  FETCH_TODAY_RIDE_STATISTIC_SUCCESS, FETCH_TODAY_RIDE_STATISTIC_FAILED,
  FETCH_COMPLETED_RIDE_STATISTIC_SUCCESS, FETCH_COMPLETED_RIDE_STATISTIC_FAILED,
  FETCH_DRIVERS_STATISTIC_SUCCESS, FETCH_DRIVERS_STATISTIC_FAILED,
  FETCH_RATING_STATISTIC_SUCCESS, FETCH_RATING_STATISTIC_FAILED,
  FETCH_CANCELLED_STATISTIC_SUCCESS, FETCH_CANCELLED_STATISTIC_FAILED,
  FETCH_DRIVER_WORKLIST_STATISTIC_SUCCESS, FETCH_DRIVER_WORKLIST_STATISTIC_FAILED, FETCH_TRIPS_STATISTIC_SUCCESS,
  FETCH_TRIPS_STATISTIC_FAILED, FETCH_VEHICLE_STATISTIC_SUCCESS, FETCH_VEHICLE_STATISTIC_FAILED,

  TRIP_SELECTED,

  SOCKET_TRIP_UPDATED,
  SOCKET_TRIP_CREATED,
  SOCKET_TRIP_DELETED,
  SOCKET_DRIVER_UPDATED
} from './actionType';

const initialState = {
  loading: true,
  general: {
    riders: 0,
    drivers: 0,
    taxiCompanies: 0,
    totalEarnings: 0,
    acceptedRides: 0,
    notAcceptedRides: 0,
  },
  todayRide: {
    total: 0,
    ongoing: 0,
    completed: 0,
    cancelled: 0,
    accepted: 0,
    revenue: 0,
  },
  totalCompletedRides: {
    today: 0,
    month: 0,
    blackToday: 0,
    blackMonth: 0
  },
  drivers: {
    registered: 0,
    online: 0,
    offline: 0,
    onTrip: 0,
  },
  ratingComments: [],
  cancelledComments: [],
  driverWorklist: [],
  tripsList: [],
  isNeedToSelect: false,
  tripForSelect: null,
  vehicle: {
    requested: [],
    accepted: [],
    completed: [],
    revenue: []
  }
};

const statistic = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_STATISTIC_DETAILS:
      return { ...state, loading: true };
    case FETCH_STATISTIC_SUCCESS:
      return { ...state, loading: false };
    case FETCH_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_GENERAL_STATISTIC_SUCCESS:
      return { ...state, loading: false, general: action.data };
    case FETCH_GENERAL_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_TODAY_RIDE_STATISTIC_SUCCESS:
      return { ...state, loading: false, todayRide: action.data };
    case FETCH_TODAY_RIDE_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_COMPLETED_RIDE_STATISTIC_SUCCESS:
      return { ...state, loading: false, totalCompletedRides: action.data };
    case FETCH_COMPLETED_RIDE_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_DRIVERS_STATISTIC_SUCCESS:
      return { ...state, loading: false, drivers: action.data };
    case FETCH_DRIVERS_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_RATING_STATISTIC_SUCCESS:
      return { ...state, loading: false, ratingComments: action.data };
    case FETCH_RATING_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_CANCELLED_STATISTIC_SUCCESS:
      return { ...state, loading: false, cancelledComments: action.data };
    case FETCH_CANCELLED_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_DRIVER_WORKLIST_STATISTIC_SUCCESS:
      return { ...state, loading: false, driverWorklist: action.data };
    case FETCH_DRIVER_WORKLIST_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_TRIPS_STATISTIC_SUCCESS:
      return { ...state, loading: false, tripsList: action.data };
    case FETCH_TRIPS_STATISTIC_FAILED:
      return { ...state, loading: false };
    case FETCH_VEHICLE_STATISTIC_SUCCESS:
      return { ...state, loading: false, vehicle: action.data };
    case FETCH_VEHICLE_STATISTIC_FAILED:
      return { ...state, loading: false };

    case SOCKET_TRIP_UPDATED:
      const index = state.tripsList.findIndex(item => item._id === action.data._id);
      const tripsList = [ ...state.tripsList ];
      tripsList[index] = action.data;
      return {
        ...state,
        tripsList,
        isNeedToSelect: true,
        tripForSelect: index
      };

    case SOCKET_TRIP_CREATED:
      const trips = [...state.tripsList];
      trips.unshift(action.data);
      return {
        ...state,
        tripsList: trips,
        isNeedToSelect: true,
        tripForSelect: 0
      };

    case SOCKET_TRIP_DELETED:
      let tripList = [...state.tripsList];
      tripList = tripList.filter((item) => { return item._id !== action.tripId });
      return { ...state, tripsList: tripList };

    case SOCKET_DRIVER_UPDATED:
      const driverIndex = state.driverWorklist.findIndex(item => item._id === action.driver._id);
      const driverWorklist = [ ...state.driverWorklist ];
      driverWorklist[driverIndex] = action.driver;
      return { ...state, driverWorklist };

    case TRIP_SELECTED:
      return {
        ...state,
        isNeedToSelect: false,
        tripForSelect: null
      };

    default:
      return state;
  }
};

export default statistic;
