import {
  FETCHING_USER_TRIP_DETAILS,
  FETCH_USER_TRIP_DETAIL_SUCCESS,
  FETCH_USER_TRIP_DETAIL_FAILED,
  FETCHING_USER_DETAILS,
  FETCH_USER_DETAIL_SUCCESS,
  FETCH_USER_DETAIL_FAILED,
  FETCHING_USER_TRIP_DETAILS_PER_DAY,
  FETCH_USER_TRIP_DETAIL_SUCCESS_PER_DAY,
  FETCH_USER_TRIP_DETAIL_FAILED_PER_DAY
} from './actionType';

const initialState = {
  loading: true,
  tripLoading: true,
  currentUserTrips: {
    data: [],
  },
  currentUserDetails: {},
};

const currentUser = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_USER_TRIP_DETAILS_PER_DAY:
      return { ...state, tripLoading: true };
    case FETCH_USER_TRIP_DETAIL_SUCCESS_PER_DAY:
      return {
        ...state,
        currentUserTrips: action.data,
        tripLoading: false,
        meta: action.data.meta,
      };
    case FETCH_USER_TRIP_DETAIL_FAILED_PER_DAY:
      return { ...state, tripLoading: false };
    case FETCHING_USER_TRIP_DETAILS:
      return { ...state, tripLoading: true };
    case FETCH_USER_TRIP_DETAIL_SUCCESS:
      return {
        ...state,
        currentUserTrips: action.data,
        tripLoading: false,
        meta: action.data.meta,
      };
    case FETCH_USER_TRIP_DETAIL_FAILED:
      return { ...state, tripLoading: false };
    case FETCHING_USER_DETAILS:
      return { ...state, loading: true };
    case FETCH_USER_DETAIL_SUCCESS:
      return { ...state, currentUserDetails: action.data, loading: false };
    case FETCH_USER_DETAIL_FAILED:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default currentUser;
