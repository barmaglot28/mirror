import {
  FETCHING_USER_TRIP_DETAILS,
  FETCH_USER_TRIP_DETAIL_SUCCESS,
  FETCH_USER_TRIP_DETAIL_FAILED,
  FETCHING_USER_DETAILS,
  FETCH_USER_DETAIL_SUCCESS,
  FETCH_USER_DETAIL_FAILED,
  FETCHING_USER_TRIP_DETAILS_PER_DAY,
  FETCH_USER_TRIP_DETAIL_SUCCESS_PER_DAY,
  FETCH_USER_TRIP_DETAIL_FAILED_PER_DAY
} from './actionType';
import UserDetailService from '../../../services/userDetails';
import File from '../../../helpers/file';
// for trips
function fetchingUserTrips() {
  return {
    type: FETCHING_USER_TRIP_DETAILS,
  };
}

function fetchUserTripsSuccess(data) {
  return {
    type: FETCH_USER_TRIP_DETAIL_SUCCESS,
    data,
  };
}

function fetchUserTripsFailed(data) {
  return {
    type: FETCH_USER_TRIP_DETAIL_FAILED,
    data,
  };
}

function fetchingUserTripsPerDay() {
  return {
    type: FETCHING_USER_TRIP_DETAILS_PER_DAY,
  };
}

function fetchUserTripsSuccessPerDay(data) {
  return {
    type: FETCH_USER_TRIP_DETAIL_SUCCESS_PER_DAY,
    data,
  };
}

function fetchUserTripsFailedPerDay(data) {
  return {
    type: FETCH_USER_TRIP_DETAIL_FAILED_PER_DAY,
    data,
  };
}
// for user deatils
function fetchingUserDetails() {
  return {
    type: FETCHING_USER_DETAILS,
  };
}

function fetchUserDetailsSuccess(data) {
  return {
    type: FETCH_USER_DETAIL_SUCCESS,
    data,
  };
}

function fetchUserDetailsFailed(data) {
  return {
    type: FETCH_USER_DETAIL_FAILED,
    data,
  };
}

export function fetchUserDetails(id) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingUserDetails());
    UserDetailService.getUserDetail(token, id)
    .then((response) => {
      if (response.success) {
        dispatch(fetchUserDetailsSuccess(response));
      }
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchUserDetailsFailed(e));
    });
  };
}

export function fetchTripDetailsPerDay(driverId, time) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingUserTripsPerDay());
    UserDetailService.getUserTripsPerDay(token, driverId, time)
      .then((response) => {
        if (response.success) {
          dispatch(fetchUserTripsSuccessPerDay(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchUserTripsFailedPerDay(e));
      });
  };
}

export function fetchTripDetails(id, pageNo) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingUserTrips());
    UserDetailService.getLastFiveRideDetails(token, id, pageNo)
    .then((response) => {
      if (response.success) {
        dispatch(fetchUserTripsSuccess(response));
      }
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchUserTripsFailed(e));
    });
  };
}

export function exportCompleted(id, ranges) {
  return (dispatch, getState) => {
    // console.log(id, ranges);
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    // dispatch(fetchingUserTrips());
    UserDetailService.exportCompletedTrips(token, id, ranges)
      .then((response) => {
        if (response) {
          File.downloadBinary(response, 'completed-trips.xlsx');
        //   dispatch(fetchUserTripsSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        // dispatch(fetchUserTripsFailed(e));
      });
  }
}
