import {
  FETCHING_CAR_DETAILS,
  FETCH_CAR_DETAIL_SUCCESS,
  FETCH_CAR_DETAIL_FAILED,
} from './actionType';
import CarDetailService from '../../../services/carDetails';
// for car deatils
function fetchingCarDetails() {
  return {
    type: FETCHING_CAR_DETAILS,
  };
}

function fetchCarDetailsSuccess(data) {
  return {
    type: FETCH_CAR_DETAIL_SUCCESS,
    data,
  };
}

function fetchCarDetailsFailed(data) {
  return {
    type: FETCH_CAR_DETAIL_FAILED,
    data,
  };
}

export function fetchCarDetails(id) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCarDetails());
    CarDetailService.getCarDetail(token, id)
    .then((response) => {
      if (response.success) {
        dispatch(fetchCarDetailsSuccess(response));
      }
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchCarDetailsFailed(e));
    });
  };
}
