import {
  FETCHING_CAR_DETAILS,
  FETCH_CAR_DETAIL_SUCCESS,
  FETCH_CAR_DETAIL_FAILED,
} from './actionType';

const initialState = {
  loading: true,
  currentCarDetails: {},
};

const currentCar = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_CAR_DETAILS:
      return { ...state, loading: true };
    case FETCH_CAR_DETAIL_SUCCESS:
      return { ...state, currentCarDetails: action.data, loading: false };
    case FETCH_CAR_DETAIL_FAILED:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default currentCar;
