import {
  FETCHING_COUPONCODES_DETAILS,
  FETCH_COUPONCODES_DETAIL_SUCCESS,
  FETCH_COUPONCODES_DETAIL_FAILED,
} from './actionType';
import CouponcodesDetailService from '../../../services/couponcodesDetails';

// for user deatils
function fetchingCouponcodesDetails() {
  return {
    type: FETCHING_COUPONCODES_DETAILS,
  };
}

function fetchCouponcodesDetailsSuccess(data) {
  return {
    type: FETCH_COUPONCODES_DETAIL_SUCCESS,
    data,
  };
}

function fetchCouponcodesDetailsFailed(data) {
  return {
    type: FETCH_COUPONCODES_DETAIL_FAILED,
    data,
  };
}

export function fetchCouponcodesDetails(id) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCouponcodesDetails());
    CouponcodesDetailService.getCouponcodesDetails(token, id)
      .then((response) => {
        if (response.success) {
          dispatch(fetchCouponcodesDetailsSuccess(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(fetchCouponcodesDetailsFailed(e));
      });
  };
}
