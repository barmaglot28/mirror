import {
  FETCHING_COUPONCODES_DETAILS,
  FETCH_COUPONCODES_DETAIL_SUCCESS,
  FETCH_COUPONCODES_DETAIL_FAILED,
} from './actionType';

const initialState = {
  loading: true,
  currentCouponcodesDetails: {},
};

const couponcodesDetails = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_COUPONCODES_DETAILS:
      return { ...state, loading: true };
    case FETCH_COUPONCODES_DETAIL_SUCCESS:
      return { ...state, currentCouponcodesDetails: action.data, loading: false };
    case FETCH_COUPONCODES_DETAIL_FAILED:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default couponcodesDetails;
