import {
  OPEN_MODAL,
  CLOSE_MODAL,
  UPDATING_TRIP_OBJECT,
  UPDATING_TRIP_OBJECT_SUCCESS,
  UPDATING_TRIP_OBJECT_FAILED,
} from './actionType';

import {
  UPDATE_TRIP_LIST_OBJECT,
} from '../trips/actionType';

import TripService from '../../../services/trip';

function openModal(tripObj) {
  return {
    type: OPEN_MODAL,
    modalStatus: true,
    tripObj,
  };
}
function closeModal() {
  return {
    type: CLOSE_MODAL,
    modalStatus: false,
  };
}

function updatingTripObject() {
  return {
    type: UPDATING_TRIP_OBJECT,
  };
}

function updatingTripObjectSuccess() {
  return {
    type: UPDATING_TRIP_OBJECT_SUCCESS,
  };
}
function updatingTripObjectFailed() {
  return {
    type: UPDATING_TRIP_OBJECT_FAILED,
  };
}

function updateTripList(tripListObject) {
  return {
    type: UPDATE_TRIP_LIST_OBJECT,
    tripList: tripListObject,
  };
}

function updateTripListArray(updatedTripObj) {
  const newTripObj = updatedTripObj.data;
  // const newTripListObj = tripListObject;
  // const tripList = newTripListObj.data;
  // console.log('trip List', tripList);
  // for (let i = 0; i < tripList.length; i += 1) {
  //   if (tripList[i]._id === newTripObj._id) { // eslint-disable-line
  //     tripList[i] = newTripObj;
  //     break;
  //   }
  // }
  // newTripListObj.data = tripList;
  return (dispatch, getState) => {
    const newTripListObj = getState().domainReducer.trips.tripList;
    const tripList = newTripListObj.data;
    // console.log('trip List############', tripList);
    // console.log('trip List to update&&&&&&&&&&&############', newTripObj);
    for (let i = 0; i < tripList.length; i += 1) {
      // console.log('inside compare function with', tripList[i]._id, newTripObj._id);
      if (tripList[i]._id == newTripObj._id) { // eslint-disable-line
        // console.log('%%%%%%%%%%%%%%%%%%%%%%%%', newTripListObj);
        tripList[i] = newTripObj;
        break;
      }
    }
    newTripListObj.data = tripList;
    // console.log('new trip list ^^^^^^^^^^^^^', newTripListObj.data);
    dispatch(updateTripList(newTripListObj));
  };
}

function updateTripObject(tripObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(updatingTripObject());
    TripService.updateTripObject(token, tripObj)
    .then((response) => {
      // console.log('trip object updated success fully', response);
      dispatch(updatingTripObjectSuccess());
      // const tripList = getState().domainReducer.trips.tripList;
      dispatch(updateTripListArray(response));
      dispatch(closeModal());
    })
    .catch((e) => {
      // console.log('error occured while updatating the trip List', e);
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(updatingTripObjectFailed());
    });
    // const tripList = getState().domainReducer.trips.tripList;
    // updateTripList(1, tripList);
  };
}


export default { openModal, closeModal, updateTripObject };
