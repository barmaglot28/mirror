import {
  FETCHING_CARCATEGORIES,
  FETCH_CARCATEGORIES_SUCCESS,
  FETCH_CARCATEGORIES_FAILED,
  CREATE_NEW_CARCATEGORIES_REQ,
  CREATE_NEW_CARCATEGORIES_REQ_SUCCESS,
  CREATE_NEW_CARCATEGORIES_REQ_FAILED,
  REMOVE_CARCATEGORIES_REQ,
  REMOVE_CARCATEGORIES_REQ_SUCCESS,
  REMOVE_CARCATEGORIES_REQ_FAILED,
} from './actionType';
import CarcategoriesService from '../../../services/carcategories';


function RequestRemoveCarcategories() {
  return {
    type: REMOVE_CARCATEGORIES_REQ,
  };
}

function RequestRemoveCarcategoriesSuccess(data) {
  return {
    type: REMOVE_CARCATEGORIES_REQ_SUCCESS,
    removeCarcategoriesSuccess: data,
  };
}

function RequestRemoveCarcategoriesFailed(data) {
  return {
    type: REMOVE_CARCATEGORIES_REQ_FAILED,
    removeCarcategoriesErrorObj: data,
  };
}

function fetchingCarcategories() {
  return {
    type: FETCHING_CARCATEGORIES,
  };
}

function fetchCarcategoriesSuccess(data) {
  return {
    type: FETCH_CARCATEGORIES_SUCCESS,
    data,
    removeCarcategoriesSuccess: {},
    removeCarcategoriesErrorObj: {},
  };
}

function fetchCarcategoriesFailed(data) {
  return {
    type: FETCH_CARCATEGORIES_FAILED,
    data,
    removeCarcategoriesSuccess: {},
    removeCarcategoriesErrorObj: {},
  };
}

function RequestCreateNewCarcategories() {
  return {
    type: CREATE_NEW_CARCATEGORIES_REQ,
  };
}

function RequestCreateNewCarcategoriesSuccess(data) {
  return {
    type: CREATE_NEW_CARCATEGORIES_REQ_SUCCESS,
    newCarcategories: data,
  };
}

function RequestCreateNewCarcategoriesFailed(data) {
  return {
    type: CREATE_NEW_CARCATEGORIES_REQ_FAILED,
    createCarcategoriesErrorObj: data,
  };
}

function fetchCarcategories(pageNo, limit) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(fetchingCarcategories());
    CarcategoriesService.getAllCarcategories(token, pageNo, limit)
    .then((response) => {
      dispatch(fetchCarcategoriesSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(fetchCarcategoriesFailed(e));
    });
  };
}

function createNewCarcategories(carcategoriesObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestCreateNewCarcategories());
    CarcategoriesService.createCarcategories(token, carcategoriesObj)
    .then((response) => {
      dispatch(RequestCreateNewCarcategoriesSuccess(response));
    })
    .catch((e) => {
      if (e.message === 'Unauthorized') {
        dispatch({ type: 'FLUSH_DATA' });
      }
      dispatch(RequestCreateNewCarcategoriesFailed(e));
    });
  };
}

function removeCarcategories(carcategoriesObj) {
  return (dispatch, getState) => {
    const token = getState().domainReducer.auth.user.jwtAccessToken;
    dispatch(RequestRemoveCarcategories());
    CarcategoriesService.removeCarcategoriesService(token, carcategoriesObj)
      .then((response) => {
        if(response.success) {
          dispatch(RequestRemoveCarcategoriesSuccess(response));
        } else {
          dispatch(RequestRemoveCarcategoriesFailed(response));
        }
      })
      .catch((e) => {
        if (e.message === 'Unauthorized') {
          dispatch({ type: 'FLUSH_DATA' });
        }
        dispatch(RequestRemoveCarcategoriesFailed(e));
      });
  };
}

export default { fetchCarcategories, createNewCarcategories, removeCarcategories };
