import {
  FETCHING_CARCATEGORIES,
  FETCH_CARCATEGORIES_SUCCESS,
  FETCH_CARCATEGORIES_FAILED,
  FETCH_CARCATEGORIES_STATS_CHARTS_SUCCESS,
  FETCH_CARCATEGORIES_STATS_CHARTS_FAILED,
  FETCHING_CARCATEGORIES_STATS_CHARTS,
  CREATE_NEW_CARCATEGORIES_REQ,
  CREATE_NEW_CARCATEGORIES_REQ_SUCCESS,
  CREATE_NEW_CARCATEGORIES_REQ_FAILED,
  UPDATE_CARCATEGORIES_LIST_OBJECT,
  REMOVE_CARCATEGORIES_REQ,
  REMOVE_CARCATEGORIES_REQ_SUCCESS,
  REMOVE_CARCATEGORIES_REQ_FAILED,
} from './actionType';

const initialState = {
  loading: false,
  failedCarcategoriesListApi: false,
  failedCarcategoriesChartApi: false,
  carcategoriesList: {},
  meta: null,
  chartData: {},
  chartLoading: true,
  newCarcategoriesLoading: false,
  failedCreateCarcategoriesApi: false,
  createCarcategoriesErrorObj: {},
  removeCarcategoriesErrorObj: {},
  removeCarcategoriesSuccess: {},
  newCarcategoriesObj: {},
};

const carcategories = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_CARCATEGORIES:
      return { ...state, loading: true, failedCarcategoriesListApi: false };
    case FETCH_CARCATEGORIES_SUCCESS:
      return {
        ...state,
        carcategoriesList: action.data,
        loading: false,
        failedCarcategoriesListApi: false,
        meta: action.data.meta,
        removeCarcategoriesErrorObj: {},
        removeCarcategoriesSuccess: {},
      };
    case FETCH_CARCATEGORIES_FAILED:
      return {
        ...state,
        loading: false,
        failedCarcategoriesListApi: true,
        removeCarcategoriesErrorObj: {},
        removeCarcategoriesSuccess: {},
      };
    case FETCHING_CARCATEGORIES_STATS_CHARTS:
      return { ...state, chartLoading: true, failedCarcategoriesChartApi: false };
    case FETCH_CARCATEGORIES_STATS_CHARTS_SUCCESS:
      return { ...state, chartLoading: false, chartData: action.data, failedCarcategoriesChartApi: false };
    case FETCH_CARCATEGORIES_STATS_CHARTS_FAILED:
      return { ...state, chartLoading: false, failedCarcategoriesChartApi: true };
    case CREATE_NEW_CARCATEGORIES_REQ:
      return { ...state, newCarcategoriesLoading: true, failedCreateCarcategoriesApi: false, createCarcategoriesErrorObj: {} };
    case CREATE_NEW_CARCATEGORIES_REQ_SUCCESS:
      return {
        ...state,
        newCarcategoriesLoading: false,
        failedCreateCarcategoriesApi: false,
        newCarcategoriesObj: action.newCarcategories,
        createCarcategoriesErrorObj: {},
        removeCarcategoriesErrorObj: {},
        removeCarcategoriesSuccess: {},
      };
    case CREATE_NEW_CARCATEGORIES_REQ_FAILED:
      return {
        ...state,
        newCarcategoriesLoading: false,
        failedCreateCarcategoriesApi: true,
        createCarcategoriesErrorObj: action.createCarcategoriesErrorObj,
        removeCarcategoriesErrorObj: {},
        removeCarcategoriesSuccess: {},
      };
    case UPDATE_CARCATEGORIES_LIST_OBJECT: {
      const carcategoriesList = Object.assign({}, action.carcategoriesList);
      return { ...state, carcategoriesList };
    }
    case REMOVE_CARCATEGORIES_REQ:
      return { ...state, loading: true, failedCarcategoriesListApi: false };
    case REMOVE_CARCATEGORIES_REQ_SUCCESS:
      return {
        ...state,
        loading: false,
        failedCarcategoriesListApi: false,
        removeCarcategoriesSuccess: action.removeCarcategoriesSuccess
      };
    case REMOVE_CARCATEGORIES_REQ_FAILED:
      return {
        ...state,
        loading: false,
        failedCarcategoriesListApi: true,
        removeCarcategoriesErrorObj: action.removeCarcategoriesErrorObj
      };
    default:
      return state;
  }
};

export default carcategories;
