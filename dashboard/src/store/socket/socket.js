const io = require('socket.io-client');
import apiConfig from '../../services/apiConfig';
import { store } from '../../../src/components/App';
import { socketDisconnected } from '../domain/auth/action';
import { tripUpdated, tripCreated, tripDeleted, driverUpdated } from '../domain/statistic/action';

let socket = null;

export function socketInit() {
  const { dispatch, getState } = store.store;
  socket = io(apiConfig.serverUrl, { jsonp: false, transports: ['websocket'], query: `token=${getState().domainReducer.auth.user.jwtAccessToken}` });
  socket.on('connect', () => {
    dispatch(socketDisconnected(false));
  });
  socket.on('disconnect', () => {
    dispatch(socketDisconnected(true));
  });
  socket.on('tripUpdatedDashboard', (trip) => {
    dispatch(tripUpdated(trip));
  });
  socket.on('tripCreatedDashboard', (trip) => {
    dispatch(tripCreated(trip));
  });
  socket.on('tripDeletedDashboard', (tripId) => {
    dispatch(tripDeleted(tripId));
  });
  socket.on('driverUpdatedDashboard', (tripId) => {
    dispatch(driverUpdated(tripId));
  });
  socket.on('socketError', (e) => {
    console.error(e);
  });
}

export function unpinAcceptedPreBooking(trip) {
  socket.emit('adminUnpinAcceptedBooking', trip);
}

export function cancelPreBooking(trip) {
  socket.emit('adminCancelBooking', trip);
}
