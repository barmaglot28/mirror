import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { autoRehydrate } from 'redux-persist';
// import { persistStore, autoRehydrate } from 'redux-persist';
// import rootReducer from '../reducers';
import rootReducer from './rootReducer';
import createHelpers from './createHelpers';
import createLogger from './logger';
import api from './errorMiddleware';

export default function configureStore(initialState, helpersConfig) {
  const helpers = createHelpers(helpersConfig);
  const middleware = [thunk.withExtraArgument(helpers)];

  let enhancer;

  if (__DEV__) {
    middleware.push(createLogger());

    // https://github.com/zalmoxisus/redux-devtools-extension#redux-devtools-extension
    let devToolsExtension = f => f;
    if (process.env.BROWSER && window.devToolsExtension) {
      devToolsExtension = window.devToolsExtension();
    }

    enhancer = compose(
      // applyMiddleware(...middleware, api),
      applyMiddleware(...middleware, api),
      devToolsExtension,
      autoRehydrate(),
    );
  } else {
    enhancer = compose(applyMiddleware(...middleware), autoRehydrate());
  }

  // See https://github.com/rackt/redux/releases/tag/v3.1.0
  const store = createStore(rootReducer, initialState, enhancer);
  // if (typeof self === 'object') persistStore(store);

  // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
  if (__DEV__ && module.hot) {
    module.hot.accept('./rootReducer', () =>
      // eslint-disable-next-line global-require
      store.replaceReducer(require('./rootReducer').default),
    );
  }

  return store;
}
