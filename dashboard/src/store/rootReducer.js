import { combineReducers } from 'redux';
// import runtime from './runtime';
import intl from './view/intl/reducer';
import runtime from './view/runtime/reducer';
import domainReducer from './domain';
// import viewReducer from './view';

export default combineReducers({
  runtime,
  intl,
  domainReducer,
//   viewReducer,
});
