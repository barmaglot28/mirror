import history from '../core/history';
import { clearUserData } from './domain/auth/action';

export default store => next => async action => {
  if (action.type === 'FLUSH_DATA') {
    console.log('inside error middleware and calling dispatch', store.getState());
    store.dispatch(clearUserData());
    console.log('inside error middleware finished dispatch', store.getState());
    history.replace('/');
  }
  return next(action);
};
