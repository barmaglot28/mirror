import React, { Component } from 'react';
import { Button, Panel } from 'react-bootstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Sidebar.css';
import Link from '../Link/Link';

class Sidebar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      openPreBookings: false,
      openUsers: false,
    };
  }

  render() {
    return (
      <aside className={s.sidebar}>
        <div className={`sidenav-outer ${s.sidenavOuter}`}>
          <ul>
            <li>
              <span>
                <Button className={s.sideBarItem} onClick={ ()=> this.setState({ openUsers: !this.state.openUsers })}>
                  <span className="glyphicon glyphicon-user" />
                  <span className="title">Users</span>
                </Button>
              </span>
              <Panel collapsible expanded={this.state.openUsers}>
                <Link to="/dashboard/rider">
                  <span className="glyphicon glyphicon-user" style={{ visibility: 'hidden' }} />
                  <span className="title">Rider</span>
                </Link>
                <Link to="/dashboard/driver">
                  <span className="glyphicon glyphicon-user" style={{ visibility: 'hidden' }} />
                  <span className="title">Driver</span>
                </Link>
                <Link to="/dashboard/corporate">
                  <span className="glyphicon glyphicon-user" style={{ visibility: 'hidden' }} />
                  <span className="title">Corporate</span>
                </Link>
              </Panel>
            </li>
            <li>
              <Link to="/dashboard/wallets">
                <span className="glyphicon glyphicon-book" />
                <span className="title">Wallets</span>
              </Link>
            </li>
            <li>
              <Link to="/dashboard/carcategories">
                <span className="glyphicon glyphicon-user" />
                <span className="title">Car categories</span>
              </Link>
            </li>
            <li>
              <Link to="/dashboard/cars">
                <span className="glyphicon glyphicon-cd" />
                <span className="title">Cars</span>
              </Link>
            </li>
            <li>
              <Link to="/dashboard/trips">
                <span className="glyphicon glyphicon-road" />
                <span className="title">Trips</span>
              </Link>
            </li>
            <li>
              <span>
                <Button className={s.sideBarItem} onClick={ ()=> this.setState({ openPreBookings: !this.state.openPreBookings })}>
                  <span className="glyphicon glyphicon-road" />
                  <span className="title">Pre-bookings</span>
                </Button>
              </span>
              <Panel collapsible expanded={this.state.openPreBookings}>
                <Link to="/dashboard/available-pre-bookings">
                  <span className="glyphicon glyphicon-road" style={{ visibility: 'hidden' }} />
                  <span className="title">Available pre-bookings</span>
                </Link>
                <Link to="/dashboard/accepted-pre-bookings">
                  <span className="glyphicon glyphicon-road" style={{ visibility: 'hidden' }} />
                  <span className="title">Accepted pre-bookings</span>
                </Link>
                <Link to="/dashboard/cancelled-pre-bookings">
                  <span className="glyphicon glyphicon-road" style={{ visibility: 'hidden' }} />
                  <span className="title">Cancelled pre-bookings</span>
                </Link>
              </Panel>
            </li>
            <li>
              <Link to="/dashboard/couponcodes">
                <span className="glyphicon glyphicon-road" />
                <span className="title">Coupons</span>
              </Link>
            </li>
            <li>
              <Link to="/dashboard/reviews">
                <span className="glyphicon glyphicon-road" />
                <span className="title">Reviews</span>
              </Link>
            </li>
            <li>
              <Link to="/dashboard/godsview">
                <span className="glyphicon glyphicon-road" />
                <span className="title">Gods view</span>
              </Link>
            </li>
            <li>
              <Link to="/dashboard/referralcodes">
                <span className="glyphicon glyphicon-road" />
                <span className="title">Referral Codes</span>
              </Link>
            </li>
            <li>
              <a href="https://www.cabdo.de/impressum">
                <span className="glyphicon glyphicon-road" />
                <span className="title">Imprint</span>
              </a>
            </li>
            {/*
            <li>
              <Link to="/dashboard/createTrips">
                <span className="glyphicon glyphicon-road" />
                <span className="title">Create New Trips</span>
              </Link>
            </li>
            <li>
              <Link to="/dashboard/createUser">
                <span className="glyphicon glyphicon-user" />
                <span className="title">Create New User</span>
              </Link>
            </li>*/}
          </ul>
        </div>
      </aside>
    );
  }
}

export default withStyles(s)(Sidebar);
