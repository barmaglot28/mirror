import React, { Component } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { FormattedMessage, defineMessages } from 'react-intl';
import { injectIntl, intlShape, FormattedRelative } from 'react-intl';
import $ from 'jquery';
import { connect } from 'react-redux';
import {
  NavDropdown,
  MenuItem,
} from 'react-bootstrap';
import Link from '../Link';
import s from './Header.css';
import history from '../../core/history';
import { clearUserData } from '../../store/domain/auth/action';
import { setLocale } from '../../store/view/intl/action';
const src = require('../../../resources/audio.mp3');
const profileImage = require('../../common/images/flat-avatar.png');

function showMenu() {
  $('.dashboard-page').toggleClass('push-right');
}

class TopNav extends Component {
  static propTypes = {
    userDetails: React.PropTypes.object,
    clearUserData: React.PropTypes.func,
    intl: intlShape.isRequired,
    tripsStatistic: React.PropTypes.array
  };



  constructor(props) {
    super(props);
    this.state = {};
  }

  logOutUser(event) {
    event.preventDefault();
    this.props.clearUserData();
    history.push('/');
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.tripsStatistic.length && this.props.tripsStatistic.length !== nextProps.tripsStatistic.length) {
      document.getElementById('audio').play();
    }
  }

  render() {
    return (
      <nav className={`navbar navbar-fixed-top ${s.topNavbar} role="navigation`}>
        <div>
          <audio controls name="media" id={"audio"} style={{ display: 'none' }}>
            <source src={src} type="audio/x-wav" />
          </audio>
        </div>
        <div className={`navbar-header text-center ${s.navbarHeader}`}>
          <button type="button" className="navbar-toggle" onClick={(e) => { e.preventDefault(); showMenu(); }} target="myNavbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar" />
            <span className="icon-bar" />
            <span className="icon-bar" />
          </button>
          <Link to="/dashboard" className={`navbar-brand ${s.navbarBrand}`}>Cabdo Dashboard</Link>
        </div>

        <div id="myNavbar" className={`collapse navbar-collapse ${s.navbarCollapse}`}>
          <ul className={`nav navbar-nav pull-right navbar-right ${s.pullRight} ${s.ulNavbar}`}>
            <NavDropdown
              id="dropDown4"
              className={s.navbarProfile}
              eventKey={4}
              title={
                <span>
                  <img src={profileImage} className={s.topnavImg} role="presentation" />
                  <span className="hidden-sm">Admin</span>
                </span>
              } noCaret
            >
              <MenuItem onClick={(e) => this.logOutUser(e)}>
                <FormattedMessage
                  id="logout"
                  defaultMessage="Logout"
                />
              </MenuItem>
            </NavDropdown>
          </ul>
        </div>
        <ul className={`nav navbar-nav pull-right ${s.ulNavbar} ${s.hidd}`} style={{ marginTop: 8 }}>
          <NavDropdown
            id="navDropDown11"
            eventKey={4} title={
              <span>
                <img src={profileImage} className={`topnav-img ${s.topnavImg}`} alt="" />
              </span>} noCaret className={`dropdown admin-dropdown ${s.dropdown}`}
          >
            <MenuItem onClick={(e) => { e.preventDefault(); history.push('/'); }}>
              <FormattedMessage
                id="logout"
                defaultMessage="Logout"
              />
            </MenuItem>
          </NavDropdown>
        </ul>
      </nav>
    );
  }
}
function mapStateToProps(state) {
  return {
    userDetails: state.domainReducer.auth.user.user,
    tripsStatistic: state.domainReducer.statistic.tripsList
  };
}

function bindActions(dispatch) {
  return {
    clearUserData: () => dispatch(clearUserData()),
    setLocale: (data) => dispatch(setLocale(data)),
  };
}

export default injectIntl(connect(mapStateToProps, bindActions)(withStyles(s)(TopNav)));
