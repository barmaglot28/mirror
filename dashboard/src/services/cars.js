import ApiService from './apiService';

async function getAllCars(token, pageNo) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/car?pageNo=${pageNo}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllCars', response);
  return response;
}

async function createNewCar(token, carObj) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = carObj;
  apiObject.endpoint = 'api/admin/car';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside create new car', response);
  return response;
}

async function updateCarDetails(token, carObj) {
  const apiObject = {};
  apiObject.method = 'PUT';
  apiObject.authentication = token;
  apiObject.body = carObj;
  apiObject.endpoint = 'api/admin/car';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside update car data', response);
  return response;
}

async function removeCar(token, carObj) {
  const apiObject = {};
  apiObject.method = 'DELETE';
  apiObject.authentication = token;
  apiObject.body = carObj;
  apiObject.endpoint = 'api/admin/car';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside remove car data', response);
  return response;
}

export default { getAllCars, createNewCar, updateCarDetails, removeCar };
