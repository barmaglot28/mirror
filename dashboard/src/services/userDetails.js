import Moment from 'moment';
import ApiService from './apiService';

// const API_ENDPOINT = `${config.serverUrl}:${config.port}`;

async function getLastFiveRideDetails(token, id, pageNo = 1) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip/user/${id}?pageNo=${pageNo}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}
async function getUserTripsPerDay(token, driverId, time) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip/tripsByUserPerDay`;
  apiObject.body = { driverId: driverId, time: time };
  const response = await ApiService.callApi(apiObject);
  return response;
}
async function getUserDetail(token, id) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/userDetails/${id}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function exportCompletedTrips(token, id, ranges) {
  const apiObject = {};
  apiObject.headers = {
    Accept: '*/*',
    'Content-Type': 'application/json',
  };
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip/user/${id}/exportCompleted`;
  apiObject.body = {
    startRange: ranges.startRange,
    endRange: ranges.endRange,
    zone: Moment().zone()
  };
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default { getLastFiveRideDetails, getUserDetail, getUserTripsPerDay, exportCompletedTrips };
