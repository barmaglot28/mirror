import ApiService from './apiService';

async function getAllCarcategories(token, pageNo, limit) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/carcategories?pageNo=${pageNo}`;
  if (limit)
    apiObject.endpoint += `&limit=${limit}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllCarcategories', response);
  return response;
}

async function createCarcategories(token, carcategoriesObj) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = carcategoriesObj;
  apiObject.endpoint = 'api/admin/carcategories';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside create new carcategories', response);
  return response;
}

async function updateCarcategoriesDetails(token, carcategoriesObj) {
  const apiObject = {};
  apiObject.method = 'PUT';
  apiObject.authentication = token;
  apiObject.body = carcategoriesObj;
  apiObject.endpoint = 'api/admin/carcategories';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside update carcategories data', response);
  if (response.success) {
    window.location.href = "/dashboard/carcategories";
  }
  return response;
}

async function removeCarcategoriesService(token, carcategoriesObj) {
  const apiObject = {};
  apiObject.method = 'DELETE';
  apiObject.authentication = token;
  apiObject.body = carcategoriesObj;
  apiObject.endpoint = 'api/admin/carcategories';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside remove carcategories data', response);
  return response;
}

export default { getAllCarcategories, createCarcategories, updateCarcategoriesDetails, removeCarcategoriesService };
