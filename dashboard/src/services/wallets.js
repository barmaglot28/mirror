import ApiService from './apiService';

async function getAllWallets(token, pageNo, limit) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/wallets/all?pageNo=${pageNo}`;
  if (limit)
    apiObject.endpoint += `&limit=${limit}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllWallets', response);
  return response;
}

async function addWalletBalance(token, id, value) {
  const apiObject = {};
  apiObject.method = 'PUT';
  apiObject.authentication = token;
  apiObject.endpoint = 'api/admin/wallets/add-balance';
  apiObject.body = {id, value};
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside addWalletBalance', response);
  return response;
}

async function filterRiderList(token, value, pageNo, limit) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/wallets/filtered?pageNo=${pageNo}&filter=${value}`;
  if (limit)
    apiObject.endpoint += `&limit=${limit}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside filterRiderList', response);
  return response;
}

export default { getAllWallets, addWalletBalance, filterRiderList };
