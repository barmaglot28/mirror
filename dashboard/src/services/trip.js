import ApiService from './apiService';

async function getAllTripsPerDay(token, date) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = { time: date };
  apiObject.endpoint = `api/admin/trip/tripsPerDay`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function getAllTripsPerDayRevenueDetails(token, date) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip/tripsPerDay/charts/${date}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function getAllTripsPerDayDetails(token, date) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip/tripsPerDay/requests/charts/${date}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}


async function getAllTripDetails(token, pageNo, filter) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip?pageNo=${pageNo}&&filter=${filter}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function getTripRevenueDetails(token, year) {
  let revenueYear = year;
  revenueYear = year ? revenueYear : new Date().getFullYear();
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/trip/charts/${revenueYear}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}


async function updateTripObject(token, obj) {
  // console.log('inside obj updateTripObject api call', obj);
  const apiObject = {};
  apiObject.method = 'PUT';
  apiObject.authentication = token;
  apiObject.endpoint = 'api/admin/trip/';
  apiObject.body = obj;
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function createTrip(token, tripObj) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.endpoint = 'api/admin/trip';
  apiObject.body = tripObj;
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default {
  getAllTripDetails,
  getTripRevenueDetails,
  updateTripObject,
  createTrip,
  getAllTripsPerDay,
  getAllTripsPerDayRevenueDetails,
  getAllTripsPerDayDetails
};
