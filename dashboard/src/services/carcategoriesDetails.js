import ApiService from './apiService';

// const API_ENDPOINT = `${config.serverUrl}:${config.port}`;

async function getCarcategoriesDetails(token, id) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/carcategoriesDetails/${id}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default { getCarcategoriesDetails };
