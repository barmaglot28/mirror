import ApiService from './apiService';

async function getAllCategories(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/skyView/getDrivers`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllCategories', response);
  return response;
}

export default { getAllCategories };
