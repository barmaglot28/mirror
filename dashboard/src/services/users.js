import ApiService from './apiService';

async function getAllDrivers(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/user/alldrivers`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllDrivers', response);
  return response;
}

async function getAllUsers(token, pageNo, type) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/user?pageNo=${pageNo}&type=${type}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllUsers', response);
  return response;
}

async function getUserChartStats(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = 'api/admin/user/userStatsChart';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getUserChartStats', response);
  return response;
}

async function createUser(token, userObj) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = userObj;
  apiObject.endpoint = 'api/admin/user';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside create new user', response);
  return response;
}

async function updateUserDetails(token, userObj) {
  const apiObject = {};
  apiObject.method = 'PUT';
  apiObject.authentication = token;
  apiObject.body = userObj;
  apiObject.endpoint = 'api/admin/user';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside update user data', response);
  return response;
}

async function removeUser(token, userObj) {
  const apiObject = {};
  apiObject.method = 'DELETE';
  apiObject.authentication = token;
  apiObject.body = userObj;
  apiObject.endpoint = 'api/admin/user';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside remove user data', response);
  return response;
}

async function sendSmsToAll(token, messageText) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = {
    messageText: messageText
  };
  apiObject.endpoint = 'api/admin/user/sendSmsToAll';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside sendSmsToAll', response);
  return response;
}

async function sendEmailToAll(token, messageText) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = {
    messageText: messageText
  };
  apiObject.endpoint = 'api/admin/user/sendEmailToAll';
  const response = await ApiService.callApi(apiObject);
  return response;
}


async function filterUsers(token, value, userType, pageNo, limit) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/users/filtered?pageNo=${pageNo}&value=${value}&type=${userType}`;
  if (limit)
    apiObject.endpoint += `&limit=${limit}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside filterUsers', response);
  return response;
}

export default { getAllUsers, getAllDrivers, getUserChartStats, createUser, updateUserDetails, removeUser, sendSmsToAll, sendEmailToAll, filterUsers };
