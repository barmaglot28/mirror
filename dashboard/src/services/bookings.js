import ApiService from './apiService';

async function getAllAvailableBookings(token, pageNo) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/availableBookings?pageNo=${pageNo}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllAvailableBookings', response);
  return response;
}

async function getAllAcceptedBookings(token, pageNo) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/acceptedBookings?pageNo=${pageNo}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllAcceptedBookings', response);
  return response;
}

async function removePreBooking(token, item) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = item;
  apiObject.endpoint = 'api/admin/bookings';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside remove pre booking data', response);
  return response;
}

async function getAllCancelledBookings(token, pageNo) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/cancelledBookings?pageNo=${pageNo}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllCancelledBookings', response);
  return response;
}

export default { getAllAvailableBookings, getAllAcceptedBookings, removePreBooking, getAllCancelledBookings };
