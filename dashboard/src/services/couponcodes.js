import ApiService from './apiService';

async function getAllCouponcodes(token, pageNo, limit) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/couponcodes?pageNo=${pageNo}`;
  if (limit)
    apiObject.endpoint += `&limit=${limit}`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getAllCouponcodes', response);
  return response;
}

async function createCouponcodes(token, couponcodesObj) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = couponcodesObj;
  apiObject.endpoint = 'api/admin/couponcodes';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside create new coupon codes', response);
  return response;
}

async function updateCouponcodesDetails(token, couponcodesObj) {
  const apiObject = {};
  apiObject.method = 'PUT';
  apiObject.authentication = token;
  apiObject.body = couponcodesObj;
  apiObject.endpoint = 'api/admin/couponcodes';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside update coupon codes data', response);
  if (response.success) {
    window.location.href = "/dashboard/couponcodes";
  }
  return response;
}

async function removeCouponcodesService(token, couponcodesObj) {
  const apiObject = {};
  apiObject.method = 'DELETE';
  apiObject.authentication = token;
  apiObject.body = couponcodesObj;
  apiObject.endpoint = 'api/admin/couponcodes';
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside remove coupon codes data', response);
  return response;
}

export default { getAllCouponcodes, createCouponcodes, updateCouponcodesDetails, removeCouponcodesService };
