import ApiService from './apiService';

// const API_ENDPOINT = `${config.serverUrl}:${config.port}`;

async function getCouponcodesDetails(token, id) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/couponcodesDetails/${id}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default { getCouponcodesDetails };
