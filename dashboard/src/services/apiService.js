import httpStatus from 'http-status';
import apiConfig from './apiConfig';
import fetch from '../core/fetch';

async function callApi(apiObject) {
  const fetchObject = {};
  fetchObject.method = apiObject.method ? apiObject.method : 'get';
  fetchObject.body = apiObject.body ? JSON.stringify(apiObject.body) : undefined;
  fetchObject.headers = apiObject.headers || {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  if (apiObject.authentication) {
    fetchObject.headers.Authorization = apiObject.authentication;
  }
  const url = `${apiConfig.serverUrl}/${apiObject.endpoint}`;
  const fetchResult = await fetch(url, fetchObject);
  return new Promise(async (resolve, reject) => {
    if (fetchResult.status >= httpStatus.OK && fetchResult.status <= httpStatus.MULTIPLE_CHOICES) {
      let data;
      // data can be json or binary
      if (fetchResult.headers.get('Content-Type') && fetchResult.headers.get('Content-Type').includes('application/json')) {
        data = fetchResult.json();
      } else {
        data = fetchResult.blob();
      }
      return resolve(data);
    }
    const errorResponse = await fetchResult.json();
    return reject(errorResponse);
  });
}


export default { callApi };
