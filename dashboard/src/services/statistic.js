import ApiService from './apiService';

async function getGeneralStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/general`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getGeneralStatistic', response);
  return response;
}

async function getTodayRideStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/todayRide`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getTodayRideStatistic', response);
  return response;
}

async function getCompletedRideStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/completedRide`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getCompletedRideStatistic', response);
  return response;
}

async function getDriversStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/drivers`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getDriversStatistic', response);
  return response;
}

async function getRatingCommentsStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/ratingComments`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getRatingCommentsStatistic', response);
  return response;
}

async function getCancelledCommentsStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/cancelledComments`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getCancelledCommentsStatistic', response);
  return response;
}

async function getDriverWorklistStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/driverWorklist`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside geDriverWorklistStatistic', response);
  return response;
}

async function getTripsStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/trips`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getTripsStatistic', response);
  return response;
}

async function getVehicleStatistic(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/statistic/vehicle`;
  const response = await ApiService.callApi(apiObject);
  console.log('response object inside getVehicleStatistic', response);
  return response;
}

export default { getGeneralStatistic, getTodayRideStatistic, getCompletedRideStatistic, getDriversStatistic,
  getRatingCommentsStatistic, getCancelledCommentsStatistic, getDriverWorklistStatistic, getTripsStatistic,
  getVehicleStatistic};
