import ApiService from './apiService';

async function getCarDetail(token, id) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/car/${id}`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default {getCarDetail};
