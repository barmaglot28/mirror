import ApiService from './apiService';

async function saveReferralAmt(token, referralcodesObj) {
  const apiObject = {};
  apiObject.method = 'POST';
  apiObject.authentication = token;
  apiObject.body = referralcodesObj;
  apiObject.endpoint = 'api/admin/referralcode/amount';
  const response = await ApiService.callApi(apiObject);
  return response;
}

async function fetchReferralAmt(token) {
  const apiObject = {};
  apiObject.method = 'GET';
  apiObject.authentication = token;
  apiObject.endpoint = `api/admin/referralcode/amount`;
  const response = await ApiService.callApi(apiObject);
  return response;
}

export default { fetchReferralAmt, saveReferralAmt };
